"""
lemma {:axiom} example(n: nat, m: nat)
  ensures (n * m) % 2 == 0 <==> n % 2 == 0 || m % 2 == 0
{

}

Lemma with one variable of type nat
>>> list(first(check(lemma_2, ['n'] , 1000)))
[]

Lemma with two variables of type nat
>>> list(first(check(lemma, ['n', 'm'], 100, 100)))
[]

>>> set_of_possible_triples(max_n=1, max_m=1)
['(0, 0, 0)', '(0, 0, 1)', '(0, 1, 0)', '(1, 1, 1)']

This check is redundant (by modulus properties) but what if formula changed?
We need some general way to easily check some facts.

>>> set_of_possible_triples(max_n=100, max_m=100)
['(0, 0, 0)', '(0, 0, 1)', '(0, 1, 0)', '(1, 1, 1)']


"""

from collections import namedtuple
from itertools import islice, product

fib_cache = dict()
def fib(n):
  """Get Fibonacci number
  >>> assert fib(0) == 0 and fib(1) == 1 and fib(2) == 1 and fib(10) == 55 and fib(16) == 987

  TODO: use exponential formula to fast compute fib function result
  """
  cached_value = fib_cache.get(n)
  if cached_value is not None:
    return cached_value
  if n <= 1:
    return n
  a, b = 0, 1
  for i in range(1, n):
    fib_cache[i] = b
    a, b = b, a + b
  fib_cache[n] = b
  return b


def lemma(n):
  return (fib(n) % 2 == 0) == (n % 3 == 0)


def lemma_2(n, m):
  return ((n * m) % 2 == 0) == (n % 2 == 0 or m % 2 == 0)


def first(iterable):
  return take(iterable, 1)


def take(iterable, n):
  return islice(iterable, n)


def check(lem, arg_names: list, *max_args):
  lst = [range(max_args[i]) for i in range(len(max_args))]
  Tup = namedtuple('Tup', arg_names)
  for tup in product(*lst):
    if not lem(*tup):
      yield Tup(*tup)


def set_of_possible_triples(max_n, max_m):
  Pair = namedtuple('Pair', ['n', 'm'])
  set_of_rel = set()
  for n in range(max_m + 1):
    for m in range(max_n + 1):
      pre = Pair(n, m)
      rel = ((n * m) % 2, n % 2, m % 2)
      set_of_rel.add(str(rel))
      # print(f'{pre}: {rel}')
  return sorted(set_of_rel)


if __name__ == "__main__":
  import doctest
  doctest.testmod()