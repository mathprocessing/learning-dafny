# /compile:0 - only verify without compilation
# /compile:3 - if there is a Main method and there are no verification compiles program in memory and runs it

# /dafnyVerify:<n> 0 - stop after typechecking, 1 - continue on to translation, verification, and compilation
# /printModel:1
# /timeLimit:1   Limit the number of seconds spent trying to verify each procedure
# /errorTrace:1 (default), 2 - include all trace errors

# Example of usage: 
# $ bash run.sh "src\for-advanced-users\Gcd\strange\trees.dfy"

"C:/dafny-08-20/dafny/Dafny.exe" /compile:3 /dafnyVerify:1 $1