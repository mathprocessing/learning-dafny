# About Boogie intermediate procedural language
### BoogiePL: A typed procedural language for checking object-oriented programs

| Authors           | Email                 |
| ------------------| --------------------- |
| Robert DeLine     | rdeline@microsoft.com | 
| K.Rustan M. Leino |  leino@microsoft.com  |

Data of publication: 27 May 2005

## Shortened version
BoogiePL - intermediate language for program analysis and program verification.
The language is a simple coarsely type imperative language with procedures and
arrays, plus support for introducing mathematical functions and declaring
properties of these functions.

From the resulting BoogiePL program, one can then generate verification conditions
or perform other program analyses such as the *inference of program invariants*.
In this way, BoogiePL also serves as a programming-notation front end to
theorem provers.
BoogiePL is accepted as input to Boogie, the Spec# static program verifier.

## Program and types
At the top level, a BoogiePL program is a set of declarations.
Disclaimer: I use rust syntax highlighting for better view, it's not a runnable program.

```rust
Program ::= Decl*
Decl    ::= Type | Constant | Function | Axiom
          | Variable | Procedure | Implementation
```

Value-holding entities (such that ...) in BoogiePL are typed, despite the fact
that a theorem prover (Boogie) used on BoogiePL programs may be untyped.

The purpose of types in BoogiePL, like the purpose of explicit declarations
of variables and functions, is to guard against certain easy-to-make mistakes in
the input.

There are four built-in basic types:
* user-defined types
* one-
* or two dimensional arrays
* the supertype `any`

```rust
Type ::= bool | int | ref | name
       | Id | any
       | "[" Type "]" Type
       | "[" Type "," Type "]" Type
```

Some facts about:
* `bool`, `int`, `ref`, `name`, `Id`, `any` are concrete types
* `Type` is a type variable
* `bool` represents the boolean value `false` and `true`
* `int` represents the mathematical integers
  + Used in arithmetics
* `ref` represents references (object, pointers, addresses).
  + `ref` can be `null`
  + Defined operations on `ref` are equality `==` and disequality `!=` tests
* `name` represents various kinds of defined names (like types and field names)
  + Defined operations on `name` are `==`, `!=`, and the partial order `<:`
  + Symbolic constants defined to have type `name` and special property that they
    have distinct values (this axiom is provided by the language automatically)
* `"[" Type "]" Type` and `"[" Type "," Type "]" Type` corresponds to arrays
  + Arrays can have one `a[x]` or two `a[x, y]` dimensions
  + To make more dimensions use arrays of arrays
  + In the array type, the types of the array indices are given first
  












