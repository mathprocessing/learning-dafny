datatype Point = Point(x: int, y: int) {
  function add(p: Point): Point
  {
    Point(this.x + p.x, this.y + p.y)
  }

  function mul(lam: int): Point
  {
    Point(this.x * lam, this.y * lam)
  }

  function vabs(): nat
  {
    dist(this, Point(0, 0))
  }

  lemma {:induction false} vabsAsSum()
  ensures this.vabs() == abs(this.x) + abs(this.y)
  {}
}

datatype Game = Game {
  const time: nat
  const credits: nat
  const spiceVolume: nat
  const Units: set<Unit>
}

function abs(x: int): nat
{
  if x < 0 then -x else x
}

function dist(p1: Point, p2: Point): (res: nat)
ensures p1 == p2 ==> res == 0
{
  abs(p2.x - p1.x) + abs(p2.y - p1.y)
}

datatype UnitType = Quad | Trike

datatype Unit = Unit {
  const pos: Point
  const speed: nat
  const hp: nat
  const maxhp: nat
  const unittype: UnitType

  lemma HPbounded()
  ensures hp <= maxhp

  lemma MoveBound(g: Game, t1: nat, t2: nat, p1: Point, p2: Point)
  requires t1 <= t2
  requires g.time == t1 ==> this.pos == p1
  requires g.time == t2 ==> this.pos == p2
  ensures dist(p1, p2) <= this.speed * (t2 - t1)

  lemma DefineTypes()
  ensures this.unittype == Quad ==> this.speed == 2 && this.maxhp == 130
  ensures this.unittype == Trike ==> this.speed == 2 && this.maxhp == 100

  predicate inRect(x: int, y: int, w: nat, h: nat) {
    && x <= this.pos.x <= x + w
    && y <= this.pos.y <= y + h
  }
}