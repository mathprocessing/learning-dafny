"""
How to choose fastest way to move?
see: https://youtu.be/jBhP9HLPAa8?t=258
Сардаукар и Сварщик объединяются! Ордосы, часть 1.

 cS
ab

target = a
start = S

two ways exists: {[S, b, a], [S, c, a]}

v(a) = speed of moving from neighbor(a) to a

1. [S, b, a]
time = |Sb| / v(b) + |ba| / v(a) = sqrt(2) / v(b) + 1 / v(a)

2. [S, c, a]
time = |Sc| / v(c) + |ca| / v(a) = 1 / v(c) + sqrt(2) / v(a)

Let's compare time1 and time2:
  sqrt(2) / v(b) > (sqrt(2) - 1) / v(a) + 1 / v(c)

  * v(a)v(b)v(c)

  sqrt(2) *v(a)*v(c) + (1 - sqrt(2)) * v(b) * v(c) - v(a)*v(b) > 0

  define u such that v(b) = u * v(a) and assume u > 1

  (u + (1 - u) * sqrt(2)) * v(a) * v(c) > u * v(a)^2

  / v(a)^2

  (u + (1 - u) * sqrt(2)) * v(c)/v(a) > u

  if u = 1.01
    (1.01 - 0.0141) * v(c)/v(a) > 1.01

    0.986 * v(c) > v(a)

  We can simplify: if v(c) > v(a) * (u + (u - 1) * sqrt(2)) then minpath = "Sca"

assume v(a) = v(b) (i.e. a and b have same tile)
  if v(c) > v(a) then time(Sba) > time(Sca)
                 then minpath = "Sca"

                                   cS
                                  ab

s = v * t ==> t = s / v

slow: t1 = 2.4 / v(a)
fast: t2 = 1 / v(c) + 1.4 / v(a)

we planning to show t1 - t2 > 0
t1 - t2 = 2.4 / v(a) - (1 / v(c) + 1.4 / v(a))

t1 - t2 = 1 / v(a) - 1 / v(c)

1 / v(a) > 1 / v(c) [* v(c)v(a)]
v(c) > v(a) [* v(c)v(a)]



"""

import random
import pygame as pg
import sys
from lib import *
# from pg.locals import *

pg.init()
screen = pg.display.set_mode((WIDTH, HEIGHT))
pg.display.set_caption("Dune model")
clock = pg.time.Clock()

vec = pg.math.Vector2

def render_grid():
  for i in range(HEIGHT // GRID_SIZE + 1):
    pg.draw.line(screen, C.GRAY, (0, GRID_SIZE * i), (WIDTH, GRID_SIZE * i), width=1)
  for j in range(WIDTH // GRID_SIZE + 1):
    pg.draw.line(screen, C.GRAY, (GRID_SIZE * j, 0), (GRID_SIZE * j, HEIGHT), width=1)

"""
Если юнит периодически отмечается в нескольких точках цикла, то
мы можем соединить эти точки эллипсами, причем точки будут фокусами эллипса.
объединение множеств эллипсов даст территорию достижимости для юнита, на всех интервалах времени
от начала отметок до текущего момента времени.
Из последняя точки строится не эллипс, а окружность с центром в этой точке и радиусом пропорциональным
времени прошудшему после отметки.

Почему эллипсы?
Эллипс - результат пересечения двух конусов:
верхнего (x1, t1), напрвленного в прошлое и нижнего (x0, t0), направленного в будущее.
где t0 <= t1.

График:
x0 -> ellipse(x0, x1, v*t-d(x0, x1)) -> x1 -> ellipse(x1, x2, ...) -> x2 -> ... -> xn -> circle(xn, R:=v*t) -> ...

"""

units = []
u = Unit(2, 5, "Tank")
u2 = Unit(2, 7, "Tank")
u3 = Unit(1, 2, "Quad")
units.append(u)
units.append(u2)
units.append(u3)
myfont = pg.font.SysFont('Consolas', 32)

run = True
while run:
  clock.tick(FPS)
  current_time = pg.time.get_ticks()
  
  for event in pg.event.get():
    if event.type == pg.QUIT or (event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE):
      run = False
    if event.type == pg.KEYDOWN:
      if event.key == pg.K_UP:
        u.move()
      if event.key == pg.K_UP:
        u.move()
      if event.key == pg.K_RIGHT:
        u.rotate(1)
      if event.key == pg.K_LEFT:
        u.rotate(-1)
    if event.type == pg.MOUSEBUTTONDOWN:
      if event.button == LEFT_MB:
        point_end = pg.mouse.get_pos()

  screen.fill((224, 192, 160))
  # for entity in all_sprites:
  #   displaysurface.blit(entity.surf, entity.rect)
  #   entity.move()
  # pg.draw.line(screen, C_YELLOW, (100, 100), point_end, width=3)
  # pg.draw.lines(screen, C_GREEN, True, [(200, 300), (300, 300), (310, 350)], width=3)
  render_grid()
  for unit in units:
    unit.render(screen)

  text = myfont.render(repr(clock), True, (255, 255, 255))
  screen.blit(text, text.get_rect())
  pg.display.flip()

pg.quit()
sys.exit()