function sqrt(x: real): (r: real)
requires x >= 0.0
ensures sqrt(x)*sqrt(x) == x
ensures r >= 0.0

lemma MulGt (x: real, y: real)
requires x >= 0.0 && y >= 0.0
ensures x <= y ==> x * x <= y * y

lemma MulGt2 (x: real, y: real)
requires x >= 0.0 && y >= 0.0
ensures x < y ==> x * x < y * y

lemma A()
ensures sqrt(2.0) > 1.0
{
  if sqrt(2.0) <= 1.0 {
    assert sqrt(2.0) * sqrt(2.0) == 2.0;
    assert sqrt(2.0) * sqrt(2.0) <= 1.0 * 1.0 by { MulGt(sqrt(2.0), 1.0); }
  }
}

lemma B()
ensures sqrt(2.0) < 2.0
{
  // MulGt(2.0, sqrt(2.0));
}

lemma C()
ensures 1.0 < sqrt(2.0) < 2.0
{
  A();
}

lemma D()
ensures sqrt(2.0) < 1.415
{}

lemma E()
ensures 1.414 < sqrt(2.0) < 1.415
{
  MulGt(sqrt(2.0), 1.414);
}

lemma SqrtZero()
ensures sqrt(0.0) == 0.0

lemma Inc(x: real)
requires x >= 0.0
ensures sqrt(x + 1.0) - sqrt(x) <= 1.0
{
  if x == 0.0 {
    assert sqrt(0.0) == 0.0 by { SqrtZero(); }
    assert sqrt(1.0) - sqrt(0.0) <= 1.0;
  } else {
    if sqrt(x + 1.0) - sqrt(x) > 1.0 {
      // sqrt(x + 1.0)^2 - 2 sqrt(x + 1.0) * sqrt(x) - sqrt(x)^2 > 1.0
      // 2 x + 1.0 - 2 sqrt(x + 1.0) * sqrt(x) > 1.0
      // 2 x - 2 sqrt(x + 1.0) * sqrt(x) > 0
      // x > sqrt(x + 1.0) * sqrt(x)
      // x^2 > (x + 1.0) * x
      assume x > sqrt(x + 1.0) * sqrt(x);
      assert x * x > (x + 1.0) * x by { MulGt2(sqrt(x + 1.0) * sqrt(x), x); }
      assert x > (x + 1.0);
    }
  }
}