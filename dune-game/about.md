# Define types using predicates
```dafny
datatype Unit = Unit {
  const pos: Point
  const speed: nat
  const hp: nat
  const maxhp: nat
  const unittype: UnitType

  lemma HPbounded()
  ensures hp <= maxhp

  lemma MoveBound(g: Game, t1: nat, t2: nat, p1: Point, p2: Point)
  requires t1 <= t2
  requires g.time == t1 ==> this.pos == p1
  requires g.time == t2 ==> this.pos == p2
  ensures dist(p1, p2) <= this.speed * (t2 - t1)

  // lemma DefineType()
  // ensures this.unittype == Quad ==> this.speed == 2 && this.maxhp == 130
  // ensures this.unittype == Trike ==> this.speed == 2 && this.maxhp == 100

  predicate isQuad()
  {
    this.speed == 2 && this.maxhp == 130
  }

  predicate isTrike()
  {
    this.speed == 2 && this.maxhp == 100
  }

  predicate inRect(x: int, y: int, w: nat, h: nat) {
    && x <= this.pos.x <= x + w
    && y <= this.pos.y <= y + h
  }
}
```

# Вдохновляющие возможности для модели

* Поиск, визуализация соседних состояний: Создаем гипотезу, что три пули находятся рядом (< some dist) и
  может рассчитать, что могло быть до такой ситуаций, и какие варианты будущего могут быть после на малом масштабе времени(используя главные уравнения) на большом маштабе времени (используя выведенные из уравнения теоремы). Вместо времени может быть любое свойство, время рассмотрено для наглядности и ясности.

* Ограничение на очередь событий: Для фиксированной позиции на карте можно поставить ограничение на список событий (точное время не важно, важна лишь очередность or "topological invariants matters"), которые могли бы происходить во время катки. На выходе генерируется видео (one possible instance) причем можно выбрать режим Human vs Human, Human vs PC и другие комбинации.

* Поддержка текстовых промптов: Можно описать текстом ситуацию из жизни (когда например было трудно пройти уровень в игре) и модель, используя LLM сначала построит приблизительный контекст описывамой ситуации, затем после преобразования его в корректную модель (не картинку или видео, а именно модель) с набором эвристик для доказательства её корректности вычислит доказательство. На выходе тоже будет видео игры, но в отличии от видео в неё можно будет сыграть и строго рассмотреть варианты действий как за противника, так и за игрока. К тому же, как бонус, будет найдены эвристики и можно, также, заняться изучением их свойств, может это даже интересней будет.

* Возможность вычисления решений с определенным коэффициентом устойчивости. Возможность доказательства устйчивости семейства решений относитльно перемены начальных условий для решателя.
  (see Simplex method in Linear Programming)

* Возможность продлить методы, используемые в этом проекте на непрерывное пространство принятия решений (теория управления).
  - Как эффективно применить понятие вариационной производной по отношению к дискретным моделям? Имеет ли это смысл?
  - Из топологии мы знаем, что седловые точки могут иметь важное значение. Например они могут являтся проекциями многомерных геометрических объектов (мб. 4-мерный тор).
    Приблизительный план применения идеи:
      Что если для обучения модели (типа Q-learning) может быть достаточно отобразить реальное пространство в некоторую "песочницу"правила для которой ищутся индивидуально для каждого класса ситуаций. Мало того, создание/вычисление характеристик такой модели будет просто напросто подобно процессу создания игры человеком (или созданию модели игры потому, что сначала родается модель, а потом мы её программируем).

      Хотя стоит отметить, что модель может дорабатываться также и в
      процессе программирования. Хотя бы потому что сам так делаю.

# Более детальное описание работы/взаимодействия пользователя с моделью



