include "./dune.dfy"

method testDefineType(u: Unit)
{
  u.DefineTypes();
  assert u.unittype == Quad || u.unittype == Trike;
  assert u.maxhp >= 100;
}

method testQuad1(u: Unit)
requires u.unittype == Quad
{
  u.DefineTypes();
  assert 0 <= u.hp <= 130 by { u.HPbounded(); }
}

method testQuad2(g: Game, u: Unit)
requires u.unittype == Quad
{
  u.DefineTypes();
  assume g.time == 0 ==> u.pos == Point(0, 0);
  if g.time == 10 {
    // Unit cannot move far than 20 cells if it has speed = 2 and dt = 10
    assert -20 <= u.pos.x <= 20 && -20 <= u.pos.y <= 20 by {
      u.MoveBound(g, 0, 10, Point(0, 0), u.pos);
      assert dist(Point(0, 0), u.pos) <= 20;
      assert abs(u.pos.x) + abs(u.pos.y) <= 20;
      assert abs(u.pos.x) <= 20;
    }
    assert u.inRect(-20, -20, 40, 40);
  }
}

method testTrike1(g: Game, u: Unit)
{
  
}