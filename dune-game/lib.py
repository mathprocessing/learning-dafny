import pygame as pg

GRID_SIZE = 64
HEIGHT = 800
WIDTH = 600
FPS = 60
LEFT_MB, MIDDLE_MB, RIGHT_MB = 1, 2, 3
DIRS = [(1, 0), (0, 1), (-1, 0), (0, -1)]
U_TYPES = {
  "Quad": {"speed": 1, "maxhp": 130, "cost": 200},
  "Trike": {"speed": 2, "maxhp": 100, "cost": 150},
  "Soldier": {"speed": 1, "maxhp": 20, "cost": 60},
  "Tank": {"speed": 1, "maxhp": 200, "cost": 300},
}

tank_surf = pg.Surface((GRID_SIZE, GRID_SIZE), pg.SRCALPHA)
pg.draw.rect(tank_surf, (0, 96, 0), (0 + 8, 00 + 12, 50, 40))
pg.draw.rect(tank_surf, (0, 128, 0), (10 + 8, 10 + 12, 30, 20))
pg.draw.rect(tank_surf, (32, 32, 96), (20 + 8, 16 + 12, 40, 8))

class C:
  RED = (255, 0, 0)
  GREEN = (0, 255, 0)
  BLUE =  (0, 0, 255)
  YELLOW = (255, 255, 0)
  GRAY = (50, 50, 50)

def dir_to_vec(d):
  assert d in range(len(DIRS))
  return DIRS[d]

class Unit:
  """
  >>> u = Unit(1, 2, "Quad")
  >>> assert u.direction == 0
  >>> assert u.speed == 1
  >>> u.move(); u
  Unit(x=2, y=2)

  >>> u.speed = 2; u.move(); u
  Unit(x=4, y=2)

  >>> list(U_TYPES.keys())[:2]
  ['Quad', 'Trike']
  """
  def __init__(self, x, y, unit_type, hp=-1, direction=0):
    self.unit_type = unit_type
    self.speed = U_TYPES[unit_type]["speed"]
    self.maxhp = U_TYPES[unit_type]["maxhp"]
    self.cost = U_TYPES[unit_type]["cost"]
    # ---
    self.x = x
    self.y = y

    self.hp = self.maxhp if hp < 0 else hp
    assert direction in range(4), "direction out of range"
    self.direction = direction

  def __repr__(self):
    return f'Unit(x={self.x}, y={self.y})'

  def move(self):
    dx, dy = dir_to_vec(self.direction)
    self.x += dx * self.speed
    self.y += dy * self.speed

  def rotate(self, n):
    self.direction = (self.direction + n) % 4
    
  def render(self, scr):
    gs = GRID_SIZE
    if self.unit_type == "Tank":
      tank_rect = pg.Rect(self.x * gs, self.y * gs, gs, gs)
      tank_surf_2 = pg.transform.rotate(tank_surf, -90 * self.direction)
      scr.blit(tank_surf_2, tank_rect)
    else:
      r = pg.Rect(gs * self.x + gs // 4, gs * self.y + gs // 4, gs // 2, gs // 2)
      pg.draw.rect(scr, C.RED, r, 0)
      dx, dy = dir_to_vec(self.direction)
      x = gs * self.x + gs // 2 + dx * gs // 4
      y = gs * self.y + gs // 2 + dy * gs // 4
      pg.draw.circle(scr, C.GREEN, (x, y), 5)

if __name__ == "__main__":
  import doctest
  doctest.testmod(optionflags=doctest.FAIL_FAST)