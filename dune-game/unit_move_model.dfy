type pos = x: int | x >= 0

const gtime: int
type dir = d: int | d == 1 || d == -1 witness 1

datatype Option<T> = Some(x: T) | None
datatype CanonStatus = On | Off

// 1D world
predicate isWall(x: int, t: int)

datatype Unit = Unit(damage: pos, armor: pos, reloadTime: pos, moveDelay: pos) {
  const target: Option<Unit> := None
  const canonStatus := On
  function after(dt: pos): Unit
  predicate damagedByAtTime(B: Unit, time: int)
  predicate spawnedBulletAtTime(t: int)
  function getPos(t: int): int // 1D pos for simple case
  // if havePosAtTime always return non-null value then unit don't dissapears
  // if havePosAtTime returns singleton then unit position always deterministic
  // Note: havePosAtTime not the same as u.beginsMoveAt(x, t) because
  // unit can stay on the cell some time and we might campture some arbitrary point
  // in such interval of "unit staying"
  predicate beginsMoveAt(x: int, t: int, d: dir)
  ensures !isWall(x, t) && !isWall(x + d, t)

  // tStart
  predicate movedOptimallyFromTo(xStart: int, xFinish: int, tStart: int)

  predicate movedTo(x: int, d: dir, t: int)

  lemma MovedStep(xStart: int, xFinish: int, tStart: int, d: dir)
  ensures movedOptimallyFromTo(xStart, xFinish, tStart) <==>
    (movedOptimallyFromTo(xStart + d, xFinish, tStart - this.moveDelay) && 
    movedTo(xStart, d, tStart) )
}

// We don't need to add u.moveTimer or some var for saving time from last move
// Instead we define axiom that says: 
// "If u begins move in (x, t) then it must appears in (x+dx, t+dt)"

lemma BeginsMoveImplhavePos(u: Unit, x: int, t: int, d: dir)
requires u.beginsMoveAt(x, t, d)
ensures u.getPos(t) == x

lemma Connect(u: Unit, x: int, t: int, d: dir)
requires u.beginsMoveAt(x, t, d)
ensures u.getPos(t) == x // We can move it over here
ensures u.getPos(t + u.moveDelay) == x + d
ensures forall t' | t < t' < t + u.moveDelay :: u.getPos(t') == x

lemma StayInPlace(u: Unit, x: int, t: int, d: dir, dt: int)
requires 0 <= dt < u.moveDelay
requires u.beginsMoveAt(x, t, d)
ensures u.getPos(t) == u.getPos(t + dt)
{
  Connect(u, x, t, d);
  // assert u.getPos(t) == x by { A(u, x, t, d); }
  // assert u.getPos(t + u.moveDelay) == x + d by { A(u, x, t, d); }
  // assert t < t + 1 < t + u.moveDelay;
  // assert u.getPos(t + 1) == x by { A(u, x, t, d); }
}

/*
About paths and goals:
If we don't know model of game level piece in rect [(x, t), (x+dx, t+dt)]
we can assume that model of unit behavior approximately can be described as 
intermediate goal or path slice.
For example real goal can be really far away from starting point, but
we assume that behaviour don't change if we replace real goal by fictional
possible intermediate goal (even if it not real intermediate).

----|
|**g|****G
|*  |
|u  |
-----

-| - piece of game level that we able to model with given amount of computational resources

G - real goal
g - fictional goal that we add, to add "logic" to behaviour of unit `u`

*/

method test(u: Unit)
{
  assume u.getPos(0) == 0;
  assume u.getPos(u.moveDelay) == 1;
  assume u.getPos(2 * u.moveDelay) == 2;
  assert u.movedOptimallyFromTo(0, 2, 2 * u.moveDelay);


}