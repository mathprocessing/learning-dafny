predicate i(a: bool, b: bool) { a ==> b }
predicate f(a: bool, b: bool, c: bool, d: bool)

method chain(a: bool, b: bool, c: bool, d: bool)
{
  // Expantion of implication chain
  var lhs := ((a ==> b) ==> c) ==> d;
  var rhs := i(a, b) && i(b, c) && i(c, d);
  
  // How to find all such f using some algorithm?
  // One solution already exist: f = false, but how many others?
  assume f(a, b, c, d) == false;
  assert (rhs || f(a, b, c, d)) ==> lhs;


  // assume (rhs || f(a, b, c, d)) ==> lhs;
  // Plan
  // 1. assime that it true
  // 2. Find possible f's that makes it true
  // if f(a, b, c, d) == true {
  //   assert false;
  // }
}

// method chain(a: bool, b: bool, c: bool, d: bool)
// {
//   // Expantion of implication chain
//   var lhs := ((a ==> b) ==> c) ==> d;
//   var rhs := i(a, b) && i(b, c) && i(c, d);
//   assert rhs ==> lhs; // but not vise versa
//   var rhs2 := i(a, b) && i(b, c) && i(c, d) && i(b, d);
//   assert rhs2 ==> lhs; // Ok
//   // assert lhs <==> rhs2; // not
//   // Problem:
//   // how to find minimum set of ordered pairs such that if we connect them using `==>` the
//   // whole expression will be equivalent to lhs?
//   var rhs3 := i(a, b) && i(b, c) && i(c, d);
//   assert rhs3 ==> lhs;
//   assert rhs3 <== lhs; // not holds because rhs3 set of states is too small
//   // possible fix: add new _ && term to right side

// }