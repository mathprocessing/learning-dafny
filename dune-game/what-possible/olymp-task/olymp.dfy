function pow(x: int, y: nat): (r: nat)
decreases y
requires x >= 1
ensures r >= 1
ensures (y == 0 || x == 1) ==> r == 1
{
  if y == 0 then 1 else pow(x, y-1) * x
}

// lemma Test(a: nat, b: nat, x: int)
// requires a * a + b * b == 544
// requires a + b == x
// {
//   assert 544 == 4 * 4 * 2 * 17;
//   assert a != b; // 544/2 not perfect square
//   assert a > 0; // 544 not perfect square
//   if a < b {
//     assert pow(2, 2) == 4;
//   }
// }

/*a^2 + b^2 = 544 = 17 * 2^5
a + b = x


x^2 = 544 + 2ab


1. a even and b even

let a = 2k, b = 2m

(2k)^2 + (2m)^2 = 544
4k^2 + 4m^2 = 544

4(k^2 + m^2) = 544 
k^2 + m^2 = 125 + 11 = 136 = 34 * 4 = 17 * 8




2. a odd and b odd

(2k+1)^2 + (2m+1)^2 = 544

4k^2 + 4k + 1 + 2m^2 + 4m + 1


method 2


a^2 + b^2 = 544
a + b = x


a^2 + b^2 = 544
a^2 + b = ax

b^2 - b = 544 - ax

b (b - 1) + a(a + b) = 544

..


a^2 + b^2 = 544
a + b = x

a - b = y

x^2 - y^2 = 4ab
*/