include "./olymp.dfy"

lemma Test()
  ensures pow(2, 3) == 8
  ensures pow(2, 10) == 1024
  ensures pow(3, 2) == 9
  ensures pow(3, 3) == 27
{}