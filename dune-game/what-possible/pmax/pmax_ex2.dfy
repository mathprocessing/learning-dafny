include "./lib.dfy"
include "./model1.dfy"

const r: prob

function R(s: state, a: action): prob
{
  if s == 2 then 0.0 else
    if s == 4 then 1.0 else r
}

lemma {:verify false} TnormalizedLemma(s: state, a: action)
ensures Tnormalized(s, a, T)
{}

lemma vopt(s: state)
ensures V(s) == max(Q(s, 0), Q(s, 1))

lemma qsum(s: state, a: action)
ensures Q(s, a) == R(s, a) * (mul(T(s, a, 0), V(0)) + mul(T(s, a, 1), V(1)) + mul(T(s, a, 2), V(2)) + mul(T(s, a, 3), V(3)) + mul(T(s, a, 4), V(4)))

const stop: action := 0
const right: action := 1

method game1()
{
  assert forall a :: R(4, a) == 1.0;

  // equations for state = 0
  assert Q(0, stop) == r * (0.8 * V(0) + 0.1 * V(1) + 0.1 * V(2)) by { qsum(0, stop); }
  assert Q(0, right) == r * V(1) by { qsum(0, right); }

  assert V(0) == max(r * (0.8 * V(0) + 0.1 * V(1) + 0.1 * V(2)), r * V(1)) by { vopt(0); }
  if V(0) == r * V(1) {
    assert Q(0, stop) == 0.8 * r * r * V(1) + 0.1 * r * (V(1) + V(2));
    assert Q(0, right) == V(0);
  } else {
    // assert Q(0, stop) == V(0);
    // assert Q(0, right) == 0.9 * V(1);
    // assert V(0) == 0.9 * (0.8 * V(0) + 0.1 * V(1) + 0.1 * V(2));
    // assert 0.28 * V(0) == 0.09 * (V(1) + V(2));
    // assert 0.28 * V(0) <= 0.18 by {
    //   assert V(1) + V(2) <= 2.0;
    //   assert 0.09 * (V(1) + V(2)) <= 0.18;
    // }
    // assert V(0) <= 9.0 / 14.0;
  }


}
