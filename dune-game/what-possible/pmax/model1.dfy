include "./lib.dfy"

function T(s: state, a: action, s': state): prob
{
  var delta := (5 + s' - s) % 5;
  assert s == s' ==> delta == 0;
  match a
    case 0 => if delta == 0 then 0.8 else (if delta == 1 || delta == 2 then 0.1 else 0.0)
    case 1 => if delta == 1 then 1.0 else 0.0
}