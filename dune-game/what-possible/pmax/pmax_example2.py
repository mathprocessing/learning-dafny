"""
  1. Observe reward(s, a1, s') = r1
  if exists a2 :: T(s', a2, s) = 1.0 then we can loop infinitely to make constant income (+ reward r1) but r2 can be uncertain
    i.e. 
        if policy(s) = {a1, a2, a1, ...} is optimal then
          Q(s, a1) = r1 + g * r2 + g^2 * r1 + g^3 * r2 + ...
 
          Q(s, a1) = r1 (1 + g^2 + g^4 + ...) + r2 * g * (1 + g^2 + g^4 + ...)

          Q(s, a1) = (r1 + r2 * g) * (1 + g^2 + g^4 + ...)

          Q(s, a1) = (r1 + r2 * g) / (1 - g^2)

          averageQ(s, a1) = r1 * (1 + g * r2/r1) * (1 - g) / (1 - g^2)
          averageQ(s, a1) = r1 * (1 + g * r2/r1) * (1 - g) / (1 - g) / (1 + g)
          averageQ(s, a1) = r1 * (1 + g * r2/r1)  / (1 + g)

          if r1 = r2 then averageQ(s, a1) = r1
          if r2 = 0 then averageQ(s, a1) = r1  / (2 - (1 - g)) > r1 / 2

          We have general conclusion if g < 1 then
            reward sequence Q([1/2, 1/2, 1/2, ...]) < Q[1, 0, 1, 0, ...])
          Why? What a logic behind that? This means g < 1 are must be balanced somehow? Using some `alpha` or other parameters?
        else
          exists better policy(s) = {a1, ...}
          Q(s, a1) > (r1 + r2 * g) / (1 - g^2)

    where r1 = R(s, a1, s')
          r2 = R(s', a2, s)

  where a2 = inv(a1) i.e. a2 inverse action of a1

  1 + 0.25 + 0.0625 + ... = 4/3

  1 + 0.5 + 0.25 + 0.125 + ... = 2

  1 + (1 + g + g^2 + ...) * g = 1 + g + g^2 + ...
  1 + S * g = S
  S = 1 / (1 - g)

  1 + (1 + g^2 + ...) * g^2 = 1 + g^2 + g^4 + ...
  1 + S * g^2 = S
  S = 1 / (1 - g^2) = 1 / 0.75 = 4 / 3


Introduce inner states

known:
  policy(s) -> action
probability driven:
  policy(s, a) -> probability


Assume exist a win state `ws` :: V(ws) = 1 = 100%
We can create reward  function with desired properties:
How to mark that V(4) is a terminal state? Using T(4, _, 4) = 1 and T(4, _, !=4) = 0?
>>> R = lambda s, a: 0 if s == 2 else 0.9

In that game if we just move right we always lose, but this policy is better:
(0, right) -> (1, stop/jump) -> (2 -> _)
                             -> (3 -> right)

>>> show_equation(0)
Q(0, stop) = 0.9 * (0.8 * V(0) + 0.1 * V(1) + 0.1 * V(2))
Q(0, right) = 0.9 * V(1)

V(0) = 0.9 * max(0.8 * V(0) + 0.1 * V(1) + 0.1 * V(2), V(1))
if V(0) = 0.9 * V(1) then
  





"""
def createQ():
  return [[A]*size, [A]*size]

A = -1 # asterisk means unknown value
size = 5
_actions = range(2)
_states = range(size)
Q = createQ()

def getQ(s, a):
  return Q[a][s]

def copyQ(q_arr):
  c = []
  for a in _actions:
    c.append(q_arr[a].copy())
  return c

def showQ(q_arr):
  def get_line(action):
    acc = ""
    for s in range(size):
      if s > 0:
        acc += ", "
      val = q_arr[action][s]
      if val == A:
        acc += "*"
      else:
        acc += str(val) 
    return acc
  print(f"Stop  -> [{get_line(0)}]")
  print(f"Right -> [{get_line(1)}]")

def T(state, action, next_state):
  delta = (next_state - state + size) % size
  if action == 0:
    if delta == 0:
      return 0.8
    elif delta == 1: # or delta == 4: # 4 == -1 (mod 5)
      return 0.2
    else:
      return 0
  elif action == 1:
    if delta == 1:
      return 1
    else:
      return 0

def max(x, y):
  if x == A or y == A:
    return A
  if x <= y:
    return y
  return x

def V(s: int):
  return max(Q[0][s], Q[1][s])

def isp(x): 
  """
  >>> assert isp(1)
  >>> assert isp(0)
  >>> assert not isp(2)
  >>> assert isp(A)
  """
  return 0 <= x <= 1 or x == A

def mul(x, y):
  """
  >>> mul(A, 1)
  -1

  >>> mul(A, A)
  -1

  >>> mul(0.5, 0.5)
  0.25

  >>> assert mul(0.5, 1) == mul(1, 0.5)
  """

  assert isp(x) and isp(y)
  if x == 0 or y == 0:
    return 0
  if x == 1:
    return y
  if y == 1:
    return x
  if x == A or y == A:
    return A
  return x * y

def add(x, y):
  assert isp(x) and isp(y)
  if x == 0:
    return y
  if y == 0:
    return x
  if x == A or y == A:
    return A
  return x + y

def pnot(x):
  if x == A:
    return A
  return 1 - x

def padd(x, y):
  """ 1 - (1 - p1) * (1 - p2) """
  return add(1, pnot(mul(pnot(x), pnot(y))))

def targetQ_step(s, a, s_next):
  # T(s, a, s_next) * V(s_next)
  return mul(T(s, a, s_next), V(s_next))

def targetQ(s, a):
  # SUM_a(T(s, a, s_next) * V(s_next))
  acc = 0
  for s_next in _states:
    acc = add(acc, mul(T(s, a, s_next), V(s_next)))
  return acc

def show_equations(s):
  for a in _actions:
    acc = ""
    add_plus = False
    for s_next in _states:
      pt = T(s, a, s_next)
      if add_plus:
        acc += " + "
      add_plus = False
      if pt != 0:
        add_plus = True
        if pt == 1:
          acc += f"V({s_next})"
        else:
          acc += f"{pt} * V({s_next})"
    print(f"Q({s}, {a}) = {acc.rstrip(' +')}")

def trainQ(s, a, s_next):
  t = targetQ_step(s, a, s_next)
  if Q[a][s] == A: 
    # if Q is not known then we just move
    # knowledge from one side to other
    Q[a][s] = t
  else:
    # we don't use `alpha`, just developing formula
    Q[a][s] = add(Q[a][s], t)


if __name__ == "__main__":
  import doctest
  doctest.testmod(optionflags=doctest.FAIL_FAST)
  # doctest.testmod()