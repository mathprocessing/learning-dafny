type state = x: int | 0 <= x < 5
type action = x: int | 0 <= x < 2

type prob = x: real | 0.0 <= x <= 1.0

function abs(x: int): nat {
  if x < 0 then -x else x
}

function rabs(x: real): real {
  if x < 0.0 then -x else x
}

function max(p1: prob, p2: prob): prob
{
  if p1 <= p2 then p2 else p1
}

function {:verify false} mul(p1: prob, p2: prob): (res: prob)
ensures 0.0 <= res <= 1.0
{
  p1 * p2
}

lemma HelperMulLeRight(x: prob, y: prob, n: prob)
ensures x <= y ==> n * x <= n * y
{}

lemma MulLeRight(n: prob, x: prob, y: prob)
ensures x <= y ==> n * x <= n * y
{
  HelperMulLeRight(x, y, n);
}

lemma MulLe(n: prob, x: prob, y: prob)
requires n > 0.0
ensures x <= y <==> n * x <= n * y
{
  MulLeRight(n, x, y);
}

function add(p1: prob, p2: prob): prob
{
  1.0 - mul((1.0 - p1), (1.0 - p2))
}

lemma AddComm(p1: prob, p2: prob)
ensures add(p1, p2) == add(p2, p1)
{}

lemma AddZero(p: prob)
ensures add(p, 0.0) == p
{}

function V(s: state): prob
function Q(s: state, a: action): prob

function sum(left: state, right: state, f: state -> real): real
requires left <= right
decreases right - left
{
  if left == right then f(right) else f(left) + sum(left + 1, right, f)
}

predicate Tnormalized(s: state, a: action, t: (state, action, state) -> prob)
{
  sum(0, 4, (s': state) => t(s, a, s')) == 1.0
}

lemma MaxMul(n: prob, x: prob, y: prob)
requires 0.0 <= n * x <= 1.0
requires 0.0 <= n * y <= 1.0
ensures max(n * x, n * y) == n * max(x, y)
{
  if x <= y {
    MulLeRight(n, x, y);
    // calc == {
    //   n * max(x, y);
    //   n * y;
    //   { assert n * x <= n * y by {  }}
    //   max(n * x, n * y);
    // }
  } else {
    MulLeRight(n, y, x);
    // calc == {
    //   n * max(x, y);
    //   n * x;
    //   { assert n * y <= n * x by { MulLeRight(n, y, x); }}
    //   max(n * x, n * y);
    // }
  }
}