include "./lib.dfy"

lemma TProps(s: state, a: action, s': state)
ensures 0.0 <= T(s, a, s') <= 1.0
ensures T(s, a, s') == 0.8 ==> s == s'
ensures T(3, 0, 4) == 0.1
// ensures abs(s - s') == 0 ==> T(s, 1, s') == 0.0 || T(s, 1, s') == 0.1
// ensures abs(s - s') == 0 ==> T(s, 0, s') == 0.8
// ensures s - s' == 1 ==> T(s, a, s') == 0.0
// ensures s' - s == 1 ==> T(s, 0, s') == 0.1
// ensures s' - s == 1 ==> T(s, 1, s') == 1.0
// ensures s' - s == 1 ==> T(s, 0, s') == 0.0 || T(s, 0, s') == 0.1
// ensures abs(s - s') == 1 ==> T(s, 0, s') == 0.0 || T(s, 0, s') == 0.1
// ensures abs(s - s') == 1 ==> T(s, 1, s') == 0.0 || T(s, 1, s') == 1.0
// ensures abs(s - s') == 1 ==> T(s, a, s') == 0.0 || T(s, a, s') == 1.0 || T(s, a, s') == 0.1
{}