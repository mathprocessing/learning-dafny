include "./lib.dfy"
include "./model2.dfy"

lemma EqV(s: state)
ensures V(s) == max(Q(s, 0), Q(s, 1))

lemma EqT(s: state, a: action)
ensures Q(s, a) == mul(T(s, a, 0), V(0)) + mul(T(s, a, 1), V(1)) + mul(T(s, a, 2), V(2)) + mul(T(s, a, 3), V(3)) + mul(T(s, a, 4), V(4))

method test()
{
  assume V(4) == 1.0;
  // Q(s, a) = R(s, a, s') + V(s')
  /*
  [s1]:
  Q(s, a) = R(s, a, s1) + T(s, a, s1) * V(s1)
  
  Equation T:
  forall s' in state :: Q(s, a) = R(s, a, s') + T(s, a, s') * V(s')

  Equation V:
  V(s) = max_a(Q(s, a))
  */
  // forall a: action ensures Q(3, a) == a as real {
  //   // assert Q(3, a) == mul(T(3, a, 4), V(4)) by { EqT(3, a); }
  //   if case a == 0 => {
  //     assert Q(3, 0) == mul(T(3, 0, 4), 1.0);
  //     assert T(3, 0, 4) == 0.0;
  //     assert Q(3, 0) == 0.0;
  //   } case a == 1 => {
  //     assert Q(3, 1) == 1.0;
  //   }
  // }

  
  assert Q(3, 0) == 0.0 && Q(3, 1) == 1.0 by {
    forall a: action ensures Q(3, a) == a as real;
  }
  
  assert V(3) == V(4) by { EqV(3); }

  /*
  1. Observe reward(s, a1, s') = r1
  if exists a2 :: T(s', a2, s) = 1.0 then we can loop infinitely to make constant income (+ reward r1) but r2 can be uncertain
    i.e. 
        if policy(s) = {a1, a2, a1, ...} is optimal then
          Q(s, a1) = r1 + g * r2 + g^2 * r1 + g^3 * r2 + ...
 
          Q(s, a1) = r1 (1 + g^2 + g^4 + ...) + r2 * g * (1 + g^2 + g^4 + ...)

          Q(s, a1) = (r1 + r2 * g) * (1 + g^2 + g^4 + ...)

          Q(s, a1) = (r1 + r2 * g) / (1 - g^2)

          averageQ(s, a1) = r1 * (1 + g * r2/r1) * (1 - g) / (1 - g^2)
          averageQ(s, a1) = r1 * (1 + g * r2/r1) * (1 - g) / (1 - g) / (1 + g)
          averageQ(s, a1) = r1 * (1 + g * r2/r1)  / (1 + g)

          if r1 = r2 then averageQ(s, a1) = r1
          if r2 = 0 then averageQ(s, a1) = r1  / (2 - (1 - g)) > r1 / 2

          We have general conclusion if g < 1 then
            reward sequence Q([1/2, 1/2, 1/2, ...]) < Q[1, 0, 1, 0, ...])
          Why? What a logic behind that? This means g < 1 is must balanced somehow? using some alpha or other parameters?
        else
          exists better policy(s) = {a1, ...}
          Q(s, a1) > (r1 + r2 * g) / (1 - g^2)

    where r1 = R(s, a1, s')
          r2 = R(s', a2, s)

  where a2 = inv(a1) i.e. a2 inverse action of a1

  1 + 0.25 + 0.0625 + ... = 4/3

  1 + 0.5 + 0.25 + 0.125 + ... = 2

  1 + (1 + g + g^2 + ...) * g = 1 + g + g^2 + ...
  1 + S * g = S
  S = 1 / (1 - g)

  1 + (1 + g^2 + ...) * g^2 = 1 + g^2 + g^4 + ...
  1 + S * g^2 = S
  S = 1 / (1 - g^2) = 1 / 0.75 = 4 / 3
  */
  
}