method test2(p: bool, q: bool, r: bool)
{
  var equiv := (p <==> q) && (q <==> r);
  var imps := (p ==> q) && (q ==> r) && (r ==> p);
  assert equiv ==> (p <==> r); // From two equalities we have third equality
  assert imps <==> equiv; // Minimum requirement for prove p,q,r equivalence is to prove 3 implications. How to prove that two implications not enough?

  /* Idea define queue of implications:
  First class of bringing several implications to one term is `chain`:
  a -> b
    3 states [true, true], [false, true], [false, false]
  a -> b -> c
    4 states [true, true, true], [false, true, true], [false, false, true], [false, false, false]
  model with n variables: x1 -> x2 -> .. -> xn
    have (n+1) states i.e. number of steps linear relative to number of variables in predicate

  Next class of agregating implications is trees:
  * -> *: class [a -> b] contains only one possible term, states = 3
  * -> * -> * [chain] states = 3 + 1 = 4
  * -> * <- * [tree with consumer] states = 2 * 2 + 1 = 5
    0 -> 0 <- 0
    -----------
    0 -> 1 <- 0
    0 -> 1 <- 1
    -----------
    1 -> 1 <- 0
    1 -> 1 <- 1
    
  * <- * -> * [tree with source] states = 2 * 2 + 1 = 5
    0 <- 0 -> 0
    0 <- 0 -> 1
    -----------
    1 <- 0 -> 0
    1 <- 0 -> 1
    -----------
    1 <- 1 -> 1

  Let's for example check states for a -> b -> c -> a, states = 2
    0 -> 0 -> 0 -> 0
    1 -> 1 -> 1 -> 1

  Also we can think about policy gradient, i.e. about what we change when add variable `-> a` to term `a -> b -> c`
  a -> b -> c:  states = 3 + 1 = 4
  a -> b -> c -> x:
    if x ~ a then states = 2 (changed)
             else states = 4 (unchanged)

  Use idea of derivative:
    states(Term + dTerm) - states(Term) = -2 if dTerm ~ a else 0

    if we interested only when abs(dstates) > 0 then
      we can care only about such dTerms and name it `interesting` becouse
        it's obvious that 
        Interest(a -> b -> c -> a) > Interest(a -> b -> c -> c) or Interest(a -> b -> c -> b)

    Hmm... my euristic is might be wrong

    What we can say about?
    a -> b -> c -> b
      0 -> 0 -> 0 -> 0 ==> states = 3 ==> (change = -1)
      0 -> 1 -> 1 -> 1
      1 -> 1 -> 1 -> 1

    i.e. all patterns have different number of states?
    a -> b -> c -> a ==> states = 2 just circle of 3 implications
    a -> b -> c -> b ==> states = 3 because b <=> c and it reduces to a -> b which have 3 states
    a -> b -> c -> c ==> states = 4 ambigious c, we can remove it, c -> c not adding any constraint to model because it evaluates to `true`
    a -> b -> c      ==> states = 4
    a -> b -> a      ==> states = 2 circle of 2 implications
    a -> !a          ==> states = 1 when `false -> true`

    f(Term1, Term2) = Term1 + Term2;
    df/dTerm2 = x ~ Term2


  Идея информационных состояний.
  Пусть нам известно, что ИС может быть описано только ипользуя `==>`.
  Тогда мы можем перебрать всевозможные ИС и реакцию на них например вычислив policy(is, action)

  Очевидно, что если мы рассматриваем произвольную модель, то тогда мы вынуждениы ввести метрику для сложности модели, чтобы иметь воможность их сравнивать.
  Например в какой-то ситуации Game модель нейросети M1 требует больше памяти, чем символьная модель M2.
  Метрика Complexity: M -> Comparable

  Но, что если сначала задуматься  что еще
  как мы можем сопоставить внутренние процессы психики с моделью, с зоопарком компонентов для построения модели, которая ведет себя подобно этой психике
  */
}