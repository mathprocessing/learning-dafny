"""
Proving props about sin and cos

>>> a = I(-1, 5)
>>> b = I(2, 8)
>>> a.inter(b)
I(2, 5)

>>> I(-1, 1).inter(I(1, 3))
>>> I(1, 1)

"""

class I:
  """Real interval"""
  def __init__(self, l, r, is_empty_set=False):
    self.is_empty_set = is_empty_set
    if is_empty_set:
      self.l = None
      self.r = None
      return
    assert l <= r
    self.l = l
    self.r = r
  
  def __repr__(self):
    return f'I({self.l}, {self.r})'

  def __eq__(self, other):
    """ Smooth equality operator """
    if self.inter(other).is_empty_set:
      # Var in self interval can't be equal to var in other
      return I(False, False)
    if 

  
  def expr_eq(self, other):
    """ Interval equivalent to each other as expressions """
    assert is_instance(other, I)
    if self.is_empty_set:
      return other.is_empty_set
    return other.l == self.l and other.r == self.r

  def inter(self, other):
    if self.is_empty_set or other.is_empty_set:
      return I(is_empty_set=True)
    
    left = max(self.l, other.l)
    right = min(self.r, other.r)
    if left > right:
      return I(is_empty_set=True)

    return I(left, right)


# Safe sin calculation
def sin(x: float) -> I:
  if x == 0:
    return I(0, 0)

if __name__ == "__main__":
  import doctest
  doctest.testmod(optionflags=doctest.FAIL_FAST)