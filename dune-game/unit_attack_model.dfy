type pos = x: int | x >= 0

const gtime: int

datatype Option<T> = Some(x: T) | None
datatype CanonStatus = On | Off

datatype Unit = Unit(damage: pos, armor: pos, reloadTime: pos) {
  const target: Option<Unit> := None
  const canonStatus := On
  function after(dt: pos): Unit
  predicate damagedByAtTime(B: Unit, time: int)
  predicate spawnedBulletAtTime(t: int)
  predicate havePosAtTime(pos: int, t: int) // 1D pos for simple case
}

/*

if unitA.damagedBy(unitB at time = t) then
  unitB.canonStatus = Off in time interval [t - unitB.reloadTime, t + unitB.reloadTime]

another way of thinking:
  if unitB.canonStatus = Off at time = t then
    forall u:Unit :: u.damagedBy(unitB, t) == false


if we say:
  u.canonStatus = Off that means cononStatus = Off forall possible values of time

if we say:
  u.atTime(t).canonStatus = Off then we know status only for time = t but for other time we fill status unknown
*/


// lemma Property1(A: Unit, B: Unit, t: int)
// ensures 
// if A.damagedByAtTime(B, t) then
//   if B.atTime(t).canonStatus == Off

// requires u2.delay <= dt
// requires u2.target == Some(u)
// ensures u.after(dt).armor + u2.damage == u.armor

/*
u.spawnedBulletAtTime(t) = true ==> 
  forall t' | 0 < t' < u.delay :: !u.spawnedBulletAtTime(t + t')


 */

lemma {:axiom} A1(u: Unit, t: int)
ensures u.spawnedBulletAtTime(t) ==>
(forall t' | 0 < t' < u.reloadTime :: !u.spawnedBulletAtTime(t + t') &&
  u.spawnedBulletAtTime(t + u.reloadTime))

lemma {:axiom} CantSpawnAfter(u: Unit, t: int, t': int)
requires u.spawnedBulletAtTime(t)
ensures (0 < t' < u.reloadTime ==> !u.spawnedBulletAtTime(t + t'))
                                && u.spawnedBulletAtTime(t + u.reloadTime)


// If unit u now is spaned a bullet then some steps before it's cannon must off
lemma CantSpawnBefore(u: Unit, t: int, t': int)
requires 0 < t' < u.reloadTime
requires u.spawnedBulletAtTime(t)
ensures !u.spawnedBulletAtTime(t - t')
{
  if u.spawnedBulletAtTime(t - t') {
    CantSpawnAfter(u, t - t', t');
  }
}

lemma CantSpawnInterval(u: Unit, t: int, t': int)
requires t' != 0
requires -(u.reloadTime as int) < t' < u.reloadTime
requires u.spawnedBulletAtTime(t)
ensures !u.spawnedBulletAtTime(t + t')
{
  if t' > 0 {
    CantSpawnAfter(u, t, t');
  } else {
    assert t' < 0;
    CantSpawnBefore(u, t, -t');
  }
}

/*
if we assume u.reloadTime = 2
  we have full model of possible states:
  ["0*0", "*01", "10*"]

  How we can decribe this using logical formula?
  Our goal is to define axiom that instantly helps to prove any lemma about spawn time model by analysis of full set of states.
*/

predicate P(t: int)

method test(t: int)
{
  calc <==> {
    (!P(t-1) && !P(t+1)) || (!P(t) && P(t+1)) || (P(t-1) && !P(t));
    if P(t) then !P(t-1) && !P(t+1) else true;
  }
  // This pattern "proves" that from CantSpawnInterval we can prove anything that we might need about spawn model
  // i.e We can use only one axiom `CantSpawnInterval` define `CantSpawnBefore` and `CantSpawnAfter` only as theorems
}

lemma CantSpawnBeforeTheorem(u: Unit, t: int, t': int)
requires 0 < t' < u.reloadTime
requires u.spawnedBulletAtTime(t)
ensures !u.spawnedBulletAtTime(t - t')
{
  CantSpawnInterval(u, t, -t');
}

lemma CantSpawnAfterTheorem(u: Unit, t: int, t': int)
requires 0 < t' < u.reloadTime
requires u.spawnedBulletAtTime(t)
ensures !u.spawnedBulletAtTime(t + t')
{
  CantSpawnInterval(u, t, t');
}

// This means that THREE axioms `CantSpawnAfter`, `CantSpawnBefore`, `CantSpawnInterval` equivalent to each other?
// How to fastly prove p <==> q <==> r?

// See fast_prove_equiv.dfy