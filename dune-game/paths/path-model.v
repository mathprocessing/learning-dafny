From Coq Require Import ssreflect ssrfun ssrbool.

(*
Unit id: time -> state
Building id: time -> state

1. Scenario of developing DistinctPosition
Assume
unit 0 (t:=0) = State(dir:=left, pos=[0, 0])
unit 1 (t:=0) = State(dir:=right, pos=[0, 0])
==> contradiction

axiom DistinctPosition :
  forall u1 u2, u1.id <> u2.id -> u1.pos <> u2.pos

2. Scenario of developing DistanceMaxBound

Assume
unit 0 (t:=0) = State(dir:=left, pos=[0, 0])
unit 0 (t:=1) = State(dir:=left, pos=[1, 0])
==> contradiction because we need to turn around first
dir:  left -> up or down -> right -> pos=[1, 0]
time: 0       1              2       3

Axiom DistanceMaxBound :
distance (unit id (t:=t1)) (unit id (t:=t2)) <= t2 - t1

3. Scenario of developing axiom3

*)

Inductive Direction : Type :=
| left | right | up | down.

Definition RotateLeft (d: Direction) :=
match d with
| left => down
| right => up
| up => left
| down => right
end.

(* Rotation with little bug, it's more interesting to prove *)
Definition RotateRight (d: Direction) :=
match d with
| left => up
| right => down
| up => up
| down => left
end.

Definition getDirClass(d: Direction): nat :=
match d with
| left => 0
| right => 1
| up => 0
| down => 2
end.

Lemma R_left_right d :
getDirClass (RotateLeft (RotateRight d)) = getDirClass d.
Proof.
elim d; try trivial.
Qed.

(*

For fixed time
we have 
Some Set of units
t -> {U left (0, 0), U right (0, 1)}

time 0:
{}

time 1: produced unit
{U left (0, 0)}

time 2:
{U right (0, 0)}

time 3:
{U right (1, 0)}

time 4: producted unit at (0, 0)
{U right (1, 0), U left (0, 0)}

lemma forall u1 u2 in Set, pos1 <> pos2




We need to define 


G : time -> GameState

If we say G 0 = G 1 this means that
game state don't change at first tick


Also we can say about symmetries
mirror(G 0) = G 1




*)


Record UState : Type := mkU
{ dir : nat -> nat -> Direction
; pos : prod nat nat
}.

Definition getId (u : UState) :=
  match u with
  | mkU i _ => i
  end.

Inductive UState : Type :=
| U : nat -> nat -> UState.

Definition getId (u : UState) := 
match u with
| U i _ => i
end.

Goal getId (U 3 0) = 3. 
Proof ltac:(by []).



(* Check pos (U 0 0) : nat * nat.*)

Check (5, 6) : nat * nat.

Check (pos (U 0 0 _ _)).1. (*fst: (x, y) -> x *)
(* Same as *)
Check fst (pos (U 0 0 _ _)).

Check @fst : forall A B : Type, A * B -> A.
Check @snd : forall A B : Type, A * B -> B.

Axiom DistinctPosition :
  forall (u1 u2 : UState), getId u1 <> getId u2 -> pos u1 <> pos u2.

Check DistinctPosition _ _.

(*
Assume
unit 0 (t:=0) = State(dir:=left, pos=[0, 0])
unit 1 (t:=0) = State(dir:=right, pos=[0, 0])
==> contradiction
*)


Definition u0 := (U 0 0 left (0, 0)).
Definition u1 := (U 1 0 left (0, 0)).

Goal 

Check @DistinctPosition (U 0 0 left (0, 0)) (U 1 0 left (0, 0)).
































