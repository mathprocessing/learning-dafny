include "./../dune.dfy"

// ghost predicate isOptimalMove(dr: Point, A: Point, C: Point)
// requires dr.vabs() <= 1
// {
//   var B := A.add(dr);
//   forall dr2: Point | dr2.vabs() <= 1 :: dist(B, C) <= dist(A.add(dr2), C)
// }

// method test()
// {
//   //assert Point(0, 0).vabs() == 0;
//   //assert isOptimalMove(Point(0, 0), Point(0, 0), Point(0, 0));

//   assert !isOptimalMove(Point(0, 0), Point(0, 0), Point(0, 1)) by {
//     assert dist(Point(0, 0), Point(0, 1)) == 1;
//     assert dist(Point(0, 0), Point(0, 1)) > dist(Point(0, 1), Point(0, 1)) by {
//       assert dist(Point(0, 1), Point(0, 1)) == 0;
//       assert dist(Point(0, 0), Point(0, 1)) == 1;
//     }
//     assert forall p: Point :: p.vabs() == 0 ==> p == Point(0, 0);
//     assert forall p: Point :: p.vabs() == 1 ==> (p == Point(0, 1) || p == Point(1, 0) || p == Point(-1, 0) || p == Point(0, -1));
//     assume false;
//   }
// }

method testforall()
{
  var s := [1, 2, 3];
  forall i: nat <- s ensures 1 <= i <= 3 {
    
  }
}