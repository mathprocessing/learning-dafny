"""
>>> UNITS.quad["damage"]
10

>>> assert Unit(UNITS.quad).power > Unit(UNITS.trike).power

>>> Unit(UNITS.quad).power, Unit(UNITS.trike).power
(1300, 800)

>>> Unit(UNITS.quad).cost, Unit(UNITS.trike).cost
(200, 150)

>>> q = Unit(UNITS.quad); q
Unit(name="Quad")

> assert q.move(1, 0).move(-1, 0) == q

> q.pos
(0, 0)

> q.move_attack(Cell(4, 0))
[...
q.moveTo(1, 0)
check(distance) <= quad.range (=3)
q.fire_at(4, 0)
]

>>> q.attack_range
3

"""
from math import hypot

class Point:
  """
  >>> Point(3, 4)
  Point(3, 4)

  >>> _.abs()
  5.0
  """
  def __init__(self, x, y):
    self.x, self.y = x, y

  def __repr__(self):
    return f'Point({self.x}, {self.y})'

  def abs(self):
    return hypot(self.x, self.y)

  def __eq__(self, other):
    """
    >>> assert Point(0, 1) != Point(0, 0)
    >>> assert Point(0, 1) == Point(0, 1)
    """
    return self.x == other.x and self.y == other.y

class Map:
  def __init__(self):
    pass

  def cell(self, x, y):
    return Cell(x, y, self.m.get_cell_type(x, y))

class Action:
  pass

class MoveTo(Action):
  def __init__(self, x, y):
    self.pos = Point(x, y)

  def __repr__(self):
    return f'MoveTo({self.pos.x}, {self.pos.y})'

  def is_possible(self, map_object):
    return not map_object.get_cell_type() != "wall"

  def __eq__(self, other):
    assert isinstance(other, MoveTo)
    assert isinstance(self.pos, Point)
    assert isinstance(other.pos, Point)
    return self.pos == other.pos

class Movable:
  def __init__(self, x, y):
    self.x, self.y = x, y
    self.moving_to = None # None | Point

  @property
  def is_moving(self):
    return self.moving_to != None

  def get_actions(self):
    """
    * If unit is in already moving state then at beginning of move target cell must be empty
    * When move in moving state unit belongs to two cells at one moment

    >>> m = Movable(0, 0)
    >>> actions = list(m.get_actions()); actions
    [MoveTo(-1, -1), MoveTo(0, -1), MoveTo(1, -1), MoveTo(-1, 0), MoveTo(1, 0), MoveTo(-1, 1), MoveTo(0, 1), MoveTo(1, 1)]

    >>> assert MoveTo(0, 0) == MoveTo(0, 0)
    >>> assert MoveTo(0, 0) != MoveTo(0, 1)

    Can we count null Action?
    >>> assert MoveTo(1, 0) in actions
    >>> assert MoveTo(0, 0) not in actions
    """
    for i in range(-1, 2):
      for j in range(-1, 2):
        if i == 0 and j == 0:
          continue
        yield MoveTo(self.x + j, self.y + i)
  
  
class UNITS:
  quad = {"name": "Quad", "damage": 10, "armor": 130, "cost": 200, "attack_range": 3}
  trike = {"name": "Trike", "damage": 8, "armor": 100, "cost": 150, "attack_range": 3}

class Unit:
  def __init__(self, unit_opt):
    self.name = unit_opt["name"]
    self.damage = unit_opt["damage"]
    self.armor = unit_opt["armor"]
    self.cost = unit_opt["cost"]
    self.attack_range = unit_opt["attack_range"]

  def __repr__(self):
    return f'Unit(name="{self.name}")'

  @property
  def power(self):
    return self.damage * self.armor

class Trig:
  """
  >>> t = Trig()

  >>> t.get_goal()
  0

  >>> t.set("tg", 0.5)
  True

  >>> t.get_goal()
  1

  >>> t.values[0]
  0.5

  >>> t.have("tg"), t.have("sin")
  (True, False)

  >>> t.values[:2]
  [0.5, None]

  >>> t.clear()
  >>> t.set("sin", 0.5)
  True
  >>> t.set("cos", 0.5)
  True
  >>> t.relax(0)
  True
  >>> t.get("tg")
  1.0

  >>> t.clear(); t
  tg = None, sin = None, sin2a = None, cos = None

  >>> t.set("sin", 0.5)
  True
  >>> t.relax(3)
  True

  >>> t.relax(0)
  True

  >>> t





  """
  def __init__(self):
    self.ind = {"tg": 0, "sin": 1, "sin2a": 2, "cos": 3}
    self.funcs = ["tg", "sin", "sin2a", "cos"]
    self.values = [None, None, None, None]

  def __repr__(self):
    s = ""
    for i, n, v in self.props():
      if i > 0: s += ', '
      s += f'{n} = {v}'
    return s

  def clear(self):
    for i in range(len(self)):
      self.values[i] = None

  def have(self, name):
    value = self.values[self.ind[name]]
    return value is not None

  def relax_all():
    pass

  def relax(self, goal) -> bool:
    goal_name = self.funcs[goal]
    # lemma: goal = None
    if goal_name == "tg":
      if self.have("sin") and self.have("cos"):
        return self.set("tg", self.get("sin") / self.get("cos"))
    elif goal_name == "sin":
      if self.have("cos"):
        return self.set("sin", (1 - self.get("cos") ** 2) ** 0.5)
    elif goal_name == "cos":
      if self.have("sin"):
        return self.set("cos", (1 - self.get("sin") ** 2) ** 0.5)
    else:
      return False

  def set(self, name, value):
    for i, n, v in self.props():
      if n == name:
        self.values[i] = value
        return True
    return False

  def get(self, name):
    return self.values[self.ind[name]]

  def __len__(self):
    return len(self.funcs)

  def props(self):
    for i in range(len(self)):
      yield i, self.funcs[i], self.values[i]

  def get_goal(self):
    for i, name, value in self.props():
      if value is None:
        return i



if __name__ == '__main__':
  import doctest
  doctest.testmod(optionflags=doctest.FAIL_FAST)
