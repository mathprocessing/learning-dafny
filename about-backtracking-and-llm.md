# About

Минусы LLM:
* Нет возможности получить очки confidence для каждого ответа
* output может повторяться и это замедляет поиск решений
  + Как можно гарантировать исчерпывающий перебор классов решений?

Люди ошибочно считают это минусом:
* Неточности в математических расчетах
  + Точность не обязательно - главное способ перебора решений, даже если часть решений будет пересекаться (как множества) и содержать ошибки можно все равно просто пытаясь до победного, перебирая и создавая модификации наиденного и т.д. найти корректное множество решений для проблемы, грубо говоря неточность можно компенсировать обратным вызовом или какой-либо другой абстрактной парадигмой поиска решений из комбинаторики.

Плюсы:
* LLM может создавать контекты(интерфейсы из функций, подмножества лексикона языка), которые по сути и являются промежуточным звеном на пути к решению математической или инженерной задачи
* LLM может создавать по сути что угодно, она ведь просто дополняет текст, т.е. в случае если какая-либо теория (о том как работают языковые пространтсва например), то мы за счет большого количества запросов (проведя множество экспериментов) сможем в этом убедится просто удостоверившись что метод A менее эффективен, чем метод B применимо к проблеме C.
+ Критика: Иногда анализ с помощью уравнений или формул более эффективен, чем эксперимент, но для этого нужна теория... может быть теория эффективного поиска решений или что-то подобное
  Проблема крайне общая поэтому может понадобиться не просто рассматривать частные случаи (они могут слабо помогать), а скорее отображать все простроанство проблем в некое пространство с более простой геометрией, чем средняя (медиана) проблем. Можно такое пространство назвать "линейным миром" S и постулировать:
1. Любые факты в S могут быть связаны только xor и not функциями, что упрощает процесс вывода формул.
2. Из (1) следует, что какие-то два свойства могут быть связаны двумя видами связей: Альтерация(x=!y), Эквивалентность(x=y).

Линейное пространство это одно из решений, наверняка есть что-то проще и что-то сложнее. Оно интересно в плане изучения ограничений над предикатами на сетке(клеточные автоматы замороженные во времени, пример: still life).

## gpt answer
Q: Минусы LLM:
* Нет возможности получить очки confidence для каждого ответа
* output может повторяться и это замедляет поиск решений
  + Как можно гарантировать исчерпывающий перебор классов решений?

Люди ошибочно считают это минусом:
* Неточности в математических расчетах
  + Точность не обязательно - главное способ перебора решений, даже если часть решений будет пересекаться (как множества) и содержать ошибки можно все равно просто пытаясь до победного, перебирая и создавая модификации наиденного и т.д. найти корректное множество решений для проблемы, грубо говоря неточность можно компенсировать обратным вызовом или какой-либо другой абстрактной парадигмой поиска решений из комбинаторики.

Плюсы:
* LLM может создавать контекты(интерфейсы из функций, подмножества лексикона языка), которые по сути и являются промежуточным звеном на пути к решению математической или инженерной задачи
* LLM может создавать по сути что угодно, она ведь просто дополняет текст, т.е. в случае если какая-либо теория (о том как работают языковые пространтсва например), то мы за счет большого количества запросов (проведя множество экспериментов) сможем в этом убедится просто удостоверившись что метод A менее эффективен, чем метод B применимо к проблеме C.
+ Критика: Иногда анализ с помощью уравнений или формул более эффективен, чем эксперимент, но для этого нужна теория... может быть теория эффективного поиска решений или что-то подобное
  Проблема крайне общая поэтому может понадобиться не просто рассматривать частные случаи (они могут слабо помогать), а скорее отображать все простроанство проблем в некое пространство с более простой геометрией, чем средняя (медиана) проблем. Можно такое пространство назвать "линейным миром" S и постулировать:
1. Любые факты в S могут быть связаны только xor и not функциями, что упрощает процесс вывода формул.
2. Из (1) следует, что какие-то два свойства могут быть связаны двумя видами связей: Альтерация(x=!y), Эквивалентность(x=y).

Линейное пространство это одно из решений, наверняка есть что-то проще и что-то сложнее. Оно интересно в плане изучения ограничений над предикатами на сетке(клеточные автоматы замороженные во времени, пример: still life).

План как решить эти проблемы: (Напишите возможный план)

A: План по решению указанных проблем:

1. Относительно отсутствия возможности получить очки уверенности для каждого ответа:
   - Используйте альтернативные подходы, такие как модификация LLM или других языковых моделей, чтобы оценить уверенность в предоставленных ответах.
   - Рассмотрите использование дополнительных методов статистической обработки данных для оценки достоверности результатов.

2. Относительно повторяющегося вывода и замедления процесса поиска решений:
   - Введение механизма фильтрации и удаления дублирующихся результатов на основе сравнения текстовых шаблонов может помочь избежать повторений.
   - Поиск более эффективных алгоритмических подходов к обработке и фильтрации вывода LLM может значительно ускорить процесс.

3. Гарантированный исчерпывающий перебор классов решений:
   - Анализируйте все возможные комбинации переменных, функций или логических условий в рамках задачи формализации Problem A.
   - Разработайте стратегии генерации новых классов решений путем систематического изменения параметров или добавления новых элементов в текущее решение.
   - Используйте методы оптимизации, такие как отсечение неперспективных классов решений на основе заранее известных ограничений и эвристик.

4. Относительно неточности в математических расчетах:
   - Понимайте, что LLM не является полностью точ$самым инструментом для математических расчетов и может допускать ошибки.
   - Важно проверять результаты, полученные от LLM, с помощью более точных методов, таких как символьные вычисления или численные методы.
   - Используйте встроенные функции и библиотеки математических расчетов для улучшения точности результатов.

5. Относительно использования абстрактных парадигм поиска решений:
   - Рассмотрите применение различных техник комбинаторной оптимизации или метаэвристики (например, генетические алгоритмы или имитация отжига) для перебора классов решений и нахождения наилучшего из них.
   - Продолжайте экспериментировать и модифицировать найденное решение, чтобы отслеживать его корректность и эффективность.

Обратите внимание, что предложенный план является общей стратегией и может потребоваться дальнейшая конкретизация в зависимости от конкретного контекста вашей задачи формализации Problem A.

# Let's ask gpt-3.5-turbo

Q: Can we use backtracking with LLM's? How to do it? How to find new solutions and cut already discovered? Maybe you can give me some "Prompt" that helps to ask you such things correctly and with maximum quality of result.

A: Yes, backtracking can be used with LLMs (Language Models) to find new solutions and cut already discovered ones. To utilize backtracking with LLMs, you need to define a search space and use the model iteratively.

Here is a prompt that helps ask your question more effectively:

"Can you provide insights on how to implement backtracking using Language Models (LLMs) for problem-solving? Specifically, I'm interested in understanding how to find new solutions while efficiently eliminating previously discovered ones. Additionally, any guidance on ensuring high-quality results would be greatly appreciated."

Q: Can you provide insights on how to implement backtracking using Language Models (LLMs) for problem-solving? Specifically, I'm interested in understanding how to find new solutions while efficiently eliminating previously discovered ones. Additionally, any guidance on ensuring high-quality results would be greatly appreciated. Assume we want to find solution for "How to formalize problem A in Dafny language".

A: Certainly! Implementing backtracking using Language Models (LLMs) for problem-solving involves defining a search space and iteratively exploring different possibilities. Here's a step-by-step approach to finding a solution for formalizing Problem A in the Dafny language:

1. Define the search space: Begin by identifying the elements or components required to formalize Problem A in Dafny. This could include variables, functions, classes, and logical constraints.

2. Generate initial solutions: Start with an initial solution that partially addresses Problem A in Dafny. You can do this manually or use existing knowledge about Dafny syntax and conventions.

3. Iterate through possible expansions: Use the LLM to generate potential expansions of your initial solution by posing questions like "What additional variables/functions/classes are needed?" or "How can I refine the logical constraints?". The LLM will provide suggestions based on its language understanding capabilities.

4. Evaluate proposed expansions: For each suggested expansion from the LLM, consider whether it brings you closer to solving Problem A efficiently and effectively within the context of Dafny's requirements and limitations.

5. Apply constraint checking: If there are specific syntactic or semantic rules that must be adhered to in Dafny, incorporate them into your evaluation process at each iteration of expanding solutions provided by the LLM.

6. Backtrack when necessary: If an expansion turns out to be incorrect or leads away from finding a suitable solution, backtrack by undoing that particular change and exploring alternative options generated by the LLM.

7. Repeat steps 3-6 until satisfactory solution found/termination criteria met: Continuously iterate through generating expansions from suggestions made by the LLM while evaluating their feasibility according to both syntactic and semantic constraints imposed by Dafny until you arrive at a satisfactory formalization of Problem A.

To ensure high-quality results during this process:

- Familiarize yourself with relevant documentation or tutorials on Dafny to have a solid understanding of its syntax, features, and limitations.
- Use the LLM's suggestions as guidance but exercise critical thinking and judgment when evaluating proposed expansions.
- Regularly validate your solutions against test cases or known examples to verify correctness and effectiveness.
- Seek feedback from experts in Dafny or formal methods if possible.

Remember that while LLMs can provide valuable suggestions, they are probabilistic models. Therefore, it is crucial to review and evaluate the generated solutions manually for accuracy before implementing them.