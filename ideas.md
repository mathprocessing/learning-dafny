# about QL

Assume we have simple idea of continuous space with V-function with endpoint `s = 0`

How to connect problem of finding path in 2d space with RL in 2d space?

```
If our task is find optimal path then we can assume that reward in edge = -length of edge

V(0) = 0
if exists action a and T(0, a, 1) = 100% then
  Q(1, a) = R + T(0, a, 1) * g * V(0) = R(1->0)

in 1D:
1. s = x
2. actions = {left, stop, right}
3. time continuous
4. V(x) = distance to M(x=0)
Prop1: Q(x, a) = R(x, x+1)

V(x+dx) - V(x) = if x > 0 then -dx else dx

dV/dx = if x > 0 then -1 else 1 = -sign(x)

V = -abs(x)

in general when we have two actions move left and move right:

max(R(left) * dx + V(x - dx), R(right) * dx + V(x + dx)) - V(x) = 0
  by max entend
max(R(left) * dx + V(x - dx) - V(x), R(right) * dx + V(x + dx) - V(x)) = 0


V(x - dx) - V(x) = -V'(x) * dx
V(x + dx) - V(x) = +V'(x) * dx

max(R(left) * dx - V'(x) * dx, R(right) * dx + V'(x) * dx) = 0

max(R(x, left) - V'(x), R(x, right) + V'(x)) * dx = 0

R(x, left) = V'(x)
OR
R(x, right) = -V'(x)

for example if V(x) = -abs(x)
R(5, left) = -sign(5) = -1
R(-5, right) = sign(5) = 1





simpler rule:
if we observe R(x1, x2) then
  V'(x1 + x2 / 2) must converge to R(x1, x2) / (x2 - x1) i.e.

V'(x1 + x2 / 2) = (1 - alpha) * V'(x1 + x2 / 2) + alpha * R(x1, x2) / (x2 - x1)

or more precise

finally after learning this must holds
  R(x1, x2) ~= V(x2) - V(x1)


V(x1) -> V(x2) + 





if R(x, left) < 0 then Q'(x, left) > 0

if R(x, left) > 0 then Q'(x, left) < 0


-R(x1, left, x2) / abs(x2 - x1) = -Q'(x, left)
x2 < x1

-R(x1, right, x2) / abs(x2 - x1) = Q'(x, right)
x1 < x2

==>
R-(x1, a, x2) = Q'(x, a) * (x2 - x1)


-R(x1, a, x2) = Q'(x, a) * (x2 - x1)

-R(x1, a, x2) = Q(x2, a) - Q(x1, a)


Q(x1, a) = R(x1, a, x2) + Q(x2, a)

for optimal trajectory

Q(x1, a) = R(x1, a, x2) + max(Q(x2, left), Q(x2, right))


-R(x1, a, x2) = max(Q(x2, left), Q(x2, right)) - Q(x1, a)

-R(x1, a, x2) = max(Q(x2, a), Q(x2, next(a))) - Q(x1, a)

reward отщипывает кусок от будущей награды V и после получения награды V падает

reward для непрерывного пространства например э то пересечение финишной черты на определенной скорости vmin <= v <= vmax

-R(x1, a, x2) = max[Q(x2, a)       - Q(x1, a),
                    Q(x2, next(a)) - Q(x1, a)]

-R(x1, a, x2) = max[Q'(x, a) * (x2 - x1),
                    Q(x + dx, a + da) - Q(x, a)]



f(x + dx, y + dy) - f(x, y) = 

[f(x + dx, y) - f(x, y)] + [f(x + dx, y + dy) - f(x + dx, y)]

df/dx * Dx + df/dy * Dy


Df/Dx = df/dx + df/dy * Dy/Dx = f'x + f'y * Dy/Dx

Df/Dy = df/dx * Dx/Dy + df/dy = f'x / (Dy/Dx) + f'y

if x = y ==> Df/Dx = 2f'x
  if phi(s) = pi/4 = 45 deg
  Df/Ds = Df/D(i/sqrt(2) dx + j/sqrt(2) dy) = 2f'x / sqrt(2) = sqrt(2) * f'x

Taylor expantion of V:
V(x(t + dt), t + dt) = V(x(t), t) + V't * dt + V'x * x't * dt + o(dt)



```
# What if Assert can have 3 states instead of 2 states
## Before feature
```dfy
predicate Divides(d: nat, n: nat)
  requires d > 0 && n > 0
{
  exists d' :: d' > 0 && d * d' == n
}

predicate Prime(n: nat)
{
  n > 1 && forall d | 1 < d < n :: !Divides(d, n)
}

method Main() {
  assert Prime(2); // true + no counterexample (and we don't need it ==> user exprerience: GOOD)
  assert Prime(3); // true + no counterexample (and we don't need it ==> user exprerience: GOOD)
  assert Prime(4); // may not hold + no explanation why this happens, possible branches, possible actions from current state to fix (this is bad thing for user)
}
```

## After feature

```dfy
// ...

method Main() {
  assert Prime(2); // true + no conterexample (and we don't need it ==> user exprerience: GOOD)
  assert Prime(3); // true + no conterexample (and we don't need it ==> user exprerience: GOOD)
  assert Prime(4); // false + showed conterexample:                    (user exprerience: in worst case must be GOOD)
  // in  d * d' = n, d:int == 2 && d':int == d && n:int == 2 ==> Divides(d, n)
  // IDE Action: `add counterexample as assume virtually`
  // virtually means project don't change but temporary constraint model from counterexample intersets with project model
  // More formally: PROJECT_MODEL_TEMP := COUNTEREXAMPLE_MODEL && PROJECT_MODEL
  // Then fter user exit from this `temporary view`: PROJECT_MODEL_TEMP := PROJECT_MODEL

  assert Prime(SOME_BIG_NUMBER); // result of assert is undetermined, please add more assuptions or tactics
  /* 
    because dafny can't prove `assert expr == false`
    and can't prove that `assert expr == true`.
    
    Current state:
    "List of all assumptions related to definition in that file (context): "
    ...
    "Possible actions that can change status "undetermined" to true or false":
    ...
    Add expr1 as lemma in ...
    ...
    Extend definition of Divides ...
    ...
  */
}
```

Techically `feature_assert expr;` equivalent to `assert expr; OR assert !expr;` and if both fails it prints message `result of assert is undetermined` or something like that.

## Some ideas around `user experience`
How to make `user experience(state)` in all maximally worst cases maximally good or `equal` to each other in all possible states?
Main idea: If developers just try to make user exprerience as good as possible then this idea might fails becouse is really hard task.
Istead of it we can apply some mathematical constructions to this question.
If `user exprerience` of set of states is (almost) equal to each other, users can't provide that given state is bad.
This means that system evolves and grow equal in all abstract ways/directions.

How to make user exprerience good in worst case:
  * It's a like proving some theorem `forall IDE_state: min_bound(user_experience_level) > min_level_of_user_experience`
  * Another way to think about it: Create list of bad situations

List of bad states syntax:
  * "Description of problem" -> "action to solve" (task_level = score of task)

List of actions example:
  "action to solve" -> (task_level = score of task)

Example list of bad states:
  * User can't make any little step from current states to formalize target model -> (Change/ReDevelop prover) (task_level = 15)
  * Prover can't figure out how to prove something simple -> Change/ReDevelop prover (task_level = 100)
  * Prover not warning user about that all it action is (action of user) unoptimal (task_level = 10)
    + Partial case: User must check unsoundness by placing `assert false;` -> solved use option of Boogie ...

# Ideas inspired by group theory
```dafny
lemma inv_left_id()
ensures mul(inv(E), E) == E
{
  if * {
    // simpler way to prove (more short dafny code may still exists)
    // Note: shorter dafny code != shorter proof, i.e. shortest proof (that checks fast) ==> long detailed dafny code
    // id_right();
    // inv_right();
    // assoc();
    calc == {
      mul(inv(E), E);
      {
        assert inv(inv(E)) == E by {
          // How to ensure (in dafny code) that {F1, F2, F3} is minimal set of lemmas.
          // is_minimal({F1, F2, F3}) = do_proof({F1, F2, F3}) && !(do_proof({F1, F2}) || do_proof({F1, F3}) || do_proof({F2, F3}))
          // or another definition:
          // is_strongly_minimal(S) = proof_score(S) && forall reduce_action in RActions :: proof_score(reduced(S, reduce_action)) < proof_score(S)
          // is_minimal(S)          = proof_score(S) && forall reduce_action in RActions :: proof_score(reduced(S, reduce_action)) <= proof_score(S)
          // Test case (concrete example): reduced({F1, F2, F3}, reduce_action)
          //
          id_right(); // F1
          inv_right(); // F2
          assoc(); // F3
        }
        // Maybe exists other paths (not only rewrite inv_inv) and not equivalent to inv_inv?
        // What if instead of z3 we have some `god solver`? We can really happy with it? really or not?
        // What if we don't happy, why it may possible? Give me counterexample!
        // What if we must "move" to some "attractor": "Doing some strange stuff in maximum uncertanty"
        // What if we can happy only when uncertanty exists? Because we need to train yourself? Training yourself give us happiness?

        // Maybe exists some state that presicely "slowed down process of solution" making that particular task unsolvable with fixed set of actions.
      /* * Reusing old interface old_I: score 0%
         * Add to old_I new possibility: score > 0%
         * Hard way, low Q function (like travel around earth)
         * Actions in definition space of old action space/Actions that makes old_action just background_actions...
           ... It''s may be hard... Just that actions that we do NOW.
           * After we change interface we bring/grab some experience, after that virtual state/branch {we use just old_I} changes.
             And after change we don't know that using only that interface is bad (i.e. ==> old_I: score 0% changed to > 0 %)
      */

      }
      mul(inv(E), inv(inv(E)));
      {
        inv_right_inst(inv(E));
      }
      E;
    }
  } else {
    // more detailed view of same (possible same) proof
    calc == {
      mul(inv(E), E);
      {
        assert mul(inv(E), inv(inv(E))) == E by {
          if * {
            inv_right(); // Prove single rewrite step using just lemma with quantifier
          } else {
            inv_right_inst(inv(E));// Using lemma instance
          }
        }
      }
      mul(inv(E), mul(inv(E), inv(inv(E))));
      // It might be helpful if in dafny we can write equivalence classes of expressions (or simple patterns with holes)
      // Example pattern: 
      //   case1 => mul(inv(E), ?a), where ?a = (_, E) | (_, inv(E)) | (_, inv)
      //   case2 => expr@mul(?b, E), where ?b = (_, inv(E)) | number_of_lists(expr) <= 42
      // and after some try #N dafny (or z3 prover) give us cache object case_info_try_N that contains simplified (i.e. forgeting some details) "state"/"set of explored branches"
      // Then we can do try #(N+1) and use case_info_try_N to reduce amount of computation (do not "calculate facts about"/"explore" already explored branches/models)
      // For each try we might see some "statistics"/"helpful properties of processing this try or group of tries" and that info can help us to select new actions,
      // do next (possible better, or more formally with Q >= minimal_Q with probability P ) tries in other regions of computational space.
      {
        assoc();
      }
      mul(mul(inv(E), inv(E)), inv(inv(E)));
      // Now I don't know complexity of proof P for lemma `inv_inv_id: inv(E) * inv(E) = E` using only {id_right, inv_right, assoc}
      // If proof P is easy:
      //    just use inv_inv_id // selecting that case may help to reduce time of current proof but not in all future proofs
      // else:                  // how to reduce time in future proofs by using (only) this proof (state, current task, X...)? // Why we humans or animals do not doing infinite loops? Or we do but not exact loops? loop in equivalence classes but not in details?
      //    don't use inv_inv_id and try to find another way to solve problem + change experience
      {
        // We can do things like (replace X with Y) tactic in Coq.
        assume mul(inv(E), inv(E)) == E; // corresponds to patterns: {mul(?x, ?x) == _, ...}
      }
      mul(E, inv(inv(E)));
      {
        // What is better use
        // Case A: id_left and after inv_inv_id
        // or use
        // Case B: inv_inv_id and after id_right?
        // I think Case B better because we don't have id_left as proved lemma.
        assume inv(inv(E)) == E;
      }
      mul(E, E);
      {
        // assert mul(E, E) == E by { id_right(); }
        // <=> to
        //id_right();
        // but not really equivalent to (I mean that some equivalence of type1 holds, but other equivalence of type 2 not holds)
        id_right_inst(E); // <=> rewrite (@id_right E). in Coq
      }
      E;
    }
  }
}
```