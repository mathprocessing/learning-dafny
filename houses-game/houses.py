"""
ALERT: Q-LEARNING STUFF

Inspired image: https://youtu.be/jtLghGHQ-9s?t=685
* Why we sort?
  H1: We glue two branches
  H2: We map two branches using iso-arrow
  h3: We somehow prove by contradiction but what? (fact - that we can safely use isomorphisms?)
* bad: Q-function have random default strategy, instead we have two points A-strategy and B-strategy
* We can denote a way (intermediate Q(s, a, t)) between A and B. Interestingly learning process can be mapped to
  interval t (0, 1) <-> (A, B).
  Also we can make rules about how Q(s, a, t) must be change during changing `t`.
  For example: Q(t=1) can be <=> ideal solution <=> strategy B.
               Q(t=0) <=> strategy A.
               Q(t=0.5) <=> Some interesting intermediate matrix Q(s, a) that must be optimal for some reason.
           What mean optimal? Imagine that process already happens and some agent already ended learning process.
           And reach B point. We know A, we know B. But how agent may evolve in middle?
* 

"""

def check_n(seq, n):
  """
  Check first n points of sequence

  >>> seq = [0.1, 0.4, 0.8]
  >>> assert not check_n(seq, 2)

  >>> seq = [0.1, 0.6, 0.8]
  >>> assert check_n(seq, 2)
  >>> assert check_n(seq, 3)
  """
  # assert s[0] < 1/2 and s[1]
  res = True
  for i in range(n):
    left, right = i/n, (i+1)/n
    res = res and left <= seq[i] < right
  return res

def check(seq):
  """
  >>> assert check([0.0, 0.99])

  s[0] < 1/n

  s[1] must be in [1/2, 2/3):
    >>> assert check([0.1, 0.5])       # s[1] >= 1/2
    >>> assert check([0.1, 0.66, 0.9]) # s[1] < 2/3

  >>> assert check([0.1, 0.5, 0.9]) # > 1/3

  >>> assert check([0.1, 0.66, 0.9])
  
  """
  for e in seq:
    assert e >= 0 and int(e) == 0 # all elements in interval [0, 1]

  for m in range(2, len(seq) + 1):
    if check_n(seq, m) == False:
      return False
  return True

if __name__ == '__main__':
  import doctest
  doctest.testmod()
