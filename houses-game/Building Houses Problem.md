# Цель и приказы
Игра Немезида
1. отклонение от цели штрафуется
2. отклонение от цели персонажа становится не выгодным исходя из новых правил игры (штрафы это разве не изменение правил? `2 ~ 1`?)
  Как изменить правила игры так чтобы все выполняли приказы, но штрафы уже не понадобились?

# Problem
Imagine a long strip of line (it's a mile along) and someone picks a
point on this strip of land so that they can build a house
then a second person comes along and he wants to build a house as well.

So they decide to split the land into equal halves so that the second person can
have the empty plot he can build his own house then
a third person comes along and he wants to build a house as well
so again they decide to split the land into equal thirds so the third person
can take the empty plot and he can build his own house and so on.

The idea is this: every time a new person comes along they split the land into
plots of equal length two houses are not allowed to occupy the same plot and obviously
once a house has been built it cannot be moved.

How many houses can you build?
Can you do this indefinitely or is there some sort of limit to it?

Now the question i'm really asking is these aren't houses these are points on
the line so imagine the line zero to one.

I want a sequence of points (in 0..1) so that
* the first two points occupy different halves
* the first three point occupy different thirds
* and so on

What is the longest sequence that you can write?

Now you can do this problem with pen and paper but to make it easier for you
i've got a friend of mine to turn it into a game so i'm going to put the link in the
description to this game.

All you have to do is point and click, so I can place a point here and so on if you want to be
more accurate which some of you might be there's even a text box there so I can put in 0.2.

