// How to model point movement in time
datatype Point = Point(x: int, v: int)
{
  // x(t+1) = x(t) + v
  lemma Vel(x: int, t: int)
  ensures (time == t ==> this.x == x) ==> (time == t + 1 ==> this.x == x + v)
}

const time: int

method test(p: Point)
{
  assume p == Point(0, 1);
  assume time == 0 ==> p.x == 0;
  p.Vel(0, 0);
  assert time == 1 ==> p.x == 1;
}

lemma Vel(p: int -> Point, t: int)
ensures p(t+1).x == p(t).x + p(t).v

lemma Vconst(p: int -> Point, t1: int, t2: int)
// ensures t1 == t2 ==> p(t1) == p(t2)
ensures |{1, 2}| == 2
{}


method testtime(p: int -> Point)
{
  assume p(0) == Point(0, 1);
  forall p, t: int {
    Vel(p, t);
  }
  assert p(1).x == 1;
  assume forall t: int :: p(t).v == p(t+1).v;
  assert p(0).v == p(1).v;
  assert p(2).x == 2;
}

method sets()
{
  var s: set<int> := {1, 2, 3};
  assert s + {4} == {1,2,3,4};
  assert |s * {2}| == 1 by {
    assert s * {2} == {2};
  }
}

lemma SetMul(s1: set<int>, s2: set<int>)
requires |s1| == 1
requires |s2| == 1
ensures |s1 * s2| <= 1
{}

// lemma SetMul2(s1: set<int>, s2: set<int>, n: nat)
// requires |s1| == n
// requires |s2| == 1
// ensures |s1 * s2| <= 1
// decreases n
// {
//   if n == 0 {
//     assert s1 == {};
//     assert s1 * s2 == {};
//     assert |s1 * s2| == 0;
//   } else if n == 1 {
//     assert |s1 * s2| <= 1;
//   } else {
//     var x :| {x} <= s1;
//     var s3 :| (s3 + {x} == s1) && !({x} <= s3);
//     assert |s1| == n;
//     assert |s3| == n - 1;

//     SetMul2(s3, s2, n - 1);
//     assert |s3 * s2| <= 1;
//   }
// }

lemma ListConcat(xs: seq<int>, ys: seq<int>)
ensures |xs + ys| == |xs| + |ys|
{}

method testsets2()
{
  assert !(3 in {1, 2});
  assert 2 in {1, 2};
  assert [1, 2] + [3] == [1, 2, 3];
}