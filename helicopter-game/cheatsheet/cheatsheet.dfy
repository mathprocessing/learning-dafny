// https://dafny.org/latest/DafnyCheatsheet.pdf

method test()
{
  var i: int := 0;
  assert i == 0;
  assert i !in [2, 3];
  var u: bool := {1, 2, 3} !! {4, 5, 6};
  assert u == true;
  assert u is bool;
}


method test2()
{
  var s := seq(3, i requires 0 <= i <= 3 => i*i);
  assert s == [0, 1, 4];
  expect s == [], "s not []"; // halt at runtime if conditions is false
  print s;
}

method test3()
{
  var s1 := iset{1, 2, 3};
  print s1;
  var s2 := set x: nat | x < 3 :: x*x;
  assert 0*0 == 0;
  assert 1*1 == 1;
  assert 2*2 == 4;
  assert s2 == {0, 4, 1};
  print s2 == {4, 1, 0};
}

method test4()
{
  var hex := 0xABCD;
  assert hex + 1 == 0xABCE;
  assert 0x0001 == 1;
  assert 0xF == 15;
  assert 0xFF == 255;
}


function add(x: int, y: int): int
{
  x + y
}

method test5()
{
  var z := add(y:=6, x:=3);
  assert z == 9;
}

predicate P(x: int)

method testPredicates()
{
  label L: {
    var h := 5;
  }
  forall i | 0 <= i < 5 ensures P(i) {
    assume P(i);
  }
  assert P(0);
  assert P(4);
}

datatype R = R(int, int)

method testvar()
{
  var Z := var R(x, y) := R(2, 3); x+y;
  assert Z == 5;
}

method testasserts(x: int)
requires P(x) ==> x > 0
ensures assume P(x); x > 0
{}

method OpaquePrecondition(a: int, b: int) returns (r: int)
  requires a == 0
  requires b == 1
  ensures r > 0
{
  r := a + b;
  assert r == b;// by { reveal A; }
  assert b > 0;// by { reveal B; }
}

lemma Ex(x: nat)
requires Base: P(0)
requires IH: forall n: nat :: P(n) == P(n + 1)
ensures P(x)
decreases x
{
  if x == 0 {
    assert P(x) by { reveal Base; }
  } else {
    assert P(x - 1) by {
      reveal IH, Base;
      Ex(x - 1);
    }
    assert P(x) by {
      reveal IH;
    }
  }
}

lemma Ex2(x: nat)
requires Base: P(0)
requires Base2: P(1)
requires IH: forall n: nat :: P(n) == P(n + 2)
ensures P(x)
decreases x
{
  if x <= 1 {
    assert P(x) by { reveal Base, Base2; }
  } else {
    assert P(x - 2) by {
      reveal IH, Base, Base2;  
      Ex2(x - 2);
    }
    assert P(x) by {
      reveal IH;
    }
  }
}

lemma Ex3(x: nat)
requires P(0)
requires P(1)
requires forall n: nat :: P(n) == P(n + 2)
ensures P(x)
decreases x
{
  if x > 1 {
    Ex3(x - 2);
  }
}

function sum(l: nat, r: nat): nat
requires l <= r
decreases r - l
{
  if l == r then 
    if P(l) then 1 else 0
  else
    (if P(r) then 1 else 0) + sum(l, r-1)
}

lemma SumMonotonic(x: nat, y: nat)
requires x <= y
ensures sum(0, x) <= sum(0, y)
{}

lemma infOr(x: nat, dx: int)
requires 0 <= dx <= 1
requires forall n: nat :: P(n) || P(n + 1)
// ensures sum(0, 2*x) >= x
ensures sum(0, 2*x + dx) >= x + dx
decreases x
{
  if x == 0 {
    // assert sum(0, 1) >= 1;
  } else {
    infOr(x - 1, dx);
    // assert sum(0, 2*x-1) >= x by { infOr(x - 1); }
    // assert sum(0, 2*x+1) > sum(0, 2*x-1);
    // to help z3 we need to change sum definition to be recursive from right
  }
  // if P(0) {
  //   if P(1) {
  //     assert sum(0, 2) >= 2;
  //   } else {
  //     assert P(2);
  //     assert sum(0, 2) == 2;
  //   }
  // } else {
  //   assert P(1); // 01
  //   assert sum(0, 1) == 1;
  //   if P(2) {
  //     assert sum(0, 2) == 2;
  //   } else {
  //     assert sum(0, 2) == 1;
  //   }
  // }
}

// ghost
predicate {:opaque} myexists<T(!new)>(p: T -> bool)
{
  exists x:T :: p(x)
}

lemma Exists<T>(x0: T, p: T -> bool)
requires p(x0)
ensures myexists(p)
{
  reveal myexists();
}

method UseExists(x0: int)
{
  if P(x0) {
    Exists(x0, P);
    assert myexists(P);
  }
}

method Main()
{
  test3();
}