

method Modify(x: object, y: object)
requires x != y
modifies x, y
{
  modify x, y;
  assert x != y;
}

const P: bool
const Q: bool
const R: bool

method test()
ensures P <==> Q
ensures Q <==> R
{
  assume P ==> Q;
  assume Q ==> R;
  assume R ==> P;
}