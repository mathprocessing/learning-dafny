const time: int

datatype Weapon = Weapon(damage: nat, speed: nat, ammo: nat)

// Enemy weapon (infinite ammo)
datatype EWeapon = EWeapon(damage: nat, speed: nat)

// Guns (weakest weapon 3 damage, most ammo)
const gun := Weapon(3, 10, 1000)
// Hydra Rockets (medium weapon-25 damage, 45 ammo)
const hellfire := Weapon(25, 2, 45)
// Missiles (strongest weapon-100 damage, 8 ammo)
const missile := Weapon(100, 1, 8)

const soldiersgun := EWeapon(5, 2)

function abs(x: int): nat
{
  if x < 0 then -x else x
}

datatype Helicopter = Helicopter(weapons: seq<Weapon>, armor: nat, fuel: nat, lives: nat, load: nat)
{
  method move(t: int, d: int)
  requires time == t ==> this.fuel > d
  ensures time == t + 1 ==> this.fuel == 0
}

datatype LandUnit = LandUnit(weapon: EWeapon, armor: nat)

const Heli := Helicopter([gun, hellfire, missile], 600, 0, 3, 0)
const Soldier := LandUnit(soldiersgun, 10)

lemma maxFuel()
ensures Heli.fuel <= 100

method test()
{
  var h := Heli;
  assume time == 0 ==> h.fuel == 100;
  h.move(0, 1);
  assert time != 0;

  // var enemy := 
  // h.attack()
}