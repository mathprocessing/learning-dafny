Hello Mr. Leino.

Example above can be simplified:
```dafny
// Without some fix
method Main()
{
  assert exists a :: a == 0; // Fails
}

// With some fix
method Main()
{
  assert exists a :: a == 0; // Verified
}
```
I think that `exists` must work "equally" to `forall`: in all cases where forall works exists must work.

Perhaps this feature helps to view this problem of `exists` quantifier from another hand:

Possible feature:
if `assert` can return information symmetrical to `false/true` state:
This means that `assert expr;` can be simplified to `assert true;` or `assert false;` or simplification fails:
| Statement              | Message                     | Counterexamples |
| -                      | -                           | -               |
| `assert true;`         | no message                  | No              |
| `assert false;`        | `dafny proved assert !expr` | Yes             |
| `assert undertermined;`| `assert may not hold`       | Yes             |

This creates new problem: Dafny must check all asserts twice (1. for true 2. for false).
To reduce amount of computation in already existing projects (user can update Dafny and realize that time of proving in new version of Dafny is twice as long and it's leads to possible bad user experience) this feature requires adding another option to Dafny `/checkFalseCase` or something like that.

How this feature helps?
- This just about symmetries, logic states to which prover must reacts equally.
- Equally reaction for some thing in future can simplify behaviour (and testing) of whole system.
- And just to think about: How to prove `P1` that some proof `P2` is must have concrete set of properties `S`.
  Last question (may only for me) is really hard.

Also exist way working in Dafny without this feature:
```dafny
predicate P();

method test_proving_of_true()
{
  assert P();
}

method test_proving_of_false()
{
  assert !P();
}
```
But this leads to bad user experience and begger code base.

What do you think about it Mr. Leino?

Why it's not possible?
