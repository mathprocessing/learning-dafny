# Download and run examples
1. Clone repo
```
git clone https://codeberg.org/mathprocessing/learning-dafny
```
2. [Install dafny](https://github.com/dafny-lang/dafny/wiki/INSTALL)
* Download `dotnet` library if it is not in system path.
* Download dafny release and unzip to some folder.

3. Change settings of vscode
* `Ctrl + P` and enter `ext install dafny-lang.ide-vscode`
* In Settings `Ctrl + ,` search `Dafny` and
  + set `Compiler runtime path`: `${path-to-dafny}/dafny/Dafny.dll`
  + set `Dotnet Executable Path`: `${path-to-dotnet}/aspnetcore-runtime-6.0.5-win-x64/dotnet.exe`

# Dafny code examples
[Random examples](https://github.com/bor0/dafny-tutorial)

[Lambdas](https://github.com/dafny-lang/dafny/blob/master/Test/hofs/Lambda.dfy)

# Dafny docs
Html: 
  + [manual](https://dafny-lang.github.io/dafny/DafnyRef/DafnyRef.html#sec-reference-types)
  + [tutorial](https://dafny-lang.github.io/dafny/OnlineTutorial/guide)

Pdf manual:
  + See `DafnyRef.pdf` at https://github.com/dafny-lang/dafny/releases/download/v3.6.0/DafnyRef.pdf

Dafny options:
  + https://github.com/dafny-lang/dafny/blob/master/Source/Dafny/DafnyOptions.cs#L613

Dafny power user:
  + http://leino.science/dafny-power-user/

# Videos
[Install Dafny on Windows](https://www.youtube.com/watch?v=2qQzZjk4d4A)

[Lectures by Rustan Leino](https://github.com/dafny-lang/dafny#read-more)

# Other links
* [Links from dafny repo](https://github.com/dafny-lang/dafny#read-more)

* About isabelle Theorem Prover (to compare with dafny)
  + [Isabelle tutorial](https://isabelle.in.tum.de/doc/tutorial.pdf)
  + [Functional Data Structures with Isabelle/HOL, Tutorial 1: Natural Numbers and Lists](https://www.youtube.com/watch?v=VDwdFPrum0E)

* [Rigorous Software Development](https://cs.nyu.edu/~wies/teaching/rsd-13/)

* [VScode extension for Alloy tools](https://github.com/s-arash/VSCodeAlloyExtension)

* [Logic programming in Rust](https://github.com/s-arash/ascent)

* [Modeling, refinement, and verification](https://www.youtube.com/watch?v=fSWZWXx5ixc)

* [Natural number game](https://www.ma.imperial.ac.uk/~buzzard/xena/natural_number_game/)

* [Search trees in Coq](https://github.com/coq-contribs/search-trees)

* [Coq tricks](https://github.com/tchajed/coq-tricks)

# Toubleshooting
On Windows we have spawn of z3.exe (SAT-solver) on each execution of Verifier.
To prevent big memory consumption by many z3's we use can some little hacks:

```bat
@rem Wait a 5 seconds
ping 127.0.0.1 -n 6 > nul && echo "command"

@rem Kill all z3 processes
taskkill /IM z3.exe /F
```

To automate z3 killing we can use `Run on save` extension for vscode

Actual command that i used:
1. Change settings `Ctrl + ,` to like in this `json`:
```json
{
    "dafny.verificationTimeLimit": 6,
    "dafny.automaticVerification": "onsave"
}
```

```json
[
  "emeraldwalk.runonsave": {
    "commands": [
      {
        "match": "\\.dfy$",
        "cmd": "ping 127.0.0.1 -n 8 && taskkill /IM z3.exe /F"
      },
    ]
  }
]   
```

```bat
@rem in `127.0.0.1 -n 8` 8-1=7 must be greater than "dafny.verificationTimeLimit" to garantee not killing working z3.exe
ping 127.0.0.1 -n 8 && taskkill /IM z3.exe /F
```

**This solution have drawbacks**: you can't save file more often than "dafny.verificationTimeLimit" seconds.

TODO: use python to create process list of z3's and calculate time of process execution.
This must periodically kill only processes with `time of execution > dafny.verificationTimeLimit`.

# Ideas

## About interfaces of proofs

Properties of **minimal** interfaces:
* `forall I1, I2, ~include I1 I2`
  Example:
  state `I1 = {A1, A2, A3} /\ I2 = {A1, A2}` not possible because
  we can just select `proof2` as better and ignore `proof1`.

If we have two interfaces (interface(Proof) = set of lemmas that proof contains)
of same lemma `L1`:
* `I(P1) = {A1, A2, A3}`
* `I(P2) = {A3, A4, A5}`
If some axiom is dissapeared (may be after generalization of object)
then we have cases:
* `deleted A1 or A2 -> select I2`
* `deleted A4 or A5 -> select I1`
* `deleted A3 -> we can't use old lemma`

Properties of interfaces (updated):
* `forall I1, I2, ~include I1 I2`
* `if axiom in intersection(I1, I2) then we can't delete it`

Interesting fact:
State when I1 = I2 almost equal is 
* Example: `I1 = {A1, A2, A3} /\ I2 = {A2, A3, A4}`,
i.e. we always have at least 2 possibilities to delete axiom
(in example `A1` or `A4`) and we can't do this simultaniously:
(inc example `delete A1 and A2`).