class C {
  var f: int
  var x: int
  method Example() returns (b: bool)
  {
    var x: int;
    // return f == this.f; <=>
    b := f == this.f;
    assert b;
    // assert x != this.x; // may not hold
    // assert x == this.x; // may not hold
    assert 1 == 1;
    assert 1 != 2;
  }
}

// Create instances
// method m()
// {
//   var c := C.Init();
// }
