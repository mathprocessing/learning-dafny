// type Point = (real, real)
// type ThreePoints = s: set<Point> | |s| == 3 witness {(0.0, 0.0), (0.0, 1.0), (1.0, 0.0)}
type pos = x:real | x >= 0.0 witness 0.0

// real addition
function {:opaque} radd(x: real, y: real): real {
  x + y
}

datatype Point = Point(x: real, y: real) {
  const sqlen := this.x * this.x + this.y * this.y

  function add (p: Point): Point {
    Point(this.x + p.x, this.y + p.y)
  }

  // function add (p: Point): Point {
  //   Point(radd(this.x, p.x), radd(this.y, p.y))
  // }

  function sub(p: Point): Point {
    Point(this.x - p.x, this.y - p.y)
  }

  function mul(value: real): Point {
    Point(this.x * value, this.y * value)
  }
}
const zero := 0.0
const origin := Point(0.0, 0.0)

function sq(x: real): real { x * x }

datatype Circle = Circle(center: Point, radius: pos) {
  predicate hasPoint(p: Point) {
    p.sub(center).sqlen <= radius
  }
}

lemma originAdd(p: Point)
ensures origin.add(p) == p
{ reveal radd(); }

lemma addOrigin(p: Point)
ensures p.add(origin) == p
{ reveal radd(); }

const c' := Circle(origin, 1.0)

predicate {:opaque} onGrid(c: Circle, p: Point) {
  c.center == p 
  || c.center.add(Point(zero, c.radius)) == p
  || c.center.add(Point(zero, -(c.radius as real))) == p
  || c.center.add(Point(c.radius, zero)) == p
  || c.center.add(Point(-(c.radius as real), zero)) == p
}

const p': Point

lemma hasAxis(c: Circle, p: Point)
requires onGrid(c, p)
ensures c.hasPoint(p)

lemma onGridhelper()
// ensures var p2 := Point(0.0, 1.0); onGrid(c', p2)
{
  assert c'.center == origin;
  // assert c'.center.add(Point(0.0, 1.0)) == Point(0.0, 1.0);
  var p2 := Point(0.0, 1.0);
  assert origin.add(p2) == p2 by {
    originAdd(p2);
  }
  // z3 Works very long with that assert
  // assert origin.add(Point(0.0, 1.0)) == Point(0.0, 1.0) by {
  //   originAdd(Point(0.0, 1.0));
  // }
}

lemma testCenter()
ensures onGrid(c', origin)
{
  reveal onGrid();
}

lemma testCenter2()
ensures onGrid(c', Point(0.0, 0.0))
{
  reveal onGrid();
}

lemma raddComm()
ensures forall a,b:real {:trigger radd(a, b)} :: radd(a, b) == radd(b, a)

lemma raddZero()
ensures forall a:real :: radd(a, 0.0) == a

// lemma testRight()
// ensures onGrid(c', Point(c'.radius, 0.0))
// {
//   reveal onGrid();
//   assert c'.center == origin;
//   assert origin.add(Point(c'.radius, 0.0)) == Point(c'.radius, 0.0) by {
//     // We shrink all lemmas about real addition to only two and z3 can prove this assert at reasonable time
//     raddComm();
//     raddZero();
//     // assert radd(1.0, 0.0) == 1.0 by { raddZero(); }
//     // assert radd(0.0, 0.0) == 0.0 by { raddZero(); }
//   }
//   // assert c'.center.add(Point(c.radius, zero)) == p by {
//   //   reveal onGrid();
//   // }
  
// }

lemma testRight_automated()
ensures onGrid(c', Point(1.0, 0.0))
{
  reveal onGrid();
  // raddComm();
  // raddZero();
}

method notWorks()
{
  // Warning: Dafny loops on this
  // assert onGrid(c', Point(0.0, 1.0)) by {
  //   reveal onGrid();
  // }
}

// method test() {
//   assert origin.sqlen == zero;
//   assert c'.hasPoint(origin);
//   assert c'.hasPoint(p') by {
//     assert onGrid(c', p') by {
//       assume p' == Point(1.0, zero);
//       reveal onGrid();
//     }
//     hasAxis(c', p');
//   }
// }