// type Point = (real, real)
// type ThreePoints = s: set<Point> | |s| == 3 witness {(0.0, 0.0), (0.0, 1.0), (1.0, 0.0)}
type pos = x:real | x >= 0.0 witness 0.0

// real addition
function {:opaque} radd(x: real, y: real): real {
  x + y
}

// real multiplication
function {:opaque} rmul(x: real, y: real): real {
  x * y
}

datatype Point = Point(x: real, y: real) {
  const sqlen := this.x * this.x + this.y * this.y

  // function {:fuel 0} add (p: Point): Point {
  //   Point(this.x + p.x, this.y + p.y)
  // }

  function add (p: Point): Point {
    Point(radd(this.x, p.x), radd(this.y, p.y))
  }

  function sub(p: Point): Point {
    Point(radd(this.x, -p.x), radd(this.y, -p.y))
  }

  function mul(value: real): Point {
    Point(rmul(this.x, value), rmul(this.y, value))
  }
}
const zero := 0.0
const origin := Point(0.0, 0.0)

function sq(x: real): pos { rmulSelfPositive(); rmul(x, x) }

lemma rmulSelfPositive()
ensures forall x :: rmul(x, x) >= 0.0

function sqrt(x: pos): (res: pos)
ensures sq(res) == x

lemma sqZeroOne()
ensures sq(0.0) == 0.0 && sq(1.0) == sq(-1.0) == 1.0
{ reveal rmul(); }

lemma sqMonotonic(x: pos, y: pos)
ensures x < y ==> sq(x) < sq(y)
// {
//   reveal rmul();
// }

lemma sqZeroSolution(x: pos)
ensures sq(x) == 0.0 ==> x == 0.0
{
  if x > 0.0 {
    assert sq(x) > 0.0 by {
      assert sq(0.0) < sq(x) by { sqMonotonic(0.0, x); }
      assert sq(0.0) == 0.0 by { sqZeroOne(); }
    }
  }
}

lemma sqBijective(x: pos, y: pos)
ensures sq(x) == y ==> forall k | k != x :: sq(k) != y

lemma sqBijective2(k: pos)
ensures sqrt(sq(k)) == k;

lemma sqrtZero() // proof 1
ensures sqrt(0.0) == 0.0
{
  forall x:pos ensures sq(x) == 0.0 ==> x == 0.0 {
    sqZeroSolution(x);
  }
}

lemma sqrtZero_proof2()
ensures sqrt(0.0) == 0.0
{
  forall x:pos ensures sq(x) == 0.0 ==> x == 0.0 {    
    if sq(x) == 0.0 {
      if x > 0.0 {
        assert H1: sq(0.0) != 0.0 by {
          assert forall k | k != x :: sq(k) != 0.0 by {
            sqBijective(x, 0.0);
          }
        }
        assert H2: sq(0.0) == 0.0 by { sqZeroOne(); }
        assert false by {
          reveal H1, H2;
        }
      }
    }
  }
  // assert rmul(0.0, 0.0) == 0.0 by { reveal rmul(); }
  // assert sq(0.0) == 0.0;
  // assert sq(sqrt(0.0)) == 0.0;
}

lemma sqUnique(x: pos, y: pos) 
ensures sq(x) == y ==> sqrt(y) == x
{
  // We already have that sqrt is a bijective by
  // `sq(sqrt(x)) == x`
  // To prove that `sq` is bijective we use mirrored combination:
  // `sqrt(sq(x)) == x`
  // assume forall k: pos :: sqrt(sq(k)) == k;
  calc {
    sq(x) == y;
    <==>
    sqrt(sq(x)) == sqrt(y);
    <==> { assert sqrt(sq(x)) == x by { sqBijective2(x); } }
    x == sqrt(y);
  }
}

lemma sqrtMonotonic(a: pos, b: pos)
ensures a <= b ==> sqrt(a) <= sqrt(b)

lemma {:induction false} sqrtUnique(a: pos, b: pos)
ensures a != b ==> sqrt(a) != sqrt(b)
{
  // Why it proves automatically?
  // assert false; fails -> smoke test ok
  // It proves because:
  assert sq(sqrt(a)) == a;
  assert sq(sqrt(b)) == b;
  // Means that sqrt is a Bijective function
  // Also we can add bijection to `sq`:
  // assume forall x :: sqrt(sq(x)) == x;
}

lemma sqrtLtMonotonic(a: pos, b: pos)
ensures a < b ==> sqrt(a) < sqrt(b)
{
  if a < b {
    assert sqrt(a) <= sqrt(b) by { sqrtMonotonic(a, b); }
  }
}

lemma sqrtTwoBounds()
ensures 1.0 <= sqrt(2.0) <= 2.0
{
  assert sq(1.0) == 1.0 by { sqZeroOne(); }
  assert sq(2.0) == 4.0 by { reveal rmul(); }
  assert H1: sqrt(4.0) == 2.0 by { sqUnique(2.0, 4.0); }
  assert H2: sqrt(1.0) == 1.0 by { sqUnique(1.0, 1.0); }
  assert sqrt(1.0) <= sqrt(2.0) <= sqrt(4.0) by {
    sqrtMonotonic(1.0, 2.0);
    sqrtMonotonic(2.0, 4.0);
  }
  reveal H1, H2;
}

datatype Circle = Circle(center: Point, radius: pos) {
  predicate hasPoint(p: Point) {
    p.sub(center).sqlen <= radius
  }
}

lemma originAdd(p: Point)
ensures origin.add(p) == p
{ reveal radd(); }

lemma addOrigin(p: Point)
ensures p.add(origin) == p
{ reveal radd(); }

const c' := Circle(origin, 1.0)

predicate {:opaque} onGrid(c: Circle, p: Point) {
  c.center == p 
  || c.center.add(Point(zero, c.radius)) == p
  || c.center.add(Point(zero, -(c.radius as real))) == p
  || c.center.add(Point(c.radius, zero)) == p
  || c.center.add(Point(-(c.radius as real), zero)) == p
}

const p': Point

lemma hasAxis(c: Circle, p: Point)
requires onGrid(c, p)
ensures c.hasPoint(p)

lemma testCenter()
ensures onGrid(c', origin)
{
  reveal onGrid();
}

lemma testCenter2()
ensures onGrid(c', Point(0.0, 0.0))
{
  reveal onGrid();
}

lemma raddComm()
ensures forall a,b:real {:trigger radd(a, b)} :: radd(a, b) == radd(b, a)

lemma raddZero()
ensures forall a:real :: radd(a, 0.0) == a

lemma testRight()
ensures onGrid(c', Point(c'.radius, 0.0))
{
  reveal onGrid();
  assert c'.center == origin;
  assert origin.add(Point(c'.radius, 0.0)) == Point(c'.radius, 0.0) by {
    // We shrink all lemmas about real addition to only two and z3 can prove this assert at reasonable time
    raddComm();
    raddZero();
    // assert radd(1.0, 0.0) == 1.0 by { raddZero(); }
    // assert radd(0.0, 0.0) == 0.0 by { raddZero(); }
  }
  // assert c'.center.add(Point(c.radius, zero)) == p by {
  //   reveal onGrid();
  // }
  
}

lemma testRight_automated()
ensures onGrid(c', Point(1.0, 0.0))
{
  reveal onGrid();
  raddComm();
  raddZero();
}
