type Prob = p: real | 0.0 <= p <= 1.0

lemma mul_bounds(a: real, b: real, x: real, y: real)
requires a >= 0.0
requires b >= 0.0
requires 0.0 <= x <= a
requires 0.0 <= y <= b
ensures x * y <= a * b
{}

lemma and_correct(x: Prob, y: Prob)
ensures 0.0 <= x * y <= 1.0
{
  assert x * y >= 0.0;
  assert x * y <= 1.0 * 1.0 by {
    mul_bounds(1.0, 1.0, x, y);
  }
}

function and(x: Prob, y: Prob): Prob
{
  and_correct(x, y);
  x * y
}

lemma or_correct(x: Prob, y: Prob)
ensures 0.0 <= x + y - x * y <= 1.0
{
  assert x + y - x * y <= 1.0 by {
    // calc {
    //   x + y - x * y <= 1.0;
    //   <==>
    //   x + (1.0 - x) * y <= 1.0;
    //   <==>
    //   (1.0 - x) * y <= 1.0 - x;
    //   <==>
    //   x + (1.0 - x) * y <= 1.0;
    // }
  }
  assert x + y - x * y >= 0.0 by {
    assert (1.0 - x) * y >= 0.0;
    calc {
      x + y - x * y >= 0.0;
      <==>
      x + (1.0 - x) * y >= 0.0;
    }
  }
}

function or(x: Prob, y: Prob): Prob
{
  or_correct(x, y);
  x + y - x * y
}

// PROBLEM BEGIN
// Problem: asymmetry in proof possibilities:
//  If we just copy all body if `xor_correct` to xor definition then we see that proof is works.
// 
// Expected behaviour: we can safely think that this copy operations must not change z3 behaviour,
//   because of it's `independence`
lemma {:verify false} xor_correct_fail_of_same_proof(x: Prob, y: Prob)
ensures 0.0 <= x + y - 2.0 * x * y <= 1.0
{
  assert 0.0 <= x + y - 1.0 * x * y <= 1.0 by { or_correct(x, y); }
  assert 0.0 <= x + y - 2.0 * x * y <= 1.0 by {
    if {
      // Proof version 1
      case true => {
        calc {
          x + y - 2.0 * x * y;
          ==
          and(x, 1.0 - y) + and(1.0 - x, y);
        }
      }
      // Proof version 2.1
      // case true => { // Idea just rewrite as `and` and `or`
      //   calc {
      //     x + y - 2.0 * x * y;
      //     ==
      //     or(x, y) - and(x, y); // Don't works :(
      //   }
      // }
      // Proof version 2.2
      case true => { // Idea same as in proof version 2.1
        calc {
          x + y - 2.0 * x * y;
          ==
          x + y - 2.0 * and(x, y); // Works!
        }    
      }
    }
  }
}

function xor(x: Prob, y: Prob): Prob
{
  // xor_correct(x, y);
assert 0.0 <= x + y - 1.0 * x * y <= 1.0 by { or_correct(x, y); }
  assert 0.0 <= x + y - 2.0 * x * y <= 1.0 by {
    if {
      // Proof version 1
      case true => {
        calc {
          x + y - 2.0 * x * y;
          ==
          and(x, 1.0 - y) + and(1.0 - x, y);
        }
      }
      // Proof version 2.1
      // case true => { // Idea just rewrite as `and` and `or`
      //   calc {
      //     x + y - 2.0 * x * y;
      //     ==
      //     or(x, y) - and(x, y); // Don't works :(
      //   }
      // }
      // Proof version 2.2
      case true => { // Idea same as in proof version 2.1
        calc {
          x + y - 2.0 * x * y;
          ==
          x + y - 2.0 * and(x, y); // Works!
        }    
      }
    }
  }
  x + y - 2.0 * x * y
}

// PROBLEM END

function not(x: Prob): Prob
{
  1.0 - x
}

lemma not_not_is_id(x: Prob)
ensures not(not(x)) == x
{}

lemma xor_def_and_plus_not(x: Prob, y: Prob)
ensures xor(x, y) == and(x, not(y)) + and(not(x), y)
{}

// WISH:
/**
state:
Don't know true or false:
B1: xor(x, y) <= or(and(x, not(y)), and(not(x), y))
B2: xor(x, y) == or(and(x, not(y)), and(not(x), y))
B3: xor(x, y) >= or(and(x, not(y)), and(not(x), y))

What branch have more/less probebility to be true?

For example result might be:
  p(B1) not comparable with p(B2)
  p(B2) <= p(B3)

Then we focusing on hypothesis set {B1, B3} i.e. ignore B2.

Or more short task(user state) description:
xor(x, y) `order` or(and(x, not(y)), and(not(x), y))
  where `order` == {<=, ==, >=}

or we can parametrize it:
  order(3) == {<=, ==, >=}
  order(6) == {<=, ==, >=, ???, ???, ???}
  ...

  because if some `easy to spot` solution exists, it construction must don't take much energy (number of construction steps)
*/
// Add some const just for typecheking (for reduce errors)
// This is holds: compare_func in {<=, ==, >=}
const compare_func: (Prob, Prob) -> bool

function le(x: Prob, y: Prob): bool { x <= y }
function ge(x: Prob, y: Prob): bool { x >= y }
function eq(x: Prob, y: Prob): bool { x == y }

lemma compare_func_def()
ensures compare_func == le || compare_func == ge || compare_func == eq

method learn_about_space_of_possible_rewrites_for_xor_def_and_or_not(x: Prob, y: Prob)
ensures compare_func == ge // added from comments near `xor_def_and_or_not`
{
  // We need `Prob` type because otherwise `error in line `x' + y' - x' * y'`:
  //   "result of operation might violate subset type constraint for 'Prob'"
  // WISH: automatically infer `Prob` (Why default infer not works? 
  // Or we want to use rule "save maximum information about type")
  //   because changing Prob to int looses some information
  // General rule WISH: Do not loose any information by default, but if user specify particular knowledge (and possible REASON (also can have formal proof)) by some keywords then information loosing allowed.
  ghost var x': Prob := and(x, not(y));
  ghost var y': Prob := and(not(x), y);
  calc {
    or(and(x, not(y)), and(not(x), y));
    == // rewrite def of Or,  WISH: how to rewrite automatically like in Coq?
    // Try to manually reduce complexity of problem: we can create ghost variables `x'` and ``y'
    or(x', y');
    == // or(x, y) == x + y - x * y
    x' + y' - x' * y';
    == // target: find sequence of rewrites that achieve `expression state == Expr(xor(x, y))` or prove that it not possible
    // questions: can we simplify `x' + y'`? `x' * y'`? ||| High order rule: If we think about func1(x', y') then we must think about func2(x', y').
    {
      // simplify x' -> we can't simplify it because ...
      // simplify x' + y' -> result x' + y' == xor(x, y)
      calc {
        x' + y';
        ==
        and(x, not(y)) + and(not(x), y);
        ==
        x * (1.0 - y) + (1.0 - x) * y;
        ==
        x + y - 2.0 * x * y;
        ==
        xor(x, y);
      }
    }
    xor(x, y) - x' * y';
  }
  // Now we reformulate undeteministic goal
  ghost var goal_before := compare_func(xor(x, y), or(x', y'));
  ghost var goal := compare_func(xor(x, y), xor(x, y) - and(x', y'));
  assert goal_before == goal; // Very cool that this meta-knowledge (thinking about goals) can be reformulated as assert, i.e. if we change or(x', y') to or(x', x') then assert not holds  
  // Then we can write out all branches!
  // But proof is right IF EXACT {1, 2, 3} BRANCHES CONTAINS `assert goal`
  // i.e. 0 branches not possible
  // But how to do(make it checkable, typecheckable and so on) this STRANGE_ANY_CASE in Dafny?
  // just simple scheme: STRANGE_ANY_CASE ~ exists, `usual if case` ~ forall

  compare_func_def(); // To help dafny understand that all cases covered
  if {
    // B1: xor(x, y) <= or(and(x, not(y)), and(not(x), y))
    case compare_func == le => {
      // In that branch `goal` can't be proved. But how to check it in Dafny?
      // We must demonstrate counterexample!
      assert goal <==> xor(x, y) <= xor(x, y) - and(x', y');
      // or assert goal.expr_eq(compare_func(xor(x, y), xor(x, y) - and(x', y')));

      assume false; // Bad fix because we don't have STRANGE_ANY_CASE in Dafny
    }
    // B2: xor(x, y) == or(and(x, not(y)), and(not(x), y))
    case compare_func == eq => {
      // In that branch `goal` can't be proved. But how to check it in Dafny?
      // We must demonstrate counterexample!
      // Bonus problem (just for fun): How to check that some comments are exact copies of each other?

      assert goal <==> xor(x, y) == xor(x, y) - and(x', y');
      // To find counterexample: We must find some particular case when `assert goal` not holds
      if (x', y') == (0.0, 0.0) || (x', y') == (1.0, 1.0) { // this can't be counterexamples (for positive case `assert goal`)
        assert goal; // But this is a counterexample for negative case `assert !goal`
      }
      if (x, y) == (0.0, 0.0) || (x, y) == (1.0, 1.0) { // also can't
        assert goal; // This one also a counterexample for negative case `assert !goal`
      }

      if (x, y) == (0.5, 0.5) {
        // assert goal; // Assert not holds but how to show that some expr can be either true or false?
        // Maybe answer: Give two counterexamples one for positive case and one for negative.
        assert !goal;
      }

      // Note: example for `assert goal` <=> counterexample for `assert !goal` and vise versa.
      // In this line we must guarentee that we at least one example of `assert goal` and at least one example of `assert !goal` exist in code above.
      // assert exists x, y :: goal_func(x, y);
      // assert exists x, y :: !goal_func(x, y);

      assume false; // Bad fix because we don't have STRANGE_ANY_CASE in Dafny
    }
    // B3: xor(x, y) >= or(and(x, not(y)), and(not(x), y))
    case compare_func == ge => {
      // Goal have proof:
      assert goal by {
        assert xor(x, y) >= xor(x, y) - and(x', y');
      }
    }
  }
}

// Second try to `learn_about_space_of_possible_rewrites_for_xor_def_and_or_not`

predicate goal_pred(x: Prob, y: Prob) {
  compare_func(xor(x, y), or(and(x, not(y)), and(not(x), y)))
}



// from tries above we have that compare_func must be in set {ge}
// But how to prove this formally? Let's add ensures clause: `ensures compare_func == ge` to lemma
// `learn_about_space_of_possible_rewrites_for_xor_def_and_or_not`

lemma xor_def_and_or_not(x: Prob, y: Prob)

ensures xor(x, y) <= or(and(x, not(y)), and(not(x), y))
{

  xor_def_and_plus_not(x, y);
  ghost var left := and(x, not(y));
  ghost var right := and(not(x), y);
  // assert and(and(x, not(y)), and(not(x), y)) == 0.0 by {
  //   if x == 0.5 {
  //     assert and(and(0.5, 1.0 - y), and(0.5, y)) == 0.0;
  //     assert 0.5 * (1.0 - y) * 0.5 * y == 0.0;
  //     assert 0.25 * (1.0 - y) * y == 0.0;
  //   }
  // }
}
