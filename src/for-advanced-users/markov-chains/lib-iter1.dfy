include "./probability-def.dfy"

datatype Node = Node(p: Prob)
{
  
  // Defines P of transition from `this` node to node `n`
  function trans(n: Node): Prob

  // Defines node that corresponds to current node `this` in next tick (time step)
  function next(): Node
}

method test1()
{
  // Setup Initial state
  // StartState == {A: 1.0, B: 0.0, C: 0.0}
  var a := Node(1.0); // Initially we have 1.0 probability that we in state `a`
  var b := Node(0.0);
  var c := Node(0.0);

  // Setup transition model
  
  assume a.trans(a) == 0.1;
  assume a.trans(b) == 0.2;
  assert a.trans(c) != 0.7;
  assert false; // Wtf??? ==> we can use ` Node(p: Prob)`, try change it to `Node { const p: Prob }`
  assert a.trans(a) + a.trans(b) + a.trans(c) == 1.0; // Check normality of distribution

  // Test
  // State.next == {A: 0.1, B: 0.2, C: 0.7}
  assert a.next().p == 0.1;
  assert b.next().p == 0.2;
  assert c.next().p == 0.7;
}