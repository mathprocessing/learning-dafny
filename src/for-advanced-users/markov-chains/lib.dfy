include "./probability-def.dfy"

// datatype Node = Node(id: nat)
// {
//   const p: Prob
  
//   // Defines P of transition from `this` node to node `n`
//   function trans(n: Node): Prob

//   // Defines node that corresponds to current node `this` in next tick (time step)
//   function next(): Node
// }

datatype Dist = Dist(data: seq<Prob>)
{
  // Defines P of transition from node with index `i` to node with index `j`
  function trans(i: nat, j: nat): Prob
}

// Define logic constraint on elements of type
type PDist = x: Dist | sum(x.data) == 1.0 witness Dist([1.0])

method test1()
{
  // Setup Initial state
  // StartState == {A: 1.0, B: 0.0, C: 0.0}
  var s: PDist := Dist([1.0, 0.0, 0.0]); // Initially we have 1.0 probability that we in state `a`

  // Setup transition model
  var a := 0; var b := 1; var c := 2;

  assume s.trans(a, a) == 0.1;
  assume s.trans(a, b) == 0.2;
  assume s.trans(a, c) == 0.7;
  // Simple id's
  assume s.trans(b, a) == 0.0;
  assume s.trans(b, b) == 1.0;
  assume s.trans(b, c) == 0.0;
  assume s.trans(c, a) == 0.0;
  assume s.trans(c, b) == 0.0;
  assume s.trans(c, c) == 1.0;

  // Check normality of distribution
  assert s.trans(a, a) + s.trans(a, b) + s.trans(a, c) == 1.0;
  assert s.trans(b, a) + s.trans(b, b) + s.trans(b, c) == 1.0;
  assert s.trans(c, a) + s.trans(c, b) + s.trans(c, c) == 1.0;
  // Setup Transition matrix
//   // * Transition rule
//   assert Trule: forall n: Node :: n.next().p == a.p * a.trans(n) + b.p * b.trans(n) + c.p * c.trans(n) by { assume false; }
//   assert TruleNext: forall n: Node :: n.next().p == a.p * a.trans(n) + b.p * b.trans(n) + c.p * c.trans(n) by { assume false; }
//   // * Transition function is constant over time
//   assert Tconst: forall n: Node, m: Node :: n.next().trans(m.next()) == n.trans(m) by { assume false; }
//   // Trans(a, a); Trans(b, a); Trans(c, a);
//   // Trans(a, b); Trans(b, b); Trans(c, b);
//   // Trans(a, c); Trans(b, c); Trans(c, c);

//   // Test
//   // State.next == {A: 0.1, B: 0.2, C: 0.7}
//   assert a.next().p == 0.1 by {
//     calc == {
//       a.next().p;
//         { reveal Trule; }
//       a.p * a.trans(a) + b.p * b.trans(a) + c.p * c.trans(a);
//       a.p * 0.1 + b.p * 0.0 + c.p * 0.0;
//       0.1;
//     }
//   }
//   assert b.next().p == 0.2 by { reveal Trule; }
//   assert c.next().p == 0.7 by { reveal Trule; }
//   // assert false;

//   // Checking second state
//   assert a.next().next().p == 0.01 by {
//     var n := a.next();
//     calc == {
//       n.next().p;
//       n.p * n.trans(n) + b.next().p * b.next().trans(n) + c.next().p * c.next().trans(n);
//       //                 ^^^ Now we have a problem: We want to replace `b` with something like State.next().b
//       a.next().p * a.next().trans(n) + b.next().p * b.next().trans(n) + c.next().p * c.next().trans(n);
//       0.1 * a.next().trans(n) + 0.2 * b.next().trans(n) + 0.7 * c.next().trans(n);
//       { reveal Tconst; }
//       0.1 * a.trans(a) + 0.2 * b.trans(a) + 0.7 * c.trans(a);
//       0.1 * 0.1 + 0.2 * 0.0 + 0.7 * 0.0;
//       0.01;
//     }
//   }
//   // assert b.next().next().p == 0.2;
//   // assert c.next().next().p == 0.7;
}