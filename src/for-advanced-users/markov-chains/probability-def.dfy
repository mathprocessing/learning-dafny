type Prob = p: real | 0.0 <= p <= 1.0

function {:verify false} sum(s: seq<real>): real
{
  if |s| == 0 then 0.0
  else s[0] + sum(s[1..])
}
