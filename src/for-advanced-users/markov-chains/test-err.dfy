include "./lib.dfy"

method test1()
{
  // Setup Initial state
  // StartState == {A: 1.0, B: 0.0, C: 0.0}
  var a := Node(0);
  var b := Node(1);
  var c := Node(2);
  assume a.p == 1.0; // Initially we have 1.0 probability that we in state `a`

  // Setup transition model
  
  assume a.trans(a) == 0.1;
  assume a.trans(b) == 0.2;
  assert a.trans(c) != 0.7;
  assert false; // Wtf??? ==> we can use ` Node(p: Prob)`, try change it to `Node { const p: Prob }`
  assert a.trans(a) + a.trans(b) + a.trans(c) == 1.0; // Check normality of distribution

  // Test
  // State.next == {A: 0.1, B: 0.2, C: 0.7}
  assert a.next().p == 0.1;
  assert b.next().p == 0.2;
  assert c.next().p == 0.7;
}