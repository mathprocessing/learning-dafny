include "./lib.dfy"
/* (x.re + j * x.im) * (y.re + j * y.im) = 
  x.re     * y.re
+ j * x.im * y.re
+ x.re     * j * y.im
+ j * x.im * j * y.im

= (x.re * y.re - x.im * y.im) + (x.re * y.im + x.im * y.re) * j
*/

method test1(x: int, y: int)
{
  assert mul(Re(x), Re(y)) == Re(x * y);
}

method test2(x: int, y: int)
{
  // Complex(x.re * y.re - x.im * y.im, x.re * y.im + x.im * y.re)
  assert mul(Im(x), Im(y)) == Re(-x * y);
}

method testLinearDeformation(c: cpx, L: int)
{
  assert mul(c, Complex(L, 0)) == Complex(c.re * L, c.im * L);
}

method testSquare(x: cpx)
{
  assert mul(x, x) == Complex(x.re * x.re - x.im * x.im, 2 * x.re * x.im);
}

method testOneIm()
{
  assert mul(I, I) == Re(-1);
}