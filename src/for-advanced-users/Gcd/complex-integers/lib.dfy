/* Complex integers.
Think about Gaussian primes:
https://oeis.org/wiki/Gaussian_primes

Lucas numbers:
http://oeis.org/A000032
Also the number of independent vertex sets and vertex covers for the cycle graph C_n for n >= 2. - Eric W. Weisstein, Jan 04 2014

Also the number of matchings in the n-cycle graph C_n for n >= 3. - Eric W. Weisstein, Oct 01 2017

Also the number of maximal independent vertex sets (and maximal vertex covers) for the n-helm graph for n >= 3. - Eric W. Weisstein, May 27 2017

Also the number of maximal independent vertex sets (and maximal vertex covers) for the n-sunlet graph for n >= 3. - Eric W. Weisstein, Aug 07 2017 

Example of constraints on complex functions:
* https://en.wikipedia.org/wiki/Schottky%27s_theorem
 */

// Complex integer number (Gaussian number)
datatype cpx = Complex(re: int, im: int)

// fast constructors
function Re(x: int): cpx { Complex(x, 0) }
function Im(x: int): cpx { Complex(0, x) }

const ZERO := Complex(0, 0)
const I := Complex(0, 1)

// How to check that one number is divider of another?
// Let's define multiplication on `complex integers`
function mul(x: cpx, y: cpx): cpx {
  Complex(x.re * y.re - x.im * y.im, x.re * y.im + x.im * y.re)
}

function add(x: cpx, y: cpx): cpx {
  Complex(x.re + y.re, x.im + y.im)
}

lemma {:verify false} AddComm(x: cpx, y: cpx)
ensures add(x, y) == add(y, x)
{}

lemma {:verify false} MulComm(x: cpx, y: cpx)
ensures mul(x, y) == mul(y, x)
{}

lemma {:verify false} AddAssoc(x: cpx, y: cpx, z: cpx)
ensures add(add(x, y), z) == add(x, add(y, z))
{}

lemma {:verify false} MulAssoc(x: cpx, y: cpx, z: cpx)
ensures mul(mul(x, y), z) == mul(x, mul(y, z))
{}

lemma {:verify false} MulOneRe(x: cpx)
ensures mul(x, Complex(1, 0)) == x
{}

lemma {:verify false} MulOneIm(x: cpx)
ensures mul(x, Complex(0, 1)) == Complex(-x.im, x.re)
{}

predicate divisor(d: cpx, n: cpx) {
  exists c: cpx :: mul(c, d) == n
}

lemma ZeroIsNotDivisor(n: cpx)
requires n != ZERO
ensures !divisor(ZERO, n)
{
  assert 0 * 0 == 0;
}

lemma ZeroDivisorSelf()
ensures divisor(ZERO, ZERO)
{
  assert 0 * 0 == 0; // This helps to reduce time of checking
  assert mul(ZERO, ZERO) == ZERO by {
    // Why this simple substitution (3 steps) not works without this assert?
    // Because: we need 0 * 0 == 0 hint, but why z3 can't figure out about so simple hint?
    // assert mul(Complex(0, 0), Complex(0, 0)) == Complex(0, 0);
  }
}
