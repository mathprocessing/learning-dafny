include "./seq_basic.dfy"
const Q := [0];

function mtaux(n: nat, xs: bits): bits
{
  if n == 0 then xs
  else mtaux(n-1, xs) + inv(mtaux(n-1, xs))
}

function mt(n: nat): bits {
  mtaux(n, Q)
}

lemma {:induction false} MtLength(n: nat)
ensures |mt(n)| == pow(2, n)
decreases n
{
  if n > 0 {
    assert |mt(n-1)| == pow(2, n-1) by {
      MtLength(n - 1);
    }
    calc {
      |mt(n-1)| == pow(2, n-1);
      <==>
      2 * |mt(n-1)| == 2 * pow(2, n-1);
      <==> { 
              assert |inv(mt(n-1))| == |mt(n-1)|; 
            }
      |mt(n-1)| + |inv(mt(n-1))| == 2 * pow(2, n-1);
      <==>
      |mt(n-1) + inv(mt(n-1))| == 2 * pow(2, n-1);
      <==> { 
              assert mt(n) == mt(n-1) + inv(mt(n-1));
            }
      |mt(n)| == pow(2, n);
    }
  }
}

lemma Sumbits(n: nat)
requires n > 0
ensures 2 * sum(mt(n)) == |mt(n)|
decreases n
{
  if n == 1 {} else {
    // calc {
    //   2 * sum(mt(n)) == |mt(n)|;
    //   <==>
    //   sum(mt(n)) == |mt(n-1)|;
    // }
    assert |mt(n-2)| == sum(mt(n-1)) by {
      Sumbits(n-1);
    }
    assert Goal: sum(mt(n)) == 2 * sum(mt(n-1)) by {
      calc {
        sum(mt(n)) == 2 * sum(mt(n-1));
        <==>
        sum(mt(n-1) + inv(mt(n-1))) == 2 * sum(mt(n-1));
        <==> { sumOfConcat(mt(n-1), inv(mt(n-1))); }
        sum(mt(n-1)) + sum(inv(mt(n-1))) == 2 * sum(mt(n-1));
        <==> { SumOfInv(mt(n-1)); }
        sum(mt(n-1)) + |mt(n-1)| - sum(mt(n-1)) == 2 * sum(mt(n-1));
        <==>
        |mt(n-1)| == 2 * sum(mt(n-1));
        <==> assert |mt(n-1)| == 2 * |mt(n-2)| by { MtLength(n-1); }
        2 * |mt(n-2)| == 2 * sum(mt(n-1));
        <==>
        |mt(n-2)| == sum(mt(n-1));
      }
    }
    reveal Goal;
  }
}

// lemma Help(x: int)
// ensures x in {0, 1, 2} ==> 0 <= x <= 2

// lemma test()
// {
//   var q := [3,4,5];
//   assert |q| == 3;
//   assert q[0] == 3 && q[1] == 4 && q[2] == 5;
//   forall x { Help(x); }
//   // var s := fmap(x => q[x], [0, 1, 2]);
//   var s := seq<int>(|q|, x => q[x]); // Index out of range
//   assert s == [3,4,5];
// }

// lemma test2()
// {
//   var s := set x | 0 <= x <= 2 :: x;
//   assert s == {0, 1, 2};
// }