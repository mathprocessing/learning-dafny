include "./seq_lemmas_2.dfy"

lemma {:verify false} ReversedIsInverted(n: nat)
ensures reversed(mt(2 * n + 1)) == inv(mt(2 * n + 1))
{
  var xs := mt(2 * n + 1);
  if case n == 0 => {
    assert xs == [0, 1];
  } case n == 1 => {
    assert xs == [0, 1, 1, 0, 1, 0, 0, 1] by { assert 2 * n + 1 == 3; }
    assert reversed(xs) == [1, 0, 0, 1, 0, 1, 1, 0] by {
      calc == {
        reversed([0, 1, 1, 0, 1, 0, 0, 1]);
        reversed([1, 0, 0, 1]) + [0, 1, 1, 0];
        [1, 0, 0, 1, 0, 1, 1, 0];
      }
    }
  } case n == 2 => {
    assume false;
  } case n >= 3 => {
    assume false;
  }
}

lemma Palindromic''(n: nat)
ensures reversed(mt(2 * n)) == mt(2 * n)
decreases n
{
var xs := mt(2 * n);
  if case n == 0 => {
    assert xs == [0];
  } case n == 1 => {
    assert xs == [0, 1, 1, 0] by { assert 2 * n == 2; }
    calc == {
      reversed([0, 1, 1, 0]);
      reversed([1, 0]) + [1, 0];
    }
  } case n == 2 => {
    assert xs == mt(4) by { assert 2 * n == 4; }
    assert IH: reversed(mt(2)) == mt(2) by { Palindromic''(n - 1); }
    assert mt(2) == [0, 1, 1, 0] by {assert 2 * 1 == 2; }
    assert 1 * 2 + 1 == 3;
    calc == {
      reversed(mt(4));
        { assert mt(4) == mt(3) + inv(mt(3)); }
      reversed(mt(3) + inv(mt(3)));
        { 
          var a := mt(3);
          var b := inv(mt(3));
          assert reversed(a + b) == reversed(b) + reversed(a) by { ReversedOfConcat(a, b); }
        }
      reversed(inv(mt(3))) + reversed(mt(3));
        { ReversedIsInverted(1); }
      reversed(reversed(mt(3))) + reversed(mt(3));
        { ReversedTwiceIsId(mt(3)); }
      mt(3) + reversed(mt(3));
        { ReversedIsInverted(1); }
      mt(3) + inv(mt(3));
      mt(4);
    }
  } case n >= 3 => {
    assume false;
  }
}

lemma Palindromic'(n: nat)
ensures reversed(mt(2 * n)) == mt(2 * n)
decreases n
{
var xs := mt(2 * n);
  if case n == 0 => {
    assert xs == [0];
  } case n == 1 => {
    assert xs == [0, 1, 1, 0] by { assert 2 * n == 2; }
    calc == {
      reversed([0, 1, 1, 0]);
      reversed([1, 0]) + [1, 0];
    }
  } case n == 2 => {
    assert xs == [0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0] by { assert 2 * n == 4; }
    assert IH: reversed(mt(2)) == mt(2) by { Palindromic'(n - 1); }
    assert mt(2) == [0, 1, 1, 0] by {assert 2 * 1 == 2; }
    calc == {
      reversed([0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0]);
      reversed([1, 0, 0, 1, 0, 1, 1, 0]) + [1, 0, 0, 1, 0, 1, 1, 0];
      reversed([0, 1, 1, 0]) + [1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0];
      { assert reversed(mt(2)) == mt(2) by { reveal IH; } }
      [0, 1, 1, 0] + [1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0];
      [0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0];
    }
  } case n >= 3 => {
    assume false;
  }
}