include "./main.dfy"

lemma EvenMtIsPalindrom(n: nat)
ensures reversed(mt(2 * n)) == mt(2 * n)
decreases n
{
  var xs := mt(2 * n);
  if n == 0 {
    assert mt(0) == [0];
    assert reversed(xs) == xs;
  } else {
    assert 2 * n - 2 == 2 * (n - 1); // helper for arithmetics
    var ys := mt(2 * n - 1);
    var zs := mt(2 * n - 2);
    assert IH: reversed(zs) == zs by { EvenMtIsPalindrom(n - 1); }
    assert Hys: reversed(ys) == inv(ys) by { ReversedIsInverted(n - 1); }
    calc == {
      reversed(xs);
        { assert xs == ys + inv(ys); }
      reversed(ys + inv(ys));
        { ReversedOfConcat(ys, inv(ys)); }
      reversed(inv(ys)) + reversed(ys);
        { reveal Hys; }
      reversed(reversed(ys)) + reversed(ys);
        { ReversedTwiceIsId(ys); }
      ys + reversed(ys);
        { reveal Hys; }
      ys + inv(ys);
      xs;
    }
  }
}