include "./main.dfy"

lemma {:induction false} ReversedOfMt(n: nat)
ensures if even(n) then reversed(mt(n)) == mt(n) else reversed(mt(n)) == inv(mt(n))
decreases n
{
  var xs := mt(n);
  if n > 0 {
    assert MtDef: mt(n) == mt(n - 1) + inv(mt(n - 1));
    assert IHcase: if !even(n) then reversed(mt(n - 1)) == mt(n - 1) else reversed(mt(n - 1)) == inv(mt(n - 1)) by { ReversedOfMt(n - 1); }
    assert firstStep: reversed(mt(n)) == reversed(inv(mt(n - 1))) + reversed(mt(n - 1)) by {
      calc == {
        reversed(mt(n));
          { reveal MtDef; }
        reversed(mt(n - 1) + inv(mt(n - 1)));
          { ReversedOfConcat(mt(n - 1), inv(mt(n - 1))); }
        reversed(inv(mt(n - 1))) + reversed(mt(n - 1));
      }
    }
    if even(n) {
      assert IH: reversed(mt(n - 1)) == inv(mt(n - 1)) by { reveal IHcase; }
      calc == {
        reversed(mt(n));
          { reveal firstStep; }
        reversed(inv(mt(n - 1))) + reversed(mt(n - 1));
          { reveal IH; }
        reversed(reversed(mt(n - 1))) + reversed(mt(n - 1));
          { ReversedTwiceIsId(mt(n - 1)); }
        mt(n - 1) + reversed(mt(n - 1));
          { reveal IH; }
        mt(n - 1) + inv(mt(n - 1));
        mt(n);
      }
    } else {
      assert IH: reversed(mt(n - 1)) == mt(n - 1) by { reveal IHcase; }
      calc == {
        reversed(mt(n));
          { reveal MtDef; }
      /*
      Ways:
      1. use inv(reversed(?)) == reversed(int(?))
        + IH
      2. use like in case before (for even n):
        lemma reversed(reversed(xs)) == xs
        lemma reversed(a + b) == reversed(b) + reversed(a)
        + IH
      */
        reversed(mt(n - 1) + inv(reversed(mt(n - 1))));
        reversed(mt(n - 1) + inv(reversed(mt(n - 1))));
        // inv(mt(n));
      }
    }
  }
}