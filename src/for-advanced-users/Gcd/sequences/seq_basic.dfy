type bit = x: int | 0 <= x <= 1
type bits = seq<bit>

function fmap<T>(f: T -> T, xs: seq<T>): seq<T>
{
  if xs == [] then [] else
  [xs[0]] + fmap(f, xs[1..])
}

function pow(m: int, n: nat): int {
  if n == 0 then 1
  else m * pow(m, n - 1)
}

function sum(xs: bits): (s: nat)
ensures s <= |xs|
{
  if xs == [] then 0
  else xs[0] + sum(xs[1..])
}

predicate even(n: nat)
{
  if n == 0 then true
  else !even(n - 1)
}

lemma Helper(i: int, xs: bits)
ensures 0 <= i < |xs|

function inv(xs: bits): bits {
  seq(|xs|, i => Helper(i, xs); 1 - xs[i])
}

lemma InvRecursive(xs: bits)
requires xs != []
ensures inv(xs) == [1 - xs[0]] + inv(xs[1..])
{}

function reversed(xs: bits): bits {
  if |xs| <= 1 then xs
  else reversed(xs[1..]) + [xs[0]]
}

lemma ReversedTwiceIsId(xs: bits)
ensures reversed(reversed(xs)) == xs

lemma ReversedOfConcat(a: bits, b: bits)
ensures reversed(a + b) == reversed(b) + reversed(a)

// pal = palindromic sequence of bits like [1, 1, 0, 1, 1] or [0, 1, 1, 0]
type pal = xs: bits | xs == reversed(xs)

// lemma sumEven(xs: pal)
// requires even(|xs|)
// ensures even(sum(xs))
// {}

// lemma sumOdd(xs: pal)
// requires !even(|xs|)
// ensures !even(sum(xs))
// {}

// lemma sumEquiv(xs: pal)
// ensures even(|xs|) <==> even(sum(xs))
// {
//   if even(|xs|) {
//     sumEven(xs);
//   } else {
//     sumOdd(xs);
//   }
// }

lemma SumOfInv(xs: bits)
ensures sum(inv(xs)) + sum(xs) == |xs|
{
  if xs == [] {} else {
    assert sum(inv(xs[1..])) + sum(xs[1..]) == |xs[1..]|; // by induction
    calc <==> {
      sum(inv(xs)) + sum(xs) == |xs|;
      { assert inv(xs) == [1 - xs[0]] + inv(xs[1..]); /* from InvRecursive */ }
      sum([1 - xs[0]] + inv(xs[1..])) + sum(xs) == |xs|;
      // by sum def
      1 - xs[0] + sum(inv(xs[1..])) + sum(xs) == |xs|;
      // by sum def
      1 - xs[0] + sum(inv(xs[1..])) + xs[0] + sum(xs[1..]) == |xs|;
      // arithmetics, length of list
      1 + sum(inv(xs[1..])) + sum(xs[1..]) == 1 + |xs[1..]|;
      // arithmetics
      sum(inv(xs[1..])) + sum(xs[1..]) == |xs[1..]|;
    }
  }
}

lemma sumConcatComm(xs: bits, ys: bits)
ensures sum(xs + ys) == sum(ys + xs)

lemma concatSingle(xs: bits, ys: bits)
requires |xs| > 0
ensures sum(xs + ys) == xs[0] + sum(xs[1..] + ys)
{
  if ys == [] {
    assert sum(xs + []) == xs[0] + sum(xs[1..] + []) by {
      // assert sum(xs) == xs[0] + sum(xs[1..]);
      assert xs[1..] + [] == xs[1..]; // Dafny need this assert
      assert xs + [] == xs; // Dafny need this assert
    }
  } else {
    assert sum(xs + ys[1..]) == xs[0] + sum(xs[1..] + ys[1..]) by {
      concatSingle(xs, ys[1..]);
    }
    calc {
      sum(xs + ys[1..]) == xs[0] + sum(xs[1..] + ys[1..]);
      <==> { sumConcatComm(xs, ys[1..]); }
      sum(ys[1..] + xs) == xs[0] + sum(xs[1..] + ys[1..]);
      <==>
      ys[0] + sum(ys[1..] + xs) == xs[0] + ys[0] + sum(xs[1..] + ys[1..]);
      <==>
      sum(ys + xs) == xs[0] + ys[0] + sum(xs[1..] + ys[1..]);
      // <==> // !!! We can't use sumConcat !!! How to guarantee that?
      // sum(ys + xs) == xs[0] + ys[0] + sum(ys[1..] + xs[1..]);
      // <==>
      // sum(ys + xs) == xs[0] + sum([ys[0]] + ys[1..] + xs[1..]);
      // <==>
      // sum(ys + xs) == xs[0] + sum(ys + xs[1..]);
      // <==> {}
      // sum(ys + xs) == xs[0] + sum(xs[1..] + ys);
    }
  }
}

lemma sumOfConcat(xs: bits, ys: bits)
ensures sum(xs + ys) == sum(xs) + sum(ys)
{
  if xs == [] {
    assert sum([] + ys) == sum([]) + sum(ys) by {
      assert sum([]) == 0;
      assert [] + ys == ys;
    }
  } else {
    calc {
      sum(xs + ys);
      == { concatSingle(xs, ys); }
      xs[0] + sum(xs[1..] + ys);
      == { assert sum(xs[1..] + ys) == sum(xs[1..]) + sum(ys) by { sumOfConcat(xs[1..], ys); } }
      xs[0] + sum(xs[1..]) + sum(ys);
      == { assert xs[0] + sum(xs[1..]) == sum(xs); } // by def of sum
      sum(xs) + sum(ys);
    }
  }
}