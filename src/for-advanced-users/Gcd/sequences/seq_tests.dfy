include "./main.dfy"

lemma testMt()
{
  var s := [0,1,1];
  assert mtaux(0, s) == s;
  assert mtaux(1, s) == [0,1,1,1,0,0];
  assert mtaux(2, s) == [0,1,1,1,0,0,1,0,0,0,1,1];
  assert mt(0) == [0];
  assert mt(1) == [0, 1];
  assert mt(2) == [0, 1, 1, 0];
  assert mt(3) == [0, 1, 1, 0, 1, 0, 0, 1];
}

lemma testSum()
{
  assert sum([1,1,0,0,1]) == 3;
  assert forall xs :: |xs| == 5 ==> sum(xs) <= 5;
}

lemma testInv()
{
  assert inv([1,0,0]) == [0,1,1];
  assert inv([1,0,0,1,0,1,1,0]) == [0,1,1,0,1,0,0,1];
}

lemma testReversed()
{
  assert reversed([1,0,0]) == [0,0,1];
  var xs := [0,1,1,0,1,0,0,1];
  assert reversed(xs) == inv(xs);
}