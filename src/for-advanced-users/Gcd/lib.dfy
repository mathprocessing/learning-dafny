include "./factors.dfy"
import opened Definitions

function Gcd(x: pos, y: pos): (res: pos)

method EuclidGcd(x: pos, y: pos) returns (gcd: pos)
ensures gcd == Gcd(x, y)

lemma LemmaGcdMin(x: pos, y: pos)
ensures Gcd(x, y) <= Min(x, y)

lemma LemmaGcdMultiplier(x: pos, k: pos)
ensures Gcd(x, k * x) == x

lemma LemmaGcdDiag(x: pos)
ensures Gcd(x, x) == x
{
  LemmaGcdMultiplier(x, 1);
}

function MinElement(s: iset<pos>): pos
requires s.size > 0

lemma BezoutIdentity(x: pos, y: pos)
ensures Gcd(x, y) == MinElement(iset p, q, d | d == x * p + y * q :: d)