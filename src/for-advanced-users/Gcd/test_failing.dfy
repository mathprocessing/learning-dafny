include "./factors.dfy"
import opened Definitions

import M = ModuleFactors

lemma test()
{
  assert H1: M.IsFactor(2, 4) by {
    if * {
      assert 2 * 2 == 4;
    } else {
      // assert 1 * 4 == 4; 
      // Empty branch must fail!
      // How to fix this?
      // * Create another method or lemma for testing (works)
    }
  }
}

lemma {:verify false} test_another() {
  assert M.IsFactor(2, 4) by {
    assert 1 * 4 == 4; // Fails -> What we expected
  }
}