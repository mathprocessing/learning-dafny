/**
Let's specify algorithm to compute the greatest common divisor (GCD)
of two numbers. And also least common multiplier (LCM)

We know that:
  Gcd(x, y) * Lcm(x, y) = x * y

For example:
  if x, y = 8, 12
  Gcd(8, 12) * Lcm(8, 12) = 8 * 12
                   4 * 24 = 96

  Gcd properties:
  
  gcd3(a, b, c) = gcd(a, gcd(b, c)) = gcd(gcd(a, b), c) =
    gcd(gcd(b, c, a)

Also we can compute set of factors(x) for x >= 1 (type of pos):
  factors(10) = {1, 2, 5, 10}
                  ^        ^^ we always have {1, x} in that set

  n is divisible by m we write as `m | n`
  m | n = exists k :: n = k * m

  i.e. usually m <= n, i.e. m is a factor of n
  factors(n) = {.., m, ..}
  
commonFactors(x, y):
  commonFactors(20, 30) =
  factors(30) `intersect` factors(20) =
  {1, 2, 5, 10, 30} `intersect` {1, 2, 4, 5, 10, 20} =
  {1, 2, 5, 10} = factors(Gcd(20, 30))

  i.e. lemma
  commonFactors(x, y): set = factors(Gcd(x, y)): set

Partial order by divisibility:
  for set {1,2,3,4,5,6,10,12,15,20,30,60} = factors(60)
  we have set of edges(orders):
  {1 | 2, 2 | 4, 4 | 12, 12 | 60, .. }

  and set of paths:
  P = {[1, 2, 4, 12, 60],
   [1, 2, 4, 20, 60],
   [1, 2, 10, 20, 60],
   [1, 2, 10, 30, 60],
   [1, 3, 6, 12, 60],
   [1, 3, 15, 30, 60],
   [1, 5, 15, 30, 60]}

   I.e. cardinality of P = |factorPaths(60)| = 7


coprime(x, y): bool
  coprime(x, y) <==> Gcd(x, y) == 1

  coprime(x, y) && coprime(y, z) ==> coprime(x, z)?
  
 */

include "./lib.dfy"

method test()
{
  assert Gcd(1, 1) == 1 by {
    if * {
      LemmaGcdDiag(1);
    } else {
      LemmaGcdMin(1, 1);
    }
  }
  assert H: Gcd(2, 2) == 2 by { LemmaGcdDiag(2); }
  assert H2: 1 <= Gcd(2, 2) <= 2 by { LemmaGcdMin(2, 2); }
  assert H3: 1 <= Gcd(2, 2) <= 2 by {
    var witness' := Gcd(2, 1*2);
    forall x, y { // Not works without witness'
      LemmaGcdMultiplier(x, y);
    }
    
  }
}