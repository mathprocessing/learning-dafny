datatype Color = R | G | B {
  const asNumber: nat

  function toNumber(): nat
  {
    match this
      case R => 0
      case G => 1
      case B => 2
  }

  predicate eq(c: Color) {
    this == c
  }

  predicate le(c: Color) {
    this.asNumber <= c.asNumber
  }

  lemma def()
  ensures forall c: Color :: c.asNumber == toNumber()
}

lemma test(c: Color)
{
  assert 0 <= c.asNumber <= 2 by {
    c.def();
  }
  if c == R {
    assert c.eq(R);
  }
  assert c.eq(c);
}

lemma test2(a: Color, b: Color)
requires a.asNumber == b.asNumber
ensures a.eq(b)
{
  a.def();
  b.def();
}

lemma LeSelf(c: Color)
ensures c.le(c)
{}

lemma test3(a: Color, b: Color)
requires a.le(b) && b.le(a)
ensures a.eq(b)
{
  a.def();
  b.def();
}