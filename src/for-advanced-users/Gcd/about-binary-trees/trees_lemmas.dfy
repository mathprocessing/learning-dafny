include "./trees.dfy"

lemma testShow()
{
  assert show(E) == "E";
  assert show(Singleton(Yes)) == "Node(E, E, Yes)";
  assert show(Node(Singleton(Yes), Singleton(No), Yes)) == "Node(Node(E, E, Yes), Node(E, E, No), Yes)";
}


// method mHeight(t: T) returns (h: nat) {
//   var currentH := 0;
//   var node := t;
//   var parentNode := node;
//   while true
//   invariant currentH <= height(t)
//   {
//     if node == E {
//       h := 0;
//       return;
//     }
//     assert node != E;
//     if node.left == E {
//       if node.right == E {
//         if parentNode != node {
//           node := parentNode;
//           currentH := currentH - 1;
//         } else {
//           break;
//         }
//       } else {
//         node := node.right;
//         currentH := currentH + 1;
//       }
//     } else {
//       node := node.left;
//       currentH := currentH + 1;
//     }
//   }
//   assert currentH >= 0;
//   h := currentH;
// }