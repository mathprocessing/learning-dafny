include "../definitions.dfy"

import opened Definitions

datatype Bit = Yes | No {
  function method toString(): string {
    match this
      case Yes => "Yes"
      case No  => "No"
  }
}
datatype T = E | Node(left: T, right: T, data: Bit)

function Mirror (t: T): T
{
  match t
    case E => E
    case _ => Node(Mirror(t.right), Mirror(t.left), t.data)
}

lemma MirrorTwice(t: T)
ensures Mirror(Mirror(t)) == t
{}

function Symmetric(t: T): (isSymm: bool)
ensures t != E && isSymm ==> Mirror(t.right) == t.left
{
  MirrorTwice(t);
  match t
    case E => true
    case _ => Mirror(t.left) == t.right
}

predicate isSingleton(t: T) {
  match t
    case E => false
    case _ => t.left == E && t.right == E
}

function method Singleton(x: Bit): T {
  Node(E, E, x)
}

type Sing = t: T | isSingleton(t) witness Node(E, E, Yes)

function height(t: T): nat {
  match t
    case E => 0
    case _ => 1 + Max(height(t.left), height(t.right))
}

lemma SingletonHeight(t: Sing)
ensures height(t) == 1
{}

lemma SingletionsEqual(a: Sing, b: Sing)
requires a.data == b.data
ensures a == b
{}


lemma HeightOfMirror(t: T)
ensures height(t) == height(Mirror(t))
{}

lemma HeightOfSymmetric(t: T)
requires Symmetric(t) && t != E
ensures height(t.left) == height(t.right)
{  
  HeightOfMirror(t.left);
  HeightOfMirror(t.right);
}

lemma MirrorEq(t1: T, t2: T)
requires Mirror(t1) == Mirror(t2)
ensures t1 == t2
{
  HeightOfMirror(t1);
}

/**
      *
    /   \
   x     x
  / \   / \
 0   1 1   0

            *
         /     \
        /       \
      *           *
    /   \       /   \
   *     *     *     *
  / \   / \   / \   / \
 0   1 1   1 1   1 1   0
No counterexample found by manual work (z3 not really helps, may be we need quickcheck)
*/
lemma {:induction false} SymmMirrorSelf(t: T)
requires Symmetric(t)
ensures Mirror(t) == t
{
  // assert t != E ==> t.left == t.left;
  if t == E {} else {
    if isSingleton(t) { // maybe redundant but question: Can it improve speed of proving?
    } else {
      assert t.left != E;
      assert t.right != E;
      // HeightOfSymmetric(t);
      // assert isSingleton(t.left) <==> isSingleton(t.right);
      // assert height(t.left) == height(t.right);
      calc {
        Mirror(t);
        ==
        Node(Mirror(t.right), Mirror(t.left), t.data);
        == {
          assert Mirror(t.left) == t.right;
          assert Mirror(t.right) == t.left;
          // assert Mirror(t.right) == t.left by { MirrorTwice(t); } // We added ensures to `Symmetric` to automate this
        }
        Node(t.left, t.right, t.data);
      }
    }
  }
}

// Let's find some example tree with some special properties by backtracking:
// 1. Use manual backtracking and visualising process in comment and verifying it in Dafny (like in src/experiments/game-of-life)
// 2. Write verified (helper) backtracking solver (single function), compile it, run and find example.

method mHeight(t: T) returns (h: nat)
ensures h == height(t)
{
  if t == E {
    h := 0;
    return;
  }
  assert t != E;
  var left := mHeight(t.left);
  var right := mHeight(t.right);
  h := 1 + if left < right then right else left;
}

function method show(t: T): string {
  if t == E then "E" else "Node(" + show(t.left) + ", " + show(t.right) + ", " + t.data.toString() + ")"
}

method Main()
{
  print "Hello\n";
  print show(E);
  print "\n";
  print show(Singleton(Yes));
  print "\n";
}

