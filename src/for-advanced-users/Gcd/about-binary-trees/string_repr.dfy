include "./trees.dfy"

lemma {:induction false} LengthOfShow(t: T)
ensures |show(t)| >= 1 // How to prove that `1` is maximum value? (`2` can't proved) See: `lemma OneIsMaximal()`
{}

lemma {:induction false} OneIsMaximal()
ensures exists t :: |show(t)| == 1 // Assume we can prove `>= 2` case: that means we can't give example for constraint `|show(t)| == 1`
{
  ghost var witnessTree := E;
  assert |show(witnessTree)| == 1;
}

lemma {:induction false} ShowLengthOfEmpty(t: T)
requires |show(t)| <= 13 // upper bound `13` computed manually by binary search + checking (how to improve/speedup?)
ensures t == E
{}

lemma {:induction false} ShowLengthRange(t: T)
ensures !(|show(t)| >= 2 && |show(t)| <= 13)
{}

lemma {:induction false} ShowLengthOfSingleton(t: T)
requires |show(t)| == 14
ensures show(t) == "Node(E, E, No)"
{
  assert |"Node(E, E, No)"| == 14;
}

predicate isMinValue(val: int, pred: T -> bool, t: T) {
  (pred(t) ==> |show(t)| >= val) && exists t :: |show(t)| == val
}

// This lemma proves that second minimim of |show(t)| is 14
// I.e. in general we can prove |show(t)| in {1, 14, 15, ...}
lemma isMinTest(t: T)
ensures isMinValue(14, t' => t' != E, t)
{
  ghost var witnessTree := Node(E, E, No);
  assert |show(witnessTree)| == 14;
}


lemma {:induction false} ShowLengthOfSingleton2(t: T)
requires |show(t)| == 15
ensures show(t) == "Node(E, E, Yes)"
{}

lemma {:induction false} FirstLetter(t: T)
ensures var c := show(t)[0]; c == 'N' || c == 'E'
{}