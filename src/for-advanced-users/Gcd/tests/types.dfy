include "../datatypes.dfy"
// if module_name == Tests then we have error:
// `the included file datatypes.dfy contains error(s)`

module TestsOther {
  import opened DataTypes

  method t1(o: Option<int>) {
    if o == Some(42) {
      assert o.value != 0;
      assert o.Some?;
    } else {
      assume !exists x :: o == Some(x);
      assert o.None?;
    }
  }
}