include "definitions.dfy"
module ModuleFactors {
// import opened Defs = Definitions
import opened Definitions

predicate IsFactor(m: pos, n: pos) {
  exists d :: m * d == n
}

function Factors(x: pos): set<pos> {
  set p: pos | p <= x && IsFactor(p, x)
}

lemma FactorsHasAllFactors(x: pos)
ensures forall n :: n in Factors(x) <==>
  n in iset p: pos | IsFactor(p, x)
{}

lemma FactorsContains1(x: pos)
ensures 1 in Factors(x)
{
  assert 1 * x == x;
}

lemma FactorsContainsSelf(x: pos)
ensures x in Factors(x)
{
  assert x * 1 == x;
}

}