/**
We can find:
* point p with minimal distance to other point in worst case.
* Pair Pmin of points with minimal distance.
* Pmin is a set of two elements it may contain point p - set with one lement
*/

const left: int
const right: int

lemma boundsDef()
ensures left <= right

type P = x: int | left <= x <= right witness *
type Seq = seq<int> // alias, shorthand

predicate le(x: P, y: P) {
  x <= y
}

predicate Sorted(a: Seq)
{
  (|a| >= 2) ==> a[0] <= a[1] && Sorted(a[1..])
}

predicate EqualValues(a: Seq)
{
  exists val :: (forall i | 0 <= i < |a| :: a[i] == val)
}

lemma SortedSameHelper(a: Seq)
requires EqualValues(a)
ensures forall i | 0 <= i < |a| :: a[i] == a[0]
{}

lemma {:induction false} SortedSame3(a: Seq)
requires EqualValues(a)
ensures Sorted(a)
{
  if |a| <= 1 {}
  else {
    // Not works
    // assert exists val :: (forall i | 0 <= i < |a| :: a[i] == val);
    // var w :| forall i | 0 <= i < |a| :: a[i] == w;
    // Let's try to prove it by contradiction?
    assert forall i, j | 0 <= i < j < |a| :: a[i] == a[j];
    assert forall i | 0 <= i < |a| :: a[i] == a[0];
    var wit := a[0];
    assert EqualValues(a);
    // assert forall i | 0 <= i < |a| - 1 :: a[i] == a[0];
    assume EqualValues(a[..|a|-1]);
    SortedSame3(a[..|a|-1]);
    SortedSameHelper(a[..|a|-1]);
    // assume false;
  }
}

// lemma {:induction false} SortedSame2(a: Seq)
// requires exists j | 0 <= j < |a| :: (forall i | 0 <= i < |a| :: a[i] == a[j])
// ensures Sorted(a)
// {
//   if |a| <= 1 {}
//   else {
//     SortedSame2(a[1..]);
//   }
// }

lemma SortedSame(a: Seq)
requires forall i | 0 <= i < |a| :: a[i] == a[0]
ensures Sorted(a)
{}

lemma testSorted()
{
  assert !Sorted([2, 1]);
  assert Sorted([1, 2]);
  assert Sorted([1, 2, 3, 4, 5]);
  assert !Sorted([1, 2, 3, 2, 5]);
}

const A: bool
const B: bool

lemma test() {
  boundsDef();
  assert le(left, right);
  assert right - left >= 0;
  var x := if A then true else B;
  var y := !A ==> B;
  assert x <==> y;
}
