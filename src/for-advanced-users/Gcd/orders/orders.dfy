/*
// Option datatype:
// https://github.com/dafny-lang/dafny/blob/1d68bcd406b6f08b23e7e16d86330ce03f163fc1/Test/git-issues/git-issue-1016.dfy

We can find:
* point p with minimal distance to other point in worst case.
* Pair Pmin of points with minimal distance.
* Pmin is a set of two elements it may contain point p - set with one lement
* https://en.wikipedia.org/wiki/Mandelbrot_set
  See: Attracting cycles and Julia sets
* https://en.wikipedia.org/wiki/Newton_fractal
  We can use blurred random point probes to prove that
  image(Julia set) on complex plane have big areas stable colors +
  small area borders with mixed 3 colors.
*/

include "../datatypes.dfy"
include "../../pick-from-set.dfy"
import opened DataTypes

type num = x: int | 0 <= x <= 6
//           0   1   2   3   4   5   6
datatype T = A | B | C | D | E | F | G
type Pair = (T, T)
type OrderedPair = p: Pair | le(p.0, p.1) witness (A, A)

function allPairs(): set<OrderedPair>
{
  set p: OrderedPair | le(p.1, G) && p.0 != p.1 :: p
}

function ArgMin<T(!new)>(s: set<T>, f: T -> nat): Option<T> {
  if s == {} then None else
  var e := Pick(s);
  if s == {e} then
    Some(e)
  else
    assert |s| >= 2; // s have at least two elements
    var s' := s - {e};
    assert s == s' + {e};
    var other := ArgMin(s', f);
    if f(e) < f(other.value) then Some(e) else other
}

function minDistPair(distance: OrderedPair -> nat): (res: Option<OrderedPair>)
ensures res == ArgMin(allPairs(), distance)

function toNum(x: T): num
{
  match x
  case A => 0
  case B => 1
  case C => 2
  case D => 3
  case E => 4
  case F => 5
  case G => 6
}

// Let's define le using convertion to numbers
predicate le(x: T, y: T) {
  toNum(x) <= toNum(y)
}

lemma test()
{
  assert !le(C, A);
  forall k {
    assert le(k, k);
    assert le(A, k);
    assert le(k, G);
  }
}

lemma PairsEqual(p: Pair, q: Pair)
requires p.0 == q.0 && p.1 == q.1
ensures p == q
{}



// lemma t()
// ensures 
// {

// }