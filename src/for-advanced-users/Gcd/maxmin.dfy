include "definitions.dfy"

module Elements {
import opened Definitions

function MaxElement(s: set<pos>): pos
requires s != {}
{
  MaxElementExists(s);
  var x :| x in s && forall y :: y in s ==> y <= x;
  x
}

lemma MaxElementExists(s: set<pos>)
requires s != {}
ensures exists x :: x in s && forall y :: y in s ==> y <= x
{
  // Give witness to exists
  var wit := FindMax(s);
}

function FindMax(s: set<pos>): (max: pos)
requires s != {}
ensures max in s && forall y :: y in s ==> y <= FindMax(s)
{
  var x :| x in s;
  if s == {x} then
    x
  else
    var s' := s - {x};
    assert s == s' + {x}; // Why we need it?
    var y := FindMax(s');
    if x < y then y else x
}

}