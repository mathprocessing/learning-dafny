module DataTypes {

  datatype Option<T> = Some(value: T) | None {
    predicate method IsFailure() {
      None?
    }
    function method PropagateFailure<U>(): Option<U>
      requires None?
    {
      None
    }
    function method Extract(): T
      requires Some?
    {
      value
    }
  }

}

// module Tests {
//   import opened DataTypes

//   method t1(o: Option<int>) {
//     if o == Some(42) {
//       assert o.value != 0;
//       assert o.Some?;
//     } else {
//       assume !exists x :: o == Some(x);
//       assert o.None?;
//     }
//   }
// }