module Definitions {
  type pos = x: int | x > 0 witness 1

  function Min(x: int, y: int): int
  {
    if x < y then x else y
  }

  function Max(x: int, y: int): int
  {
    if x < y then y else x
  }
}