// H - coin head, T - coin tail
datatype Coin = H | T {
  // If we define Default value like that:
  const default: Coin
  // We need to add axiom
  lemma default_def()
  ensures forall c1:Coin, c2:Coin :: c1.default == c2.default

  lemma default_def_this()
  ensures forall c2:Coin :: this.default == c2.default

  // flip as property
  const flip: Coin
  // lemma f_def() ensures flip_as_prop != this

  // function flip(): Coin
}

// Default value must be same for all Coin instances
const global_default: Coin

function flipset(A: set<Coin>): set<Coin>
{
  set a:Coin | a in A :: a.flip
}

lemma Helper_set(s: set<Coin>)
ensures |s| <= 2

lemma Helper_len_two(s: set<Coin>)
requires |s| == 2
ensures s == {H, T}

function Pick(s: set<Coin>): Coin
  requires s != {}
{
  var x :| x in s; x
}

// TODO: how to define set of all sets such that:
// SetA.flip == SetA
// i.e. {x.flip | x in SetA} == SetA
// Defenition of SuperSet:
// var SuperSet := {s | s.flip == s}
// assert SuperSet == {{H, T}, {}}
// assert !SuperSet == {{H}, {T}}
lemma flipset_super(s: set<Coin>)
  requires flipset(s) == s
  ensures s == {} || s == {H, T}
{
  assert 0 <= |s|;
  assert |s| <= 2 by { Helper_set(s); }
  // assert {} != {H} && {H} != {T} && {H, T} == {T, H};
  assert s == {} || s == {H} || s == {T} || s == {H, T} by { // if user changed order of elements `s == {T, H}` then proof "time" must don't increase much
    if {
      case |s| == 0 => assert s == {};
      case |s| == 1 => {
        assert s != {};
        assert H in s || T in s by { assume false; }
        if y :| y in s {
          assert |s - {y}| == 0; // User time costly assert (I spend some time to found it)
        }
        assert s == {H} || s == {T};
      }
      case |s| == 2 => assert s == {H, T} by { Helper_len_two(s); }
      // case |s| >= 3 => { 
      //   // We already know it from |s| == 2 branch
      //   // But we can't use it directly in Dafny, let's add `Helper_len_two`
      //   var z :| z in s;
      //   var s' := s - {z};
      //   assert s' == {H, T} by { Helper_len_two(s'); }
      //   assume false;
      // }
    }
  }
  forall n:Coin { flip_def(n); }
  // assume false;
  // reveal Flip_def;
  // assert flipset({H}) != {H};
  // assert flipset({T}) != {T};
}

lemma two_elements<T>(sup: set<T>, a: T, b: T)
requires forall c:T | c in sup :: c == a || c == b 
ensures sup == {a, b}

function Supset(): set<set<Coin>> {
  set s: set<Coin> | flipset(s) == s
}

lemma show_solutions()
  ensures Supset() == {{}, {H, T}}
{
  forall s: set<Coin> | s in Supset() ensures s == {} || s == {H, T} {
    flipset_super(s);
    assert s == {} || s == {H, T};
  }
  assert forall s: set<Coin> | s in Supset() :: s == {} || s == {H, T};
  two_elements(Supset(), {}, {H, T});
}

method {:verify false} test_flip_as_prop(c: Coin)
{
  // Helper hypothesis
  assert Flip_def: forall n:Coin :: n.flip != n by {
    forall n { flip_def(n); }
  }
  // assert c.flip_as_prop in {H, T}; // not checks why? Answer: it needs witness
  assert L1: c.flip in {H, T} by {
    ghost var witness' := c.flip == H;
  }
  assert L2: c == T ==> c.flip == H by {
    flip_def(c);
  }

  assert 1: flipset({H}) == {T} by { reveal Flip_def; }
  assert 2: flipset({T}) == {H} by { reveal Flip_def; }
  assert 4: flipset({H, T}) == {H, T} by { reveal Flip_def; }

  // For {} we only need definition of Coin = H | T
  assert 3: flipset({}) == {};
}

method {:verify false} test_default(c: Coin)
{
  forall n { flip_def(n); }
  var h: Coin;
  assert h.default == c.default by {
    h.default_def_this();
  }

  if h == c.default {
    assert h == h.default == h.default.default by {
      h.default_def_this();
    }
  }
  
  assert forall u:Coin :: u.default == u.default.default by {
    forall u:Coin {
      u.default_def_this();
    }
  }
}

lemma {:axiom} flip_def(x: Coin)
ensures x.flip != x

lemma flip_twice(c: Coin)
ensures c.flip.flip == c
{
  assert c.flip != c by { flip_def(c); }
  assert c.flip.flip != c.flip by { flip_def(c.flip); }
}

// Definition of N-th composition of flip.
function iterate_flip(c: Coin, n: nat): Coin 
decreases n
{
  if n == 0 then c else iterate_flip(c.flip, n-1)
}

lemma {:induction n} iterate_even_is_id(c: Coin, n: nat)
requires n % 2 == 0
ensures iterate_flip(c, n) == c
decreases n
{
  if n == 0 {
    assert iterate_flip(c, 0) == c;
  } else {
    assert iterate_flip(c.flip.flip, n-2) == c by {
      assert c.flip.flip == c        by { flip_twice(c); }
      assert iterate_flip(c, n-2) == c by { iterate_even_is_id(c, n-2); }
    }
  }
}

method test() {
  assert forall x {:trigger} :: x == H || x == T;
  assert forall x:Coin :: x.flip.flip == x by {
    forall coin {
      flip_twice(coin);
    }
  }
  assert forall c :: iterate_flip(c, 1000) == c by {
    forall coin {
      iterate_even_is_id(coin, 1000);
    }
  }
}