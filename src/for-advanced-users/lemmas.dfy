
// http://leino.science/dafny-power-user/

// Aggregate Lemma Invocations

predicate P(x: int)

lemma LemmaForOneX(x: int)
  ensures P(x) == P(1 - x)

lemma LemmaForEveryX()
  ensures forall x :: P(x) == P(1 - x)
{
  forall x {
    LemmaForOneX(x);
  }
}

method test() {
  if P(0) {
    assert P(1) by {
      if {
        case true => LemmaForOneX(0);
        case true => LemmaForOneX(1);
      }
    }
  }
}

method equality_iter1()
  ensures P(0) == P(1) == P(2)
{
  assert P(0) ==> P(1) by {
    assume false;
  }
  assert P(1) ==> P(2) by {
    assume false;
  }
  assert P(2) ==> P(0) by {
    assume false;
  }
}

method equality_iter2()
  ensures P(0) == P(1) == P(2)
{
  if {
    case P(0) ==> P(1) => {
      assume P(1) ==> P(2);
      assume P(2) ==> P(0);
    }
    case P(1) ==> P(2) => {
      assume P(0) ==> P(1);
      assume P(2) ==> P(0);
    }
    case P(2) ==> P(0) => {
      assume P(0) ==> P(1);
      assume P(1) ==> P(2);
    }
  }
}

const A: bool
const B: bool

// Assumption A ==> B, Goal: A /\ B  we need only to prove A
method and_imp()
  requires A ==> B
  ensures A && B
{
  if * {
    // Proof 1
    assume A;
  } else {
    // Proof 2
    if {
      case  A => {}
      case !A => { assume false; }
    }
  }
}

/**
if X {
  Proof 1
} else {
  Proof 2
}

(1) and (2) can't be not identical because if (1) ~ (2) then we can simplify `if` statement.
 */