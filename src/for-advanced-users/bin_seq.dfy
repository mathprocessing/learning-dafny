
type flag = x: int | 0 <= x <= 1 witness 0
const X: seq<flag> := [0, 1, 0, 1]

function {:fuel 1} sum(a: seq<int>): int
{
  if a == [] then 0
  else a[0] + sum(a[1..])
}

lemma {:induction false} binSum(a: seq<flag>)
ensures 0 <= sum(a) <= |a|
decreases |a|
{
  if a != [] {
    assert 0 <= sum(a[1..]) <= |a[1..]| by {
      binSum(a[1..]);
    }
  }
}

const Y: seq<flag>
const Z: seq<flag>

lemma seqLen(a: seq<flag>, b: seq<flag>)
ensures |a| + |b| == |a + b|
{}

lemma sumConcat(a: seq<flag>, b: seq<flag>)
ensures sum(a + b) == sum(a) + sum(b)

lemma sumFull(a: seq<flag>)
requires a != []
requires |a| == sum(a)
ensures a[0] == 1

// if {:fuel 2} sum(a: seq<int>): int (fuel == 2)
// computations takes very long time (recursively loops)
lemma {:induction false} sumLen(a: seq<flag>, b: seq<flag>)
requires |a| + |b| == sum(a) + sum(b)
ensures |a| == sum(a)
ensures |b| == sum(b)
decreases a
{
  if a != [] {
    assert H: |a + b| == sum(a + b) by {
      calc {
        |a| + |b| == sum(a) + sum(b);
        <==>
        |a + b| == sum(a) + sum(b);
        <==> { sumConcat(a, b); }
        |a + b| == sum(a + b);
      }  
    }
    
    assert |a[1..]| == sum(a[1..]) && |b| == sum(b) by {
      calc {
        |a| + |b| == sum(a) + sum(b);
        <==>
        |a| - 1 + |b| == sum(a) - 1 + sum(b);
        <==> {
          assert |a| == |a[1..]| + 1;
          assert sum(a) == sum(a[1..]) + a[0];
          assert a[0] == 1 by { reveal H; sumFull(a + b); }
        }
        |a[1..]| + |b| == sum(a[1..]) + sum(b);
      }
      sumLen(a[1..], b);
    }
    
    calc {
      |a| == sum(a);
      <==>
      |a[1..]| + 1 == sum(a[1..]) + a[0];
      <==> { assert a[0] == 1 by {
        reveal H;
      } }
      |a[1..]| == sum(a[1..]);
    }
  }
}

method {:verify false} test() 
{
  assume |Y| + |Z| == 2;
  assume sum(Y) + sum(Z) == 2;
  assert 0 <= sum(Y) <= |Y| by { binSum(Y); }
  assert 0 <= sum(Z) <= |Z| by { binSum(Z); }
  if |Y| <= |Z| {
    assert (|Y|, |Z|) == (0, 2) || (|Y|, |Z|) == (1, 1);
    if {
      case (|Y|, |Z|) == (0, 2) => {
        assert Y == [];
        assert Z == [1, 1] by {
          // if we increase fuel we don't need this assert
          // assert sum(Z[1..]) == 1;
        }
      }
      case (|Y|, |Z|) == (1, 1) => {
        assert Y == Z;
      }
    }
  }

}

method {:verify false} main()
{
  assert |X| == 4;
  assert X[0..2] == [0, 1];
  assert X[..] == X;
  assert X[0 := 1] == [1, 1, 0, 1];
  assert X + [0] == [0, 1, 0, 1, 0];
  assert X[1..] == [1, 0, 1];
}