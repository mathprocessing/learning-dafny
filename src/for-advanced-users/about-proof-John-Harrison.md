# Cite from
See on wiki:
https://en.wikipedia.org/wiki/Prime_number_theorem
section `Computer verifications`

* Paper: Formalizing an analytic proof of the Prime Number Theorem
  John Harrison.
  Dedicated to Mike Gordon on the occasion of his 60th birthday.
  Journal of Automated Reasoning, vol. 43, pp. 243-261, 2009.
  https://www.cl.cam.ac.uk/~jrh13/papers/mikefest.pdf

```text
There isn’t a perfect match between what humans find easy and what proof assistants find easy [7, 22, 27], so it’s possible that a proof that a human would regard as more difficult is actually easier to formalize, even setting aside a comparison of the amount of knowledge assumed. For example, proofs involving extensive ‘brute-force’ case analysis can be almost trivial to formalize with a programmable proof assistant, perhaps obviating the need for more sophisticated symmetry arguments.

An instance is the proof of the Kochen-Specker paradox from quantum mechanics. A typical human-oriented proof [6] would make use of symmetry to reduce case analysis without loss of generality: ‘the three coordinate axes are all orthogonal, so we have a combination 0, 1, 1 in some order, so let’s assume the zero is in the x direction...’. By contrast, in a computer formalization it’s even easier just to program the computer to run through all the cases 
```

# Other links
* https://en.wikipedia.org/wiki/Kochen%E2%80%93Specker_theorem

* The De Bruijn Factor (Freek Wiedijk)
  https://www.cs.ru.nl/~freek/factor/factor.pdf