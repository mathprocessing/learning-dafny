module SetLib {
// Define set like a list and
// add permutation equivalence
// add element  multiplicity equivalence

datatype Set<T(!new)> = Empty | Rest(head: T, tail: Set) {
  predicate noDups()
  {
    this != Empty ==> forall e | this.tail.has(e) :: e != this.head
  }

  function add(e: T): Set<T>
    requires this.noDups()
    ensures this.noDups()
  {
    if this.has(e) then this
    else Rest(e, this)
  }

  predicate {:verify false} has(e: T) {
    match this
      case Empty => false
      case Rest(e', s') => if e == e' then true else s'.has(e)
  }

  function {:verify false} sub(e: T): Set<T> {
    if this.has(e)
    then 
      if this.head == e
      then this.tail
      else Rest(this.head, this.tail.sub(e))
    else
      this
  }

  predicate le(b: Set<T>) {
    forall e:T | this.has(e) :: b.has(e)
  }

  predicate lt(b: Set<T>) {
    this.le(b) && !this.eq(b)
  }

  predicate {:opaque} eq(b: Set<T>) {
    this.le(b) && b.le(this)
  }

  lemma {:induction false} eq_self()
  ensures this.eq(this) { reveal eq(); }

  lemma {:induction false} eq_symm(b: Set<T>)
  ensures this.eq(b) == b.eq(this) { reveal eq(); }

  lemma {:induction false} lt_cases(b: Set<T>)
  ensures this.lt(b) || !this.le(b) || this.eq(b)
  {
    // assert this.lt(b) <==> this.le(b) && !this.eq(b);
    // assert b.lt(this) <==> b.le(this) && !this.eq(b);
    // eq_symm(b);
    // if this.lt(b) {} else { // if not (a < b)
    //   var c1 := !this.le(b); // not (a <= b)
    //   var c2 := this.eq(b); // a == b
    //   assert c1 || c2;
    // }
  }
}

lemma eq_to_eq<T(!new)>(a: Set<T>, b: Set<T>)
  requires a == b
  ensures a.eq(b)
{
  assert a.eq(a) by { a.eq_self(); }
}

lemma HasZeroImpliesEmpty<T(!new)>(a: Set<T>)
  requires forall e: T :: !a.has(e)
  ensures a == Empty
{
  // var revealer: Set<T>; // reveal eq don't help (How to formally prove that some reveal don't help (write such formal fact in comments is not good in general), what if i'd make mistake? Generally: How to check that user don't make mistakes in ALL mind activity written as text?
  // reveal revealer.eq();
  /**
  Possible workaround:
  ```
  if eq.is_revealed() {
    assert Proof_checked("if a != Empty { assert a.has(a.head); }", with_context=...)
  } else {
    assert not Proof_checked("if a != Empty { assert a.has(a.head); }", with_context=...)
  }
  ```

  Если можно сделать так:
  const N: nat
  (+ Какие то сложные доказательства, с которыми z3 может не справиться при N < max_N)
  И тогда ставим вопрос: можно ли доказать, что при N < max_N и для ткито настроек `Settings == currect_z3_settings`?
  Нельзя доказать целевые теоремы. (Цель) за время `t` (или за количество тактов процессора).

  Или просто вычислить F :: (z3_settings, N_max: nat, t: nat) -> bool

  Или просто вычислить F :: (Dafny_version, N_max: nat, t: nat) -> bool
   */
  if a != Empty {
    assert a.has(a.head);
  }
}
}

module Test1 {
  import opened SetLib
  const A: Set<int>
  const B: Set<int>

  function singleton<T(!new)>(x: T): Set<T> { Empty.add(x) }

  method testSet3()
  {
    var revealer: Set<int>;
    reveal revealer.eq();
    // Set equations
    assume A == singleton(1);
    assume B.lt(A);
    assert B == Empty by {
      HasZeroImpliesEmpty(B);
      assert !B.eq(A);
      assert B.le(A);
      assert forall n :: B.has(n) ==> false;
    }
  }


  method testSet()
  {
    var s := Empty;
    assert 1: forall n :: !s.has(n);
    var s2 := s.add(5).add(5).add(2);
    assert s2.has(5);
    assert s2.has(2);
    var s3 := s2.sub(5);
    assert !s3.has(5);
    assert s3.has(2);

    var s4 := s3.sub(2);
    assert s4 == Empty;
  }

  method testSet2()
  {
    var revealer: Set<int>;
    reveal revealer.eq();
    assert Empty.sub(0) == Empty;
    assert Empty.sub(0).eq(Empty);
    var x := Empty.add(1).add(2);
    var y := Empty.add(2).add(1);
    assert x.eq(y);
  }
}