type pos = x | 1 <= x witness 1

function Gcd(x: pos, y: pos): pos

method EuclidGcd(x: pos, y: pos) returns (gcd: pos)
  ensures gcd == Gcd(x, y)

predicate IsFactor(p: pos, x: pos) {
  exists q :: p * q == x
}

function Factors(x: pos): iset<pos> {
  iset p: pos | IsFactor(p, x)
}

lemma FactorsContains1(x: pos)
  ensures 1 in Factors(x)
{
  assert IsFactor(1, x) by {
    assert 1 * x == x; // demonstrating a witness `exists q = x`
  }
}

lemma FactorsContainsSelf(x: pos)
  ensures x in Factors(x)
{
  assert IsFactor(x, x) by {
    assert x * 1 == x; // demonstrating a witness `exists q = 1`
  }
}

/**
To talk about _greatest_ common divisor, we need a function that picks out
the largest number in a set.

We use len-such-that expression
*/
function Max(s: set<pos>): pos
  requires s != {}
{
  // General form of existanse:
  // assume exists x :: x in s && P(x);

  // In that particular case:
  // assume exists x :: x in s && forall y :: y in s ==> y <= x;
  // i.e. P(x) == forall y :: y in s ==> y <= x

  MaxExists(s); // need to establishing the well-formedness of the subsequent expression.
  var x :| x in s && forall y :: y in s ==> y <= x;
  x
}

lemma MaxExists(s: set<pos>)
  requires s != {}
  ensures exists x :: x in s && forall y :: y in s ==> y <= x
{
  assume false;
}

function FindMax(s: set<int>): (max: int)
  requires s != {}
  ensures max in s && forall y :: y in s ==> y <= max
{
  var x :| x in s;
  // assert s == {x} ==> |s| == 1;
  // assert |s| == 1 ==> s == {x} by {
  //   if y :| (y in s && (x != y)) {
  //     assert |s| == 2 by {
  //       // assume false;
  //       assume x != y;
  //       // assert |s - {y} - {x}| == 0;
  //     }
  //   } else {
  //     assume false;
  //   }
  // }
  // if |s| == 1 then // value does not satisfy the subset constraints of 'pos'
  if s == {x} then
    assert |s| == 1;
    x
  else
    var s' := s - {x};
    assert s == s' + {x};
    var y := FindMax(s');
    if x < y then y else x
}

method test_set(s: set<pos>)
  requires s != {}
{
  assert |s| > 0;
  var n :| n in s;
  var m :| m in s;
  assert n >= 1;
  if n == m {} else {
    assert n != m;
    assert |s| >= 2 by {
      // assert m in (s - {n});
      assert |s - {n}| >= 1;
    }
  }
}