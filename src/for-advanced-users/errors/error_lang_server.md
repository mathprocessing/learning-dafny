# Error
```
[Error - 23:23:57] Request textDocument/documentSymbol failed.
  Message: Internal Error - System.NullReferenceException: Object reference not set to an instance of an object.
   at Microsoft.Dafny.CallStmt.get_NonSpecificationSubExpressions()+MoveNext()
   at System.Linq.Enumerable.Any[TSource](IEnumerable`1 source, Func`2 predicate)
   at Microsoft.Dafny.ProgramTraverser.Traverse(Statement stmt, String field, Object parent)
   at System.Linq.Enumerable.Any[TSource](IEnumerable`1 source, Func`2 predicate)
   at Microsoft.Dafny.ProgramTraverser.Traverse(Statement stmt, String field, Object parent)
   at System.Linq.Enumerable.Any[TSource](IEnumerable`1 source, Func`2 predicate)
   at Microsoft.Dafny.ProgramTraverser.Traverse(Statement stmt, String field, Object parent)
   at Microsoft.Dafny.ProgramTraverser.Traverse(MemberDecl memberDeclaration, String field, Object parent)
   at System.Linq.Enumerable.Any[TSource](IEnumerable`1 source, Func`2 predicate)
   at Microsoft.Dafny.ProgramTraverser.Traverse(TopLevelDecl topd)
   at Microsoft.Dafny.ProgramTraverser.Traverse(List`1 topLevelDecls)
   at Microsoft.Dafny.Resolver.ResolveTopLevelDecls_Core(List`1 declarations, Graph`1 datatypeDependencies, Graph`1 codatatypeDependencies, Boolean isAnExport)
   at Microsoft.Dafny.Resolver.ResolveModuleDefinition(ModuleDefinition m, ModuleSignature sig, Boolean isAnExport)
   at Microsoft.Dafny.Resolver.ResolveProgram(Program prog)
   at Microsoft.Dafny.LanguageServer.Language.Symbols.DafnyLangSymbolResolver.RunDafnyResolver(TextDocumentItem document, Program program)
   at Microsoft.Dafny.LanguageServer.Language.Symbols.DafnyLangSymbolResolver.ResolveSymbols(TextDocumentItem textDocument, Program program, Boolean& canDoVerification, CancellationToken cancellationToken)
   at Microsoft.Dafny.LanguageServer.Workspace.TextDocumentLoader.LoadInternal(DocumentTextBuffer textDocument, CancellationToken cancellationToken)
   at Microsoft.Dafny.LanguageServer.Workspace.TextDocumentLoader.<>c__DisplayClass12_0.<<LoadAsync>b__0>d.MoveNext()
--- End of stack trace from previous location ---
   at Microsoft.Dafny.LanguageServer.Workspace.TextDocumentLoader.LoadAsync(DocumentTextBuffer textDocument, CancellationToken cancellationToken)
   at Microsoft.Dafny.LanguageServer.Workspace.CompilationManager.ResolveAsync()
   at Microsoft.Dafny.LanguageServer.Workspace.DocumentManager.GetResolvedDocumentAsync()
   at Microsoft.Dafny.LanguageServer.Handlers.DafnyDocumentSymbolHandler.Handle(DocumentSymbolParams request, CancellationToken cancellationToken)
   at OmniSharp.Extensions.LanguageServer.Server.Pipelines.SemanticTokensDeltaPipeline`2.Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate`1 next)
   at OmniSharp.Extensions.LanguageServer.Server.Pipelines.ResolveCommandPipeline`2.Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate`1 next)
   at MediatR.Pipeline.RequestPreProcessorBehavior`2.Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate`1 next)
   at MediatR.Pipeline.RequestPostProcessorBehavior`2.Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate`1 next)
   at MediatR.Pipeline.RequestExceptionProcessorBehavior`2.Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate`1 next)
   at MediatR.Pipeline.RequestExceptionProcessorBehavior`2.Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate`1 next)
   at MediatR.Pipeline.RequestExceptionActionProcessorBehavior`2.Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate`1 next)
   at MediatR.Pipeline.RequestExceptionActionProcessorBehavior`2.Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate`1 next)
   at OmniSharp.Extensions.JsonRpc.RequestRouterBase`1.<RouteRequest>g__InnerRoute|7_0(IServiceScopeFactory serviceScopeFactory, Request request, TDescriptor descriptor, Object params, CancellationToken token, ILogger logger)
   at OmniSharp.Extensions.JsonRpc.RequestRouterBase`1.RouteRequest(IRequestDescriptor`1 descriptors, Request request, CancellationToken token)
   at OmniSharp.Extensions.JsonRpc.DefaultRequestInvoker.<>c__DisplayClass10_0.<<RouteRequest>b__5>d.MoveNext()
  Code: -32603 
```

# Code
```
datatype Set<T(!new)> = Empty | Rest(head: T, tail: Set) {
  function add(e: T): Set<T> {
    if this.has(e) then this
    else Rest(e, this)
  }

  predicate {:verify false} has(e: T) {
    match this
      case Empty => false
      case Rest(e', s') => if e == e' then true else s'.has(e)
  }

  function {:verify false} sub(e: T): Set<T> {
    if this.has(e)
    then 
      if this.head == e
      then this.tail
      else Rest(this.head, this.tail.sub(e))
    else
      this
  }

  predicate le(b: Set<T>) {
    forall e:T | this.has(e) :: b.has(e)
  }

  predicate lt(b: Set<T>) {
    this.le(b) && !this.eq(b)
  }

  predicate {:opaque} eq(b: Set<T>) {
    this.le(b) && b.le(this)
  }

  lemma {:induction false} eq_self()
  ensures this.eq(this) { reveal eq(); }

  lemma {:induction false} eq_symm(b: Set<T>)
  ensures this.eq(b) == b.eq(this) { reveal eq(); }

  lemma {:induction false} lt_cases(b: Set<T>)
  ensures this.lt(b) || !this.le(b) || this.eq(b)
  {}
}

lemma eq_to_eq<T(!new)>(a: Set<T>, b: Set<T>)
requires a == b
ensures a.eq(b)
{
  assert a.eq(a) by { a.eq_self(); }
}

method testSet2()
{
  reveal Set.eq(); // --- Create error ---
  /**
  To fix replace `reveal Set.eq();` with this code lines
  ```
  var revealer: Set<int>;
  reveal revealer.eq();
  ```
  */
  assert Empty.sub(0) == Empty;
  assert Empty.sub(0).eq(Empty);
  var x := Empty.add(1).add(2);
  var y := Empty.add(2).add(1);
  assert x.eq(y);
}
```