# Error
```
Dafny encountered an internal error. Please report it at <https://github.com/dafny-lang/dafny/issues>.
System.AggregateException: One or more errors occurred. (Value cannot be null. (Parameter 'key'))
 ---> System.ArgumentNullException: Value cannot be null. (Parameter 'key')
   at System.Collections.Generic.Dictionary`2.FindValue(TKey key)
   at System.Collections.Generic.Dictionary`2.TryGetValue(TKey key, TValue& value)
   at Microsoft.Dafny.Translator.CheckDefiniteAssignment(IdentifierExpr expr, BoogieStmtListBuilder builder)
   at Microsoft.Dafny.Translator.CheckWellformedWithResult(Expression expr, WFOptions options, Expr result, Type resultType, List`1 locals, BoogieStmtListBuilder builder, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.CheckWellformed(Expression expr, WFOptions options, List`1 locals, BoogieStmtListBuilder builder, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.CheckWellformedWithResult(Expression expr, WFOptions options, Expr result, Type resultType, List`1 locals, BoogieStmtListBuilder builder, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.TrStmt_CheckWellformed(Expression expr, BoogieStmtListBuilder builder, List`1 locals, ExpressionTranslator etran, Boolean subsumption, Boolean lValueContext)
   at Microsoft.Dafny.Translator.TrPredicateStmt(PredicateStmt stmt, BoogieStmtListBuilder builder, List`1 locals, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.TrStmt(Statement stmt, BoogieStmtListBuilder builder, List`1 locals, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.TrStmtList(List`1 stmts, BoogieStmtListBuilder builder, List`1 locals, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.TrStmt(Statement stmt, BoogieStmtListBuilder builder, List`1 locals, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.TrPredicateStmt(PredicateStmt stmt, BoogieStmtListBuilder builder, List`1 locals, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.TrStmt(Statement stmt, BoogieStmtListBuilder builder, List`1 locals, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.TrStmtList(List`1 stmts, BoogieStmtListBuilder builder, List`1 locals, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.TrStmt(Statement stmt, BoogieStmtListBuilder builder, List`1 locals, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.TrStmt2StmtList(BoogieStmtListBuilder builder, Statement block, List`1 locals, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.TrIfStmt(IfStmt stmt, BoogieStmtListBuilder builder, List`1 locals, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.TrStmt(Statement stmt, BoogieStmtListBuilder builder, List`1 locals, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.TrStmtList(List`1 stmts, BoogieStmtListBuilder builder, List`1 locals, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.TrStmt(Statement stmt, BoogieStmtListBuilder builder, List`1 locals, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.TrPredicateStmt(PredicateStmt stmt, BoogieStmtListBuilder builder, List`1 locals, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.TrStmt(Statement stmt, BoogieStmtListBuilder builder, List`1 locals, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.CheckWellformedWithResult(Expression expr, WFOptions options, Expr result, Type resultType, List`1 locals, BoogieStmtListBuilder builder, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.CheckWellformedWithResult(Expression expr, WFOptions options, Expr result, Type resultType, List`1 locals, BoogieStmtListBuilder builder, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.CheckWellformedLetExprWithResult(LetExpr e, WFOptions options, Expr result, Type resultType, List`1 locals, BoogieStmtListBuilder builder, ExpressionTranslator etran, Boolean checkRhs)
   at Microsoft.Dafny.Translator.CheckWellformedWithResult(Expression expr, WFOptions options, Expr result, Type resultType, List`1 locals, BoogieStmtListBuilder builder, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.AddWellformednessCheck(Function f)
   at Microsoft.Dafny.Translator.AddFunction_Top(Function f, Boolean includeAllMethods)
   at Microsoft.Dafny.Translator.AddClassMembers(TopLevelDeclWithMembers c, Boolean includeAllMethods, Boolean includeInformationAboutType)
   at Microsoft.Dafny.Translator.AddTypeDecl(RevealableTypeDecl d)
   at Microsoft.Dafny.Translator.DoTranslation(Program p, ModuleDefinition forModule)
   at Microsoft.Dafny.Translator.Translate(Program p, ErrorReporter reporter, TranslatorFlags flags)+MoveNext()
   at System.Collections.Generic.List`1..ctor(IEnumerable`1 collection)
   at System.Linq.Enumerable.ToList[TSource](IEnumerable`1 source)
   at Microsoft.Dafny.LanguageServer.Language.DafnyProgramVerifier.<>c__DisplayClass9_0.<GetVerificationTasksAsync>b__1()
   at System.Threading.Tasks.Task`1.InnerInvoke()
   at System.Threading.ExecutionContext.RunInternal(ExecutionContext executionContext, ContextCallback callback, Object state)
--- End of stack trace from previous location ---
   at System.Threading.Tasks.Task.ExecuteWithThreadLocal(Task& currentTaskSlot, Thread threadPoolThread)
--- End of stack trace from previous location ---
   at Microsoft.Dafny.LanguageServer.Language.DafnyProgramVerifier.GetVerificationTasksAsync(DafnyDocument document, CancellationToken cancellationToken)
   at Microsoft.Dafny.LanguageServer.Workspace.TextDocumentLoader.PrepareVerificationTasksAsync(DafnyDocument loaded, CancellationToken cancellationToken)
   at Microsoft.Dafny.LanguageServer.Workspace.DocumentDatabase.LoadVerificationTasksAsync(Task`1 resolvedDocumentTask, CancellationToken cancellationToken)
   --- End of inner exception stack trace ---Crash
```

# Code
```dafny
function FindMax(s: set<int>): (max: int)
  // requires s != {}
  // ensures max in s && forall y :: y in s ==> y <= max
{
  ghost var x :| x in s;
  assert |s| == 1 ==> s == {x} by {
    if y :| (y in s && (x != y)) {
      assert |s| == 2 by {
        assume x != y;
      }
    }
  }

  if s == {x} then
    x
  else
    var s' := s - {x};
    assert s == s' + {x};
    var y := FindMax(s');
    if x < y then y else x
}
```