# Version
`Dafny 3.7.1.40621`

# Error 1
```
Dafny encountered an internal error. Please report it at <https://github.com/dafny-lang/dafny/issues>.
System.AggregateException: One or more errors occurred. (Unable to cast object of type 'Microsoft.Dafny.BinaryExpr' to type 'Microsoft.Dafny.QuantifierExpr'.)
 ---> System.InvalidCastException: Unable to cast object of type 'Microsoft.Dafny.BinaryExpr' to type 'Microsoft.Dafny.QuantifierExpr'.
   at Microsoft.Dafny.Translator.TrForallStmtCall(IToken tok, List`1 boundVars, List`1 bounds, Expression range, ExpressionConverter additionalRange, List`1 forallExpressions, CallStmt s0, BoogieStmtListBuilder definedness, BoogieStmtListBuilder exporter, List`1 locals, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.TrStmt(Statement stmt, BoogieStmtListBuilder builder, List`1 locals, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.TrStmtList(List`1 stmts, BoogieStmtListBuilder builder, List`1 locals, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.TrStmt(Statement stmt, BoogieStmtListBuilder builder, List`1 locals, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.AddMethodImpl(Method m, Procedure proc, Boolean wellformednessProc)
   at Microsoft.Dafny.Translator.AddMethod_Top(Method m, Boolean isByMethod, Boolean includeAllMethods)
   at Microsoft.Dafny.Translator.AddClassMembers(TopLevelDeclWithMembers c, Boolean includeAllMethods, Boolean includeInformationAboutType)
   at Microsoft.Dafny.Translator.AddTypeDecl(RevealableTypeDecl d)
   at Microsoft.Dafny.Translator.DoTranslation(Program p, ModuleDefinition forModule)
   at Microsoft.Dafny.Translator.Translate(Program p, ErrorReporter reporter, TranslatorFlags flags)+MoveNext()
   at System.Collections.Generic.List`1..ctor(IEnumerable`1 collection)
   at System.Linq.Enumerable.ToList[TSource](IEnumerable`1 source)
   at System.Threading.Tasks.Task`1.InnerInvoke()
   at System.Threading.ExecutionContext.RunInternal(ExecutionContext executionContext, ContextCallback callback, Object state)
--- End of stack trace from previous location ---
   at System.Threading.Tasks.Task.ExecuteWithThreadLocal(Task& currentTaskSlot, Thread threadPoolThread)
--- End of stack trace from previous location ---
   at Microsoft.Dafny.LanguageServer.Language.DafnyProgramVerifier.GetVerificationTasksAsync(DafnyDocument document, CancellationToken cancellationToken)
   at Microsoft.Dafny.LanguageServer.Workspace.TextDocumentLoader.PrepareVerificationTasksAsync(DafnyDocument loaded, CancellationToken cancellationToken)
   at Microsoft.Dafny.LanguageServer.Workspace.DocumentDatabase.LoadVerificationTasksAsync(Task`1 resolvedDocumentTask, CancellationToken cancellationToken)
   --- End of inner exception stack trace ---Crash
```

# Code 1
```dafny
function {:opaque} Fib(n: nat): nat {
  if n == 0 then 0
  else if n == 1 then 2
  else Fib(n-2) + Fib(n-1)
}

lemma FibIsEven(n: nat)
  ensures Fib(n) % 2 == 0
{
  reveal Fib();
  // auto proved
}

lemma test()
  ensures forall n :: Fib(n) % 2 == 0
{
  assert Fib(3) % 2 == 0 by {
    FibIsEven(3);
  }
  
  forall x {
    FibIsEven(x);
    assert Fib(x) % 2 == 0;
  }
  
  assert forall n :: Fib(n) % 2 == 0 by {
    forall n:nat ensures Fib(n) % 2 == 0 {
      FibIsEven(n);
      assert Fib(n) % 2 == 0;
      reveal Fib();
    }
  }
  reveal Fib();
}

// Aggregate Lemma Invocations

predicate P(x: int)

lemma {:axiom} LemmaForOneX(x: int) ensures P(x)
  ensures P(x) == P(1 - x)

lemma LemmaForEveryX()
  ensures forall x :: P(x)
{
  forall x {
    LemmaForOneX(x);
  }
}
```

# Error 2
```
Dafny encountered an internal error. Please report it at <https://github.com/dafny-lang/dafny/issues>.
System.AggregateException: One or more errors occurred. (Unable to cast object of type 'Microsoft.Dafny.BinaryExpr' to type 'Microsoft.Dafny.QuantifierExpr'.)
 ---> System.InvalidCastException: Unable to cast object of type 'Microsoft.Dafny.BinaryExpr' to type 'Microsoft.Dafny.QuantifierExpr'.
   at Microsoft.Dafny.Translator.TrForallStmtCall(IToken tok, List`1 boundVars, List`1 bounds, Expression range, ExpressionConverter additionalRange, List`1 forallExpressions, CallStmt s0, BoogieStmtListBuilder definedness, BoogieStmtListBuilder exporter, List`1 locals, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.TrStmt(Statement stmt, BoogieStmtListBuilder builder, List`1 locals, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.TrStmtList(List`1 stmts, BoogieStmtListBuilder builder, List`1 locals, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.TrStmt(Statement stmt, BoogieStmtListBuilder builder, List`1 locals, ExpressionTranslator etran)
   at Microsoft.Dafny.Translator.AddMethodImpl(Method m, Procedure proc, Boolean wellformednessProc)
   at Microsoft.Dafny.Translator.AddMethod_Top(Method m, Boolean isByMethod, Boolean includeAllMethods)
   at Microsoft.Dafny.Translator.AddClassMembers(TopLevelDeclWithMembers c, Boolean includeAllMethods, Boolean includeInformationAboutType)
   at Microsoft.Dafny.Translator.AddTypeDecl(RevealableTypeDecl d)
   at Microsoft.Dafny.Translator.DoTranslation(Program p, ModuleDefinition forModule)
   at Microsoft.Dafny.Translator.Translate(Program p, ErrorReporter reporter, TranslatorFlags flags)+MoveNext()
   at System.Collections.Generic.List`1..ctor(IEnumerable`1 collection)
   at System.Linq.Enumerable.ToList[TSource](IEnumerable`1 source)
   at Microsoft.Dafny.LanguageServer.Language.DafnyProgramVerifier.<>c__DisplayClass9_0.<GetVerificationTasksAsync>b__1()
   at System.Threading.Tasks.Task`1.InnerInvoke()
   at System.Threading.ExecutionContext.RunInternal(ExecutionContext executionContext, ContextCallback callback, Object state)
--- End of stack trace from previous location ---
   at System.Threading.Tasks.Task.ExecuteWithThreadLocal(Task& currentTaskSlot, Thread threadPoolThread)
--- End of stack trace from previous location ---
   at Microsoft.Dafny.LanguageServer.Language.DafnyProgramVerifier.GetVerificationTasksAsync(DafnyDocument document, CancellationToken cancellationToken)
   at Microsoft.Dafny.LanguageServer.Workspace.TextDocumentLoader.PrepareVerificationTasksAsync(DafnyDocument loaded, CancellationToken cancellationToken)
   at Microsoft.Dafny.LanguageServer.Workspace.DocumentDatabase.LoadVerificationTasksAsync(Task`1 resolvedDocumentTask, CancellationToken cancellationToken)
   --- End of inner exception stack trace ---Crash
```

# Code 2
```dafny
function P(x: int): bool

lemma LemmaForOneX(x: int)
  ensures P(x)
  ensures P(x) <==> P(1 - x)

lemma LemmaForEveryX()
  ensures forall x :: P(x)
{
  forall x {
    LemmaForOneX(x);
  }
}
```

