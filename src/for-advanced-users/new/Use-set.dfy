include "../Set.dfy"

module MySet {
  import opened SetLib
  const s: Set<int>

  function card<T(!new)>(s: Set<T>): nat {
    match s
      case Empty => 0
      case Rest(_, tail) => 1 + card(tail)
  }

  method test() {
    var s := Empty;
    assert card<int>(s) == 0;
    var s2 := Empty.add(2).add(-2);
    assert card(s2) == 2;
    var s3 := Empty.add(2).add(2);
    assert card(s3) == 1;
  }

  lemma SingleHas<T(!new)>(s: Set<T>, e: T)
    requires card(s) == 1
    requires s.has(e)
    ensures s.head == e
  {}

  lemma CardAdd<T(!new)>(s: Set<T>, e: T)
  requires s.noDups()
  requires !s.has(e)
  ensures var cnew := card(s.add(e)); var cold := card(s);
    if s.has(e) then cnew == cold else cnew == cold + 1
  {}

  lemma LemmaNoDups<T(!new)>(s: Set<T>, e: T)
  requires s.noDups()
  requires s != Empty
  requires s.tail.has(e)
  ensures e != s.head
  {}

  method {:verify false} test_2() {
    assert 1: card(s) == 0 <==> s == Empty;
    assert 2: card(s) == 1 ==> s != Empty;
    assert 3: card(s) == 1 ==> exists x :: s == Rest(x, Empty);
    if card(s) == 1 {
      var x :| s.has(x);
      // var y :| s == Rest(y, Empty);
      // if x != y {
      //   assert s.has(x) && s.has(y);
      //   assert s.head == y;
      //   assert s.head == x by {
      //     assert forall e :: s.has(e) ==> s.head == e
      //   }
      // }
      assert (SingleHas(s, x); s) == Rest(x, Empty);
    }
  }

  method {:verify false} test_3() {
    if card(s) == 2 && s.noDups() {
      var x :| s.has(x);
      if x == s.head {
        ghost var w := s.tail.head;
        LemmaNoDups(s, x);
        assert s.has(w);
        assert exists z | z != x :: s.has(z);
      } else {
        assert x == s.tail.head by {
          assert card(s.tail) == 1;
          SingleHas(s.tail, x);
        }
        // LemmaNoDups(s, x);
        assert exists z | z != x :: s.has(z);
      }
      
      // assert exists z | z != x :: s.has(z) by {
      //   if x == s.head {
      //     ghost var w := s.tail.head;
      //     assert s != Empty && s.tail != Empty;
      //     assert s.tail.tail == Empty by {
      //       assert card(s.tail.tail) == 0;
      //     }
      //     assert w != x && s.has(w) by {
      //       if w == x {
      //         assert s.head == s.tail.head;
      //         assert false by {
      //           LemmaNoDups(s, x);
      //           assert s.has(s.head);
      //           assert s.has(s.tail.head);
      //         }
      //       }
      //     }
      //   } else {
      //     ghost var w := s.head;
      //     assert w != x;
      //   }
      // }
      var y :| (s.has(y) && y != x);
    }
  }

  lemma CardTwoTailNotEmpty<T(!new)>(s: Set<T>)
    requires card(s) == 2
    ensures s.tail != Empty
  {}

  lemma {:induction false} TwoHas<T(!new)>(s: Set<T>, e: T)
  requires card(s) == 2
  requires s != Empty // Added
  requires s.tail != Empty // Added
  requires s.has(e)
  // ensures e == s.head || (if s.tail != Empty then e == s.tail.head else true)
  ensures e == s.head || e == s.tail.head
  {
    assert H1: s.tail.tail == Empty by {
      assert card(s.tail) == 1;
      assert card(s.tail.tail) == 0;
    }
    // if e != s.head && e != s.tail.head {
    //   assert H2: s.tail.tail != Empty by {
    //     assert s.tail.has(e);
    //     assert s.tail.tail.has(e);
    //   }
    //   assert false by {reveal H1, H2;}
    // }

    assert s != Empty;
    assert s.tail != Empty;
    if e != s.head {

      assert s.tail.head == e by {
        SingleHas(s.tail, e);
      }
    }

    assert e == s.head || e == s.tail.head;
    // reveal H1;
    // assert card(s.tail) == 1;
    // assert card(s.tail.tail) == 0;

    // assert exists z :: s.tail == Rest(z, Empty);
    // 
  }

  method test_4() {
    assume card(s) == 2;
    assume forall e | s.tail.has(e) :: s.head != e;
    assert s != Empty;
    assert s.tail != Empty by {
      CardTwoTailNotEmpty(s);
    }
    ghost var x :| s.has(x);
    
    assert x == s.head || x == s.tail.head by {
      TwoHas(s, x);
    }
    assert exists z :: s.has(z) && z != x by {
      ghost var w;
      if x == s.head {
        w := s.tail.head;
        assert w != x by {
          assert s.head != s.tail.head by {
            if w == x {
              assert s.tail.has(s.head);
            }
          }
        }
      } else {
        w := s.head;
        assert w != x;
      }
      TwoHas(s, w);
    }
    ghost var y :| s.has(y) && (y != x);
    // if x != y {
    //   if x == s.head {
    //     assert y == s.tail.head by {
    //       TwoHas(s, y);
    //     }
    //   } else {
    //     assert y == s.head by {
    //       TwoHas(s, y);
    //     }
    //   }
    // }

  }
}