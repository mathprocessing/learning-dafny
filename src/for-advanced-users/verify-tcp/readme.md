# Links
- [Implementing TCP in Rust](https://www.youtube.com/watch?v=bzja9fQWzdA)
- [Tcp spec 1](https://www.ietf.org/rfc/rfc793.txt)
- [Header Format](https://www.rfc-editor.org/rfc/rfc791)

# SubGoals
* Parse TCP Header (see https://docs.rs/etherparse/latest/etherparse/)
  etherprase example of manual sending packet
  https://github.com/JulianSchmid/etherparse/blob/0.10.1/examples/write_ipv4_udp.rs
57:37

questions:
* How ensure that data don't corrupted during transmitting via TCP?
* Protocol before sending payload send checksum?


# Explanation

```text
Protocol Layering

  +---------------------+
  |     higher-level    |
  +---------------------+
  |        TCP          |
  +---------------------+
  |  internet protocol  |
  +---------------------+
  |communication network|
  +---------------------+

Basic Data Transfer:

  The TCP is able to transfer a continuous stream of octets in each
  direction between its users by packaging some number of octets into
  segments for transmission through the internet system.  In general,
  the TCPs decide when to block and forward data at their own
  convenience.

  Sometimes users need to be sure that all the data they have
  submitted to the TCP has been transmitted.  For this purpose a push
  function is defined.  To assure that data submitted to a TCP is
  actually transmitted the sending user indicates that it should be
  pushed through to the receiving user.  A push causes the TCPs to
  promptly forward and deliver data up to that point to the receiver.
  The exact push point might not be visible to the receiving user and
  the push function does not supply a record boundary marker.
```

## Header Format
```
 A summary of the contents of the internet header follows:


    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |Version|  IHL  |Type of Service|          Total Length         |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |         Identification        |Flags|      Fragment Offset    |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |  Time to Live |    Protocol   |         Header Checksum       |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                       Source Address                          |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                    Destination Address                        |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                    Options                    |    Padding    |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

                    Example Internet Datagram Header

                               Figure 4.

  Note that each tick mark represents one bit position.
```

## Definitions
Octet - a unit of digital information in computing and telecommunications that consists of eight bits. Example info("01101001") = 8 bits = 1 byte
Data stream - 

# Problems of past versions of TCP
1. 

## Additional rfc's
* 1122 - "Requirements for Internet Hosts - Communication Layers"
  Comment: Set initial values in memory for different bits...
* 2460 Need ipv6, ignored
* 2873 ignored
* 5681 "TCP Conjunction Control"
