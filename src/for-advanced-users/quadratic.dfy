// Quadratic equations with parameter(s)
/**
x ^ 2 + a * (x + 1) == 0

Find interval `I` such that if equation above have N roots then
parameter `a` must be inside that interval.

Observations: (in theory can be automatically proved)
N in {0, 1, 2} because equation is quadratic.

D = a^2 - 4 a = a (a - 4)

D < 0 if a < 0 or a > 4
D = 0 if a in {0, 4}
D > 0 if a in [0..4]

if N = 0:
  a < 0 or a > 4

if N = 1:
  a = 0 or a = 4

if N = 2:
  a >= 0 and a <= 4

*/


// Let's solve it only for integers.


function add(x: real, y: real): real
function mul(x: real, y: real): real
function sq(x: real): real { mul(x, x) }

lemma add_comm(x: real, y: real)
ensures add(x, y) == add(y, x)

lemma mul_comm(x: real, y: real)
ensures mul(x, y) == mul(y, x)

lemma mul_one(x: real)
ensures mul(x, 1.0) == x

// How to define function that returns number of solutions for some general constraints?

// Let's define form of constraints

datatype List = Nil | Cons(tail: List)

method test() {
  var x := Nil;
  assert x.tail == x.tail;
}