/**
1. https://www.cl.cam.ac.uk/~jrh13/papers/mikefest.html
Formalizing an analytic proof of the Prime Number Theorem
John Harrison.

Dedicated to Mike Gordon on the occasion of his 60th birthday.

Journal of Automated Reasoning, vol. 43, pp. 243-261, 2009. 

2. See: about-proof-John-Harrison
*/