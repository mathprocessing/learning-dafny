trait OneTrait
{
  predicate P() { true }
}

datatype Enum = One | Two

predicate f(x: OneTrait) {
  x == x
}

class C extends OneTrait {
  const value: int

  constructor(value: int)
  requires value >= 0
  ensures this.value == value
  {
    this.value := value;
  }
}

method test(u: OneTrait) {
  assert u.P();
  assert f(u);
}

method testC(u: C) {
  var k := new C(42);
  assert k.P();
  assert k.value >= 0;
  assert k.value == 42;
  assert u.P();
  assert f(u);
}