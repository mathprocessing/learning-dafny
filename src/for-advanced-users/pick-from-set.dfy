// https://github.com/dafny-lang/dafny/blob/1ac1a47eaf24c9c9b01ffa653983f56da5052511/Test/comp/ByMethodCompilation.dfy

// Also one can find info about `Pick` on leino.science

function Pick<T(!new)>(s: set<T>): T
  requires s != {}
{
  var x :| x in s; x
}
