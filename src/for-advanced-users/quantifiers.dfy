
const P: bool
const Q: bool
const R: bool
const And: (bool, bool) -> bool
const Or: (bool, bool) -> bool
const Not: bool -> bool

predicate reflexive(func: (bool, bool) -> bool)
{
  forall x :: func(x, x) == x
}

lemma {:opaque} all()
  ensures reflexive(And)
  ensures reflexive(Or)
  ensures forall x :: And(Not(x), x) == false

method test()
{
  assert A1: reflexive(And) by { assume false; }
  assert A2: reflexive(Or) by { assume false; }
  assert A3: forall x :: And(Not(x), x) == false by { assume false; }

  assert 1: And(true, true) == true by { reveal A1; }
  assert 2: And(false, false) == false by { reveal A1; }
  assert 3: And(Not(true), true) == false by { reveal A3; }
  assert 4: And(Not(false), false) == false by { reveal A3; }

  if {
    case forall x :: Not(x) == !x => {
      assert And(false, true) == false by { reveal 3; }
      assert And(true, false) == false by { reveal 4; }
      assert And(true, true) == true by { reveal 1; }
      assert And(false, false) == false by { reveal 2; }
      // I.e.
      assert forall x, y :: And(x, y) <==> x && y;
    }
    case forall x :: Not(x) == x => {

    }
    case true => {}
  }
}

method test2() {
  assert And_def: forall x, y :: And(x, y) <==> x && y by {assume false;}
  if * {
    // if exists m :: !And(m, P)
    if m :| !And(m, P) {
      assert !(m && P) by { reveal And_def; }
      assert !P;
    } else {

    }
  } else {
    if exists m :: !And(m, P) {
      var m :| !And(m, P);
      assert !And(m, P);
    } else {
      assert forall m :: And(m, P);
      reveal And_def;
      assert P;
      // assert 
    }
  }

}