datatype Enum = One | Two | Three {
  predicate method Even() {
    this.Two?
  }

  function {:fuel 0} len(): (res: nat)
  ensures 3 <= res <= 5
  {
    match this
      case One => 3
      case Two => 3
      case Three => 5
  }

  lemma len_prop_2(x: Enum)
  // still verifies even if :fuel is 0 because
  // `ensures` expose post condition regardless from :fuel
  ensures 3 <= x.len() <= 5
  {}

  // It will be very helful if that overloading can be possible:
  // predicate (==)(x: Enum, y: Enum) {
    
  // }

  lemma {:verify false} len_prop(x: Enum, y: Enum)
  // we cannot prove that lemma if :fuel is 0
  requires x.len() == y.len()
  ensures x == y
  {
    len_prop_2(x);
    len_prop_2(y);
  }

  predicate method Even'() {
    match this
      case One | Three => false
      case Two => true
  }

  predicate method Even''() {
    this == Two
  }

  lemma EvenOk() ensures Even() == Even'() == Even''() {}
}

datatype D = D(q:int, r:int, s:int, t:int)
type Point = (int, int)
datatype Compared = LE | EQ | QE

// How to make trait `Comparable` for Point type?
/*
Something like that:
trait Comparable<T> {
  fn (==)(p: Point) {
    return p.0 < 
  }
}
*/

function method compare(p: Point): Compared
{
  if p.0 < p.1 then LE else 
  if p.0 > p.1 then QE else
  EQ
}


method Main()
{
  var cards: int := 10;
  print "Send cards to: ", cards, "\n";

  var A: set<int> := {1, 2, 3, 1 + cards};
  print A, "\n";

  print D(10, 20, 30, 40), "\n";
  var x: Point := (1, 42);
  var add: Point -> int := (p: Point) => p.0 + p.1;
  assert add(x) == 43;
  assert compare(x) == LE;

  print "x = ", x, "\n";
  print "compare(x) = ", compare(x), "\n";

  // expect 1 == 2;
  // after run
  // [Program halted] .../main.dfy(7,2): expectation violation
}