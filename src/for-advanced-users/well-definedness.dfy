/*
e0 - left subexpression
e1 - right subexpression

For conjunction:
WD(e0 && e1) = WD(e0) && (e0 ==> WD(e1))



---------------------------------------------

How to simulate well-definedness in Dafny?
(simulate = Prove some lemmas and etc.)
*/
