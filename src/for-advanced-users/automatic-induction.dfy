datatype List<A> = Nil | Cons(A, List<A>)

function method {:opaque} Length(list: List): nat
  decreases list
{
  match list
  case Nil => 0
  case Cons(_, tail) => 1 + Length(tail)
}

function method {:opaque} Range(start: int, len: nat): List<int>
  decreases len
{
  if len == 0 then Nil else
    Cons(start, Range(start+1, len-1))
}

lemma {:induction false} RangeLength(start: nat, len: nat)
  ensures Length(Range(start, len)) == len
  decreases len
{
  if len == 0 {
    // trivial case
    assert Length(Range(start, 0)) == 0 by {
      assert Range(start, 0) == Nil by {
        reveal Range();
      }
      ghost var xs: List<int> := Nil; 
      assert Length(xs) == 0 by {
        reveal Length();
      }
    }
  } else {
    calc {
      Length(Range(start, len));
    == {
      // def. Range, since len > 0
      reveal Range();
    }
      Length(Cons(start, Range(start+1, len-1)));
    == {
      // def. Length on Cons
      reveal Length();
    }
      1 + Length(Range(start+1, len-1));
    ==  { RangeLength(start+1, len-1); }  // apply induction hypothesis
      1 + len - 1;
    ==  // arithmetic
      len;
    }
  }
}

// Don't work:
// * Without `descreases`
// * With {:induction len}
lemma RangeLength_2(start: nat, len: nat)
  ensures Length(Range(start, len)) == len
  decreases len
  
{
  reveal Range();
  reveal Length();
}


/**
Meta task:
How to prove (in Dafny) that Rustan Leino not lie:
`No, this won't prove termination`

```
What about using the lexicographic tuple decreases start, len? No, this won't prove termination, because start+1, len-1 is not lexicographically smaller than start, len (in fact, it is lexicographically larger). Had we left off the decreases clause from RangeLength altogether, Dafny would have generated one for us. It generates it to be decreases start, len, because the parameters of RangeLength are start and len, in that order. So, without an explicit decreases clause, Dafny would complain about not being able to prove termination. 
```

 */