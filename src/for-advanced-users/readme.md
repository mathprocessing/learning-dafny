# Examples
https://github.com/dafny-lang/dafny/blob/master/Test/dafny2/Z-BirthdayBook.dfy

## Opaque types
```dafny
abstract module Specification {
  // Names and dates are two types. We leave these types as opaque
  // for now.
  // We need to say that names support equality in compiled
  // code. This is done with the `(==)` suffix.
  type Name(==)
  type Date

  // Here are some example names and types
  const John: Name
  const Mike: Name
  const Susan: Name
  const Mar25: Date
  const Dec20: Date

  ...
}
```

And refined types:
```
module Implementation refines Specification {
  type Name = string
  type Date = int

  const John := "John"
  const Mike := "Mike"
  const Susan := "Susan"
  const Mar25 := 325
  const Dec20 := 1220

  ...
}
```

# Notes on real arithmetics

## How to optimize

## by focusing
Instead of tactics like `Lia`, may helpful just "focus" on some group of lemmas:
`z3.focus_on("Real arith")`
or some syntactic sugar for
```
  z3.push_focus_on("Real arith")
    BODY
  z3.pop_focus_on()
```
`<=>`
```
@focus_on "Real arith"
BODY
```

* Other idea might be "How to find optimal focus?", because for user it also takes time.
```
  z3.find_optimal_focus_by_stochastic_search()
  or
  z3.reduce_current_focus_on_actions_set()
  or
  z3.recalc_rating_of_focus_actions()
```

### by using symbolic engines and evaluators of states
```dafny
@proved by execution "or.py"
function or()...

// OR

assert Expr: (x + 1) * (x + 1) == x * x + 2 * x + 1 by exec {
  // Some particular method (tactic language? or script language with doctest)
  >>> import module sumpy
  >>> Expr_state = create_context_state_from_expr(Expr)
  >>> Expr_state.get_actions()
  [Use lemma 1, Use lemma 2, expand, glue context with lemma group LG1, minimize_context, ...]

  >>> Expr.root.args[0]
  (x + 1) * (x + 1)
  >>> sympy.expand(Expr.root.args[0])
  x * x + 2 * x + 1

  # This function don't save actial proof, this function only save `action function` i.e. some blurred way how do it
  >>> Expr_state.save_proof_hint()

  # Internal code
  > DafnyManager.load_state(Expr_state)
}
```

## Simplified projections of `technical`(with big number of details) proof states
Often is hard to debug, help to prover when we see some detailed objects, maybe better if we constantly 
improve such `model projections` during development of formal correct library.

# Notes on projects structure `compiler - metacompiler`
* We can use it because `Prove equivalence of some methods is simpler than prove it correctness from scratch`

Example: in the main language (for example `C#`) we often write some `Enum` structures (if we think about dafny repo).
But code is repeating when we do it.

It's not good for
* testing
* shrinking to minimal code(to prove bugs) 

... when we repeating code in main repo.

How to prevent this?
* Answer: Write this code in some meta-language (spec-language) + write proofs for each group:
  `some group of methods in meta-lang equivalent to some group of methods in C#`

* Also we can replace `C#` with `Dafny`


# Почему детальное доказательство это плохо?
Коротко: Потому что если изменятся условия:
* изменится группа лемм(какая-то лемма пропадет, либо просто немного подкорректируется)
* Наш proof state будет похож на уже известный и мы как бы оказываемся в другой системе отсчета, откуда
  может быть произведен переброс информации(как часть процесса обобщения).

...то нужно будет менять сам proof.

А что если можно сделать его универсальнее?
```pseudo
if delta < max_non_change_delta then 
  proof P stay the same
else
  @adoption style
    we try to find maximally closest proof to P
  @create new things style
    forget almost all info about P and find new proof P_new from scratch
```

# About full-auto and semi-auto generator of solutions (counterexamples)
## Full-auto
Just change input parameters and run `==>` new solution found

## Semi-auto
1. Change(because we need new solution that `!=` old solution) input parameters
1. Change(fix) proofs of internal methods, lemmas if proof not works.
  Because when `input_params = old` proof exists(work perfectly), but
  when `input_params = new` proof may work may not, it working status depends on how much we change.
  I.e. 
  ```
  if param_dist(old, new) > delta
    proof may work may not
  else
    proof must work, we can guarantee it
  ```
1. Run

Note: we can define something like `lifecycle` for `facts`.
## Fact lifecycle
Change fact (code, library, ...)
1. Start: Fact no longer proves (check fact; returns false)
1. Calculate possible set of fixes
1. Apply optimal fix and save proof about "why we choose this particular fix"
1. Finish: Fact fixed, changed

Motivating example:
see `13. Summary`
http://leino.science/papers/krml280.html

```
Dafny supports `5`(this our target) type-parameter modes that a type constructor can use to express the desired combination of variance and cardinality preservation. 
```

Let's reformulate this:
We need proof that some function is equal to `5`.
And we need to define some process how we change it
time to time... (really this can be hard to desribe)
How to achieve this? Using lifecycles!

## Addiional to lifecycles
Ниже некая фантазия на счёт формализации сотрудничества
`человек-машина`.

Например мы храним в виде doctest файла check-sums для картинок чтобы их сравнивать.
Если вдруг код библиотеки, которую мы тестируем изменился, то
мы это сразу видим по завалившемуся тесту.
Но можно пойти дальше и полу-автоматически подготовиться к такой ситуации. 
  (может быть изменить код библиотеки нужно будет обязательно, так как изменилась спецификация проекта)

Как подготовиться?
* Заранее просчитываем те `data-links`, которые имеют отношение к доказательству этого тест случая с картинкой.
* ...

И когда "изменение кода" происходит, то
* Формально: в констект пользователя(разработчика) `user-links` можно будет добавить `data-links`.
  
* Тоже, но по-русски: Система намекнет разработчику, что
  нужно ещё "подтянуть эту информацию(`data-links`)" к уже
  имеющимся у него знаниям (которые уже формализованы и частично содержатся в памяти системы)

И, естественно, что у него теперь больше релевантной информации чтобы выполнить работу более качественно (но не факт что за меньшее время, т.е. можно собирать статистику о влиянии метода на время решения задач и тем самым, обобщая, добавлять полезные ограничения).

# Cite from `Automating Theorem Proving with SMT` by Rustan Leino
Link: http://leino.science/papers/krml234.pdf

Cite is about typical user problems when using proof automation.

---
The given examples showcase the high degree of automation that is possible in a tool powered by an SMT solver and designed to keep the interaction at the problem level (and not, for example, at the level of defining and using necessary prover tactics).

Users are not bothered with trivial details (like the associativity of logical and arith-metic operators) and the human involvement to prove that user-defined functions are mathematically consistent is small. 

Even the tricky recursive call of Filter is solved by defining and using StepsToNext as a variant function, which does not require an excessive amount of human effort. 

When more information is needed, human-readable calculations can be used, putting proofs in a format akin to what may be done by hand.

For each function and method in the examples shown, the verifier needs to spend only a small fraction of a second. This makes performance good enough to be running the verifier continuously in the background of the integrated development environment, which is what Dafny does.

Most changes of the program text yield a near-instant re- sponse, which is important when developing proofs. Note that performance is at least as important for failed proofs as for successful proofs, because failed proofs happen on the user’s time (see `[27]` for some research directions for auto-active verification environments).

---

`[27]`: 
[Usable Auto-Active Verification](https://fm.csl.sri.com/UV10/submissions/uv2010_submission_20.pdf)

Cite:

---

### Bridging the Gap Between User and Tool
Despite these advances, we argue, based on our own experience in building and using
auto-active program verification tools (e.g., [1, 8]), that program verification is currently
unusable, because current tools require too much expertise from the user. 

Stated differently, tools can understand our programs, but we cannot understand our tools.

---

# Holes (sets of possible values that we can enter to it)

Holes can be:
* type holes
  Example: (x: `_`) ^ 2 + 2 * (x: real) + 1 == 0
* value holes
  - (inhabited) value hole with default value
    Example: x ^ 2 + 2 * x + 1 == (`_` maybe 0)
  - (N-inhabited) value hole with N default values

# Обратный анализ (back-analysis)
* Разрешение неопределенности в сторону источника информации (в сторону обратной направлению информации)
  Пример (не совсем удачный):
  К нам поступают разведданые (может зашифрованные), а мы их должны правильно интерпретировать и получить факты о их источнике.
* Пусть есть процесс в чёрном ящике. И имеется много результатов его продукции/работы. Вскрыть можно, но все-равно не понятно как он работает.
  Тогда можно для объектов (решений), которые он генерирует "прокручивать время вспять" и получать их возможные версии за "секунду до" их рабочей версии (финальной, выходной)

Обратный анализ может быть полезен, когда нам нужно достичь не полной (not fully) автоматизации, а достаточно лишь частичной, но с такой структурой, которая легко обобщаема, переиспользуема и др.