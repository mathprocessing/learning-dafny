function sq(x: int): int
{
  x * x
}

method test_map() 
{
  // map perfect squares to it's roots
  var M := map x {:trigger sq(x)} | 0 <= x < 10 :: sq(x) := x;
  // This unused var surprisingly helps to prove assertion
  ghost var m := sq(2);
  assert M[4] == 2;
  // assert 9 in M.Keys;
}

method {:verify false} test_set()
{
  /**
  the result of a set comprehension must be finite, but Dafny's heuristics can't figure out how to produce a bounded set of values for 'x'
  */
  // var S := set x,y | 0 <= x <= y <= 100 && x + y == 100 :: (x,y);

  var S := iset x,y | 0 <= x <= y <= 100 && x + y == 100 :: (x,y);
  assert 1: (50, 50) in S;
  assert 2: (25, 75) in S;
  assert 3: (1, 99) in S;
  assert 4: (0, 100) in S;
  assert 5: 100 > 0;
  assert 6: (100, 0) !in S;
  assert 7: (75, 25) !in S;
  assert 8: forall x, y | x > y :: (x, y) !in S;
  assert 9: forall x | (x, x) in S :: x == 50;
  assert A: forall x :: (x, x + 1) !in S;
  assert B: forall x | (x, x + 2) in S :: x == 49;
}

function max(x: int, y: int): int { if x < y then y else x }
function min(x: int, y: int): int { if x < y then x else y }

lemma max_double_succ(x: int, y: int)
  ensures max(x + 1, y + 1) == max(x, y) + 1
  // decreases if x < y then y else x
{
  // Example proof by induction: (but we don't need induction)
  // if x <= y {
  //   if x == y {} else {
  //     if x == 0 {} else {
  //       assert 0 < x < y;
  //       assert max(x + 1, y + 1) == max(x, y) + 1 by {
  //         assert max(x + 1, y) + 1 == max(x + 1, y + 1);
  //         assert max(x, y - 1) + 1 == max(x, y);
  //         max_double_succ(x, y - 1);
  //       }
  //     }
  //   }
  // } else {
  //   assert y < x;
  //   max_double_succ(y, x - 1);
  // }
}

/**
    if B == {1, 2, 3} && A == {1, 4} {
      assert max(|A|, |B|) == 3;
      assert A + B == {1, 2, 3, 4};
      assert |A + B| == 4;
      var x := 1;
      assert x in A * B;
      var B' := B - {x};
      assert max(|A|, |B'|) <= |A + B'| by {
        union_card_low(A, B');
      }
      assert max(|A|, |B'|) == 2;
      assert |B'| == |B| - 1;
      assert |A + B'| == |A + B|;

      assert max(|A|, |B|) <= |A + B|;
    }
 */

lemma union_assoc(A: set<int>, B: set<int>, C: set<int>)
  ensures (A + B) + C == A + (B + C)

lemma union_card_increases(A: set<int>, B: set<int>)
  ensures |A| <= |A + B|
{
  // Limit cases: 1. A * B == {} 2. all elements in X contains in Y
  assert |A| <= |B - A| + |A|; // Really simple proof... I don't think that such proof can work in the beginning of solving task
  
  // if {
  //   // all elements of A contains in B, i.e. B includes A
  //   case A <= B => {
  //     assert A + B == B;
  //     assume |A| <= |B|;
  //   }
  //   case A * B == {} => {
  //     assert |A| + |B| == |A + B|;
  //     assert |A| <= |A| + |B|;
  //   }
  //   case B <= A => {
  //     assert A + B == A;
  //   }
  //   case true => {
  //     /*
  //     Goal:
  //     |A| <= |A + B|
  //     |A| <= |(B - A) !+ A|
  //     |A| <= |B - A| + |A|
  //     */
  //     assert |A| <= |B - A| + |A|;
  //   }
  // }
}

lemma union_card_low_proof2(A: set<int>, B: set<int>)
  ensures max(|A|, |B|) <= |A + B|
{
  if |A| <= |B| {
    assert |B| <= |B + A| by {
      union_card_increases(B, A);
    }
    assert A + B == B + A; // Dafny needs this hint
  } else {
    assert |A| <= |A + B| by {
      union_card_increases(A, B);
    }
  }
}

lemma {:verify false} union_card_low(A: set<int>, B: set<int>)
  ensures max(|A|, |B|) <= |A + B|
  decreases |A| + |B|
{
  if B == {} {
    assert max(|A|, 0) == |A|;
  } else {
    assert |B| >= 1;
    var x :| x in B;
    var B' := B - {x};
    assert Bsucc: |B'| == |B| - 1;
    if x in A {
      // assert x in A * B;
      var A' := A - {x};
      assert Asucc: |A'| == |A| - 1;
      assert H: |A' + B'| == |A + B| - 1 by {
        calc {
          |A + B|;
          == { assert A' + {x} == A && B' + {x} == B; }
          |(A' + {x}) + (B' + {x})|;
          == { assert (A' + {x}) + (B' + {x}) == (A' + B') + {x}; }
          |(A' + B') + {x}|;
          == {
            assert x !in A' + B' by {
              assert x !in A' && x !in B';
            }
            assert (A' + B') + {x} == A + B;
          }
          |A' + B'| + 1;
        }
      }
      assert max(|A'|, |B'|) <= |A' + B'| by {
        // Short hint: We decrease symmetrically from |A| and from |B|
        // i.e. Not just from B for example `union_card_low(A, B');`
        //                                                  ^ - argument stay the same (!= A')
        union_card_low(A', B');
      }
      // Goal
      assert max(|A|, |B|) <= |A + B| by {
        // Fact from induction hypothesis
        assert max(|A| - 1, |B| - 1) <= |A + B| - 1 by {
          reveal Asucc, Bsucc, H; // reveal + labels used for optimization + explanation
        }
        assert max(|A|, |B|) == max(|A| - 1, |B| - 1) + 1 by {
          max_double_succ(|A| - 1, |B| - 1);
        }
      }
    } else {
      assert x !in A;
      assert H: |A + B'| + 1 == |A + B| by {
        calc {
          |A + B|;
          == { assert B == B' + {x}; }
          |A + (B' + {x})|;
          == { union_assoc(A, B', {x}); }
          |(A + B') + {x}|;
          == { assert x !in A + B'; }
          |A + B'| + 1;
        }
      }
      // Fact
      assert max(|A|, |B| - 1) + 1 <= |A + B| by {
        union_card_low(A, B');
        reveal H;
      }

      /* apply transitivity
      if we prove
        `? <= |A + B|`
        and
        `max(|A|, |B|) <= ?`
        then we prove goal
        `max(|A|, |B|) <= ? <= |A + B|`
      */
      // Goal
      assert max(|A|, |B|) <= |A + B| by {
        assert max(|A|, |B|) <= max(|A|, |B| - 1) + 1;
      }
    }
  }
}

lemma union_comm(A: set<int>, B: set<int>)
  ensures A + B == B + A
{}

method test_union(A: set<int>, B: set<int>) {
  // assert A + B == {} ==> A * B == {};
  assert H1: max(|A|, |B|) <= |A + B| <= |A| + |B| by {
    if * {
      // Proof version 1
      union_card_low_proof2(A, B);
    } else {
      // Proof version 2
      assert |A| <= |A + B| by { union_card_increases(A, B); }
      assert |B| <= |A + B| by { union_card_increases(B, A); union_comm(A, B); }
    }
  }
}

lemma include_card(A: set<int>, B: set<int>) 
  requires A <= B
  ensures |A| <= |B|
{
  ghost var witness' := |B - A| + |A|;
  // assert |B - A| + |A| == |B|;

  // Interesting...
  // TODO: Can we find some constraints on possible witness in general?
  // Example: 
  // 1. In current state we don't know actual witness' but we know that 
  //    "we don't properly checked expressions with nodes {'+', '-', cardinality |X|}"
  // 2. Assume that Non-deterministically we already have some witness' than
  //    from that point maybe easier to find it. (obvious fact, non-useful)
  // 3. Solve some another but simple task (find simpler proof) and from it "bring" all objects
  //    to our context and see what happens, i.e. check: out set of proof actions extended or not?
}

method test_intersection(A: set<int>, B: set<int>) {
  assert |A * B| <= |A| + |B|;
  assert Proof1: |A * B| <= min(|A|, |B|) by {
    assert |A * B| <= |A| by {
      if * { 
        include_card(A * B, A);
      } else {
        ghost var witness' := |A - A * B| + |A * B|;
      }
    }
    assert |A * B| <= |B| by {
      include_card(A * B, B);
    }
  }

  assert Short_Proof2: |A * B| <= min(|A|, |B|) by {
    // ghost var witness1 := |A - A * B| + |A * B|; can be simplified
    // ghost var witness1 := |A - A * B|; // Can be omitted! Why?
    var w:=|B-A*B|; // code golf: 15 chars, Dafny version: 3.8.0.40729
  }
}

method {:print} {:test} task1(A: set<int>, B: set<int>) {
  var add := (x, y) => x + y; // lambda expression in Dafny
  // We know that
  assert |A * B| <= add(|A|, |B|);
  // But can we do better?
  // Suppose we can and denote that function as.. fun!
  ghost var fun: (int, int) -> int;
  assume fun == add; // Can you spot such function? Replace `add` with it's name.
  assert |A * B| <= fun(|A|, |B|) by {
    // Can you write some 30 chars (or less) inside this brackets to make assertion holds?
    // TODO: write code line with `line.Length == 30`
    var u:=|A-A*B|;var w:=|B-A*B|;
  }
  print "hello";
  expect 1 == 2;
}