// Also knows as secretary problem and nevesta selection problem in Russia.

include "../markov-chains/probability-def.dfy"

datatype Action = Next | Select | Stop
datatype List<T> = Nil | Cons(tail: List)
type Data = int

/**
State contains:
* All data that agent know in currect time step.
    This data must be interpreted in some optimal form to
    reduce space of non-equivalent possibilities.
Example:
* it the beginning (t = 0) agent know nothing
  Or do `possible null action` i.e. first action can be = id
* t = 1 ==> 

*/
type State = List<Data>
// + Compare values of Data

// Probability distribution
// Example: [S1 -> 0.3, S2 -> 0.7, ...]
// With property sum(probs) = 1.0
type ProbDistr = State -> Prob

const actFunc: State -> Action

// Number of all elements that agent must observe
const size: nat

// Transition function like T(s, a) in Q-learning
/**
For example:
Assume we in states S1 and S2.
* in S1 we have actions {next}
* in S2: {stop, next, jump}
Where jump is non-deterministic action (random jump to some state)

And for example we can construct one of possible transition function:
T(S1, stop) = [S1 -> 0.4, S2 -> 0.6]
  or t(S1, stop, S1) == 0.4
     t(S1, stop, S2) == 0.6
T(S1, next) = [S1 -> 0.0, S2 -> 1.0]
T(S2, next) = [S1 -> 1.0, S2 -> 0.0]


*/
const t: (State, Action, State) -> Prob
const winProb: State -> Prob

const S0: State
const S1: State

const allStates := [S0, S1];

function tsum(s: State, a: Action, slist: seq<State>): real
{
  if |slist| > 0 then t(s, a, slist[0]) + tsum(s, a, slist[1..])
  else 0.0
}

lemma NormedGeneralized(s: State, a: Action)
ensures tsum(s, a, allStates) == 1.0

lemma Normed(s: State, a: Action)
ensures t(s, a, S0) + t(s, a, S1) == 1.0
{
  assert tsum(s, a, allStates) == t(s, a, S0) + tsum(s, a, [S1]);
  NormedGeneralized(s, a);
}

lemma test()
{
  assume t(S0, Next, S1) == 1.0;
  assume t(S0, Stop, S1) == 0.0;
    assert t(S0, Stop, S0) == 1.0 by {
      assert t(S0, Stop, S0) + t(S0, Stop, S1) == 1.0 by { Normed(S0, Stop); }
    }
  // print "hello";
  // assume actFunc()
}