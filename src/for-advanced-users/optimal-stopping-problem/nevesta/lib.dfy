/**
Rules:
* Agent can store info about comparison of variants:
  State = xi <= xj
* Possible actions in { Next, Select }
* Agent can't move back to skipped variant

if length == 3:
  s0 -> Next -> s1 -> Next -> s2 -> Next
    -> Select     -> Select     -> Select
  s0 = {
    [?, ?, ?]
  }
  s1 = {
    [x0, ?, ?]
  }
  s2 = {
    H: x0 <= x1 or x1 <= x0
    [x0, x1, ?]
  }

  s3 = {
    H: x0 <= x1 <= x2 or x1 <= x0 <= x2 or ...
    [x0, x1, x2]
  }

Start:
012
021
102
120
201
210

Actions from start:
1. Next
   + Store info about order of variants
2. Select


Start -> Observe:
  gathered: {x0}, order don't changed
  knowledge_state: {} // Know nothing in context of orders (comparisons of x_i and x_j, i != j)

Start -> Select:
  win states: [201, 210]
  Now=Finish, P(win, Finish) = P(win, Now) = 2/6 = 1/3

Start -> Next:
  states: [...] = 6 states
  win states: [012, 021, 102, 120]
  P(win, Finish) <= P(win, Now) = 4/6 = 2/3

  Start -> Next -> Observe:
    gathered: {x0, x1}
    knowledge_state: {x0 < x1, x1 < x0}

  Start -> Next 
  -> Observe(x0 < x1) -> Select(x1):
    states: x0 < x1 ==> [012, 021, 120]
    win states: [021, 120]
    P(win, Now) = 2/3

  -> Observe(x1 < x0) -> Select(x1): // It's obvious that agent don't want to select in that case
    states: x1 < x0 ==> [102, 201, 210]
    win states: []
    P(win, Now) = 0/3 = 0 // zero because `x1` is 100%-ly not the best variant, but agent want best

  -> Observe(x1 < x0) -> Next:
    states: x1 < x0 ==> [102, 201, 210]
    P(win, Now) = 0/3 = 0

    
  


  ...

Pwin = [1/3, 1/3, ]
*/

// datatype Action = Next | Select | Observe

datatype state = State(nat)

const Start: state
/*
We can create Monad (structure equivalent to `composition of functions`)
Ordinary composition:
  Next(state1) = state2

Monad "composition":
1. We need to use `applyTo` instead of usual composition
  applyTo(Next, state1) = state2
2. Because composition now return a tuple, i.e. if we want chain of compositions 
  `otherstate = ...(Next(Next(Select(state))))` we can't use it
  Next(state1) = (state2, some additional info: error || message || useful things || ...)
*/
type status = nat
const Next: state -> (state, status)
const Select: state -> (state, status)

