/*
Also interesting:
* Note on a set of Simultanious Equations
  https://www.imsc.res.in/~rao/ramanujan/CamUnivCpapers/Cpaper3/page2.htm

* http://leino.science/puzzles/summing-pairs-of-numbers-to-primes/

Source:
A PROOF OF BERTRAND'S POSTULATE
Journal of the Indian Mathematical Society, _, 1919, 181-182

https://www.imsc.res.in/~rao/ramanujan/CamUnivCpapers/Cpaper24/page2.htm

1. Landau in his Handbuch, pp. 89-92, gives a proof of a
theorem the truth of which was conjectured by Bertrand:
namely that there is at least one prime `p` such that
`x < p <= 2x, if x >= 1`. Landau's proof is substantially
the same as that given by Tschebyschef.
The following is a much simpler one.

-------------------------------------------------------
Page 209
...
But it is obvious that x/6 - 3 sqrt(x) >= 0, if x >= 324.
Hence v(2x) - v(x) > 0, if x >= 162.

In other words there is at least one prime between x and 2x
if x >= 162.
Thus Bertrand's Postulate is proved for all values of x
not less than 162; and, by actual verification, we find
that it is true for smaller values.

-------------------------------------------------------
My commentaries:
How to divide work: "actual verification" into
more steps?
i.e.  Ramanujan divided it into 2 parts:
1. for x >= 162: Use symbolic proof
2. for x <= 161: Use actual verification

Maybe possible to divide:
1. for x >= 162: Use symbolic proof
2. for a1 <= x <= 161: Use ?
3. for a2 <= x <= a1: Use ?
...
N. for x <= a[N-1]: Use ?

Or we can just say:
How to beatifully(without much stupid work)
verify all cases for x <= 161?
*/

type pos = x: int | x >= 1 witness 1

function range(left: int, right: int): set<int>
{
  set x {:nowarn} | left <= x <= right :: x
}

lemma range_bounds(left: int, right: int)
requires left <= right
ensures {left, right} <= range(left, right)
{}

function divisor(d: pos, n: pos): (res: bool)
ensures d > n ==> !res
{
  assert d > n ==> forall m: pos :: d * m != n;
  exists k: pos :: d * k == n
}

lemma divisor_self(n: pos)
ensures divisor(n, n)
{
  assert n * 1 == n;
}

lemma divisor_one(n: pos)
ensures divisor(1, n)
{
  assert 1 * n == n;
}

predicate is_prime(n: int)
ensures is_prime(n) ==> n >= 2
{
  if n < 2 then false
  else forall k | k in range(2, n-1) :: !divisor(k, n)
}

// TODO: rewrite this
// Return number of primes in range
function nprimes_in_range(left: int, right: int): nat
{
  var s := set p:Prime | p in range(left, right);
  |s|
}

lemma nprimes_in_range_single(n: int)
ensures nprimes_in_range(n, n) == 1 <==> is_prime(n)
{
  // assert n in range(n, n);
  // assert is_prime(n) <==> exists p: Prime :: p in range(n, n);
  // assert H: |set x: int | x in range(n, n)| <= 1 by {
  //   var s := set x: int | x in range(n, n); 
  //   assert s == {n};
  // }
  // assert forall p:Prime :: p in range(p, p);
  var sp := set p:Prime | p in range(n, n);
  if is_prime(n) {
    assert sp == {n};
  } else {
    assert sp == {};
  }
}

// Type of prime number + infinite set of primes
type Prime = x: int | is_prime(x) witness 2
const Primes: iset<Prime>

lemma Primes_def()
ensures Primes == iset n | is_prime(n) :: n
{
  assert forall n | n in Primes :: is_prime(n);
  assume forall n | is_prime(n) :: n in Primes; // ASSUME
  assert forall p:Prime :: p in Primes;
  assert forall p:Prime :: is_prime(p);
}

predicate Bertrand_postulate(x: int)
requires x > 1
{
  exists p | p in Primes :: x < p < 2 * x
}

lemma div_less(n: pos)
ensures forall d:pos | d > n :: !divisor(d, n)
{}

function divisors(n: pos): set<pos>
{
  set x:pos | x <= n && divisor(x, n) :: x
}

method test_divs()
{
  var s := divisors(4);
  assert forall n | n >= 5 || n <= 0 :: n !in s;
  assert 3 !in s;
  assert s == {1, 2, 4} by {
    assert 1 in s by { divisor_one(4); }
    assert 2 in s by { assert 2 * 2 == 4; }
    assert 4 in s by { divisor_self(4); }
  }
  
}

method test()
{
  assert !is_prime(0);
  assert !is_prime(1);
  assert is_prime(2);
  assert is_prime(3);
  assert !is_prime(4) by {
    assert 2 * 2 == 4;
    ghost var witness' := divisor(2, 4);
  }

  assert nprimes_in_range(0, 1) == 0;
  assert nprimes_in_range(2, 2) == 1 by { nprimes_in_range_single(2); }

}

// lemma actual_verification(x: nat, bound: nat)
//   ensures x <
// {

// }