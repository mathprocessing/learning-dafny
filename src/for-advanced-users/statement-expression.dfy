// 

function method F(x: int): int
lemma AboutF(x: int)
  requires 0 <= x
  ensures 0 < F(x)

method example1()
{
  var y;
  if 0 <= y && 10 / F(y) == 2 { // possible division by zero

  }
}

method example2()
{
  var y;
  AboutF(y); // error: precondition violation (may not hold in new versions of Dafny)
  if 0 <= y && 10 / F(y) == 2 {

  }
}

method example3()
{
  var y;
  if 0 <= y && 10 / (AboutF(y); F(y)) == 2 { // ok

  }
}

// # Demonstrate `(reveal; F(y))`

function {:opaque} G(x: int): int
requires x >= 0
{
  if x >= 0 then 1
  else 0 // G(x) might be zero, i.e. `exists some_x: int, G(some_x) == 0`
}

method example4()
{
  var y;
  if 0 <= y && 10 / (AboutF(y); F(y)) == 1 / (reveal G(); G(y)) { // ok

  }
}

method example5()
{
  var y;
  if 0 <= y && 10 / (AboutF(y); F(y)) == (reveal G(); 1) / G(y) { // ok

  }
}

method example6()
{
  var y;
  if (reveal G(); 0) <= y && 10 / (AboutF(y); F(y)) == 1 / G(y) { // ok

  }
}

method example7()
{
  var y;
  reveal G(); // Work in all positions because actually applies only when it needed
  // AboutF(y); // But that didn't work in example2 (see above)
  if 0 <= y && 10 / (AboutF(y); F(y)) == 1 / G(y) { // ok

  }
}