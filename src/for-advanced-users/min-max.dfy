// Averaging Raindrops, Part 2
// Scott E. Brodie
// https://www.cut-the-knot.org/Probability/RainDropsX.shtml

function {:opaque} max(x: real, y: real): real
{
  if x > y then x else y
}

function {:opaque} min(x: real, y: real): real
{
  if x < y then x else y
}

lemma min_max_sum(x: real, y: real)
ensures min(x, y) + max(x, y) == x + y
{
  reveal min();
  reveal max();
}

lemma min_max_sum_2(x: real, y: real)
ensures min(x, y) + max(x, y) == x + y
{
  // more detailed proof
  if x < y {
    assert min(x, y) + max(x, y) == x + y by {
      assert min(x, y) == x by { reveal min(); }
      assert max(x, y) == y by { reveal max(); }
    }
  } else {
    // Notice that x + y changed to y + x
    // i.e proof can be more detailed!
    // But we can't use `+` operator,
    // and instead of it we define `add(x, y) function`
    assert min(x, y) + max(x, y) == y + x by {
      assert min(x, y) == y by { reveal min(); }
      assert max(x, y) == x by { reveal max(); }
    }
  }
}

// Note: also possible to define function `add` as constant
const Add: (real, real) -> real
//         ^^^^^^^^^^^^ in dafny we need this tuple syntax
// i.e. this do not work:
// `const Add: real -> real -> real`

function {:opaque} add(x: real, y: real): real {
  x + y
}

lemma add_comm(x: real, y: real)
ensures add(x, y) == add(y, x)
{
  reveal add();
}

lemma min_max_sum_3(x: real, y: real)
ensures add(min(x, y), max(x, y)) == add(x, y)
{
  reveal min(), max();
  if x < y {
    // assert add(x, y) == add(x, y); obvious case
  } else {
    assert add(y, x) == add(x, y) by {
      if * {
        // Proof version 1
        reveal add();
      } else {
        // Proof version 2
        if {
          // Proof version 2.1
          case true => add_comm(x, y);
          // Proof version 2.2
          case true => add_comm(y, x);
        }
      }
    }
  }
}
