predicate AllDistinct(s: seq<nat>) {
  forall i, j | 0 <= i < j < |s| :: s[i] != s[j]
}

predicate AllUnique(m: multiset<nat>)
{
  forall e | e in m :: m[e] == 1
}

// type Path = s: seq<nat> | AllDistinct(s) {
//   function next(i: nat): nat
//   {
//     i + 1
//   }

//   prev()
// }
// type Cycle = p: Path | |p| > 0 && p[0] == p[|p|-1] witness [0]
// type Cycle = p: seq<nat> | AllUniquemultiset()

datatype Path = Path(s: seq<nat>)
{
  function begin(): nat
  requires this.len() > 0
  {
    this.s[0]
  }

  function end(): nat
  requires this.len() > 0
  {
    this.s[this.len()-1]
  }

  function len(): nat {
    |this.s|
  }

  predicate isCycle()
  {
    this.len() > 0 && this.begin() == this.end()
  }

  lemma {:induction false} BeginEndEqual()
  requires this.len() == 1
  ensures this.begin() == this.end()
  {}
}

type Cycle = p: Path | p.isCycle()  witness Path([0])

/*
Path:
* First node
* Last node
  Lemmas: 
  * ShortestPath(a..b) && ShortestPath(b..c) ==> ShortestPath(a..c)

Cycle:
First node == Last node




*/

lemma test()
{
  assert multiset([1,1]) == multiset{1,1};
  assert multiset{42,42}[42] == 2;
  assert multiset({1,1}) == multiset{1};
  var p: Path := Path([0, 1, 3]);
  assert !p.isCycle();
  var p2: Path := Path([0, 1, 0]);
  assert p2.isCycle();
  // assert p
}

