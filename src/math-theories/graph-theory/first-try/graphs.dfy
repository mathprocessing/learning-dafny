type pos = x:int | x >= 1 witness 1
ghost const Size: pos;
type V = x:nat | x < Size
// type V = nat

predicate adj(x: V, y: V)

lemma loopless()
ensures forall x:V :: !adj(x, x)

lemma symm_imp()
ensures forall x:V,y:V :: adj(x, y) ==> adj(y, x)  

lemma symmetric()
ensures forall x:V,y:V :: adj(x, y) <==> adj(y, x)
{
  symm_imp();
}

lemma loopless_inst(x: V)
ensures !adj(x, x)

lemma symmetric_inst(x: V, y: V)
ensures adj(x, y) <==> adj(y, x)
{
  /*
  To prove A <-> B
  step1: A -> B
  step2: B -> A

  OR another way:

  To prove A <-> B
  step1: A -> B
  step2: !A -> B -> A
  In this way step2 can be easier to prove.
  */
  if adj(x, y) {
    assume adj(y, x);
  } else {
    if adj(y, x) {
      // assume adj(x, y);
      assume false;
    }
  }
}

// Neighbourhood
// function nh(x: V): set<V>
// {
//   // the result of a set comprehension must be finite, but Dafny's heuristics can't figure out how to produce a bounded set of values for 'y'
//   set y:V | adj(x, y)
// }
// set m,dst | dst in s && m in s[dst] :: m

function d_aux(x: V, n: V): nat
  ensures d_aux(x, n) <= n+1
{
  if n == 0 then
    if adj(x, 0) then 1 else 0
  else
  if adj(x, n) then
    // if n == 0 then 1 else
    1 + d_aux(x, n-1)
    else
    // if n == 0 then 0 else
    d_aux(x, n-1)
}

function deg(x: V): nat
  ensures deg(x) <= Size
{
  d_aux(x, Size-1)
}

lemma d_aux_less(x: V, n: V)
  ensures d_aux(x, n) <= n

lemma deg_size(x: V)
  ensures deg(x) <= Size - 1;
{
  loopless();
  assert d_aux(0, 0) == 0;
  assert forall m:V | m > 0 :: d_aux(m, m) == d_aux(m, m-1);
  assert d_aux(x, Size-1) <= Size - 1 by {
    d_aux_less(x, Size-1);
  }
  // symmetric();
  // if deg(x) == Size - 1 {
  //   assert d_aux(0, Size-1) == Size - 1;
  // }
}

method test()
{
  assume Size == 10;
  assert adj(1, 1) ==> false by {
    loopless();
  }

  assert adj(0, 1) ==> adj(1, 0) by {
    symmetric();
  }

  deg_size(0);
  assert deg(0) <= Size - 1;

  // if deg(0) == 0 {
  //   symmetric();
  //   loopless();
  //   assert !adj(0, 1);
  // }



}

// lemma (x: V, y: V)
