function f(x: nat): nat

lemma {:axiom} MulTwo(x: nat)
ensures f(x+1) == 2 * f(x)

predicate mt(x: nat)
{
  f(x+1) == 2 * f(x)
}

lemma Lemma1()
requires exists y: nat :: f(y) == 1
ensures exists z: nat :: f(z) == 2
{
  // if exists w :: f(w) == 1 {
  //   var w: nat :| f(w) == 1;
  //   assert f(w+1) == 2 by { MulTwo(w); }
  // }
  var y: nat :| f(y) == 1;
  MulTwo(y);
}

lemma AllZeros(n: nat)
requires f(0) == 0
ensures f(n) == 0
decreases n
{
  if n == 0 {} else {
    assert f(n-1) == 0 by { AllZeros(n-1); }
    assert f(n) == 2 * f(n-1) by { MulTwo(n-1); }
  }
}

// more challenging lemma for proof
lemma Lemma2()
requires exists y: nat :: f(y) > 0
ensures f(0) > 0
{
  var y: nat :| f(y) > 0;
  if f(0) == 0 {
    // We must find contradiction
    assert f(y) == 0 by { AllZeros(y); }
    // && f(y) > 0 ==> Contradiction
  }
}

lemma Lemma3()
requires f(1) == 2
ensures f(0) == 1
{
  // assert f(0) > 0 by { Lemma2(); }
  MulTwo(0);
}

lemma test()
{
  // ghost var y: nat;
  // assume forall x: nat | x < 5 :: f(x) + f(x + 1) == 0;
//   assume forall x: nat :: f(x+1) == 2 * f(x);
//   assume exists y: nat :: f(y) == 1;
//   assert exists z: nat :: f(z) == 2 by {
//     r();
//   }
// }
}
