// https://github.com/dafny-lang/dafny/blob/master/Test/wishlist/exists-b-exists-not-b.dfy

method M() {
  assert exists b : bool {:nowarn} :: b; // WISH
  assert exists b : bool {:nowarn} :: !b; // WISH
}