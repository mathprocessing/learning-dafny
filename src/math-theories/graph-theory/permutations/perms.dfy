predicate allUnique(s: seq<nat>): (res: bool)
  ensures |s| <= 1 ==> res // trivial case for better automation
{
  forall i, j | 0 <= i < j < |s| :: s[i] != s[j]
}

predicate hasInverse(s: seq<nat>): (res: bool)
  ensures |s| == 0 ==> res
{ 
  forall i | 0 <= i < |s| :: (exists j :: 0 <= j < |s| && s[j] == i)
}

predicate isPerm(s: seq<nat>): (res: bool)
{
  allUnique(s) && hasInverse(s)
}

type perm = s: seq<nat> | isPerm(s) witness []
type pos = x: int | x > 0 witness 1

// lemma helper2(p: perm, i: int)
// requires 0 <= i < |p|
// ensures exists j :: 0 <= j < |p| && p[j] == i
// {
//   var Q := (i : nat) => (exists j :: 0 <= j < |p| && p[j] == i);
//   forall i | 0 <= i < |p| ensures Q(i) {
//     assert hasInverse(p);
//     assert Q(i);
//   }
// }

lemma Helper(p: perm)
ensures forall i | 0 <= i < |p| :: (exists j :: 0 <= j < |p| && p[j] == i)
{
  forall i | 0 <= i < |p| {
    assert hasInverse(p);
    assert exists j :: 0 <= j < |p| && p[j] == i;
  }
}

// lemma SimpleCases(p: perm)
// ensures |p| == 0 ==> p == []
// ensures |p| == 1 ==> p == [0]
// {
//   if |p| == 1 {
//     assert hasInverse(p);
//     assert forall i :: 0 <= i < |p| ==> exists j :: 0 <= j < |p| && p[j] == i by { assume false; }
//     assert exists j :: 0 <= j < 1 && p[j] == 0;
//   }
// }