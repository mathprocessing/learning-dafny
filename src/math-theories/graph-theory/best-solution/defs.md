# Which solution (code patterns, architecture, layout of definitions, set of axioms) is better?
If we can use some Dafny featues and data types during writing and checking proofs, we can say that `it's better solution`.
But sometime original data structures not work, or Z3 fails, loops, and etc. And we must define new datatypes by yourself (and maybe prove that no other way exists).

# Graph definition
https://ocw.mit.edu/courses/6-042j-mathematics-for-computer-science-fall-2010/resources/lecture-6-graph-theory-and-coloring/

A graph `G` is a pair of sets `(V, E)` where
* `V` is a nonempty set of items called vertices or nodes
* `E` is a set of 2-item subsets of `V` called edges
Note: `V` is a nonempty because some theorems requires nonemptiness of graph.

# Counting graphs
https://www.maths.ed.ac.uk/~v1ranick/papers/wilsongraph.pdf
page 19

There are 8 labeled graphs with 3 vertices
There are 4 non-labeled graphs with 3 vertices

# Multipliing adjacent matrices
We can rise adj matrix into power:
`g = {(0, 1), (1, 0)}`
`pow(g, 2) = {(0 -> 1 -> 0), (1 -> 0 -> 1)} = {(0, 0), (1, 1)}`