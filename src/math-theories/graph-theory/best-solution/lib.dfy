function method abs(x: int): nat { if x >= 0 then x else -x }

// TODO: how to add requires to `rel: (nat, nat) -> bool`?
// We need to restrict `nat` to `0 <= v < size`
// And after that we can use `g1 == g2` for graphs (this helpful for sets of graphs)
// instead of using only `g1.eq(g2)`
datatype graph = G(size: nat, rel: (nat, nat) -> bool)
{
  // const loopless := forall v | 0 <= v < this.size :: !this.rel(v, v)
  predicate loopless() { forall v | 0 <= v < this.size :: !this.rel(v, v) }
  predicate empty() { this.size == 0 }

  function edges(): set<(nat, nat)>
  {
    set v1, v2 | 0 <= v1 < this.size && 0 <= v2 < this.size
      && this.rel(v1, v2) :: (v1, v2)
  }

  function input(v: int): set<nat>
  requires 0 <= v < this.size
  { set v' | 0 <= v' < this.size && this.rel(v', v) :: v' }

  function out(v: int): set<nat>
  requires 0 <= v < this.size
  { set v' | 0 <= v' < this.size && this.rel(v, v') :: v' }

  function degin(v: int): nat
  requires 0 <= v < this.size
  { |this.input(v)| }

  function degout(v: int): nat
  requires 0 <= v < this.size
  { |this.out(v)| }

  function deg(v: int): nat
  requires 0 <= v < this.size
  { this.degin(v) + this.degout(v) }

  predicate eq(other: graph) {
    this.size == other.size && 
      forall v1, v2 | 0 <= v1 < this.size && 0 <= v2 < this.size
        :: this.rel(v1, v2) <==> other.rel(v1, v2)
  }
}

const Triangle: graph := G(3, (v1, v2) => v1 != v2)
const Square := G(4, (v1, v2) => v1 != v2 && abs(v1 - v2) != 2)

function Star(n: nat): (res: graph)
ensures res.loopless()
{
  G(n + 1, (v1, v2) => v1 == 0 && v2 > 0)
}

lemma TriangleEdges(n: nat)
ensures |Triangle.edges()| == 6
{
  assert Triangle.size == 3;
  assert Triangle.edges() == {(0, 1), (0, 2), (1, 0), (1, 2), (2, 0), (2, 1)};
  // assert (0, 1) in Triangle.edges();
  // assert (0, 2) in Triangle.edges();
  // How to use symmetry of triangle graph to count edges?
  // For example:
  // |{(0, 1), (0, 2)}| = 2 = Triangle.size - 1
  // |{(1, 0), (1, 2)}| = 2 = Triangle.size - 1
  // |{(2, 0), (2, 1)}| = 2 = Triangle.size - 1
  // assert |Triangle.edges()| == (Triangle.size - 1) * 3
}

lemma SquareEdges(n: nat)
ensures |Square.edges()| == 8
{
  assert Square.edges() == {(0, 1), (0, 3), (1, 0), (1, 2), (2, 1), (2, 3), (3, 0), (3, 2)};
}

lemma StarEdges(n: nat)
ensures |Star(n).edges()| == 2 * n

// lemma test()
// {
//   assert Star(5).size == 6;
//   assert Triangle.size == 3;
//   assert Triangle.loopless;
//   assert Triangle.out(0) == {1, 2} == Triangle.input(0);
//   assert Triangle.out(1) == {0, 2} == Triangle.input(1);
//   assert Triangle.out(2) == {0, 1} == Triangle.input(2);
//   assert forall v | 0 <= v < Triangle.size :: Triangle.deg(v) == 4;
//   assert Square.loopless;
//   assert Square.size == 4;
//   assert Square.out(0) == {1, 3} == Square.out(2);
//   assert Square.out(1) == {0, 2} == Square.out(3);
// }

lemma emptyGraphsTheSame(g1: graph, g2: graph)
requires g1.empty()
requires g2.empty()
ensures g1.eq(g2)
ensures g1.edges() == g2.edges() == {}
{
  // All empty graphs equal to each other, and set of empty graphs have cardinality = 1
  // assert g1 == g2; // we can't prove that because graph.rel defined on all natural numbers
}

lemma SizeOneDeg(g: graph)
requires g.size == 1
ensures g.degin(0) <= 1
ensures g.degin(0) == g.degout(0)
ensures if g.loopless() then g.deg(0) == 0 else g.deg(0) == 2
ensures if g.loopless() then g.edges() == {} else g.edges() == {(0, 0)}
{
  if g.rel(0, 0) {
    assert g.input(0) == g.out(0) == {0};
    assert g.deg(0) == 2;
  } else {
    assert g.input(0) == g.out(0) == {};
    assert g.deg(0) == 0;
    assert g.loopless() by { // TODO: when we use g.loopless defined as const, assertion fails
      // assert forall v | 0 <= v < g.size :: !g.rel(v, v);
      // assert !g.rel(0, 0);
    }
  }
}