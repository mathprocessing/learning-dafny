/* (MST) Minimum spanning tree definition:
Mst = set of edges (subset of graph G) such that sum of all it's weights minimal and all edges forming a tree (connected graph without cycles) that connects all nodes of G.

Example: 
if G = [{A, B, 1}, {A, C, 2}, {B, C, 1}] then
  Hyps:
    EDGES(mst(G)) == NODES(G) - 1 from def of mst
                  == 2

    EDGES(mst(G)) != 1 because mst must contain all vertices of G i.e. NODES(mst(G)) = NODES(G)
  

    mst(G) != [{B, C, 1}, {A, C, 2}] because exist other ST that have less sum of edge weights, sum = 3
    mst(G) != [{A, B, 1}, {A, C, 2}] because exist other ST that have less sum of edge weights, sum = 3

  mst(G) = [{A, B, 1}, {B, C, 1}], sum = 2


Algorithm for graph with one cycle {
  1. delete one edge with maxumum weight
}

Algorithm for graph with two non-intersected cycles {
  Graph not connected ==> error
}

Algorithm for graph with two intersected cycles {
  Note: We can't delete any edge, we can delete only edge that not in "bridge" of graph
  1. delete one edge with maxumum weight in cycle 1
    + check that graph stay connected
  2. delete one edge with maxumum weight in cycle 2
    + check that graph stay connected
}
*/