```ts
// mst.ts
export type VertexId = number;
export type Edge = [VertexId, VertexId];
export type WeightedEdge = { weight: number, edge: Edge };
export type Graph = WeightedEdge[];
export type Matrix = number[][];
const INF: number = Infinity;
// Theprem: For any cycle C in the graph, if the weight of an edge e of C is larger than the individual weights of all other edges of C, then this edge cannot belong to an MST.
// Link to theorem: https://en.wikipedia.org/wiki/Minimum_spanning_tree#Cycle_property
// https://e-maxx.ru/algo/mst_kruskal
export function kruskal(g: Graph) {
  let n = countVertices(g); /* amount of vertices */
  let m = g.length; // amount of edges
  let cost: number = 0;
  const res: Edge[] = [];
  sortEdges(g);

  let tree_id : VertexId[] = new Array(n).fill(0);
  for (let i = 0; i < n; i++) {
    tree_id[i] = i; // for all vertices
  }
  for (let i = 0; i < m; i++)
  { // for all sorted edges
    let weighedEdge = g[i];
    let a: VertexId = weighedEdge.edge[0];
    let b: VertexId = weighedEdge.edge[1];
    let l: number = weighedEdge.weight;
    if (tree_id[a] !== tree_id[b])
    { // apply theorem: forall cycle in G, maxWeight edge `NotIn` MST
      // MST = Minimum Spanning Tree
      cost += l;
      res.push([a, b] as Edge);
      let old_id = tree_id[b];
      let new_id = tree_id[a];
      for (let j = 0; j < n; j++)
      {
        if (tree_id[j] === old_id) {
          tree_id[j] = new_id;
        }
      }
    }
  }
  return res;
}

// Prim's algorithm useful for euclid metric space.
// https://e-maxx.ru/algo/mst_prim  O(n^2)
export function prims1(g: Graph) {
  // input values
  let n = countVertices(g);
  let matrix = getAdjacencyMatrix(g, n);
  // algorithm
  let used: boolean[] = new Array(n).fill(false);
  let min_e: number[] = new Array(n).fill(INF);
  let sel_e: number[] = new Array(n).fill(-1);
  min_e[0] = 0;
  
  console.log(min_e, sel_e);
  // TODO: add code
  return showMatrix(matrix);
}

// https://e-maxx.ru/algo/mst_prim  O(m * log n)
export function prims2(g: Graph) {

}

// from observablehq.com
export function prims3(g: Graph) {

}

function createMatrix(rows: number, columns: number, value = 0): Matrix {
  let matrix = [];
  for (let rowIndex = 0; rowIndex < rows; rowIndex++) {
    let row = [];
    for (let columnIndex = 0; columnIndex < columns; columnIndex++) {
      row.push(value);
    }
    matrix.push(row);
  }
  return matrix;
}

function showMatrix(matrix: Matrix): string[][] {
  return matrix.map(row => row.map(x => x === INF? "∞" : x.toString()));
}

export function getAdjacencyMatrix(g: Graph, n: number): Matrix {
  let matrix = createMatrix(n, n, INF);
  for (let i = 0; i < n; i++) {
    matrix[i][i] = 0;
  }
  for (const weightedEdge of g) {
    let v1: VertexId = weightedEdge.edge[0];
    let v2: VertexId = weightedEdge.edge[1];
    matrix[v1][v2] = weightedEdge.weight;
  }
  // make Symmetrical Matrix
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < n; j++) {
      let elem = matrix[j][i];
      if (elem !== INF) {
        matrix[i][j] = elem;
      }
    }
  }
  
  return matrix;
}

export function countVertices(graph: Graph) {
  let maxId = -1;
  for (const weightedEdge of graph) {
    const [v1, v2] = weightedEdge.edge;
    const maxOfPair = Math.max(v1, v2);
    if (maxOfPair > maxId) {
      maxId = maxOfPair;
    }
  }
  return maxId + 1;
}

function sortEdges(g: Graph) {
  g.sort((e1: WeightedEdge, e2: WeightedEdge) =>
    e1.weight < e2.weight ? -1 : 1);
}
```

```ts
// main.ts
import {
  kruskal, prims1, prims2,
  VertexId, Edge, WeightedEdge, Graph,
  countVertices, getAdjacencyMatrix
} from './mst';

function* range(start: number, end: number, step = 1) {
  for (let i = start; i <= end; i += step) yield i;
}

// https://en.wikipedia.org/wiki/Kruskal%27s_algorithm
const [A, B, C, D, E, F, G] = [...range(0, 6)];
let g: Graph = [
  { weight: 7,  edge: [A, B] },
  { weight: 5,  edge: [A, D] },
  { weight: 8,  edge: [B, C] },
  { weight: 9,  edge: [B, D] },
  { weight: 7,  edge: [B, E] },
  { weight: 5,  edge: [C, E] },
  { weight: 15, edge: [D, E] },
  { weight: 6,  edge: [D, F] },
  { weight: 8,  edge: [E, F] },
  { weight: 9,  edge: [E, G] },
  { weight: 11, edge: [F, G] },
]

const n = countVertices(g);
console.log('amountOfVertices =', n);
console.log(prims1(g));

let tree = kruskal(g);
console.log("tree =\n",
  verticesAsChars(tree)
);


function hasDuplicateEdges(graph: Graph) {
  // if graph contains [A, B] and [B, A] then true
}

const Alfabet = [...range(0, 25)].map(i => String.fromCharCode(i + 65)).join('');
function showVertex(id: VertexId) : string {
  return String.fromCharCode(id + 65); // or Alfabet.charAt(id);
}

function verticesAsChars(mstree: Edge[]) {
  let newTree = [];
  for (let edge of mstree) {
    let newEdge = edge.map(showVertex);
    newTree.push(newEdge)
  }
  return newTree;
}
```