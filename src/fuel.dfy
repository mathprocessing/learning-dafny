// https://github.com/dafny-lang/dafny/blob/1d68bcd406b6f08b23e7e16d86330ce03f163fc1/Test/dafny4/Bug93.dfy

module Fuel {
    function FunctionA(x:int) : int
    {
        x + 2
    }

    function FunctionB(y:int) : int
    {
        FunctionA(y - 2)
    }

		method {:fuel FunctionA,0,0}  MethodX(z:int)
    {
        assert FunctionB(z) == z; // error: Cannot see the body of FunctionA
    }
}

module Opaque {
    function {:opaque} FunctionA(x:int) : int
    {
        x + 2
    }

    function FunctionB(y:int) : int
    {
        FunctionA(y - 2)
    }

    method MethodX(z:int)
    {
        assert FunctionB(z) == z;
    }
}