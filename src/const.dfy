type Vertex = int

const i: bv4 := 9
const j: bv4 := 3
const v: Vertex
const x: bool
const y: bool

method m()
  // requires X, ensures Y <=> ensures X ==> Y + requires change state of body
  requires y == false
{
  assert y ==> !x; // y == false ==> (y ==> !x)
  assert (!x ==> x) ==> (x == true);

  assert (x == false) ==> ((!x == x) <==> (true == false));
  // Тут мы применили правило подстановки
  // И чтобы его верифицировать проверки формулы не достаточно:
  // При условии что формула верная
  // работа алгоритма подстановки может быть неверной
  // Пример:
  assert (x == false) ==> ((!x == x) <==> x);
  // (!x == x) <==> x имеет слишком большое расстояние от x == false
  // потому что кроме подстановки применены еще кое-какие операции
  // а нам нужно для корректности алгоритма чтобы distance было равно 1

  assert (i & j) == (1 as bv4);
  assert (i | j) == 11;

}