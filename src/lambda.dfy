// https://github.com/dafny-lang/dafny/blob/master/Test/hofs/Lambda.dfy

method M<A>()
{
  var f1 : A -> A := x => x;
  var flip : (A, A) -> (A, A) := (x, y) => (y, x);
  assert flip((1, 2)) == (2, 1);
  // TODO: how to make a tuple?
}