const k: nat

// `function fib(n: nat) : nat`
// is a 'ghost' object and can't be used directly in method
// but `function method fib(n: nat) : nat` can
function method fib(n: nat) : nat
  requires k == 0
  requires 0 <= n
  ensures 0 <= fib(n) - k
{
  if n < 2 then n else fib(n-2) + fib(n-1)
}

method using(n: nat) returns (r: nat) 
  requires k == 0 // else we got error: 
  // possible violation of function precondition
{
  return fib(n);
}

// method cfib(n: nat) returns (r: nat) 
// {
//   if (n < 2) {
//     return n;
//   } else {
//     return cfib(n-2) + cfib(n-1);
//   }
// }