// datatype U = U (Left (x : int)) (Right x)


// type OrderedPair = (x :int, y: int) | x <= y
datatype Pair = P(x: nat, y: nat)

/**

9
58
247
0136

(x, y) = 1 + (x+1, y-1)
i.e.
(x, y) = y + (x+y, 0)
(x, 0) = x*(x+1) / 2

Pair   -> Nat
(x, y) -> y + (x+y)*(x+y+1) / 2

to_number(0, 0) = 0
to_number(0, 1) = 2
to_number(0, 2) = 5
to_number(0, 3) = 9

to_number(1, 0) = 1
to_number(2, 0) = 3
to_number(3, 0) = 6

*/
function to_number(p: Pair): nat
requires p.x <= p.y
{
  ghost var s := p.x+p.y;
  p.y + s*(s+1) / 2
}

method to_num(x: nat, y: nat) returns (r: nat)
{
  var s := x + y;
  r := y + (s * (s + 1) / 2);
}

function to_numf(x: nat, y: nat): nat
{
  ghost var s := x + y;
  y + (s * (s + 1) / 2)
}

method test_to_number()
{
  // assert to_number(P(0, 0)) == 0;
  // assert to_number(P(0, 1)) == 1;
}

method test_to_num()
{
  var i:nat := to_num(0, 0);
  // assert i == 0; // Fails
  assert i >= 0;

  assert to_numf(0, 0) == 0; // inf loop
  // assert to_num(0, 1) != 0;
  assert 3 / 2 == 1;
  assert 4 / 2 == 2;
  assert 5 / 2 == 2;
  assert 1 / 2 == 0;
}




