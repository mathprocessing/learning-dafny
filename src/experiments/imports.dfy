module A {
  function inc(x:nat): nat
  {
    x + 1
  }
}

module B {
  import A

  lemma inc_zero()
    ensures A.inc(0) == 1
  {
    
  }
}

module A2 {
  // We can use simple `export name` means export as name
  // export name
  //   reveals inc

  // or we can create default export
  export reveals inc, G

  type G = nat

  function inc(x:nat): nat
  {
    x + 1
  }
}

module C {
  import opened A

  lemma inc_one()
    ensures inc(1) == 2
  {
    
  }
}

module D {
  import opened A2

  lemma inc_step(n: G)
    ensures inc(n) == n+1
  {
    
  }
}