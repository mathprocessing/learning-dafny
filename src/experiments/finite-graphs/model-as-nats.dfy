
type pos = x:int | 0 < x witness 1
const size: pos
type V  = x:int | 0 <= x < size
type flag = c:int | 0 <= c <= 1

function r(a: V, b: V): flag
ensures a == b ==> r(a, b) == 0

function degin_aux(a: V, minbound: nat): nat
decreases size - minbound
{
  if minbound < size then
    r(minbound, a) + degin_aux(a, minbound + 1)
  else 0
}

function degout_aux(a: V, minbound: nat): nat
decreases size - minbound
{
  if minbound < size then
    r(a, minbound) + degout_aux(a, minbound + 1)
  else 0
}

function degin(a: V): nat { degin_aux(a, 0) }
function degout(a: V): nat { degout_aux(a, 0) }
function deg(a: V): nat { degin(a) + degout(a) }

lemma size_1()
requires size == 1
ensures forall v:V :: deg(v) == 0
{
  forall v:V ensures deg(v) == 0 {
    //     ^ without type V dafny can't prove that
    assert v < size;
    if v == 0 {
      assert deg(0) == 0;
    }
  }
}

lemma size_2()
requires size == 2
ensures forall v, u :: deg(v) == deg(u)
{
  forall v:V, u:V ensures deg(v) == deg(u) {
    if v == u {
      // obviously
    } else {
      assert v != u;
      assert v < size;
      assert u < size;
      // if v == 0 && u == 1 {       
      //   assert deg(v) == deg(u);
      // } else {
      //   assume false;
      // }
    }
  }
  assert forall v:V, u:V :: deg(v) == deg(u);

  ghost var x:V := 0;
  ghost var y:V := 1;
  assert deg(x) == deg(y) by {
    // Funny thing:
    // In that order z3 can solve this within 3 seconds:
    // assert deg(0) == 2 ==> deg(1) == 2;
    // assert deg(0) == 0 ==> deg(1) == 0;
    // But in that order time of solution > 3 seconds:
    // assert deg(0) == 0 ==> deg(1) == 0;
    // assert deg(0) == 2 ==> deg(1) == 2;
  }
}

lemma hh(f: V -> nat)
requires exists val: nat :: (forall u :: f(u) == val)
ensures forall a:V, b:V :: f(a) == f(b)
{}

lemma size_3_sym()
requires size == 3
requires exists val : nat {:trigger deg} :: (forall u :: deg(u) == val)
requires forall a,b :: r(a, b) == r(b, a)
{
  // How to make that `hh` see second requires
  hh(deg);
  // if deg(0) == 0 {
  //   assert deg(1) == 0;
  //   assert deg(2) == 0;
  // }
    // assert deg(0) == 0 || deg(0) == 4;
}