/*
Properties of graph structure:
1. For each node we have maximal node
2. forall u, v : node :: !(u <-> v)
*/

const Size: nat

type node = n: nat | n < Size witness *

function best(n: node): node

lemma noPair(a: node, b: node)
ensures !(best(a) == b && best(b) == a)

lemma noSelf(a: node)
ensures best(a) != a

lemma SizeOne()
requires Size == 1
ensures false
{
  noSelf(0);
}

lemma SizeTwo()
requires Size == 2
ensures false
{
  noSelf(0);
  noSelf(1);
  noPair(0, 1);
}

lemma SizeThree()
requires Size == 3
ensures forall a: node :: (exists b: node :: best(b) == a)
{
  noPair(0, 1);
  noPair(0, 2);
  noPair(1, 2);
  assert best(0) != 0 by { noSelf(0); }
  assert best(1) != 1 by { noSelf(1); }
  assert best(2) != 2 by { noSelf(2); }
  if case best(0) == 1 => { // Solution 1
    assert best(1) == 2;
    assert best(2) == 0;
  } case best(1) == 0 => { // Solution 2
    assert best(0) == 2;
    assert best(2) == 1;
  }
}