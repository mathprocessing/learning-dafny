

// datatype V = A | B | C
type V = nat

predicate {:verify false} R(a: V, b: V)
ensures forall v:V :: !R(v, v)

// We need operation on graphs: subtract vertex to give definition
function degin(a: V): nat
function degout(a: V): nat

function deg(a: V): nat
{
  degin(a) + degout(a)
}

predicate is_cycle()
{
  forall v: V :: degin(v) == degout(v) == 1
}

predicate {:verify false} connected(a: V, b: V)
{
  a == b || exists c:V :: connected(a, c) && R(c, b)
}

lemma connected_self()
ensures forall v:V :: connected(v, v)
{}

lemma connected_near()
ensures forall v, u :: R(v, u) ==> connected(v, u)
{}

lemma connected_two(v: V)
ensures forall k, m :: R(k, v) && R(v, m) ==> connected(k, m);
{
  forall k, m ensures R(k, v) && R(v, m) ==> connected(k, m) {
    if R(k, v) && R(v, m) {
      assert exists c:V :: connected(k, c) && R(c, m) by {
        assert R(k, v) ==> connected(k, v);
        assert connected(k, v) && R(v, m);
      }
    }
  }
}

// distance have meaning only if nodes a and b are connected ==> we use requires for that
function {:verify false} distance(a: V, b: V): nat
requires connected(a, b)
ensures forall a:V :: distance(a, a) == 0;
ensures forall a, b :: R(a, b) ==> distance(a, b) == 1;
// {
  // if a == b then 0 else 
  // if R(a, b) then 1 else
  // min(forall k:V | R(k, b) :: distance(a, k)) + 1
// }

lemma trans_helper(a: V, b: V)
requires connected(a, b)
requires a != b
ensures exists k:V | distance(k, b) == 1 :: distance(a, k) == distance(a, b) - 1
{
  assume exists k:V :: connected(a, k) && R(k, b);
  assert forall a:V :: distance(a, a) == 0;
}

lemma connected_trans(a: V, b: V, c: V)
ensures connected(a, b) ==> connected(b, c) ==> connected(a, c)
decreases distance(b, c)
{
  if connected(a, b) && connected(b, c) {
    if R(b, c) {
    } else {
      assert exists k:V :: connected(b, k) && R(k, c) by {
        var k :| connected(b, k);
        assert distance(b, k) == distance(b, c) - 1 ==> connected(a, c) by {
          connected_trans(a, b, k);
        }
        assume false;
      }
    }
  }
}

lemma {:verify false} connected_trans_1(a: V, b: V, c: V)
ensures connected(a, b) ==> connected(b, c) ==> connected(a, c)
{
  if a == b {} else {
    if b == c {} else {
      if a == c {} else {
        assert a != b && b != c && a != c;
        if connected(a, b) && connected(b, c) {
          // connected(a, b) == R(a, k1) && R(k1, k2) && ... R(k, b)
          // connected(b, c) == R(b, m1) && R(m1, m2) && ... R(m, c)
          // apply and_comm to connected(b, c)
          // connected(b, c) == R(b, m1) && connected(m1, c)

          // Rewrite connected(b, c):
          assume exists m1: V :: R(b, m1) && connected(m1, c);

          // Rewrite connected(a, b):
          assert exists klast:V :: connected(a, klast) && R(klast, b);

          // Goal
          assert connected(a, c) by {
            
            assert (exists m1, klast :: connected(a, klast) && R(klast, b) && R(b, m1) && connected(m1, c)) ==> 
            (exists m1, klast :: connected(a, klast) && connected(klast, m1) && connected(m1, c))
            by {
              assert forall k, m :: R(k, b) && R(b, m) ==> connected(k, m) by {
                connected_two(b);
              }
            }

            assert exists m1, klast :: connected(a, klast) && connected(klast, m1) && connected(m1, c);
            // Simplify using connected_trans induction
            assert exists m1 :: connected(a, m1) && connected(m1, c);
            // assert connected(a, c);
          }
        }
      }
    }
  }
}

/**
axiom connected_def:
  forall a b :: connected(a, b) <==> (a == b || exists k :: connected(a, k) && R(k, b))

Let's connected_trans already proved for graph with n nodes
IH: forall a b c, a != x, b != x, c != x :: connected(a, b) ==> connected(b, c) ==> connected(a, c)

Then our goal is:
|- forall a b c :: connected(a, b) ==> connected(b, c) ==> connected(a, c)

intros a b c.
|- connected(a, b) ==> connected(b, c) ==> connected(a, c)

if a == c then
  |- connected(a, a)
    by connected_def: "connected(a, a) <==> a == a || ..."

if a == b then
  |- connected(b, c) ==> connected(b, c)
    by id

if b == c then
  |- connected(a, b) ==> connected(a, b)
    by id

assert a != b && a != c && b != c;

Possible 3 cases:
1) a == x && b != x && c != x
2) b == x && a != x && c != x
3) c == x && a != x && b != x
  by ...

if a == x then
  IH: forall a' b' c', a' != x, b' != x, c' != x :: connected(a', b') ==> connected(b', c') ==> connected(a', c')
  |- connected(x, b) ==> connected(b, c) ==> connected(x, c)
  
  replace connected(x, b) with (x == b || exists k :: connected(x, k) && R(k, b))
                      ... with "exists k :: connected(x, k) && R(k, b)"

  |- (exists k :: connected(x, k) && R(k, b)) ==> connected(b, c) ==> connected(x, c)

  if k == x:
    |- (connected(x, x) && R(x, b)) ==> connected(b, c) ==> connected(x, c)
    |- R(x, b) ==> connected(b, c) ==> connected(x, c)
  else k != x:
    |- (exists k :: connected(x, k) && R(k, b)) ==> connected(b, c) ==> connected(x, c)
    from (IH x k c) we have:
    H2 : connected(x, k) ==> connected(k, c) ==> connected(x, c)
  TODO: write proof

if b == x then
  |- forall a c :: connected(a, x) ==> connected(x, c) ==> connected(a, c)

if c == x then
  |- forall a b :: connected(a, b) ==> connected(b, x) ==> connected(a, x)


*/