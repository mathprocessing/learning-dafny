datatype V = A | B | C

predicate {:verify false} R(a: V, b: V)
ensures forall v:V :: !R(v, v)

// We need operation on graphs: subtract vertex to give definition
function degin(a: V): nat
function degout(a: V): nat

function deg(a: V): nat
{
  degin(a) + degout(a)
}

predicate is_cycle()
{
  forall v: V :: degin(v) == degout(v) == 1
}

// First definition  of connected (Not enough for graphs, possible some strange counterexamples with "ghost" connections)
// predicate {:verify false} connected(a: V, b: V)
// {
//   a == b || exists c:V :: connected(a, c) && R(c, b)
// }

// Second definition of connected (maybe good one)
predicate {:verify false} connected(a: V, b: V)
{
  a == b || ( (exists k:V {:trigger R} :: connected(a, k) && R(k, b)) && (exists u:V {:trigger R} :: R(a, u) && connected(u, b)) )
}

method test_good_def_for_connected()
requires connected(A, B)
ensures R(A, B) || R(A, C)
{
  if R(A, B) == R(A, C) == false {
    // assert R(B, C) && R(C, B); // from definition 1
    assert false;
  } else {}
}

lemma connected_self()
ensures forall v:V :: connected(v, v)
{}

lemma connected_near()
ensures forall v, u :: R(v, u) ==> connected(v, u)
{}

lemma connected_by_one(v: V)
ensures forall k, m :: R(k, v) && R(v, m) ==> connected(k, m);
{
  forall k, m ensures R(k, v) && R(v, m) ==> connected(k, m) {
    if R(k, v) && R(v, m) {
      assert exists c:V :: connected(k, c) && R(c, m) by {
        assert R(k, v) ==> connected(k, v);
        assert connected(k, v) && R(v, m);
      }
    }
  }
}

// This lemma can't be proved from first definition of `connected` (See above `predicate connected`)
/**
Legend: 
  connected: "--->"
          R: "->"

Counterexample:
if connected(A, B):
  A ---> B ---> C
  A ---> C ---> B
  A !-> B i.e. R(A, B) = false
  A !-> C i.e. R(A, C) = false
  B -> C and C -> B

  We can't prove `exists k:V :: R(A, k) && connected(k, B))`
  because k may exists or may not exists. (Really funny thing)
  i.e. We don't have enough information to be able to select one of this two cases.
*/
lemma {:verify false} connected_def_left_1(a: V, b: V)
ensures connected(a, b) ==> (a == b || exists k:V :: R(a, k) && connected(k, b))
{
  if connected(a, b) {
    if a == A && b == B {
      assert A != B;
      assert !R(A, A);
      // from connected(a, b) we have by defenition
      assert exists u:V :: connected(A, u) && R(u, B) && u != B;
      if connected(A, A) && R(A, B) {

      } else {
        assert connected(A, C) && R(C, B) && C != B;
        ghost var r1 := R(A, C);
        ghost var r2 := connected(C, B);
        ghost var right := R(A, C) && connected(C, B);

        assert connected(C, B);
        if R(A, C) {} else {
          assert !R(A, C);
          // Expanded exists from goal
          assert (R(A, A) && connected(A, B)) || (R(A, B) && connected(B, B)) || (R(A, C) && connected(C, B)) by {
            assert connected(B, B);
            if R(A, B) {} else {
              // Counterexample
              assert !R(A, C) && !R(A, B);
              assert connected(B, C) && connected(C, B);
              assert connected(A, B);
              // Expand connected(A, C) defintion
              assert connected(A, C);
              assert exists u:V :: connected(A, u) && R(u, C) && u != C;
              if R(A, C) { // u == A
                assert false;
              } else { // u == B
                assert connected(A, B) && R(B, C);
                assert !R(A, C) && !R(A, B);
                assert R(C, A) ==> connected(B, A);
                if connected(B, A) {
                  assert R(C, A) || R(B, A);
                } else {
                  assert R(C, A) == R(B, A) == false;
                  assert !connected(C, A);
                }
                assert connected(C, A) <==> (R(C, A) || R(B, A));
              }
            }
          }
        }

        // ghost var left := R(A, B) && connected(B, B);
        // assert forall k:V | k != A :: !(R(a, k) && connected(k, b));
        // assert left == R(A, B) by { assert connected(B, B); }
        // ghost var some := left || right;
      }

    } else {
      assume false;
    }
  }
}

lemma connected_def_left_2(a: V, b: V)
ensures connected(a, b) ==> (a == b || exists k:V :: R(a, k) && connected(k, b))
{
  if connected(a, b) {
    if (a == b) {} else {
      // Dafny needs ONE of this asserts (i.e. [0, 1] || [1, 0])
      if * {
        assert exists k:V :: connected(a, k) && R(k, b);
      } else {
        assert exists u:V :: R(a, u) && connected(u, b);
      }
    }
  }
}

method test_connected(a: V, b: V)
requires connected(A, B);
{
  assert !R(A, B) ==> R(A, C);
  assert !R(A, C) ==> R(A, B);
}

lemma connected_def_2()
{

}

// lemma connected_trans(a: V, b: V, c: V)
// ensures connected(a, b) ==> connected(b, c) ==> connected(a, c)
// {}

