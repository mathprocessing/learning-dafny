// This line added because `Parse error` happends on `type` keyword (may be it's a bug)
type Prob = x: real | 0.0 <= x <= 1.0

function imax(x: int, y: int): int
{
  if x <= y then y else x
}

function min(x: real, y: real): real
{
  if x <= y then x else y
}

function min_range(lo: nat, hi: nat, func: nat -> Prob): Prob
  requires lo <= hi
  decreases hi - lo
{
  if lo == hi then
    func(lo)
  else
    min(func(lo), min_range(lo + 1, hi, func))
}

/*
Game sticks:
f n = max_seq(max(0, n-3) <= i <= n-1, f(n))
f 0 =  0
f 1 =  1 - min(f 0)
f 2 =  1 - min(f 0, f 1) 
f 3 = 1 - min(f 0, f 1, f 2)
*/
function {:verify false} f(n: nat): Prob
  decreases n
  // requires min_range(0, 0, (x : nat) => f(x)) == f(0)
{
  if n == 0 then 0.0
  else
  1.0 - min_range(imax(0, n-3), n-1, (x : nat) => f(x))
}

lemma min_prob_zero()
ensures forall p:Prob :: min(0.0, p) == 0.0;
{
}

lemma min_range_zero(lo:nat, hi:nat, func: nat -> Prob)
requires lo <= hi
requires exists n | lo <= n <= hi :: func(n) == 0.0
ensures min_range(lo, hi, func) == 0.0
decreases hi - lo
{
  if lo == hi {
    assert func(lo) == 0.0;
  } else {
    assert min(func(lo), min_range(lo + 1, hi, func)) == 0.0 by {
      if func(lo) == 0.0 {
        min_prob_zero();
      } else {
        assert func(lo) > 0.0;
        assert exists n | lo + 1 <= n <= hi :: func(n) == 0.0;
        min_range_zero(lo + 1, hi, func);
      }
    }
  }
}

lemma min_range_gt_value(lo: nat, hi: nat, func: nat -> Prob, value: Prob)
requires lo <= hi
requires forall n | lo <= n <= hi :: func(n) >= value // Hreq
ensures min_range(lo, hi, func) >= value
decreases hi - lo
{
  if lo == hi {
    assert func(lo) >= value; // from Hreq
    assert min_range(lo, lo, func) == func(lo); // by definition of min_range
  } else {
    min_range_gt_value(lo + 1, hi, func, value);
  }
}

lemma min_constraint(Q: real -> bool, x: real, y: real)
requires Q(x)
requires Q(y)
ensures Q(min(x, y))
{
  if min(x, y) == x {
    // qed by
    assert Q(x);
  } else {
    assert min(x, y) == y by { assert min(x, y) == x || min(x, y) == y; }
    // qed by
    assert Q(y);
  }
}

lemma imax_right(x: int, y: int)
requires x <= y
ensures imax(x, y) == y
{}

// for example we can hide imax by setting :fuel parameter to 0
lemma {:fuel imax,0} {:induction n} f_zero_or_one(n: nat)
// Question: What if we mutate target goal and see how resulting proof also changes?
// In some cases to create better proof (more general proof) we must guarantee that this proof will be (can be) stable on some set of goal changes.
ensures f(n) == 0.0 || f(n) == 1.0
decreases n
{
  if n <= 3 {
    // trivial, but if we set {:fuel imax,0} then this becomes non-trivial case
    assume imax(0, n-3) == 0;
    // Idea: In general one can force yourself|solver to make more detailed proofs by setting :fuel to lower values
  } else {
    ghost var P := (r:real) => r == 0.0 || r == 1.0;
    assert P(f(n-1)) && P(f(n-2)) && P(f(n-3)) && P(f(n-4)) by {
      // by decreases n
      f_zero_or_one(n-1);
      f_zero_or_one(n-2);
      f_zero_or_one(n-3);
      // f_zero_or_one(n-4); It's true but we don't need it
    }
    assert P(f(n)) by {
      forall k: nat {:trigger P(f(n))} | k >= 4 ensures P(f(k-3)) && P(f(k-2)) && P(f(k-1)) ==> P(f(k)) {
        calc <==> {
          P(f(k));
          // by definition of `f`
          {
            // we need to reveal imax because it have :fuel 0
            assert imax(0, k-3) == k-3 by {
              // reveal imax(); doesn't work because needs `:opaque` attribute
              // But we can use lemma `imax_right`
              imax_right(0, k-3);
            }
          }
          P(1.0 - min_range(k-3, k-1, (x : nat) => f(x)));
              { assert forall r: real :: P(r) <==> P(1.0 - r); }
          P(min_range(k-3, k-1, (x : nat) => f(x)));
          // by definition of `min_range`
          P(min(f(k-3), min_range(k-2, k-1, (x : nat) => f(x))));
          <==
              { min_constraint(P, f(k-3), min_range(k-2, k-1, (x : nat) => f(x))); }
          P(f(k-3)) && P(min_range(k-2, k-1, (x : nat) => f(x)));
          // by definition of `min_range`
          P(f(k-3)) && P(min(f(k-2), f(k-1)));
          <==
              { min_constraint(P, f(k-2), f(k-1)); }
          P(f(k-3)) && P(f(k-2)) && P(f(k-1));
        }
        assert P(f(k-3)) && P(f(k-2)) && P(f(k-1)) ==> P(f(k));
      }
    }
  }
}

lemma test(p: Prob)
{
  assert p <= 1.0;
  assert p >= 0.0;
}

lemma f_compute()
ensures forall n:nat :: f(n) == if n % 4 == 0 then 0.0 else 1.0
{
  ghost var inductionStep := forall m:nat :: (forall k | imax(0, m-3) <= k <= m-1 :: f(k) == 1.0) ==> f(m) == 0.0;
  // assert inductionStep by {
  //   forall m:nat ensures (forall k | imax(0, m-3) <= k <= m-1 :: f(k) == 1.0) ==> f(m) == 0.0 {
  //     if m <= 3 {
  //       // trivial case
  //       if m == 0 {
  //         assert f(0) == 0.0;
  //       } else {
  //         // Just idea, we don't need this branch to prove inductionStep
  //         calc == {
  //           f(m);
  //               { assert imax(0, m - 3) == 0; }
  //           1.0 - min_range(0, m-1, (x:nat) => f(x));
  //               { assert f(0) == 0.0; min_range_zero(0, m-1, (x:nat) => f(x)); }
  //           1.0;
  //         }
  //       }
  //     } else { // m >= 4
  //       assert imax(0, m-3) == m - 3;
  //       if forall k | m-3 <= k <= m-1 :: f(k) == 1.0 { // intro
  //         assert f(m) == 0.0 by {
  //           // `f` replaced by `(x:nat) => f(x)` because:
  //           // Proof of using: Try to replace `(x:nat) => f(x)` by `f` and you can see that z3 can't prove that
  //           assert f(m) == 1.0 - min_range(m-3, m-1, (x:nat) => f(x));
  //           assert min_range(m - 3, m - 1, (x:nat) => f(x)) == 1.0 by {
  //             min_range_gt_value(m - 3, m - 1, (x:nat) => f(x), 1.0);
  //             assert forall k | m-3 <= k <= m-1 :: f(k) == 1.0;
  //           }
  //         }
  //       }
  //     }
  //   }
  // }

  // assert f(n) == 1.0 ==> f(n+1) == 1.0 by {
  //     assert f(n) == 1.0 - min_range(imax(0, n-3), n-1, (x : nat) => f(x));
  //     assert f(n) == 1.0 - min_range(imax(0, n-3), n-1, (x : nat) => f(x));
      
  //   }

  ghost var P: nat -> bool := (n:nat) => f(n) == 0.0;
  assert P(0) by { assert f(0) == 0.0; }
  assert !P(1) by { assert f(1) == 1.0; }
  assert !P(2) by { assert f(2) == 1.0; }
  assert !P(3) by { assert f(3) == 1.0; }
  assert forall m:nat :: P(m) == P(m + 4) by {
    // Induction step
    assume false;
  }
  induction_step4_hard(P); // this induction is hard for z3 without lemma
  assert forall n:nat :: P(n) <==> n % 4 == 0;
  forall n:nat ensures f(n) == if n % 4 == 0 then 0.0 else 1.0 {
    assert forall n:nat :: P(n) <==> f(n) == 0.0;
    if n % 4 == 0 {
      assert P(n);
      assert f(n) == if true then 0.0 else 1.0;
    } else {
      assert f(n) == 1.0 by {
        f_zero_or_one(n);
      }
    }
  }

  // Why method below not works (timed out)?
  // forall n:nat ensures P(n) <==> n % 4 == 0 {
  //   // to prove that P(n) is true for all n:nat we must down to P(0), i.e. move from P(n-1) to P(0)
  //   // or move up from P(0) to P(n-1)
    
  //   ghost var i := 0;
  //   while i < n 
  //     invariant i % 4 == 0
  //     invariant i <= n + 3
  //     invariant P(i) && !P(i+1) && !P(i+2) && !P(i+3)
  //   {
  //     // min_range_gt_value(i - 3, i - 1, (x:nat) => f(x), 1.0);
  //     // assert f(i-4) == 0.0 ==> f(i) == 0.0;
  //     i := i + 4;
  //   }
  // }
}

predicate P(n:nat)

method test_induction()
{
  assume P(0);
  assume forall m:nat :: P(m) ==> P(m+1);
  forall n:nat ensures P(n) {
    ghost var i := 0;
    while i < n
      invariant i <= n;
      invariant P(i)
    {
      i := i + 1;
    }
    assert i == n;
  }
}

method test_induction_step4_simple()
{
  assume P(0);
  assume forall m:nat :: P(m) ==> P(m+4);
  forall n:nat ensures n % 4 == 0 ==> P(n) {
    ghost var i := 0;
    while i < n
      invariant i <= n + 3;
      invariant i % 4 == 0;
      invariant P(i)
    {
      i := i + 4;
    }
  }
}

lemma induction_step4_hard(P: nat -> bool)
requires P(0) && !P(1) && !P(2) && !P(3)
requires forall m:nat :: P(m) == P(m+4)
ensures forall n:nat :: P(n) <==> n % 4 == 0
{
  forall n:nat ensures P(n) <==> n % 4 == 0 {
    ghost var i := 0;
    while i < n
      invariant i <= n + 3;
      invariant i % 4 == 0;
      invariant P(i) && !P(i+1) && !P(i+2) && !P(i+3)
    {
      i := i + 4;
    }
  }
}

method test_induction_step2()
{
  assume P(0);
  assume !P(1);
  assume forall m:nat :: P(m) == P(m+2);
  forall n:nat ensures P(n) <==> n % 2 == 0 {
    ghost var i := 0;
    while i < n
      invariant i < n + 2;
      invariant i % 2 == 0;
      invariant P(i) && !P(i+1)
    {
      i := i + 2;
    }
  }
}

lemma mod_self()
ensures forall k | k > 0 :: k % k == 0
{}

// [1, 0, 0, 0, (1, 0, 0, 0), ...]
lemma mod_prop(m: nat, k: nat, d: nat)
requires 1 <= d && 1 <= k < d
requires m % d == 0;
ensures (m + k) % d > 0
{
  if (d == 1) {
  } else {
    if (d == 2) {
      // Just check that our lemma expression is correct (because user can make mistakes in lemma expression)
      assert m % 2 == 0 ==> (m + k) % 2 > 0;
    } else {
      // Let's simplify our goal
      calc == {
        (m + k) % d;
        { assume forall a:nat,b:nat,c | c >= 1 :: (a + b) % c == (a % c + b % c) % c; } // That means z3 get stuck on this lemma, and we don't need mod_prop
        (m % d + k % d) % d;
        { assert m % d == 0; }
        (k % d) % d;
        k % d;
      }
      // Now goal is easy for z3
      assert k % d > 0 by {
        assert k % d == k && k > 0;
      }
    }
  }
}

// Timeout
method {:verify false} test_induction_step_complex(step: nat)
requires step > 0
// Base case
requires P(0) && forall k | 1 <= k < step :: !P(k)
// Induction step
requires forall m:nat :: P(m) == P(m + step)
{
  forall n:nat ensures P(n) <==> n % step == 0 {
    ghost var i := 0;
    // add_mod_distrib
    assume forall a:nat,b:nat,c | c >= 1 :: a % c == 0 ==> (a + b) % c == b % c;

    while i < n - step - 1
      invariant i <= n;
      invariant i % step == 0;
      invariant P(i) && forall k | i+1 <= k < i+step :: !P(k)
      invariant forall k | i <= k <= i+step :: P(k) <==> k % step == 0 /// P(i) <==> i % step == 0
    {
      i := i + step;
    }
    assert P(i) <==> i % step == 0;
    assert n - step - 1 <= i <= n;
    if i == n {
      // simple
      assert P(n) <==> n % step == 0;
      assume false;
    } else {
      assert n > i;
      assert n <= i + step + 1;
      if (n <= i + step) {
        assert P(n) <==> n % step == 0 by {
          assert forall k | 0 <= k <= step :: P(i+k) <==> (i+k) % step == 0;
        }
        assume false;
      } else {
        assume false;
      }
    }
    // assert n <= i <= n + step - 1;
    // assert n == i || n >= i+1 - step;
  }
}