# Fuel attribute
```dafny
module Fuel {
  function FunctionA(x:int) : int
  {
      x + 2
  }

  function FunctionB(y:int) : int
  {
      FunctionA(y - 2)
  }

  method {:fuel FunctionA,2,3}  MethodX(z:int)
  {
      assert FunctionB(z) == z; // error: Cannot see the body of FunctionA
  }
}

/**
page 230

Attributes:
* {:ignore} ???
* {:priority N}
* {:opaque}
* {:fuel functionName,lowFuel,highFuel}
*/
```

# Infinite objects in converting from bin to dec
* Assume `1/3` have representation in binary `[a0, a1, a2, ...]`.
  `1/3 = [a0, a1, a2, ...], 1/3 < 1 ==> a0 = 0`
  A: `2/3 = shift_left(1/3) = shift_left([a0, a1, a2, ...]) = [a1, a2, a3, ...], 2/3 < 1 ==> a1 = 0`
  `4/3 = [a2, a3, a4, ...], 4/3 >= 1 ==> a2 = 1`
  `a2 = 1 ==> 4/3 = 1 + [0, a3, a4, ...]`
  `1/3 = [0, a3, a4, ...]`
  B: `2/3 = [a3, a4, a5, ...], 2/3 < 1 ==> a3 = 0` this state equivalent to `A` (or partial case of `A`, i.e. `B -> A`)
  After that we have:
    `bin(1/3) = 0.(01) = 0.010101...`

# Translation tree
Rule: `freeze` sequences that contains in other sequences
```
(0)
0 ([1,1,1,...])
01                                                        | 00
010                                           | 011       | 000 (0{01} <=> 001 contains in 01)
0100                        | 0101            | 0110 0111 | [000...]
01000 01001                 | 01010 01{011}   | 01100 01101 01110 01111
010000 010001 010010 010011 | 01{0100} 010101 | 011000 011001 011010 011011 011100 011101 011110 011111
```

# Induction
```
This solution is possible? (infinite)
......|...|
.oo.oo|.oo|
.oo.oo|.oo|
......|...|
       ^^^ infinite repetition

To prove this we must check cells {(0, 0), (0, 1), (-1, 0), (-1, 1)}
* f(0, 0) = 0 and s(0, 0) = 4 ==> checked
* f(0, 1) = 1 and s(0, 1) = 3 ==> checked
* s(-1, 0) = 2 ==> checked
* s(-1, 1) = 2 ==> checked
Answer: Yes, it possible.

This solution is possible? (finite list)
0 1 2 3 = j
. . . . . . . . . . . ... . . . . . . . . . . .
. . o o . o o . o o . ... . o o . o o . o o . .
. . o o . o o . o o . ... . o o . o o . o o . .
. . . . . . . . . . . ... . . . . . . . . . . .
                      ^^^ gap filled with repetitions

To prove this we must check:
* Left case
* Right case (or use symmetry)
* Middle case (we already checked it above)
  i.e. we need to check only `Left case`:
  * f(-1, 1) = 0 and s(-1, 1) = 1 ==> ok
  * s(0, 1) = 2 ==> ok

+ trivial (grid filled with zeros)
```