datatype G3 = E | A | B

function mul(x: G3, y: G3): G3
function inv(x: G3): G3

lemma id_right()
  ensures forall a :: mul(a, E) == a

lemma  inv_right()
  ensures forall a :: mul(a, inv(a)) == E

lemma assoc_id(a: G3, b: G3)
  ensures mul(mul(a, b), E) == mul(a, mul(b, E))
{ 
  id_right();
  calc {
    mul(a, mul(b, E));
    ==
    mul(a, b);
    ==
    mul(mul(a, E), b); // just do nothing step for demonstration of interface scopes
    == 
    mul(a, b); // Succeeds, and user DON'T know that this step is wrong... Sadly.... User not happy :(
    ==
    mul(mul(a, b), E);
  }
}

/**
id_right() is a general interface (it's definition uses quantifiers... and possible other general things)
assoc_id_2_step1() is an instance of that interface (just set of possible maps (rewrite rules) from one expression to another)
 */
lemma assoc_id_2_step1(a: G3, b: G3)
  ensures mul(b, E) == b;
  ensures mul(mul(a, b), E) == mul(a, b);
{
  id_right();
}

lemma {:verify false} assoc_id_2(a: G3, b: G3)
  ensures mul(mul(a, b), E) == mul(a, mul(b, E))
{ 
  assoc_id_2_step1(a, b);
  calc {
    mul(a, mul(b, E));
    ==
    mul(a, b);
    ==
    mul(mul(a, E), b); // just do nothing step for demonstration of interface scopes
    == 
    mul(a, b); // FAILS, and user know that this step is wrong!!! User is happy now! :)
    ==
    mul(mul(a, b), E);
  }
}