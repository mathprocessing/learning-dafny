type pos = x: int | x >= 1 witness 1

function h(n: pos): pos

predicate monotonic() {
  forall n: pos :: h(n) <= h(n + 1)
}

// Example of that kind of monotonicity: [0, 10, 1, 11, 2, 12, 3, ...]
lemma {:verify false} MonotonicTwoLines()
requires forall n: pos :: h(n) <= h(n + 2)
ensures forall n: pos :: h(2 * n - 1) <= h(2 * (n + 1) - 1)
ensures forall n: pos  {:nowarn} :: h(2 * n) <= h(2 * (n + 1))
{}

lemma Rule(n: pos)
ensures h(h(n)) + h(n + 1) == n + 2

lemma {:verify false} FirstValues()
ensures h(2) == 2
ensures h(3) == 2
ensures h(4) == 3
ensures h(5) == 4
ensures h(6) == 4
ensures h(7) == 5
ensures h(8) == 5
ensures h(9) == 6
{
  assert 1 <= h(2) <= 2 by { Rule(1); }
  if h(2) == 1 {
    assert H1: h(h(1)) == 2 by { Rule(1); }
    assert h(h(2)) == h(1);
    assert h(1) + h(3) == 4 by { Rule(2); }
    assert h(1) == 3 by { reveal H1; }
    assert false by {
      assert h(3) == 1;
      assert h(3) == 2 by { reveal H1; }
    }
  }
  forall u { Rule(u); }
}

lemma Derivative(n: pos)
ensures 0 <= h(n + 1) - h(n) <= 1

lemma LessEqIdFromDerivative(n: pos)
ensures h(n) <= n // It can be better: h(n) <= (n / sqrt(2)) + 1
{
  if n == 1 {
    assert h(2) == 2 by { FirstValues(); }
    assert 0 <= h(2) - h(1) <= 1 by { Derivative(1); }
    assert 0 <= 2 - h(1) <= 1;
    assert 1 <= h(1) <= 2;
    if h(1) == 2 {
      assert h(2) == 1 by { Rule(1); }
    }
  } else {
    Rule(n - 1);
  }
}

lemma {:induction false} LessEqId(n: pos)
ensures h(n) <= n
{
  if n == 1 {
    assert h(1) == 1 by {
      var k := h(1);
      if k >= 2 {
        /*
        Let h(1) = K, h(2) = 1
        1 2 3 4 5 n
        K 1 Q _ _ h(n)
        2 K _ _ _ h(h(n))

        Q + K = 4
        K = 3, Q = 1

        but if K = 3 then h(3) = 2 ==> Q = 2 ==> Q = 1 = 2 ==> false

        Let h(1) = K, h(2) = 2
        1 2 3 4 5 6 7 8 9 n
        K 2 2 3 4 4 5 5 6 h(n)  let K 2
        1 2 2 2 3 3 4 4 4 h(h(n))
        forall m >= 2 :: h(m) <= h(m + 1) ==> K = 1

        h(1) = K
        h(K) = 1
        h(2) = 2
        */
        assume false;
      }
    }
  } else {
    assert h(h(n - 1)) + h(n) == n + 1 by { Rule(n - 1); }
    assert h(h(n - 1)) >= 1;
    assert h(n) <= n;
  }
}

lemma {:verify false} Monotonic(n: pos)
ensures h(n) <= h(n + 1)
decreases n
{
  if n == 1 {
    assert h(h(1)) + h(2) == 3 by { Rule(1); }
    if h(1) > h(2) {
      assume false;
    }
  } else {
    assert h(n - 1) <= h(n) by { Monotonic(n - 1); }
    assert h(h(n)) + h(n + 1) == n + 2 by { Rule(n); }
    assert h(h(n)) >= 1;
    assert 1 <= h(n + 1) <= n + 1;
  }
}

lemma FirstEightValues()
// ensures [h(1), h(2), h(3), h(4), h(5), h(6), h(7), h(8)] == [1, 2, 2, 3, 4, 4, 5, 5]
{
  assert R1: h(h(1)) + h(2) == 3 by { Rule(1); }
  assert 1 <= h(2) <= 2 by { reveal R1; }
  if case h(2) == 1 => {
    assert h(h(1)) == 2 by { reveal R1; }
    
    assert h(h(2)) + h(3) == 4 by { Rule(2); }
    assert h(h(3)) + h(4) == 5 by { Rule(3); }
    assert h(h(4)) + h(5) == 6 by { Rule(4); }

    // 1 2 3 4 5 6 7 n
    // _ 1 _ _ _ _ _ h(n)
    // 2 _ _ _ _ _ _ h(h(n))


  } case h(2) == 2 => {
    assert h(h(1)) == 1 by { reveal R1; }

    // 1 2 3 4 5 6 7 n
    // _ 2 _ _ _ _ _ h(n)
    // 1 _ _ _ _ _ _ h(h(n))
  }
}