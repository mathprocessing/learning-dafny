/*

1. f(f(x)) = [3,2,1,0]
where x in 0..3

2. f(f(x)) + f(x) = id(x)
where x in 0..5

*/

type pos = x: int | x >= 1 witness 1

const size: pos

type num = x: int | 0 <= x <= size - 1

function f(x: num): num

function rev(x: num): num
{
  size - x - 1
}

function id(x: num): num { x }

// ------------------------------------

lemma Rule(x: num)
ensures f(f(x)) == rev(x)

// ------------------------------------

predicate injective(F: num -> num, x: num, y: num) {
  x != y ==> F(x) != F(y)
}

lemma {:axiom} InjectiveAxiom(x: num, y: num)
ensures injective(f, x, y)

lemma IfTwiceIsNotId(F: num -> num)
requires forall x :: F(F(x)) != x
ensures forall x :: F(x) != x
{}

predicate even(n: nat)
{
  n >= 1 ==> if n == 1 then false else even(n - 2)
}

lemma EvenSize(x: num)
requires even(size)
ensures f(x) != x

lemma OddSize(x: num)
requires !even(size)
requires 2 * x != size - 1
ensures f(x) != x