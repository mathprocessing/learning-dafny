include "./ff.dfy"

method testEven()
{
  assert even(0);
  assert even(2);
  assert even(4);
  assert !even(1);
  assert !even(3);
  assert !even(5);
}