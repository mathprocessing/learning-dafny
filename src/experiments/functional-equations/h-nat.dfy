function h(n: nat): nat

lemma Rule(n: nat)
ensures h(h(n)) + h(n + 1) == n + 2

lemma FirstEightValues()
// ensures [h(0), h(1), h(2), h(3), h(4), h(5), h(6), h(7)] == [1, 2, 2, 3, 4, 4, 5, 5]
{
  assert h(0) > 0 by {
    if h(0) == 0 {
      assert h(1) == 2 by { Rule(0); }
      assert 2 * h(2) == 3 by { Rule(1); }
      assert false; // not exists such natural number h(2)
    }
  }

  assert h(h(0)) + h(1) == 2 by { Rule(0); }
  assert h(h(1)) + h(2) == 3 by { Rule(1); }
}