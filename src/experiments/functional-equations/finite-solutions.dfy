include "./ff.dfy"

lemma Size1()
requires size == 1
ensures forall x :: f(x) == id(x) // This theoretically can be replaced with false (no solutions to Rule). How to prove that we can't do that?
{
  assert f(0) == 0;
  // Substitute f(x) == id(x) to Rule
  assert id(id(0)) == rev(0); // We don't have contradiction
  // Now we conclude that number of solutions is one (cardinality of {f(x) | Rule(x)} = 1).

  // Just reference
  assert f(f(0)) == 0 by { Rule(0); }
}

lemma Size2()
requires size == 2
ensures false
{
  assert H: f(0) == 1 by {
    assert f(f(0)) == 1 by { Rule(0); }
  }
  assert H2: f(f(1)) == 1 by {
    if case f(1) == 0 => {
      assert f(0) == 1 by { reveal H; }
    } case f(1) == 1 => {
      assert f(1) == 1; // Obvious fact by assumption from case
    }
  }
  assert false by {
    assert f(f(1)) == 0 by { Rule(1); }
    assert f(f(1)) == 1 by { reveal H2; }
  }
}

lemma Size3()
requires size == 3
ensures false
{
  assert f(0) > 0 by { OddSize(0); }
  assert f(0) in {1, 2};
  if case f(0) == 1 => {
    assert f(f(0)) == f(1);
    assert f(1) == 2 by { Rule(0); }
    // f = 12*
    assert f(2) == 0 by { InjectiveAxiom(1, 2); InjectiveAxiom(0, 2); }
    if * { // Two proofs possible, but Rule(0) not possible. TODO: How to write in Dafny that fact?
      assert false by { Rule(1); }
    } else {
      assert false by { Rule(2); }
    }
  } case f(0) == 2 => {
    assert f(f(0)) == f(2);
    assert f(2) == 2 by { Rule(0); }
    // f = 2*2
    assert f(f(2)) == 2;
    assert f(f(2)) == 0 by { Rule(2); }
  }
}

lemma Size4()
requires size == 4
ensures f(0) in {1, 2}
ensures if f(0) == 1 then f(1) == 3 && f(2) == 0 && f(3) == 2 // 1302
                     else f(1) == 0 && f(2) == 3 && f(3) == 1 // 2031
{
  assert f(0) != 0 by {
    assert f(f(0)) != 0 by { Rule(0); }
  }
  assert f(0) != 3 by {
    if f(0) == 3 {
      assert f(3) == 3 by {
        calc == { rev(0); { Rule(0); } f(f(0)); f(3); }
      }
      assert false by { InjectiveAxiom(0, 3); }
    }
  }
  assert f(0) in {1, 2};
  // TODO: compute a tree of all possible actions and paths and learn about some properties of that paths (from which depends quality of proofs and etc.)
  // Also compute all symmetries and reduce (using symmetries, equivalences, orders) size of tree by squashing observed states of the tree.
  if f(0) == 1 { // WISH: syntax `if H: f(0) == 1 {`
    // 0123 x
    // 1*** f(x)
    // 3210 f(f(x))
    assert f(1) == 3 by {
      assert f(f(0)) == 3 by { Rule(0); }
      // assert f(0) == 1;
    } // use column 0: 1 -> 3
    // 0123 x
    // 13** f(x)
    // 3210 f(f(x))
    assert f(3) == 2; // use column 1: 3 -> 2
    // 0123 x
    // 13*2 f(x)
    // 3210 f(f(x))
    assert f(2) == 0; // use column 3: 2 -> 0
    // f(x) = 1302
  } else {
    // 0123 x
    // 2*** f(x)
    // 3210 f(f(x))
  }
}