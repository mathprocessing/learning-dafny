
lemma iff_def(p: bool, q: bool)
  requires p ==> q
  requires q ==> p
  ensures p == q
{}

// In general cost = N
/** I have seen this pattern in Group theory exersises */
lemma cycle_3(p: bool, q: bool, r: bool)
  requires p ==> q
  requires q ==> r
  requires r ==> p // cost: 3 < 4, i.e. proof can be shorter
  ensures p == q == r
  // ensures (p <==> q) && (q <==> r)
{}

// In general cost = (N - 1) * 2
// Where N: number if premises
/** I have seen this pattern in Graph theory book */
lemma one_two_one_three(p: bool, q: bool, r: bool)
requires p ==> q 
requires p ==> r
requires r ==> p
requires q ==> p // cost: 4
ensures p == q == r
{}

lemma better_cost(n: nat)
  requires n >= 2
  ensures n <= (n - 1) * 2
{}

lemma structly_better_cost(n: nat)
  requires n >= 3
  ensures n < (n - 1) * 2
{}

lemma cost_simplified(n: nat)
  ensures (n <= (n - 1) * 2) <==> (2 <= n)
{
  var a := n <= (n - 1) * 2;
  var b := 2 <= n;
  assert a == b;
}

/** One can use types (you can try yourself) */
type Var = nat
datatype Implies = Imp(Var, Var)

/** Or use just consts for simplisity */
const cost_pq: real
const cost_qp: real
const cost_pr: real
const cost_rp: real
const cost_qr: real
const otot: real
const c3: real

function cost(e: Implies): real
/**
In general each implication `a ==> b` can have your own cost function: `cost(a, b)`.
We can select way to prove with minimum cost, in that case we don't know which is better:
cycle_3 can be better, and also one_two_one_three can be better.
Let's prove this!
*/
method one_two_one_three_can_be_best_case1()
  requires otot == cost_pq + cost_qp + cost_pr + cost_rp
  requires c3 == cost_pq + cost_qr + cost_rp
  // optimality of cost function (similar to triangle inequality)
  requires cost_qr <= cost_qp + cost_pr
{
  assert c3 <= otot;
  // one_two_one_three can't be strictly better
  assert otot < c3 ==> false;
  /**
  cost(one_two_one_three) == cost(pq) + cost(qp) + cost(pr) + cost(rp)
  cost(cycle_3) == cost(pq) + cost(qr) + cost(rp)

  Assume that "one_two_one_three is the best":
  H: cost(one_two_one_three) <= cost(cycle_3)
  H: cost(pq) + cost(qp) + cost(pr) + cost(rp) <= cost(pq) + cost(qr) + cost(rp)
  H: cost(qp) + cost(pr) <= cost(qr)

      
  but 
  cost_optimal: cost(X) = min(cost(left1) + cost(right1), cost(left2) + cost(right2), ...)
  i.e. 
    forall {left, right} == X :: cost(X) <= cost(left) + cost(right)
  i.e.
    cost(qr) <= cost(qp) + cost(pr)

  Substitute this in H:
  H: cost(qp) + cost(pr) <= cost(qr) <= cost(qp) + cost(pr)
  */
  assert forall n:real, m:real :: (n <= m <= n) ==> (n == m);
  /**
  H: cost(qp) + cost(pr) == cost(qr)

  Answer: No,
    cost(one_two_one_three can be equal but not better
    than cost(cycle_3)

  Conclusion:
    If you always using `cycle_3` instead of 
    `one_two_one_three` your proofs still be optimal.
   */
}