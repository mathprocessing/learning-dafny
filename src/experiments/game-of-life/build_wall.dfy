type cell = c:int | 0 <= c <= 1

// state of cells
function f(i: int, j: int): cell

function sum(i: int, j: int): int
  ensures 0 <= sum(i, j) <= 8
{
  f(i - 1, j - 1) + f(i - 1, j + 0) + f(i - 1, j + 1) +
  f(i + 0, j - 1)                   + f(i + 0, j + 1) +
  f(i + 1, j - 1) + f(i + 1, j + 0) + f(i + 1, j + 1)
}

// still life constraint
predicate rule(center: cell, s: int)
  requires 0 <= s <= 8
{
  if s == 2 then
    true
  else 
  if s == 3 then
    center == 1
  else
    center == 0
}

// rule instance
predicate r(i: int, j: int)
{
  rule(f(i, j), sum(i, j))
}

predicate inner(width: nat, height: nat, value: cell) {
  forall i:int, j:int | (0 <= i < height && 0 <= j < width) :: f(i, j) == value
}

predicate outer(width: nat, height: nat, value: cell) {
  forall i:int, j:int | !(0 <= i < height && 0 <= j < width) :: f(i, j) == value
}

predicate outer_border(width: nat, height: nat, value: cell) {
  forall i:int, j:int | ((i == -1 || i == height) && (-1 <= j <= width)) || ((j == -1 || j == width) && (0 <= i <= height - 1)) :: f(i, j) == value
}

lemma assume_outer(width: nat, height: nat, value: cell)
ensures forall i:int, j:int | !(0 <= i < height && 0 <= j < width) :: f(i, j) == value;

lemma still_life(i: int, j: int)
ensures r(i, j)


/**
0 0 0 0 0 ... 0 0 0 0 0     0 0 0 0 0 ... 0 0 0 0 0
0 ? ? ? ? ... ? ? ? ? 0 ==> 0 0 0 0 0 ... 0 0 0 0 0
0 0 0 0 0 ... 0 0 0 0 0     0 0 0 0 0 ... 0 0 0 0 0
*/
lemma single_line(width: nat)
requires outer_border(width, 1, 0) 
ensures forall j:int | 0 <= j < width :: f(0, j) == 0
decreases width
{
  if width == 0 {} else {
    assert f(0, width - 1) == 0 by {
      still_life(0, width - 1);
    }
    assert forall k:int | 0 <= k < width - 1 :: f(0, k) == 0 by {
      assert outer_border(width - 1, 1, 0) by {
        // from `f(0, width - 1) == 0`
      }
      single_line(width - 1);
    }
    // goal from `f(0, width - 1) == 0`
  }
}


predicate upper_lower(width: nat, value: cell) {
  forall j:int | -1 <= j <= width :: f(-1, j) == f(1, j) == value
}

/**
0 0 0 0 0 ... 0 0 0 0 0     0 0 0 0 0 ... 0 0 0 0 0
? ? ? ? ? ... ? ? ? ? 0 ==> ? 0 0 0 0 ... 0 0 0 0 0
0 0 0 0 0 ... 0 0 0 0 0     0 0 0 0 0 ... 0 0 0 0 0
^ question mark added
*/
lemma single_line_stronger(width: nat)
requires upper_lower(width, 0)
requires f(0, width) == 0
ensures forall j:int | 0 <= j <= width - 1 :: f(0, j) == 0
decreases width
{
  if width == 0 {} else {
    assert f(0, width - 1) == 0 by {
      still_life(0, width - 1);
    }
    assert forall k:int | 0 <= k <= width - 2 :: f(0, k) == 0 by {
      assert f(0, width - 1) == 0;
      assert upper_lower(width - 1, 0);
      single_line_stronger(width - 1);
    }
    // goal from `f(0, width - 1) == 0`
  }
}

/**
This lemma is weaker than `single_line` because `still_life(1, j);` used.
*/
lemma {:induction false} single_line_2(width: nat)
requires outer(width, 1, 0)
ensures forall j:int | 0 <= j < width :: f(0, j) == 0
{
  forall j:int | 0 <= j < width ensures f(0, j) == 0 {
    // Two symmetric border cases (actually one)
    if j == 0 || j == width - 1 {
      still_life(0, j);
    } else {
      // Middle case
      if f(0, j) == 1 {
        // . . . . .
        // ? ? o ? ?
        // . . . . .
        assert sum(0, j) == 2 by {
          still_life(0, j);
        }
        assert f(0, j - 1) == f(0, j + 1) == 1;
        assert false by {
          still_life(1, j);
        }
      }
    }
  }
}

/**
? ? ? ? ? ? ==> ? ? ? ? ? ? ?
? . . . . ?     ? . . . . ? ?
? . . . . ?     ? . . . . ? ?
? ? ? ? ? ?     ? ? ? ? ? ? ?
         N-1    !forall i :: f(i, N) == 0
*/
lemma rectangle_border(width: nat, height: nat)
requires outer(width, height, 0)
requires inner(width, height, 0)
{

}