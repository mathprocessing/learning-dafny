type cell = c:int | 0 <= c <= 1

// state of cells
function f(i: int, j: int): cell

function sum(i: int, j: int): int
  ensures 0 <= sum(i, j) <= 8
{
  f(i - 1, j - 1) + f(i - 1, j + 0) + f(i - 1, j + 1) +
  f(i + 0, j - 1)                   + f(i + 0, j + 1) +
  f(i + 1, j - 1) + f(i + 1, j + 0) + f(i + 1, j + 1)
}

lemma sum_bound(i: int, j : int)
  ensures 0 <= sum(i, j) <= 8
{}

// still life constraint
predicate rule(center: cell, s: int)
  requires 0 <= s <= 8
{
  if center == 0 then
    s == 0 || s == 1 || s == 3 || s == 5 || s == 7
  else 
    s == 1 || s == 3 || s == 5 || s == 7
}

// rule instance
predicate r(i: int, j: int)
{
  rule(f(i, j), sum(i, j))
}

lemma outer(width: nat, height: nat, value: cell)
ensures forall i:int, j:int | !(0 <= i < height && 0 <= j < width) :: f(i, j) == value;

lemma rule_box(width: nat, height: nat)
ensures forall i:int, j:int {:trigger f(i, j)} | -1 <= i < height + 1 && -1 <= j < width + 1 :: r(i, j)

lemma R(i: int, j: int)
ensures r(i, j)

lemma Rs(i: int, j: int)
requires i <= j // To reduce chaos in proof
ensures r(i, j) && r(j, i)

// todo: add weaker axioms

// For easy symmetries (not work)
function fs'(i: int, j: int): cell
requires f(i, j) == f(j, i)
{
  f(i, j)
}

// For easy symmetries (not work)
predicate fs(i: int, j: int, value: cell)
{
  f(i, j) == f(j, i) == value
}

method test_not_bounded()
{
  // . . . .
  // . o . .
  // . . o o
  // . . o .
  assume forall i, j | i <= 0 :: f(i, j) == 0;
  assume forall i, j | j <= 0 :: f(i, j) == 0;
  assume f(1, 1) == 1;
  // . . . .
  // . o * *
  // . * * *
  // . * * *
  var s11 := sum(1, 1);
  R(1, 1);
  assert s11 == 1 || s11 == 3;
  if s11 == 1 {
    //   0 1 2 3 4 5 j
    // 0 . . . . . .
    // 1 . o . . * *
    // 2 . . o o * *
    // 3 . . o . . *
    // 4 . * * . * *
    // 5 . * * * * *
    assert f(1, 2) == f(2, 1) == 0 by { R(0, 1); R(1, 0); }
    assert f(2, 2) == 1;
    // assert fs(1, 3, 0) by { Rs(2, 0); } // WISH: instead of "pc might not hold" i want to see some custom error message "assertion i <= j might not hold + message: '(i, j) must be ordered pair' "
    assert fs(1, 3, 0) by { Rs(0, 2); }

    // assert f(1, 3 + 1) == 0 by { R(0, 2 + 1); } // We can imagine that some Bot (or fixed search strategy) must try this and fail

    assert f(2, 3) == f(3, 2) == 1 by { R(1, 2); R(2, 1); }
    assert f(3, 3) == 0 by { R(2, 2); }
    assert H: f(1, 4) + f(2, 4) == 1 by { R(1, 3); }
    var col4 := f(1, 4) + f(2, 4) + f(3, 4);
    assert col4 == 1 || col4 == 3 by { R(2, 3); } // 'col4 == 1 || col4 == 3' must be non-minimizable
    // Meta: NonMinimizable(col4 == 1 || col4 == 3)
    assert col4 == 1 by { reveal H; }
    assert f(3, 4) == 0 by { reveal H; }
    assert f(4, 3) == 0 by { R(3, 2); R(3, 1); }
    // To check nonMin(Set(R(3, 2), R(3, 1))), we may check that two singletions are non-proof-checkable {R(3, 2)} and {R(3, 1)}

  } else {
    //   0 1 2 3 4 5 j
    // 0 . . . . . .
    // 1 . o o * * *
    // 2 . o o * * *
    // 3 . * * * * *
    // 4 . * * * * *
    // 5 . * * * * *
    assert f(1, 2) == f(2, 1) == f(2, 2) == 1;
    assert fs(1, 3, 1) by { Rs(0, 2); }
  }
}