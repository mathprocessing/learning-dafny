const size: nat := 3
type cell = c:int | 0 <= c <= 1

// state of cells
function f(i: int, j: int): cell

function sum(i: int, j: int): int
  ensures 0 <= sum(i, j) <= 8
{
  f(i - 1, j - 1) + f(i - 1, j + 0) + f(i - 1, j + 1) +
  f(i + 0, j - 1)                   + f(i + 0, j + 1) +
  f(i + 1, j - 1) + f(i + 1, j + 0) + f(i + 1, j + 1)
}

lemma sum_bound(i: int, j : int)
  ensures 0 <= sum(i, j) <= 8
{}

// still life constraint
predicate rule(center: cell, s: int)
  requires 0 <= s <= 8
{
  if s == 2 then
    true
  else 
  if s == 3 then
    center == 1
  else
    center == 0
}

// rule instance
predicate r(i: int, j: int)
{
  rule(f(i, j), sum(i, j))
}

lemma outer(width: nat, height: nat, value: cell)
ensures forall i:int, j:int | !(0 <= i < height && 0 <= j < width) :: f(i, j) == value;

lemma still_life_box(width: nat, height: nat)
ensures forall i:int, j:int {:trigger f(i, j)} | -1 <= i < height + 1 && -1 <= j < width + 1 :: r(i, j)

lemma still_life(i: int, j: int)
ensures r(i, j)

// weaker axioms (for more interesting proof, and more general, i.e. not only for still life)
lemma {:verify false} s_le_one_absurd(i: int, j: int)
  requires sum(i, j) <= 1
  ensures f(i, j) == 0
{
  still_life(i, j);
}

lemma{:verify false}  s_le_one_absurd_no_req(i: int, j: int)
  ensures sum(i, j) <= 1 ==> f(i, j) == 0
{
  still_life(i, j);
}


method {:verify false} test_not_bounded()
{
  // o . .
  // . o . ==> false
  // . . .
  assume f(0, 0) == 1 && f(0, 1) == 0 && f(0, 2) == 0;
  assume f(1, 0) == 0 && f(1, 1) == 1 && f(1, 2) == 0;
  assume f(2, 0) == 0 && f(2, 1) == 0 && f(2, 2) == 0;
  still_life(1, 1);
  assert false;
}

method {:verify false} test_bounded()
{
  ghost var sz := 2;
  outer(sz, sz, 0);
  // outer cells = 0
  // o o ==> false
  // . o
  assume f(0, 0) == 1 && f(0, 1) == 1;
  assume f(1, 0) == 0 && f(1, 1) == 1;
  still_life(1, 0);
  assert false;
}

method {:verify false} test_quad()
{
  ghost var w := 3;
  ghost var h := 4;
  outer(w, h, 0);
  // outer cells = 0
  // . o . ==> . o .  i=0
  // o ? o     o . o  i=1
  // o ? ?     o u x  i=2
  // . ? ?     . v .  i=3 x == v && x != u
  ghost var x := f(2, 2);
  ghost var y := f(3, 2);
  ghost var u := f(2, 1);
  ghost var v := f(3, 1);

  assume f(0, 0) == 0 && f(0, 1) == 1 && f(0, 2) == 0;
  assume f(1, 0) == 1 && true         && f(1, 2) == 1;
  assume f(2, 0) == 1 && true         && true;
  assume f(3, 0) == 0 && true         && true;
  still_life_box(w, h); // {:trigger f(i, j)} helps to prove asserts below
  assert f(1, 1) == 0;
  // assert f(3, 1) == 1;
  // (x, u, v) == 0, 1, 0 || (x, u, v) == 1, 0, 1
  assert x == v;
  assert x != u;
  assert y == 0;

  if u == 1 {
    // . o .  i=0 not resolved, only view of assumption
    // o . o  i=1
    // o o x  i=2
    // . ? y  i=3
    assert x == y == v == 0;
    // . o .  i=0 resolved
    // o . o  i=1
    // o o .  i=2
    // . . .  i=3
  } else {
    assert [x, v, u] == [1, 1, 0];
    // . o .  i=0 resolved
    // o . o  i=1
    // o . o  i=2
    // . o .  i=3
    assert y == 0 by {
      still_life(2, 3);
    }
  }
}

method {:verify false} test_symmetry()
{
  // x0x1x2
  // y0y1y2
  // z0z1z2
  // ^ ^ ^     Step 1              Step 2                        Step 3              Step 4             Step 5                      Step 6
  // ? ? ? ==> ? ? ? || ? ? ? ==> (. ? . || ? ? o) || ? o o ==> (. . .) || . o o ==> . . . || . o o ==> . . . || . o o || . o o ==> . . .
  // ? . ?     . . . || ? . o ==> (. . . || . . .) || ? . o ==> (. . .) || ? . o ==> . . . || ? . o ==> . . . || . . o || o . o ==> . . .
  // ? . ?     . . . || . . . ==> (. . . || . . .) || . . . ==> (. . .) || . . . ==> . . . || . . . ==> . . . || . . . || . . . ==> . . .
  //                               ^^^^^   s <= 1                         o o o                                F by r(1,1)  F by r(1,0)
  //                            triv. sol.  False                        near border => False  
  
  ghost var x0, x1, x2;
  ghost var y0, y1, y2;
  ghost var z0, z1, z2;
  // Not works to check try `assert z2 == 1 ==> f(2, 2) == 1;`
  // assume [x0, x1, x2] == seq(3, j => f(0, j));
  // assume [y0, y1, y2] == seq(3, j => f(1, j));
  // assume [z0, z1, z2] == seq(3, j => f(2, j));
  assume [x0, x1, x2] == [f(0, 0), f(0, 1), f(0, 2)];
  assume [y0, y1, y2] == [f(1, 0), f(1, 1), f(1, 2)];
  assume [z0, z1, z2] == [f(2, 0), f(2, 1), f(2, 2)];
  // Set of Hypotheses
  assume y1 == z1 == 0;
  outer(3, 3, 0);
  // still_life_box(3, 3);
  
  // Proof
  // Step 1
  assert z0 == 0 && z2 == 0 by {
    s_le_one_absurd(2, 0);
    s_le_one_absurd(2, 2);
  }
  // State:
  // ? ? ?
  // ? . ?
  // . . .

  ghost var i := 2;
  while f(i, 0) == f(i, 2) && i >= 0
    invariant f(i, 0) == f(i, 2) ==> f(i+1, 0) == f(i+1, 2)
    invariant f(i, 0) == f(i, 2) ==> f(i+2, 0) == f(i+2, 2)
  {
    
    assert f(i, 0) == f(i, 2);
    // forall proved from `invariant f(i, 0) == f(i, 2) ==> f(i+1, 0) == f(i+1, 2)`
    assert forall k:nat {:trigger f} :: f(i+k, 0) == f(i+k, 2);
    i := i - 1;
  }
  if i >= 0 {
    assert f(i, 0) != f(i, 2);
    assume f(i, 0) <= f(i, 2); // assumption from symmetry of problem
    assert f(i, 0) < f(i, 2);
    assert f(i, 0) == 0;
    assert f(i, 2) == 1;
    // "Blurred" case
    // ? ? ? || . . . <== outer cells
    //[. . o]||[. ? o]
    // . . . || x . x
    //   ^        ^
    //   z        z
    ghost var z := f(i+1, 1);
    assert z == 0;
  }
  else {
    assert i == -1;
    assert f(0, 0) == f(0, 2);
    assert f(1, 0) == f(1, 2);
    assert f(2, 0) == f(2, 2);
    // y ? y
    // x . x
    // . . .
    assert f(0, 1) == 0 ==> f(1, 2) == 0 by {
      // s_le_one_absurd gives error, but we can use `no_req`
      s_le_one_absurd_no_req(1, 2);
      // Of course, we can use `if`, but its use is a bit more complicated than the one line above.
      // if f(0, 1) == 0 {
      //   s_le_one_absurd(1, 2); // OK
      // }
    }
    assert f(1, 2) == 0 ==> f(0, 1) == 0 by {
      if f(0, 1) == 1 && f(1, 2) == 0 {
        // y o y
        // . . .
        // . . .
        assert forall j | 1 <= j <= 2 :: f(1, j) == 0;
        assert forall j | 1 <= j <= 2 :: f(2, j) == 0;
        assert sum(2, 2) == 0;
        assert sum(0, 2) == 1;
        s_le_one_absurd_no_req(0, 2);
        assert f(0, 2) == 0;
        assert f(0, 0) == 0;
        // . o .
        // . . . ==> False
        // . . .
        assert false by {
          s_le_one_absurd_no_req(0, 1);
        }
      }
    }
    ghost var x := f(1, 0);
    ghost var y := f(0, 0);
    assert x == f(1, 0);
    // y x y
    // x . x
    // . . .
    assert x == y by {
      s_le_one_absurd_no_req(x, 0); // beautiful proof!
    }
    // x x x
    // x . x
    // . . .
  }

  // For all assumptions (hypothesis context): if we change j to 2 - j actually nothing will change.
  // Or more weakest: if we change j to 2 - j and `assumtions[j] ==> assumtions[2 - j]` holds
  // then we can reduce the problem `assumtions[2 - j]` to `assumtions[j]` but not vice versa.

  // How to work with symmetry (use "without loss of generality")?
  // Way 1. Divide state and glue symmetrically equivalent (sets of solutions|tasks).
  // Way 2. Assume that we already solve the task.
  //    i.e. exists some proof|function|methodic
  //    And create new lemma with this task with arguments (x, y) then prove commutatvity of that L(x, y) == L(y, x) 
  //    after that simply use if statement to reduce one mirrored case to another.

  // Way 1)
  // State: (divide into 3 parts)
  // ? ? ? || (? ? ? || ? ? ?)
  // x . x || (. . o || o . .)
  // . . . || (. . . || . . .)

  // Aggregate right pair to one
  // ? ? ? || ? ? ?
  // x . x || . . o
  // . . . || . . .

  // Apply same trick
  // ? ? ? ==> . ? o || y ? y ==> . ? o || y . y || y o y ==> False || . . . || o o o ==> False
  // x . x ==> x . x || x . x ==> . . . || x . x || x . x ==>       || . . . || x . x
  // . . . ==> . . . || . . . ==> . . . || . . . || . . . ==>       || . . . || . . .

  // How to prove that `way 1` is not wrong?
  // Answer: Since there is no doubt about the actions except for aggregation, we can reduce the problem to proving that the aggregation is correct.

}

method {:verify false} notes_about_symmetry()
{
  // Note: What if this two slightly asymmetric?
  // (? ? ? || ? o .)
  // (. . o || o . .)
  // (. . . || . . .)
  // We can create a "Forgetted copy" (We can do same things with two states using "mirror")
  // (? ? ? || ? o .) || ? ? ?
  // (. . o || o . .) || o . .
  // (. . . || . . .) || . . .
  // Then glue first to third
  // ? o . || ? ? ?
  // o . . || o . .
  // . . . || . . .
  // Glue 3rd to 1st back
  // ? o .
  // o . .
  // . . .
  // TODO: add code related to this `notes`
}

method {:verify false} example_using_while_gorizontal()
{
  // Prove that infinite pattern is stable
  // ... o o . o o . (o o .) ...
  // ... o o . o o . (o o .) ...
  ghost var Q := (k:int) => 
       f(0, k)   == f(1, k) == 1
    && f(0, k+1) == f(1, k+1) == 1
    && f(0, k+2) == f(1, k+2) == 0;
  ghost var h := 2;
  // Outer condition
  assume forall i:int, j:int {:trigger f(i, j)} {:trigger Q} | !(0 <= i < h) :: f(i, j) == 0;

  // Base case
  assume f(0, 0) == 1 && f(0, 1) == 1;
  assume f(1, 0) == 1 && f(1, 1) == 1;
  // o o ?
  // o o ?

  forall i ensures f(i, 2) == 0 {
    assert f(i, 2) == 0 by {
      if * {
        // proof = 
        still_life(3 * i - 1, 1);
      } else { // Or
        still_life(i, 1);
      }
    }
  }
  // o o .
  // o o .
  // Induction step
  // Analogy between mirror symmetry (assume x[0] <= x[1]) and translation symmetry (induction?):
  // Morror:
  // 1. Destruct whole state on parts.
  // 2. We can apply mirror to any solution set because `mirror ~ id`
  //    Target is two states that equivalent up to mirror.
  // 3. Glue states together, simplify
  //    Use if statement for example `lemma(a, b) := if (a < b) {} else if (b < a) {lemma(b, a)}` <-- swapped arguments
  // Translate:
  // 1. How to destruct? What a target is? What a direction of process?
  //    Answer: Target is two states that equivalent up to translation.
  // 2. Do manipulations. Do ignoring of information.
  ///   But ensure that A[translation(j)] = ignore(manip(A[j]))
  ///   In that concrete case: translation(j) = j + 3
  // 3. How to glue?
  //    Use while statement?

  ghost var right :| right > 0;
  ghost var j := 0;
  while j < right && Q(3*j)
    invariant j <= right
    invariant forall k:int | 0 <= k < j :: Q(3*k)
    // invariant j > 0 ==> Q(j-1);
    decreases right - j
  {
    
    j := j + 1;
  }

  assert 1 <= j <= right;
  if j == right { // Right case
    assert Q(3*(j-1)); // last holding constraint, Q(3*j) not holds
    // Just invariant (holds in loop and after loop)
    assert forall k | 0 <= k < j :: Q(3*k);
  } else { // Middle case
    assert 1 <= j < right;
    assert Q(3*(j-1));
    ghost var u := 3*(j-1);
    //     u
    // ... o o . ? ? ? ...
    // ... o o . ? ? ? ...
    assert f(0, u) == f(1, u) == f(0, u+1) == f(1, u+1) == 1;
    assert f(0, u+2) == f(1, u+2) == 0;

  }
  /*
  . . . . . .
  . o o ? ? ?
  . o o ? ? ?
  . . . . . .
  translated
  . . . . . .
  ? ? ? . o o
  ? ? ? . o o
  . . . . . .


  o C ? o A ? o B ? o ? ? o ? ...
  ? o ? ? o ? ? o ? ? o ? ? o ...

  if we have state A == 0 && B == 1 then
    we have additional assumption for other states: !(C == 0 && A == 1) i.e. (A == 1 ==> C == 1)
  o C ? o . ? o o ? o ? ? o ? ...
  ? o ? ? o ? ? o ? ? o ? ? o ...
  OR 
  o o ? o o ? o B ? o ? ? o ? ...
  ? o ? ? o ? ? o ? ? o ? ? o ...

  i.e. If we already have checked state `f(i) == A and f(i+1) == B`
  Then we have for other states: `forall j | j != i :: !(f(j) == A and f(j+1) == B)`

  i.e. If we already have checked state `P(i)`
  Then we have for other states: `forall j | j != i :: !P(j)`

  i.e. If we already have checked state `P(0)`
  Then we have for other states: `forall j | j != 0 :: !P(j)`

  if set of solutions includes (f0, f1) = (a, b)
  (f1, f2) != (a, b)
  (f2, f3) != (a, b)
  ...

  examples:
  1. 
    state { (0), (1, 0), (1, 1) } can be simplified to { (0), (1, 1) } because `(1, 0) in (0)`
    and after infinite number of reductions we have
    state: { (0), (1, 1, 1, ...) } i.e.
    state: exists i :: !P(i) || forall j:int :: P(j)
    or we can move forall inside exists
    state: exists i :: (!P(i) || forall j:int :: P(j))
  2.
    state { (0), (1, 0), (1, 1) } can be simplified to { (0), (1, 1) } because `(1, 0) in (0)`

  */
  var P:int -> bool;
  assert (exists k :: !P(k)) || (forall j :: P(j));
  assert exists k :: (!P(k) || forall j :: P(j));
}

lemma s7()
ensures sum(1, 1) + f(1, 1) == 7;

method s_eq_7_in_3by3_impossible()
{
  outer(3, 3, 0);
  ghost var x0, x1, x2;
  ghost var y0, y1, y2;
  ghost var z0, z1, z2;
  assume [x0, x1, x2] == [f(0, 0), f(0, 1), f(0, 2)];
  assume [y0, y1, y2] == [f(1, 0), f(1, 1), f(1, 2)];
  assume [z0, z1, z2] == [f(2, 0), f(2, 1), f(2, 2)];

  assert sum(1, 1) >= 6 by { s7(); }
  assert f(1, 1) == 0 by { still_life(1, 1); }
  if x0 == x2 {
    ghost var x := x0;
    // x ? x
    // ? . ?
    // ? ? ?
    if x == 0 {
      // . ? .
      // ? . ?
      // ? ? ?
      assert sum(1, 1) == 6;
      assert f(1, 1) == 1 by { s7(); }
      assert false;
    }
    assert x == 1;
    // o . o
    // ? . ?
    // ? ? ?
    assert x1 == 0 by {
      still_life(-1, 1);
    }
    // left boundary
    assert x0 + y0 + z0 <= 2 by {
      still_life(1, -1);
    }
    // right boundary
    assert x2 + y2 + z2 <= 2 by {
      still_life(1, 3);
    }
    assert 6 <= sum(1, 1) <= 2 + 2 + 1;
    assert false;
  } else {
    assume x0 <= x2; // by symmetry
    // . ? o
    // ? . ?
    // ? ? ?
    assert x1 + y2 == 2 by {
      still_life(0, 2);
    }
    // . o o
    // ? . o
    // ? ? .
    assert z2 == 0 by {
      assert x2 + y2 + z2 == 2 by {
        still_life(1, 3);
      }
    }
    assert sum(1, 1) == 6;
    assert false by { s7(); }
  }
}

function {:opaque} abs(x: int): nat
{
  if x < 0 then -x else x
}

lemma abs_abs()
ensures forall x :: abs(abs(x)) == abs(x)

// method {:fuel abs,0} test_abs(n: int)
// {
//   assert abs(abs(n)) == abs(n) by {
//     if n >= 0 {
//       assert abs(n) == n by {
//         reveal abs(); // proof by defenition
//       }
//       assert abs(abs(n)) == n;
//     } else {
//       assert abs(n) == -n by {
//         reveal abs();
//       }
//       calc == {
//         abs(abs(n));
//         abs(-n);
//         {
//           assert forall k:nat {:trigger abs(-n)} :: abs(k) == k by {
//             reveal abs();
//           }
//           assert (-n) >= 0;
//           assert abs(-n) == -n; // {:fuel abs,0} helps to get this work
//         }
//         -n;
//         abs(n);
//       }
      
//       assert abs(-n) == abs(n);
//       assert abs(-n) == -n;
//     }
//   }
// }

method {:fuel abs,10} infinite_cross()
  requires forall i | i != 0 :: f(i, 0) == 1
  requires forall j | j != 0 :: f(0, j) == 1
{
  //     o
  //     o
  // o o ? o o
  //     o
  //     o
  assert f(0, 0) == 0 by { still_life(0, 0); }
  forall n, k | (n == -1 || n == 1) && (k == -1 || k == 1) ensures f(n, k) == 0 {
    still_life(n, k);
  }
  //     o
  //   . o .
  // o o . o o
  //   . o .
  //     o
  forall n, k {:trigger sum} {:trigger f} | abs(n) + abs(k) == 1 ensures sum(n, k) == 3 {
    assert abs(n) <= 1;
    assert abs(k) <= 1;
    assert -1 <= n <= 1; // if this not checks use `reveal abs()` or `{:fuel}`
    assert -1 <= k <= 1;
    still_life(n, k);
  }
  assert f(1, 1) == 0;
  // assert sum(0, 1) == 0;
  assert f(1, 2) == 0;
  //   . o .
  // . . o . .
  // o o . o o o o o o o
  // . . o . .
  //   . o .
  assert sum(1, 2) >= 4 by {
    still_life(1, 2);
    if sum(1, 2) == 3 {
      assert f(1, 2) == 1;
    }
  }
  assert f(2, 2) + f(2, 3) + f(1, 3) <= 2 by {
    if f(2, 2) + f(2, 3) + f(1, 3) == 3 {
      //   . o .
      // . . o . .
      // o o . o o o o o o o
      // . . o . . o
      //   . o . o o
      still_life(1, 3);
    }
  }
  assert sum(1, 2) <= 5;

  if f(-1, -1) == f(-1, 1) {
    ghost var x := f(-1, 1);
    //     o
    //   x o x
    // o o . o o
    //     o
    //     o
    if f(1, -1) == f(1, 1) {
      ghost var y := f(1, 1);
      //     o
      //   x o x
      // o o . o o
      //   y o y
      //     o
      
      
      if x == y {
        assert x == 0 by { still_life(2, 0); }
        //     o
        //   . o .
        // o o . o o
        //   . o .
        //     o

      } else {

      }

    } else {

    }
  } else {

  }
} 