
type cell = c:int | 0 <= c <= 1

// state of cells
function f(i: int, j: int, w: int): cell

function {:verify false} sum(i: int, j: int, w: int): int
  ensures 0 <= sum(i, j, w) <= 8
{
  f(i - 1, j - 1, w) + f(i - 1, j + 0, w) + f(i - 1, j + 1, w) +
  f(i + 0, j - 1, w)                      + f(i + 0, j + 1, w) +
  f(i + 1, j - 1, w) + f(i + 1, j + 0, w) + f(i + 1, j + 1, w)
}

// still life constraint
predicate rule(next: cell, prev: cell, s: int)
  requires 0 <= s <= 8
{
  if s == 2 then
    prev == next
  else 
  if s == 3 then
    next == 1
  else
    next == 0
}

predicate {:verify false} p2(i: int, j: int, w: int)
{
  rule(f(i, j, 1-w), f(i, j, w), sum(i, j, w))
}

lemma gol(i: int, j: int, w: int)
ensures p2(i, j, w)

lemma outer(width: nat, height: nat, value: cell)
ensures forall i:int, j:int | !(0 <= i < height && 0 <= j < width) :: f(i, j, 0) == f(i, j, 1) == value;

lemma golbox(width: nat, height: nat)
ensures forall i:int, j:int {:trigger f(i, j, 0)} {:trigger f(i, j, 1)} | -1 <= i < height + 1 && -1 <= j < width + 1 :: p2(i, j, 0) && p2(i, j, 1)

method {:verify false} three_cells()
{
  ghost var sz := 3;
  outer(sz, sz, 0);
  /**
  t = 0   t = 1
  . o ? | . . ?
  ? ? ? | ? ? ?
  ? ? ? | ? ? ?
  */
  assume f(0, 0, 0) == f(0, 0, 1) == 0;
  assume f(0, 1, 0) == 1;
  assume f(0, 1, 1) == 0;
  golbox(sz, sz);
  /**
  t = 0   t = 1
  . o . | . . .
  . o . | o o o
  . o . | . . .
  */
  assert f(0, 0, 0) == 0 && f(0, 1, 0) == 1 && f(0, 2, 0) == 0;
  assert f(1, 0, 0) == 0 && f(1, 1, 0) == 1 && f(1, 2, 0) == 0;
  assert f(2, 0, 0) == 0 && f(2, 1, 0) == 1 && f(2, 2, 0) == 0;

  assert f(0, 0, 1) == 0 && f(0, 1, 1) == 0 && f(0, 2, 1) == 0;
  assert f(1, 0, 1) == 1 && f(1, 1, 1) == 1 && f(1, 2, 1) == 1;
  assert f(2, 0, 1) == 0 && f(2, 1, 1) == 0 && f(2, 2, 1) == 0;
}

method no_solution_3by3_for_kernel_in_corner()
{
  ghost var sz := 3;
  outer(sz, sz, 0);
  /**
  Problem: Prove that oscillator's "kernel" can't be placed in any corner.
  Let's select top left corner because square have some D8 symmetry `https://groupprops.subwiki.org/wiki/Dihedral_group:D8`.
  t = 0   t = 1
  o ? ? | . ? ?  x0x1x2 | x3x4x5
  ? ? ? | ? ? ?  y0y1y2 | y3y4y5
  ? ? ? | ? ? ?  z0z1z2 | z3z4z5
  */
  ghost var x0 := f(0, 0, 0); ghost var x1 := f(0, 1, 0); ghost var x2 := f(0, 2, 0);
  ghost var y0 := f(1, 0, 0); ghost var y1 := f(1, 1, 0); ghost var y2 := f(1, 2, 0);
  ghost var z0 := f(2, 0, 0); ghost var z1 := f(2, 1, 0); ghost var z2 := f(2, 2, 0);

  ghost var x3 := f(0, 0, 1); ghost var x4 := f(0, 1, 1); ghost var x5 := f(0, 2, 1);
  ghost var y3 := f(1, 0, 1); ghost var y4 := f(1, 1, 1); ghost var y5 := f(1, 2, 1);
  ghost var z3 := f(2, 0, 1); ghost var z4 := f(2, 1, 1); ghost var z5 := f(2, 2, 1);

  assume x0 == 1 && x3 == 0;
  // t = 0   t = 1
  // o ? ? | . o ?
  // ? ? ? | o o ?
  // ? ? ? | ? ? ?
  // We can move this forward amnd our proof changed...
  assert y3 + y4 + x4 as nat == 3 by {
    gol(0, 0, 1);
  }
  assert y3 == y4 == x4 == 1;
  assume y0 <= x1; // by diagonal symmetry
  if y0 < x1 {
    assert y0 == 0 && x1 == 1;
    // t = 0   t = 1
    // o o ? | . o ?
    // . . ? | o o ? 
    // ? ? ? | ? ? ?
    // If proof not minimizable ==> After discovering proof by contradiction in child case we must check that it not works in parent case
    // i.e. `assert false by { gol(1, 0, 0); }` not works
    if y1 == 1 {
      assert x3 == 1 by {
        gol(0, 0, 0);
      }
      assert false;
    }
    assert y1 == 0;
    //       t = 0   t = 1
    //       o o . | . o ?
    // F -> (.). o | o o ?
    //       . . . | ? ? ?
    // False by contradiction with gol(1, 0, 0)
    assert x2 == 0 by {
      gol(-1, 1, 0);
      // gol(-1, 1, 1); // can't prove
    }
    assert y2 == 1 by {
      gol(0, 1, 0);
      // gol(0, 1, 1); // can't prove
    }
    assert sum(1, 1, 0) == 3 by {
      gol(1, 1, 0);
      // gol(1, 1, 1); // can't prove
    }
    assert z0 == z1 == z2 == 0;
    assert false by {
      gol(1, 0, 0); // WISH: How to auto check that for di, dj, dw in [-1..1] :: gol(i + di, j + dj, w + dw); can't prove something that gol(i, j, w) can;
      // gol(1, 0, 1); // can't prove
    }
  } else {
    assert y0 == x1;
    ghost var x := x1;
    // t = 0   t = 1
    // o x ? | . o ?   x0x1x2 | x3x4x5
    // x ? ? | o o ?   y0y1y2 | y3y4y5
    // ? ? ? | ? ? ?   z0z1z2 | z3z4z5
    // Let's use symmetry again
    assume z3 <= x5;
    assert x == 1 ==> y1 == 0 by { gol(0, 0, 0); }
    if z3 < x5 {
      assert z3 == 0 && x5 == 1;
      // t = 0   t = 1
      // o x o | . o o
      // x ? ? | o o ?
      // ? ? ? | . ? ?
      assert x == 1 ==> sum(1, 1, 0) == 3 by { gol(1, 1, 0); }
      // Trick #1:
      assert x2 == 1                      by { gol(0, 2, 0); }
      // t = 0   t = 1
      // o o o | . o o
      // o ? ? | o o ?
      // o ? ? | . ? ?
      assert x == 1 by { gol(1, 0, 1); }
      // assert z4 == 0 by { gol(2, 0, 1); } // This can be omitted (solved by random mutations of proof, without "thinking")
      assert false by { gol(-1, 1, 0); }

      // Try to make same trick on diagonally symmetric cells:
      // Let's use proof by contradiction. where goal is `z0 == 1` 
      //   (Or goal must be `z0 == 0` because x5 and z3 have diag_sym positions but values is "anti": x5 == 1 - z3)
      // assert z0 == 1 by {
      //   if z0 == 0 { 
      //     // t = 0   t = 1
      //     // o o o | . o o
      //     // o ? ? | o o ?
      //     // . ? ? | . . ?
      //     assert z4 == 0 by { gol(2, 0, 1); } // We can move it forward (to beginning of proof)
      //     assert x == 1 by { gol(1, 0, 1); } // We can move it forward (to beginning of proof)
      //     assert false by { gol(-1, 1, 0); } // We can move it forward (to beginning of proof)
      //   }
      // }
      

    } else {
      assert z3 == x5;
      ghost var z := z3;
      // t = 0   t = 1
      // o x ? | . o z
      // x ? ? | o o ?
      // ? ? ? | z ? ?
      // Let's use symmetry again #2
      assume z0 <= x2;
      assert z0 == x2 by {
        if z0 < x2 {
          // t = 0   t = 1
          // o . o | . o .
          // . o ? | o o ?
          // . o ? | . ? ?
          assert x == 0 by { gol(-1, 1, 0); }
          assert y1 == z1 == 1 by { gol(1, 0, 0); }
          assert z == 0 by { gol(2, 0, 0); }
          assert false by { gol(0, 1, 1); }
        }
      }
      ghost var A := z0;
      // t = 0   t = 1
      // o x A | . o z
      // x ? ? | o o ?
      // A ? ? | z ? ?
      assert x + A <= 1 by { gol(-1, 1, 0); }
      // assert z4 == y5 by { Failed to prove "mutant" constructed from child branches
      //   gol(1, 1, 1);
      //   gol(0, 1, 1);
      //   gol(1, 0, 1); // symmetrical to previous        
      // }
      assert z == 1 ==> (y5 == 1 - x == z4) by {
        gol(0, 1, 1);
        gol(1, 0, 1); // symmetrical to previous
      }
      assert y1 == 0 by {
        if y1 == 1 {
          // t = 0   t = 1
          // o x A | . o z
          // x o ? | o o ?
          // A ? ? | z ? ?
          assert sum(1, 1, 1) == 2 || sum(1, 1, 1) == 3 by {
            gol(1, 1, 1);
          }
          assert false by { gol(0, 1, 1); }
        }
      }
      // t = 0   t = 1
      // o . A | . o z   x0x1x2 | x3x4x5
      // . . ? | o o ?   y0y1y2 | y3y4y5
      // A ? ? | z ? ?   z0z1z2 | z3z4z5
      assert Group1: sum(1, 1, 0) == 3 by {
        gol(1, 1, 0);
        // assert false; // must be non-provable
      }
      assert Group2: sum(1, 1, 0) >= 5 by {
        gol(1, 0, 0); gol(0, 1, 0); gol(0, 0, 0);
        // assert false; // must be non-provable
      }
      assert x == 0 by { gol(0, 0, 0); }
      // If we can use symmetry, we use it because it have "low cost"
      // assert false; `assert false` can't be propagated to begginning of proof in that case, WISH: automate propagation of `assert false`
      assert Contradiction1: false by {
        reveal Group1;
        assert A == z1 == y2 == 1 by { gol(1, 0, 0); gol(0, 1, 0); }
      }
      // t = 0   t = 1
      // o . o | . o z
      // . . o | o o ?
      // o o ? | z ? ?
      // false by ??? WISH calculate `???` possible contradiction branches/variants
      // I think that we have false from `sum(1, 1, 0) == 3` and `sum(1, 1, 0) >= 5` but I don't know how to prove that... TODO: prove that!
      // Hint: Use labels to divide asserts to two independent groups.
      assert false by {
        reveal Group1;
        reveal Group2;
      }
    }
  }
  assert false; // Equivalent to `Qed.`
  // I think some message from Dafny can be printed:
  // Message: "You've eliminated all the subcases! Congratulations!"
}


