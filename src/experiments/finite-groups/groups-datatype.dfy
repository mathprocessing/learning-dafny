module Groups {

datatype G3 = E | A | B

function mul(x: G3, y: G3): G3
function inv(x: G3): G3

lemma {:verify false} id_right()
  ensures forall a :: mul(a, E) == a

lemma {:verify false} id_right_inst(a: G3)
  ensures mul(a, E) == a
{
  id_right();
}

lemma {:verify false} inv_right()
  ensures forall a :: mul(a, inv(a)) == E

lemma {:verify false} inv_right_inst(a: G3)
  ensures mul(a, inv(a)) == E
{
  inv_right();
}

// I think `lemma` + `ensures "expression"` 100% equivalent to `assume "expression"`
lemma {:verify false} assoc() // {:matchingloop} {:matchinglooprewrite true}
  ensures forall a, b, c {:trigger mul(mul(a, b), c)} {:matchingloop} {:matchinglooprewrite true} :: mul(mul(a, b), c) == mul(a, mul(b, c))

lemma {:verify false} assoc_inst(a: G3, b: G3, c: G3)
  ensures mul(mul(a, b), c) == mul(a, mul(b, c))
{
  assoc();
}

lemma inv_neq_id()
  ensures inv(A) != E
  ensures inv(B) != E
{
  // We can partially replace `id_right()` with
  // several id_right_inst and main idea: we don't touch `inv_right();`
  // Note: I think this method can help to find a proof incrementally

  // At starting state `id_right(); inv_right();` we know that
  // lemma can be proved using only two lemmas, but we don't know exact proof.

  // Next we can make several calls of `id_right_inst` in some random cases:

  // id_right_inst(E);
  // id_right_inst(A);
  // id_right_inst(B);
  // assert mul(E, E) == E;
  // assert mul(A, E) == A;
  // assert mul(B, E) == B;
  // Advise: make this calls before more general lemma `id_right()`
  // our goal is to replace general class of lemmas `id_right()` with instances.
  if inv(A) == E {
    inv_right_inst(A);
    id_right_inst(A);
    assert mul(A, inv(A)) == E;
    assert mul(A, E) == E;
    assert A == E;
    assert false;
  } else if inv(B) == E {
    inv_right_inst(B);
    id_right_inst(B);
    assert mul(B, inv(B)) == E;
    assert mul(B, E) == E;
    assert false;
  }
}

lemma G3_cases()
  ensures forall a :: a == E || a == A || a == B
{
  assert forall a :: a == E || a == A || a == B;
}

// id_unique_left: a * b = b -> a = e  
lemma {:verify false} id_unique_left()
  ensures forall a, b :: mul(a, b) == b ==> a == E;
{
  // id_right();
  inv_right();
  id_right();
  G3_cases();
  assert forall x :: x != E ==> mul(x, E) != E;
  inv_neq_id();

  assert mul(A, inv(A)) == E;
  assert mul(A, inv(A)) != A;
  assert mul(A, inv(A)) != A;
  assume inv(A) == A || inv(A) == B;
  assert mul(A, A) != A || mul(A, B) != A;
  assert mul(B, A) != A || mul(B, B) != B;
  // TODO: prove
  assert mul(A, A) != A;
  

  assert (forall b :: mul(A, b) != b);
  assert (forall b :: mul(B, b) != b);
}

lemma id_left_step2()
  requires forall a :: mul(a, inv(a)) == E;
  requires forall a :: mul(mul(inv(a), E), inv(inv(a))) == E;
  ensures forall a :: mul(mul(inv(a), inv(inv(a))), a) == a;
{
  id_right();
  inv_right();
  assoc();
}

lemma id_left()
  ensures forall a :: mul(E, a) == a
{
  id_right();
  inv_right();
  assert forall a :: mul(a, inv(a)) == E;
  assert forall a :: mul(mul(inv(a), E), inv(inv(a))) == E;
  id_left_step2();
}

lemma id_left_inst(a: G3)
  ensures mul(E, a) == a
{
  id_left();
}

lemma inv_id()
  ensures inv(E) == E
{
  // step 1.
  // id_left();
  // inv_right();
  // step 2.
  // id_left_inst(E); // failed
  // id_left_inst(inv(E)); // OK
  // inv_right();
  // step 3.
  id_left_inst(inv(E));
  // inv_right_inst(inv(E)); // failed
  inv_right_inst(E); // OK
  // Let's see same proof in Coq language:
  /*
  
  Lemma inv_id : inv e = e.      >> ensures inv(E) == E
  Proof.
    rewrite -(@id_left (inv e)). >> id_left_inst(inv(E));
    exact (@inv_right e).        >> inv_right_inst(E);
  Qed.

  Legend: `>>` = `corresponds to`

  */
}

lemma {:verify false} inv_id_hard()
  ensures inv(E) == E
{
  // We can prove this using only 3 axioms:
  // {id_right, inv_right, assoc}
  assert E == E;
  inv_right_inst(inv(E));
  // inv e * inv(inv e) = e
  assert mul(inv(E), inv(inv(E))) == E;
  id_right_inst(inv(E));
  // (inv e * e) * inv(inv e) = e
  assert mul(mul(inv(E), E), inv(inv(E))) == E;
  inv_right_inst(E);
  // (inv e * (e * inv e)) * inv(inv e) = e
  assert mul(mul(inv(E), mul(E, inv(E))), inv(inv(E))) == E;
  assoc_inst(inv(E), mul(E, inv(E)), inv(inv(E)));

  // equiv to replace mul( mul(inv(E), mul(E, inv(E))), inv(inv(E)) ) with mul(inv(E), mul(mul(E, inv(E)), inv(inv(E))))
  inv_right_inst(inv(E));
  // inv e * ((e * inv e) * inv(inv e)) = e
  assert mul(inv(E), mul(mul(E, inv(E)), inv(inv(E)))) == E;
  assoc_inst(E, inv(E), inv(inv(E)));

  // inv e * (e * (inv e * inv(inv e))) = e
  assert mul(inv(E), mul(E, mul(inv(E), inv(inv(E))))) == E;

  // inv e * (e * (inv e * inv(inv e))) = e

  assoc_inst(inv(E), E, E);
  // inv e * (e * e) = e
  assert mul(inv(E), mul(E, E)) == E;
  // inv e = e
}

lemma inv_id_hard_2()
  ensures inv(E) == E;
{
  id_right();
  inv_right();
  
  assert mul(inv(E), inv(inv(E))) == E; // triggers: inv_right_inst(inv(E))
  assert mul(inv(E), E) == inv(E);      // triggers: id_right_inst(inv(E))

  assoc();
}

// TEST 

method {:verify false} test()
{
  id_unique_left();
  assert forall a, b :: mul(a, b) == b ==> a == E;

  /**

  // id_left
  assert forall x:G3 :: mul(0, x) == x;

  // Lemma: inv_id
  assert inv(0) == 0;

  // inv_left
  assert forall a :: mul(inv(a), a) == 0;

  // inv_unique: a * b = e ==> b = inv a
  assert forall a, b :: mul(a, b) == 0 ==> b == inv(a);
  
  // inv_inv: inv (inv a) = a
  assert forall a :: inv(inv(a)) == a;

  // eq_inv: a = inv b <-> b = inv a
  assert forall a, b :: a == inv(b) <==> b == inv(a);
  // --- G3_order section ---
  
  // G3_is_abelian: a * b = b * a
  assert forall a, b :: mul(a, b) == mul(b, a);

  // G3_third_power: a * a * a = e
  assert forall a :: pow(a, 3) == 0;

  // assert forall a, n :: pow(a, n+3) == pow(a, n); // FAILED: assertion may not hold

  assert forall a, n:nat :: pow(a, n+2) == mul(pow(a, 2), pow(a, n)); // helper 1
  // assert forall a, n:nat :: pow(a, n+3) == mul(pow(a, 3), pow(a, n)); // helper 2 needs helper 1
  assert forall a, n:nat :: pow(a, n+3) == pow(a, n); // target needs only helper 1

  // G3_neq_id: x <> e -> (x * x <> e /\ x * x <> x)
  assert forall x :: x != 0 ==> mul(x, x) != 0 && mul(x, x) != x;


  // G3_inv_eq_imp_id
  assert forall x :: x == inv(x) ==> x == 0;
  // from this we can derive table for `inv :: G -> G`
  assert inv(0) == 0;
  assert inv(1) == 2;
  assert inv(2) == 1;

  // if forall a, b, c :: mul(mul(a, b), c) == mul(a, mul(b, c)) { // Inner scope for assoc axiom
    
    

    // exact inv_right
    assert forall a :: mul(a, inv(a)) == 0;
    // mul(a, 0) :== a from id_right
    assert forall a :: mul(mul(a, 0), inv(a)) == 0;
    // mul(b, inv(b)) :== 0 from inv_right
    assert forall a, b :: mul(mul(a, mul(b, inv(b))), inv(a)) == 0;
    // replace mul(mul(a, b), inv(b)) with mul(a, mul(b, inv(b))) from assoc
    assert forall a, b {:trigger mul(a, mul(b, inv(b)))} :: mul(a, mul(b, inv(b))) == mul(mul(a, b), inv(b));
    assert forall a, b :: mul(mul(mul(a, b), inv(b)), inv(a)) == 0;
    //                    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    //              abba: a * b * inv b * inv a = e := by rewrite [(@assoc a), inv_right, id_right, inv_right]
  
   */
}

}