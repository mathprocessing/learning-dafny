type G3 =  x: int | 0 <= x
// 0 = identity = E

function {:verify false} mul(x: G3, y: G3): G3
function  {:verify false} inv(x: G3): G3

function {:verify false} pow(a: G3, n: nat): G3
{
  if n == 0 then 0
  else mul(a, pow(a, n-1))
}

// Axioms of group
lemma id_right()
ensures forall x: G3 {:trigger mul(x, 0)} :: mul(x, 0) == x

lemma inv_right()
ensures forall x: G3 :: mul(x, inv(x)) == 0

// for detaled rewriting
lemma assoc_inst(a: G3, b: G3, c: G3)
ensures mul(mul(a, b), c) == mul(a, mul(b, c));

// z3 don't like assoc_1 because it triggers on itself (recursively)
lemma assoc_1()
ensures forall a: G3, b: G3, c: G3 :: mul(mul(a, b), c) == mul(a, mul(b, c))

lemma assoc_2()
ensures forall a: G3, b: G3, c: G3
  {:trigger mul(a, inv(a))}
  {:trigger mul(a, mul(b, inv(c)))}
:: mul(mul(a, b), c) == mul(a, mul(b, c))

lemma assoc_3()
ensures forall a: G3, b: G3, c: G3
  {:trigger mul(a, mul(b, inv(c)))}
  {:trigger mul(inv(a), mul(b, c))}
  {:trigger mul(a, mul(inv(b), c))}
:: mul(mul(a, b), c) == mul(a, mul(b, c))

// Let's mutate assoc_2 with anti assoc_3
lemma assoc_4()
ensures forall a: G3, b: G3, c: G3
  {:trigger mul(inv(a), inv(inv(a)))}
  {:trigger mul(a, mul(inv(b), c))}
  {:trigger mul(mul(inv(a), a), inv(b))}
  {:trigger mul(mul(inv(a), b), inv(c))} // if even with more general trigger => proof fails we can squash less general to more general, or not? (I think not, may be only change probabilities {p1: t1, p2: t2})
:: mul(mul(a, b), c) == mul(a, mul(b, c))
// if proof fails we must have some interface + metric:
// * find trigger `t` with metric M: M(t1) < M(t) < M(t2)
//                                   ^^^less general^^^more general
// * On z3 side: if some triggers discover new (non-quasiequivalent) expressions z3 can repeat that actions + some minimal mutations around them
// 

lemma assoc_exact_triggers()
ensures forall a: G3, b: G3, c: G3
// All combinations of triggers do not change anything, i.e. need understanding how exacly triggers work
  {:trigger mul(mul(inv(0), 0), inv(0))} // -> first inst
  {:trigger mul(mul(inv(0), 0), mul(inv(0), inv(inv(0))))} // <- second inst
  {:trigger mul(inv(0), mul(0, inv(0)))} // <- first inst
  {:trigger mul(mul(mul(inv(0), 0), inv(0)), inv(inv(0)))} // -> second inst
:: mul(mul(a, b), c) == mul(a, mul(b, c))

// Danger lemma!
// z3: don't stop via timeout (timeout = 3 sec but it computes infinitely)
lemma {:verify false} inv_left_id_danger(x: G3)
ensures mul(inv(0), 0) == 0
{
  inv_right();
  assoc_exact_triggers();
}

lemma inv_left_id_3(x: G3)
ensures mul(inv(0), 0) == 0
{
  id_right();
  inv_right();
  assoc_inst(inv(0), 0, inv(0));
  // <- (from right to left)
  assoc_inst(mul(inv(0), 0), inv(0), inv(inv(0)));
  // assoc_exact_triggers();
  // helper subgoal
  // assert mul(inv(0), inv(inv(0))) == 0;
  // assert mul(mul(inv(0), 0), inv(inv(0))) == 0;
}

lemma {:verify false}inv_left_id_2(x: G3)
ensures mul(inv(0), 0) == 0
{
  id_right();
  inv_right();
  // -> (rewrite from left to right)
  assoc_inst(inv(0), 0, inv(0));
  // <- (from right to left)
  assoc_inst(mul(inv(0), 0), inv(0), inv(inv(0)));
}

/**
Coq proof:
Lemma inv_left_id: inv E * E = E.
Proof.
  rewrite -(@id_right (inv E * E)).
  (* inv E * E * E = E *)

  rewrite -{3}(@inv_right (inv E)).
  (* inv E * E * (inv E * inv (inv E)) = E *)

  rewrite -(@assoc     (inv E * E)  (inv E)   (inv (inv E))).
  (* inv E * E * inv E * inv (inv E) = E *)

  rewrite  (@assoc         (inv E)       E          (inv E)).
  (* inv E * (E * inv E) * inv (inv E) = E *)

  rewrite  (@inv_right          E).
  (* inv E * E * inv (inv E) = E *)

  rewrite  (@id_right     (inv E)).
  (* inv E * inv (inv E) = E *)

  exact    (@inv_right    (inv E)).
Qed.
*/
lemma {:verify false} inv_left_id(x: G3)
ensures mul(inv(0), 0) == 0
{
  // (* inv E * inv (inv E) = E *)
  assert mul(inv(0), inv(inv(0))) == 0 by { inv_right(); }
  // * inv E * E * inv (inv E) = E *)
  assert mul(mul(inv(0), 0), inv(inv(0))) == 0 by {
    assert inv(0) == mul(inv(0), 0) by { id_right(); }
  }
  // (* inv E * (E * inv E) * inv (inv E) = E *)
  assert mul(mul(inv(0), mul(0, inv(0))), inv(inv(0))) == 0 by { inv_right(); }
  // (* inv E * E * inv E * inv (inv E) = E *)
  assert mul(mul(mul(inv(0), 0), inv(0)), inv(inv(0))) == 0 by {
    assert mul(inv(0), mul(0, inv(0))) == mul(mul(inv(0), 0), inv(0)) by {
      //     -> (inv E)  E  (inv E)
      assoc_inst(inv(0), 0, inv(0));
    }
  }
  // (* inv E * E * (inv E * inv (inv E)) = E *)
  assert mul(mul(inv(0), 0), mul(inv(0), inv(inv(0)))) == 0 by {
    assert mul(mul(mul(inv(0), 0), inv(0)), inv(inv(0))) == mul(mul(inv(0), 0), mul(inv(0), inv(inv(0)))) == 0 by {
      //     <- (inv E * E)    (inv E)  (inv (inv E)))
      assoc_inst(mul(inv(0), 0), inv(0), inv(inv(0)));
    }
  }
  // (* inv E * E * E = E *)
  assert mul(mul(inv(0), 0), 0) == 0 by { inv_right(); }
  // (* inv E * E = E *)
  assert mul(inv(0), 0) == 0 by { id_right(); }
}

lemma {:verify false} id_left(x: G3)
ensures mul(0, x) == x
{
  id_right();
  inv_right();
  assert mul(x, 0) == x;
  assert mul(x, mul(inv(x), inv(inv(x)))) == x; // second step of the proof (helper for assoc)
  // assoc_1();// assoc_1 OK
  // assoc_2(); // assoc_2 fails
  // assoc_3(); // loop z3
  assoc_4();
  assert mul(0, x) == x;
  // assert mul(0, mul(x, mul(inv(x), inv(inv(x))))) == 0;
}

method test_triggers()
{
  
}

// How to prove that order of element equal to particular number?
// order_elem(x) = minimal n >= 1 when pow(x, n) == e
//
// props:
// 1) forall x :: x ^ order_elem(x) == e
// 2) forall x n :: 1 <= n < order_elem(x) ==> x ^ n != e
function {:verify false} order_elem(a: G3): nat
  ensures forall x :: pow(x, order_elem(x)) == 0
  ensures forall x, n :: 1 <= n < order_elem(x) ==> pow(x, n) > 0
  ensures forall x :: order_elem(x) > 0

/**

Attributes:
* {:priority N}
* {:opaque}
* {:fuel functionName,lowFuel,highFuel}

page 230

Here are ways one can prove assert P(j + 4);: * Add assert Q(j + 2); just
before assert P(j + 4);, so that the verifier sees the trigger. * Change the
trigger {:trigger Q(i)} to {:trigger P(i)} (replace the trigger) * Change
the trigger {:trigger Q(i)} to {:trigger Q(i)} {:trigger P(i)} (add a
trigger) * Remove {:trigger Q(i)} so that it will automatically determine all
possible triggers thanks to the option /autoTriggers:1 which is the default.

 */

method {:verify false} prove_G3_bug_breaks_next_line(u: G3)
{
  assume forall a :: mul(a, 0) == a; // id_right
  assume forall a :: mul(a, inv(a)) == 0; // inv_right

  // from [id_right, inv_right]
  assert inv(1) == 1 || inv(1) == 2;
  assert inv(2) == 1 || inv(2) == 2;

  // assoc
  assume forall a, b, c :: mul(mul(a, b), c) == mul(a, mul(b, c));
  // {:trigger mul(mul(a, b), c)}
  
// helper for id_unique_left
  // a * (b * inv b) = e from [inv_right, assoc]
  assert forall a, b {:trigger mul(b, inv(b))} :: mul(a, b) == b ==> mul(a, mul(b, inv(b))) == 0;
  
  // id_unique_left: a * b = b -> a = e
  assert forall a, b :: mul(a, b) == b ==> a == 0;

  // contrapose
  assert forall a, b :: a > 0 ==> mul(a, b) != b;

  assert forall b :: mul(1, b) != b;

  // if inv(1) == 1 {

  //   assert mul(inv(1), 1) != 1;
  //   assert mul(inv(1), 1) == 0; // how to prove `false` from this step?
  //   assume false;
  // }

  // assert inv(1) == 1 ==> false;
  // assert inv(1) != 1;

  assume inv(1) == 2;
  assert forall b :: mul(1, b) != b;

// inv_unique: a * b = e ==> b = inv a
  // assume forall a, b :: mul(a, b) == 0 ==> b == inv(a);
  assume inv(0) == 1; // This assumption breaks ability to prove next line, WTF???
  assert forall a :: mul(1, a) != a; // we need this as helper

  if inv(0) == 1 {
    assert mul(0, 1) == 0;
    // use mul(1, b) != b && (b != inv(1) ==> mul(1, b) != 0) ??? it's wrong?
    // assert forall a :: mul(1, a) != a; // make loop but, assert mul(1, u) != u; works
    // assert mul(1, 1) != 1;

    // assert 1 != inv(1);
    // assert 1 != inv(1);
    // assert mul(1, 1) != 0;

    // assert mul(1, 1) != 2;
    // assert mul(mul(0, 1), c) == mul(0, mul(1, c));
    // assert mul(0, c) == mul(0, mul(1, c));
  }

  // inv_left
  // assert forall a :: mul(inv(a), a) == 0;

  // id_left
  // assert forall x :: mul(0, x) == x;

  // inv_id: inv e = e
  // assert inv(0) == 0;


  // // inv_inv: inv (inv a) = a
  // assert forall a :: inv(inv(a)) == a;

  // // id_unique_right: a * b = a -> b = e
  // assert forall a, b :: mul(a, b) == a ==> b == 0;
}

method {:verify false} prove_G3_assoc_simplified()
{  
  assume forall a :: mul(a, 0) == a; // id_right
  assume forall a :: mul(a, inv(a)) == 0; // inv_right

  // from [id_right, inv_right]
  assert inv(1) == 1 || inv(1) == 2;
  assert inv(2) == 1 || inv(2) == 2;

  // assoc
  // assume forall a, b, c {:trigger mul(mul(a, b), c)} :: mul(mul(a, b), c) == mul(a, mul(b, c));

  // G2_assoc
  // assume mul(mul(1, 1), 1) == mul(1, mul(1, 1));

  // G3_assoc
  // assume mul(mul(2, 2), 2) == mul(2, mul(2, 2));
  // assume mul(mul(1, 2), 1) == mul(1, mul(2, 1));
  // assume mul(mul(1, 1), 2) == mul(1, mul(1, 2));
  // assume mul(mul(2, 1), 2) == mul(2, mul(1, 2));
  // assume mul(mul(2, 0), 2) == mul(2, mul(0, 2));
  // assume mul(mul(2, 0), 1) == mul(2, mul(0, 1));
  // assume mul(mul(0, 0), 2) == mul(0, mul(0, 2));
  // assume mul(mul(2, 1), 0) == mul(2, mul(1, 0));
  // assume mul(mul(0, 1), 2) == mul(0, mul(1, 2));
  // THIS ARE CHECKED: they NOT PROVE all stuff, that must be provable by G3_assoc

  // assume forall a:G3, b: G3 :: mul(mul(a, b), 1) == mul(a, mul(b, 1));
  // assume forall a:G3, b: G3 :: mul(mul(a, b), 2) == mul(a, mul(b, 2));

  // G3_assoc simplified
  assume mul(mul(0, 0), 1) == mul(0, mul(0, 1));
  assume mul(mul(0, 1), 1) == mul(0, mul(1, 1));
  assume mul(mul(0, 2), 1) == mul(0, mul(2, 1));
  assume mul(mul(1, 0), 1) == mul(1, mul(0, 1));
  assume mul(mul(1, 1), 1) == mul(1, mul(1, 1));
  assume mul(mul(1, 2), 1) == mul(1, mul(2, 1));
  assume mul(mul(2, 0), 1) == mul(2, mul(0, 1));
  assume mul(mul(2, 1), 1) == mul(2, mul(1, 1));
  assume mul(mul(2, 2), 1) == mul(2, mul(2, 1));

  assume mul(mul(0, 0), 2) == mul(0, mul(0, 2));
  assume mul(mul(0, 1), 2) == mul(0, mul(1, 2));
  assume mul(mul(1, 0), 2) == mul(1, mul(0, 2));
  assume mul(mul(1, 1), 2) == mul(1, mul(1, 2));

  // assert mul(mul(0, 2), 2) == mul(0, mul(2, 2));
  // assert mul(mul(1, 2), 2) == mul(1, mul(2, 2));
  // assert mul(mul(2, 0), 2) == mul(2, mul(0, 2));
  // assert mul(mul(2, 1), 2) == mul(2, mul(1, 2));
  // assert mul(mul(2, 2), 2) == mul(2, mul(2, 2));
  
  // assert inv(2) == 1 ==> inv(1) == 2; // by assoc
  // contrapose 
  // assert inv(1) == 1 ==> inv(2) == 2; // by assoc + contrapose

  // id_right
  assert mul(0, 0) == 0;
  assert mul(1, 0) == 1;
  assert mul(2, 0) == 2;

  // inv_right
  assert mul(0, inv(0)) == 0;
  assert mul(1, inv(1)) == 0;
  assert mul(2, inv(2)) == 0;

  // assert inv(0) == 1 ==> (mul(0, 1) == 0); // from inv_right(0)

  // id_left
  assert mul(0, 1) == 1;
  assert mul(0, 2) == 2;
  assert forall x:G3 :: mul(0, x) == x; // CAN'T PROVE if you use just assoc axiom (not simplified)

  // Lemma: inv_id
  assert inv(0) == 0;

  // inv_left
  assert forall a :: mul(inv(a), a) == 0;

  // inv_unique: a * b = e ==> b = inv a
  assert forall a, b :: mul(a, b) == 0 ==> b == inv(a);
  
  // inv_inv: inv (inv a) = a
  assert forall a :: inv(inv(a)) == a;

  // id_unique_right: a * b = a -> b = e
  assert forall a, b :: mul(a, b) == a ==> b == 0;

  // id_unique_left: a * b = b -> a = e
  assert forall a, b :: mul(a, b) == b ==> a == 0;

  // eq_inv: a = inv b <-> b = inv a
  assert forall a, b :: a == inv(b) <==> b == inv(a);
  // --- G3_order section ---
  
  // G3_is_abelian: a * b = b * a
  assert forall a, b :: mul(a, b) == mul(b, a);

  // G3_third_power: a * a * a = e
  assert forall a :: pow(a, 3) == 0;

  // assert forall a, n :: pow(a, n+3) == pow(a, n); // FAILED: assertion may not hold

  assert forall a, n:nat :: pow(a, n+2) == mul(pow(a, 2), pow(a, n)); // helper 1
  // assert forall a, n:nat :: pow(a, n+3) == mul(pow(a, 3), pow(a, n)); // helper 2 needs helper 1
  assert forall a, n:nat :: pow(a, n+3) == pow(a, n); // target needs only helper 1

  // G3_neq_id: x <> e -> (x * x <> e /\ x * x <> x)
  assert forall x :: x != 0 ==> mul(x, x) != 0 && mul(x, x) != x;


  // G3_inv_eq_imp_id
  assert forall x :: x == inv(x) ==> x == 0;
  // from this we can derive table for `inv :: G -> G`
  assert inv(0) == 0;
  assert inv(1) == 2;
  assert inv(2) == 1;

  // if forall a, b, c :: mul(mul(a, b), c) == mul(a, mul(b, c)) { // Inner scope for assoc axiom
    
    

    // exact inv_right
    assert forall a :: mul(a, inv(a)) == 0;
    // mul(a, 0) :== a from id_right
    assert forall a :: mul(mul(a, 0), inv(a)) == 0;
    // mul(b, inv(b)) :== 0 from inv_right
    assert forall a, b :: mul(mul(a, mul(b, inv(b))), inv(a)) == 0;
    // replace mul(mul(a, b), inv(b)) with mul(a, mul(b, inv(b))) from assoc
    assert forall a, b {:trigger mul(a, mul(b, inv(b)))} :: mul(a, mul(b, inv(b))) == mul(mul(a, b), inv(b));
    assert forall a, b :: mul(mul(mul(a, b), inv(b)), inv(a)) == 0;
    //                    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    //              abba: a * b * inv b * inv a = e := by rewrite [(@assoc a), inv_right, id_right, inv_right]


    
  // }

  // assert forall a:G3, b: G3 :: mul(mul(a, b), 0) == mul(a, mul(b, 0)); // PROVED by [id_right, inv_right]
  // assert forall a:G3, b: G3 :: mul(mul(a, 0), b) == mul(a, mul(0, b)); // can't prove?
  // assert forall a:G3, b: G3 :: mul(mul(0, a), b) == mul(0, mul(a, b)); // can't prove?
    // assert forall a:G3 :: mul(mul(0, a), 1) == mul(0, mul(a, 1));
      // assert mul(0, 1) == mul(0, mul(0, 1)); gived NEW INFO when `mul(0, 1) == 2 ==> 2 == mul(0, 2)`
        // assert mul(0, 1) == 2 ==> 2 == mul(0, 2); // gived NEW INFO
          // assert mul(0, 1) == 2 ==> 0 != mul(0, 2); // wrong subgoals set
          // assert mul(0, 1) == 2 ==> 1 != mul(0, 2);

    // assert mul(mul(0, 1), 1) == mul(0, mul(1, 1));
    // assert mul(mul(0, 2), 1) == mul(0, mul(2, 1));

  

  // inv_left
  // assert mul(inv(0), 0) == 0; // strange thing that z3 can't prove this
  // but can prove this
  // assert forall x :: mul(x, 0) == x;
  
  // assert mul(inv(1), 1) == 0;
  // assert mul(inv(2), 2) == 0;
  // assert inv(1) == 2;

  // assert forall a:G3, b: G3 :: mul(mul(a, b), 0) == mul(a, mul(b, 0)); // from [id_right, inv_right]

  // assert inv(1) == 2 ==> inv(2) == 1; hard to prove

  
  // assert inv(0) == 0;
  // assert inv(1) == 2;
  

  // assert forall a:G3 :: inv(a) == a;
  // e * e = x * x = e
  // x * e = e * x = x

  // We can prove assoc from G2_assoc
  // assert forall a, b, c :: mul(mul(a, b), c) == mul(a, mul(b, c));

  // G3 is Abelian
  // assert forall a:G3, b:G3 :: mul(a, b) == mul(b, a);

  // assert forall x :: pow(x, 0) == 0;
  // assert forall x :: pow(x, 1) == x;
  // assert forall x :: pow(x, 2) == mul(x, x) == 0;
  // assert forall x :: pow(x, 3) == mul(x, mul(x, x)) == mul(mul(x, x), x) == x;


  // assert forall x:G2, m:nat :: pow(x, 2*m) == pow(x, 2*(m+1)); Warning: triggering loop
  // assert forall x:G2, m:nat :: pow(x, 2*m) == 0; FAILED

  // assert forall n :: 1 <= n < order_elem(0) ==> pow(0, n) > 0;
  // assert forall n :: 1 <= n < order_elem(0) ==> 0 > 0;
  // assert forall n :: 1 <= n < order_elem(0) ==> false;
  // assert order_elem(0) == 1; // e ^ 1 = e
  // assert order_elem(1) == 2; // a ^ 2 = e but a ^ 1 != e
}