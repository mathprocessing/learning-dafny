type G = nat
const E : G := 0

function mul(x: G, y: G): G
function inv(x: G): G

lemma id_right()
  ensures forall a :: mul(a, E) == a

lemma id_right_inst(a: G)
  ensures mul(a, E) == a

lemma inv_right()
  ensures forall a :: mul(a, inv(a)) == E

lemma inv_right_inst(a: G)
  ensures mul(a, inv(a)) == E

lemma assoc()
  ensures forall a,b,c {:autotriggers false} :: mul(mul(a, b), c) == mul(a, mul(b, c));

lemma assoc_inst(a: G, b: G, c: G)
  ensures mul(mul(a, b), c) == mul(a, mul(b, c));

// ---------------------------------------------------------
// Long proofs, helpers BEGIN
// ---------------------------------------------------------

lemma Klein_pow_helper(a: G)
  requires mul(a, a) == E
  ensures forall m:nat :: pow(a, m) == pow(a, m+2)
{
  // helps to verify step3
  // forall u: G ensures mul(mul(a, a), pow(a, u)) == mul(a, mul(a, pow(a, u))) {
  //   assoc_inst(a, a, pow(a, u));
  // }

  // ghost var step3 := forall m:nat:: mul(mul(a, a), pow(a, m)) == pow(a, m+2);

  // assert pow(a, 0) == E;
  // assert forall m:nat:: mul(a, pow(a, m)) == pow(a, m+1);
  // assert forall m:nat:: mul(a, mul(a, pow(a, m))) == pow(a, m+2);
  // assert step3;

  forall m: G ensures pow(a, m) == pow(a, m+2) {
    calc {
      pow(a, m);
      ==
      {
        // We can prove intermediate steps in calc
        // And main thing: We can't see `id_left` outside of `calc` statement
        id_left();
      }
      mul(E, pow(a, m));
      == 
      mul(mul(a, a), pow(a, m));
      ==
      {
        // We can check that it's working and after it move to details...
        // assume mul(mul(a, a), pow(a, m)) == pow(a, m+2);
        // assoc(); // Also works, and let's make proof even more detaled:
        assoc_inst(a, a, pow(a, m)); // OK
        // Can we make proof even more detailed?
        // Answer: May be if we replace assoc axiom with several other axioms...
      }
      pow(a, m+2);
    }
  }
}

// ---------------------------------------------------------
// Long proofs, helpers END
// ---------------------------------------------------------

// Verified
function {:verify false} pow(a: G, n: nat): G
{
  if n == 0 then E
  else mul(a, pow(a, n-1))
}

// Not Verified: cannot prove termination; try supplying a decreases clause
function {:verify false} order_elem(a: G): nat
  ensures forall x :: pow(x, order_elem(x)) == E
  ensures forall x, n :: 1 <= n < order_elem(x) ==> pow(x, n) > 0
  ensures forall x :: order_elem(x) > 0

// -------------------------------------------------

lemma id_left()
ensures forall a :: mul(E, a) == a

lemma id_left_inst(a: G)
ensures mul(E, a) == a
{
  id_left();
}

// Verified
lemma {:verify false} inv_left_id()
ensures mul(inv(E), E) == E
{
  if * {
      id_right();
      inv_right();
      assoc();
      calc == {
        mul(inv(E), E);
        mul(inv(E), inv(inv(E)));
        E;
      }
    } 
    if * {
    // simpler way to prove (more short dafny code may still exists)
    // Note: shorter dafny code != shorter proof, i.e. shortest proof (that checks fast) ==> long detailed dafny code
      calc == {
        mul(inv(E), E);
        {
          assert inv(inv(E)) == E by {
            // WISH:
            // How to ensure (in dafny code) that {F1, F2, F3} is minimal set of lemmas.
            // is_minimal({F1, F2, F3}) = do_proof({F1, F2, F3}) && !(do_proof({F1, F2}) || do_proof({F1, F3}) || do_proof({F2, F3}))
            // or another definition:
            // is_strongly_minimal(S) = proof_score(S) && forall reduce_action in RActions :: proof_score(reduced(S, reduce_action)) < proof_score(S)
            // is_minimal(S)          = proof_score(S) && forall reduce_action in RActions :: proof_score(reduced(S, reduce_action)) <= proof_score(S)
            // Test case (concrete example): reduced({F1, F2, F3}, reduce_action)
            //
            id_right(); // F1
            inv_right(); // F2
            assoc(); // F3
          }
        }
        mul(inv(E), inv(inv(E)));
        {
          inv_right_inst(inv(E));
        }
        E;
      }
   } else {
    // more detailed view of same (possible same) proof
    calc == {
      mul(inv(E), E);
      {
        assert mul(inv(E), inv(inv(E))) == E by {
          if * {
            inv_right(); // Prove single rewrite step using just lemma with quantifier
          } else {
            inv_right_inst(inv(E));// Using lemma instance
          }
        }
      }
      mul(inv(E), mul(inv(E), inv(inv(E))));
      // It might be helpful if in dafny we can write equivalence classes of expressions (or simple patterns with holes)
      // Example pattern: 
      //   case1 => mul(inv(E), ?a), where ?a = (_, E) | (_, inv(E)) | (_, inv)
      //   case2 => expr@mul(?b, E), where ?b = (_, inv(E)) | number_of_lists(expr) <= 42
      // and after some try #N dafny (or z3 prover) give us cache object case_info_try_N that contains simplified (i.e. forgeting some details) "state"/"set of explored branches"
      // Then we can do try #(N+1) and use case_info_try_N to reduce amount of computation (do not "calculate facts about"/"explore" already explored branches/models)
      // For each try we might see some "statistics"/"helpful properties of processing this try or group of tries" and that info can help us to select new actions,
      // do next (possible better, or more formally with Q >= minimal_Q with probability P ) tries in other regions of computational space.
      {
        assoc();
      }
      mul(mul(inv(E), inv(E)), inv(inv(E)));
      // Now I don't know complexity of proof P for lemma `inv_inv_id: inv(E) * inv(E) = E` using only {id_right, inv_right, assoc}
      // If proof P is easy:
      //    just use inv_inv_id // selecting that case may help to reduce time of current proof but not in all future proofs
      // else:                  // how to reduce time in future proofs by using (only) this proof (state, current task, X...)? // Why we humans or animals do not doing infinite loops? Or we do but not exact loops? loop in equivalence classes but not in details?
      //    don't use inv_inv_id and try to find another way to solve problem + change experience
      {
        // We can do things like (replace X with Y) tactic in Coq.
        assume mul(inv(E), inv(E)) == E; // corresponds to patterns: {mul(?x, ?x) == _, ...}
      }
      mul(E, inv(inv(E)));
      {
        // What is better use
        // Case A: id_left and after inv_inv_id
        // or use
        // Case B: inv_inv_id and after id_right?
        // I think Case B better because we don't have id_left as proved lemma.
        assume inv(inv(E)) == E;
      }
      mul(E, E);
      {
        // assert mul(E, E) == E by { id_right(); }
        // <=> to
        //id_right();
        // but not really equivalent to (I mean that some equivalence of type1 holds, but other equivalence of type 2 not holds)
        id_right_inst(E); // <=> rewrite (@id_right E). in Coq
      }
      E;
    }
  }
}

lemma inv_left()
ensures forall a :: mul(inv(a), a) == E
{
  forall a : G | a > E ensures mul(inv(a), a) == E {
    assume false;
  }
  inv_left_id();
}

lemma inv_inv()
ensures forall a :: inv(inv(a)) == a
{
  forall a : G ensures inv(inv(a)) == a {
    /* 
    Way 1:
    i (i a) = a
    i (i a) * (i a) = a * (i a) by f_equal (fun x * (i a) => )
    i (i a) * (i a) = e         by inv_right
    e = e                       by inv_left

    Way 2:
    i (i a) = a
    i (i a) * e = a
    i (i a) * i a * a = a
    e * a = a
    a = a
    */
    calc == {
      a;

        { id_left_inst(a); }

      mul(E, a);

        { assert mul(inv(inv(a)), inv(a)) == E by { inv_left(); } }

      mul(mul(inv(inv(a)), inv(a)), a);

        { assoc(); }

      mul(inv(inv(a)), mul(inv(a), a));

        { inv_left(); }

      mul(inv(inv(a)), E);

        { id_right(); }

      inv(inv(a));
    }
  }
}

lemma id_unique_left()
ensures forall a, b :: mul(a, b) == b ==> a == E;

lemma id_unique_right()
ensures forall a, b :: mul(a, b) == a ==> b == E;

// Verified
lemma {:induction n} {:verify false} Klein_pow(a: G, n:nat)
  requires mul(a, a) == E
  ensures pow(a, 2*n) == E
{
  if n > 0 {
    Klein_pow_helper(a);
    // We have induction step (we disabled instantiation of quantifier by `:autotriggers false`
    // because we don't need it, also to disable warning one can use :nowarn)
    assert forall m:nat {:autotriggers false} :: pow(a, 2*m) == E ==> pow(a, 2*(m + 1)) == E;
    Klein_pow(a, n - 1);
  } else {
    // Base case
    assert pow(a, 0) == E;
  }
}

predicate isAbelian(x: G, y: G)
{
  mul(x, y) == mul(y, x)
}

// If group is Abelian we have fact1, fact2, ...
lemma {:verify true} Abelian(x: G, y: G)
requires mul(x, y) == mul(y, x)
{
  assert mul(x, y) == mul(y, x);

  if x == E {
    id_left();
    assert mul(x, x) == E;
  }
  // We can add some facts that implies from isAbelian(a, b)
}





