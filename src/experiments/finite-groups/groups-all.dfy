type G =  x: int | 0 <= x

function mul(x: G, y: G): G
function inv(x: G): G

function {:verify true} pow(a: G, n: nat): G
{
  if n == 0 then 0
  else mul(a, pow(a, n-1))
}

// How to prove that order of element equal to particular number?
// order_elem(x) = minimal n >= 1 when pow(x, n) == e
//
// props:
// 1) forall x :: x ^ order_elem(x) == e
// 2) forall x n :: 1 <= n < order_elem(x) ==> x ^ n != e
function {:verify false} order_elem(a: G): nat
  ensures forall x :: pow(x, order_elem(x)) == 0
  ensures forall x, n :: 1 <= n < order_elem(x) ==> pow(x, n) > 0
  ensures forall x :: order_elem(x) > 0

lemma id_right()
  ensures forall a :: mul(a, 0) == a

lemma inv_right()
  ensures forall a :: mul(a, inv(a)) == 0

method proof()
{
  id_right();
  inv_right();
}
