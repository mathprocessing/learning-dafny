type G2 = x: int | 0 <= x < 2
const E : G2 := 0


function mul(x: G2, y: G2): G2
function inv(x: G2): G2
// function e(): G2

function xor(x: G2, y: G2): G2
{
  if x == y then 0 else 1
}

// ensures forall a :: xor(a, a) == e()

/**
Axioms:
id_right  (a : G) :: mul(a, e()) == a
inv_right (a : G) :: mul(a, inv(a)) == e()
assoc (a b c : G) :: mul(mul(a, b), c) == mul(a, mul(b, c))

Lemmas for all groups:
inv_left (a : G) :: mul(inv(a), a) == e()
id_left (a : G) :: mul(e(), a) == a

for G3 (groups of order == 3):
G3_comm (a b : G) :: mul(a, b) == mul(b, a)
...

 */

// method G3_test(x: G3, y: G3)
// {
//   assume forall a: G3 :: mul(a, e()) == a;
//   assume forall a: G3 :: mul(a, inv(a)) == e();
//   assume forall a:G3, b:G3, c: G3 :: mul(mul(a, b), c) == mul(a, mul(b, c));

//   var i:= e();

//   // e * e = e
//   assert mul(i, i) == i;

//   // a * e = a from id_right
//   assert mul(x, i) == x;

//   // inv_right (a : G) :: inv a * a == e
//   assume forall a: G3 :: mul(inv(a), a) == e();

//   // id_left: e * a = a
//   // assert mul(i, x) == x; // 180 sec not enough to verify id_left even if inv_left is assumed

  
// }

// method G2_test(x: G2)
// {
//   assume forall a: G2 :: mul(a, e()) == a;
//   assume forall a: G2 :: mul(a, inv(a)) == e();
//   assume forall a:G2, b:G2, c: G2 :: mul(mul(a, b), c) == mul(a, mul(b, c));

//   assert mul(e(), e()) == e();
//   assert mul(x, e()) == x;

//   assert mul(e(), 1) == 1;
// }

method {:verify false} simple_G2()
{
  assume forall a: G2 :: mul(a, 0) == a;
  assume forall a: G2 :: mul(0, a) == a;
  assume forall a: G2 :: mul(a, inv(a)) == 0;
  assume forall a: G2 :: mul(inv(a), a) == 0;
  // assume forall a: G2 :: inv(0) == 0;
  assert inv(0) == 0;
  assert inv(1) == 1;

  assert forall a: G2 :: inv(inv(a)) == a;
  assert mul(0, 0) == 0;
  assert mul(1, 0) == 1;
  assert mul(0, 1) == 1;
  assert mul(1, 1) == 0;

  // G2 is Abel group
  assert forall a: G2, b: G2 :: mul(a, b) == mul(b, a);

}

method harder_without_assoc_G2()
{
  assume forall a: G2 :: mul(a, 0) == a;
  assume forall a: G2 :: mul(a, inv(a)) == 0;

  /**
    id_right:  mul(a, 0) == a
    inv_right: mul(a, inv(a)) == 0

    from id_right
    mul 0 0 == 0
    mul 1 0 == 1

    from inv_right
    mul 0 (inv 0) == 0 from inv_right(0)
    mul 1 (inv 1) == 0 from inv_right(1)

  let inv 0 = 1
    mul 0 1 == 0

    summary:
    mul 0 0 = 0
    mul 0 1 = 0
    mul 1 0 = 1
    mul 1 1 = {0, 1}

    inv 0 = 1
    inv 1 = 1
      proof:
      inv 1 = {0, 1}
      AND
      if inv 1 != 0 then
        inv 1 = 1 from inv 1 = {0, 1} from `all elements in G2` ::: |G| == 2 where |G| is order of group G
        :::
        mul 1 0 == 0 from inv_right(1)
        :::
        false from mul 1 0 = 1 [inv_right(0), assumption inv 0 = 1]
   */

  // assume inv(0) == 1;

  // assert inv(0) <= 1;
  // assert inv(0) >= 0;

  // assert mul(1, 0) == 1;
  // assert mul(1, 1) <= 1;

  // assert inv(0) == 1;
  // assert inv(1) == 1;

  assert mul(0, 0) == 0;
  assert mul(0, 1) <= 1; // don't know (how to prove formally that we `don't know`? can't know?)
  assert mul(1, 0) == 1;
  assert mul(1, 1) <= 1; // don't know
  // assert forall a: G2 :: inv(inv(a)) == a;
}

method harder_G2()
  ensures mul(0, 0) == 0;
  ensures mul(0, 1) == 1;
  ensures mul(1, 0) == 1;
  ensures mul(1, 1) == 0;
  ensures forall a:G2 :: inv(a) == a;
{
  // assume forall a, b, c {:matchingloop} {:matchinglooprewrite true} :: mul(mul(a, b), c) == mul(a, mul(b, c));
  assume forall a: G2, b: G2, c: G2 {:autotriggers false} :: mul(mul(a, b), c) == mul(a, mul(b, c));
  assume forall a: G2 :: mul(a, 0) == a;
  assume forall a: G2 :: mul(a, inv(a)) == 0;

  // from id_right
  assert mul(0, 0) == 0; 
  assert mul(1, 0) == 1;

  /* 
  if forall a, inv(a) == 0 ==> False by [id_right, inv_right]
    mul(a, 0) == a from id_right
    mul(a, 0) == 0 from inv_right
    if a == 1 then 1 == 0 i.e. FALSE

  if forall a, inv(a) == 1 - a ==> False by [id_right, inv_right]
    mul(a, 1 - a) == 0 contradicts with mul(1, 0) == 1 ==> FALSE

  if forall a, inv(a) == 1 ==> False by [assoc, inv_right]
    mul(a, 0) == a
    mul(a, 1) == 0
      from assoc: mul(mul(1, 0), 1) == mul(1, mul(0, 1)) ::: 
        mul(1, 1) == 0 ==> mul(0, 1) == 1 but from inv_right mul(_, 1) == 0

  PROOF RESULT:
    forall a, inv(a) == a
  */
   // from [id_right, inv_right]
  assert inv(1) == 0 ==> false;
  assert inv(1) == 1;
   // from [id_right, inv_right, assoc]
  assert forall a:G2 :: inv(a) == a;

  // from [id_right, inv_right, assoc]
  assert mul(1, 1) == 0 ==> mul(0, 1) == 1; 
  // if a != e && a * a == e then e * a == a
  // but forall x :: a * a == e
  // hmm... then we have id_left: forall x :: e * x == x

  // id_left: e * x = x
  assert mul(0, 0) == 0;
  assert mul(0, 1) == 1;
  assert mul(1, 0) == 1;
  assert mul(1, 1) == 0;
  // assert mul(1, 1) == 1; // hard to find counterexample by z3, how to help it?
  // assert forall x:G2 :: mul(0, x) == x; // Warning: if you add uncomment this line z3 creates an infnite loop

  // Assoc info part (when from assoc we can gain info), i.e. which triples of (a, b, c) give us some new info instead of two other axioms
  assert mul(0, 0) == 0;
  // assert mul(0, 1) <= 1; // don't know
  assert mul(1, 0) == 1;
  // assert mul(1, 1) <= 1; // don't know
  // without assoc we don't know which variant is true: `mul(1, 1) == 0` or `mul(1, 1) == 1`

  // from assoc
  // not useful mul(mul(0, 0), 0) == mul(0, mul(0, 0)) because ::: mul(0, 0) == mul(0, 0)
  // assert mul(mul(0, 0), 1) == mul(0, mul(0, 1));
  //   assert mul(0, 1) == 1 ==> 1 == mul(0, 1); // no new info
  //   assert mul(0, 1) == 0 ==> 0 == mul(0, 0); // no new info
  // assert mul(mul(0, 1), 0) == mul(0, mul(1, 0));
  //   assert mul(0, 1) == 0 ==> mul(0, 0) == 0; // no new info
  //   assert mul(0, 1) == 1 ==> mul(1, 0) == 1; // no new info
  // assert mul(mul(0, 1), 1) == mul(0, mul(1, 1));
  //   assert mul(0, 1) == 0 && mul(1, 1) == 0 ==> 0 == mul(0, 0); // no new info
  //   assert mul(0, 1) == 0 && mul(1, 1) == 1 ==> 0 == mul(0, 1); // no new info
  //   assert mul(0, 1) == 1 && mul(1, 1) == 0 ==> 0 == mul(0, 0); // no new info
  //   assert mul(0, 1) == 1 && mul(1, 1) == 1 ==> 1 == mul(0, 1); // no new info
  // assert mul(mul(1, 0), 0) == mul(1, mul(0, 0));
  //   assert mul(1, 0) == mul(1, 0); // no new info

  // assert mul(mul(1, 0), 1) == mul(1, mul(0, 1)); // HAVE INFO
    // assert mul(1, 1) == 1 ==> 1 == 1; // no new info
    // assert mul(1, 1) == 0 ==> 1 == mul(0, 1); // mul(1, x) = 1 - x, we have info: mul(1, 1) == 0 ==> mul(0, 1) == 1

  // assert mul(mul(1, 1), 0) == mul(1, mul(1, 0));
  //   assert mul(1, 1) == mul(1, 1); // no new info

  assert mul(mul(1, 1), 1) == mul(1, mul(1, 1)); // HAVE INFO
    assert mul(1, 1) == 1 ==> mul(1, 1) == mul(1, 1); // no new info
    assert mul(1, 1) == 0 ==> mul(0, 1) == 1; // now (a, b, c) = (1, 1, 1) and we GAIN same INFO that from (a, b, c) = (1, 0, 1)

  // Conclusion:
  // we can simplify assoc for G2 as:
  // (a, b, c) = (1, 0, 1) | (1, 1, 1) i.e. (a, b, c) = (1, b, 1)
  // 
  // G2_assoc: forall b :: mul(mul(1, b), 1) == mul(1, mul(b, 1))
  // 
}

// optimized from assoc rewriting loop, just by exporting all clauses using `ensures`
method harder_G2_optimized()
{
  var xor := (a:G2, b:G2) => if a != b then 1 else 0;
  harder_G2();
  assert mul(0, 0) == 0;
  assert mul(0, 1) == 1;
  assert mul(1, 0) == 1;
  assert mul(1, 1) == 0;

  assert forall a:G2 :: inv(a) == a;
  assert forall a:G2, b:G2 :: mul(a, b) == xor(a, b);
}

function pow(a: G2, n: nat): G2
{
  if n == 0 then 0
  else mul(a, pow(a, n-1))
}

// How to prove that order of element equal to particular number?
// order_elem(x) = minimal n >= 1 when pow(x, n) == e
//
// props:
// 1) forall x :: x ^ order_elem(x) == e
// 2) forall x n :: 1 <= n < order_elem(x) ==> x ^ n != e
function {:verify false} order_elem(a: G2): nat
  ensures forall x :: pow(x, order_elem(x)) == 0
  ensures forall x:G2, n :: 1 <= n < order_elem(x) ==> pow(x, n) > 0
  ensures forall x:G2 :: order_elem(x) > 0

method harder_G2_assoc_simplified()
{
  var xor := (a:G2, b:G2) => if a != b then 1 else 0;
  // G2_assoc: forall b :: mul(mul(1, 1), 1) == mul(1, mul(1, 1))
  assume mul(mul(1, 1), 1) == mul(1, mul(1, 1));
  assume forall a: G2 :: mul(a, 0) == a;
  assume forall a: G2 :: mul(a, inv(a)) == 0;


  assert inv(1) == 1; // without this helper step proof don't work

  assert forall a:G2 :: inv(a) == a;
  assert forall a:G2, b:G2 :: mul(a, b) == xor(a, b);
  // e * e = x * x = e
  // x * e = e * x = x

  // We can prove assoc from G2_assoc
  // assert forall a, b, c :: mul(mul(a, b), c) == mul(a, mul(b, c));

  // G2 is Abelian
  assert forall a:G2, b:G2 :: mul(a, b) == mul(b, a);

  // because xor is commutative
  assert forall a:G2, b:G2 :: xor(a, b) == xor(b, a);

  // and `xor is commutative` because `equality is commutative`
  assert forall a:G2, b:G2 :: a != b <==> b != a;

  assert forall x :: pow(x, 0) == 0;
  assert forall x :: pow(x, 1) == x;
  assert forall x :: pow(x, 2) == mul(x, x) == 0;
  assert forall x :: pow(x, 3) == mul(x, mul(x, x)) == mul(mul(x, x), x) == x;

  assert forall x:G2, n:nat :: pow(x, n) == 0 ==> pow(x, n+2) == 0;

  assert forall x:G2, m:nat :: pow(x, m) == pow(x, m+2);
  // assert forall x:G2, m:nat :: pow(x, 2*m) == pow(x, 2*(m+1)); Warning: triggering loop
  // assert forall x:G2, m:nat :: pow(x, 2*m) == 0; FAILED

  // assert forall n :: 1 <= n < order_elem(0) ==> pow(0, n) > 0;
  // assert forall n :: 1 <= n < order_elem(0) ==> 0 > 0;
  // assert forall n :: 1 <= n < order_elem(0) ==> false;
  assert order_elem(0) == 1; // e ^ 1 = e
  assert order_elem(1) == 2; // a ^ 2 = e but a ^ 1 != e
}