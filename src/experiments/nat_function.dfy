function sum(x: nat): nat
  decreases x
{
  if x == 0 then 0 else sum(x-1) + x
}

predicate even(x: nat)
  decreases x
{
  if x == 0 then true else
  if x == 1 then false else
  even(x - 2)
}

predicate odd(x: nat)
  decreases x
  ensures odd(x) == !even(x)
{
  if x == 0 then false else
  if x == 1 then true else
  odd(x - 2)
}

function f(x: nat): nat
function g(x: nat): nat

function product(x: nat): nat
{
  if x == 0 then 1 else
  product(x - 1) * f(x)
}

function summ(x: nat): nat
{
  if x == 0 then f(0) else
  summ(x-1) + f(x)
}

lemma prop(n: nat)
  requires forall m:nat :: f(m) + f(m + 1) == 1
  ensures f(n) <= 1 // TODO: How to elegantly prove this?
{
  assert f(0) + f(1) == 1; // we need to help rewriter by giving concrete example
  assert f(1) + f(2) == 1;
  assert f(0) == f(2);
  assert f(1) == f(3);
  assert f(2) == f(4);
  assert f(0) == f(4);
  assert summ(0) <= 1;
  assert f(0) + f(1) == 1;
  // Base case
  assert f(0) <= 1;

  // Induction step
  assert forall m:nat :: f(m) <= 1 ==> f(m+1) <= 1;
}

method main(n: nat)
{
  // assume forall m:nat {:matchingloop} {:matchinglooprewrite false} :: f(m) + f(m + 1) == 1;
  assume forall m:nat :: f(m) + f(m + 1) == 1;
  // assert even(n) || odd(n);
  assert forall m:nat :: f(m) + f(m + 1) + f(m + 2) <= 2;
  // assert f(0) * f(1) == 0;
  // assert f(1) * f(2) == 0;

  // assert product(0) == 1;
  // assert product(1) <= 1;
  // assert n >= 2 ==> product(n) == 0;
  // assert product(n) <= 1;

  // assert forall m:nat, k:nat, mlow: nat, klow: nat :: mlow <= m && klow <= k ==> mlow * klow <= m * k;
  prop(n);

  assert summ(1) == 1;
  // summ(1) = summ(0) + f(1) = f(0) + f(1) = 1

  assert 1 <= summ(2) <= 2;
  assert summ(2) == 1 + f(2);
  // summ(2) = summ(1) + f(2) = 1 + f(2) ==> summ(2) in [1..2]

  assert summ(3) == 2;
  // summ(3) = summ(2) + f(3) = 1 + f(2) + f(3) = 2 because f(2) + f(3) = 1

  assert summ(4) == summ(2) + 1;
  // summ(4) = summ(3) + f(4) = 2 + f(2) = summ(2) + 1

  assert summ(5) == 3;
  // summ(5) = summ(4) + f(5) = 2 + f(2) + f(3) = 3
  // Guess: forall n :: summ(2*n + 1) == n + 1

  // Base case
  assert summ(2*0 + 1) == 0 + 1;

  // Induction step
  assert summ(2*n + 1) == n + 1 ==> summ(2*(n+1) + 1) == (n+1) + 1;

  // We can manully assume that `summ(2*n + 1) == n + 1` in true
  assume forall m:nat :: summ(2*m + 1) == m + 1;
  assert forall m:nat :: summ(2*m) <= m + 1;

  // Base case
  assert summ(0) <= 0 + 1;

  // Induction step
  assert forall m:nat :: summ(m) <= m+1 ==> summ(m+1) <= (m+1) + 1;
  assert forall m:nat :: f(m) + f(m + 1) == 1;

  

  assert f(n) <= 1;

  // Base case
  assert f(0) == f(0 + 2);

  // Induction step
  assert forall m:nat :: f(m) == f(m + 2) ==> f(m+1) == f((m+1) + 2);

  // assert forall m:nat :: f(m+1) == 1 - f(m); // can't help to prove
  // i mean `f(m + 2) == f(m)` can be derived from `f(m + 2) == 1 - f(m+1)` and `f(m + 1) == 1 - f(m)`
  // assert forall m:nat :: f(m + 2) == f(m);
  assume forall m:nat :: f(m) == f(m + 2);

  assert forall m:nat :: f(m + 2) + 1 == f(m) + 1;

  
  // assert false;


  
}