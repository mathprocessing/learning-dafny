// Graph defined by list of Weighted edges
// weight must be positive
type Tweight = x : int | x > 0 witness 1
type id = nat // vertex id in graph
datatype wedge = WE(i: id, j: id, w: Tweight)
type graph = seq<wedge>

// square graph
const g := [WE(0, 1, 10), WE(1, 0, 5)]

// assert vertices g == [0, 1] OR g == iset{0, 1}

/* S.Set interface: 
  S.null :: Set -> bool -- returns set is empty or not
  S.insert - insert new element to set
  S.delete - delete element from the set
*/