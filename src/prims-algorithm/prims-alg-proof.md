# Algorithm in haskell

```haskell
vertices :: Graph a l -> [l]
vertices g = ...

prim :: forall a l. Graph a l -> [Uedge a l]
-- cn - current node
prim g = go cn s1 s2 [] H.empty_ where
                            -- split on current node and rest of vertices from g
                            (cn:vs) = vertices g -- predef

                            -- S = Set
                            s1 = S.fromList [cn] -- type for s1 ?
                            s2 = S.fromList vs
                            go :: a -> S.Set a -> S.Set a -> [Uedge a l] -> [Uedge a l] -> H.carrM
                            -- ...

-- tests
-- ...
```

# Proof
Consider bipartite graph with parts s1 and s2
min edge X in (bipartite s1 s2) is in min spanning tree
`X = (v1, v2)`

# Link
See video `Prim's alogrithm in haskell with proof`