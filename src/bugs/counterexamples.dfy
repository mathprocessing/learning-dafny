method test_counterexamples(x: nat)
{
  assume x in {2, 3, 5};
  assert x < 5; // counterexample: x = 5, OK
}

method test_counterexamples_failed()
{
  var b := forall x | x in {2, 3, 5} :: x < 5;
  // Dafny output counterexample: b = false, x = 2 (but actually counterexample is x = 5)
  assert b;
}