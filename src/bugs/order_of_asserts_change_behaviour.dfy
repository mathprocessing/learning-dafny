module Groups {

datatype G3 = E | A | B

function mul(x: G3, y: G3): G3
function inv(x: G3): G3

lemma id_right()
  ensures forall a :: mul(a, E) == a

lemma  inv_right()
  ensures forall a :: mul(a, inv(a)) == E

// I think `lemma` + `ensures "expression"` 100% equivalent to `assume "expression"`
lemma assoc()
  ensures forall a, b, c {:trigger mul(mul(a, b), c)} :: mul(mul(a, b), c) == mul(a, mul(b, c))

method main()
{
  // assume forall a, b, c :: assoc(a, b, c);
  // assume forall a, b, c {:trigger mul(mul(a, b), c)} :: mul(mul(a, b), c) == mul(a, mul(b, c));
  id_right();
  inv_right();
  assoc();

  assert mul(A, E) == A;
  assert mul(B, E) == B;
  
  assert mul(mul(A, A), A) == mul(A, mul(A, A));

  // Verification Succeded
  // assert inv(B) != E;
  // // assert inv(A) != E;

  // Verification Succeded
  // // assert inv(B) != E;
  // assert inv(A) != E;

  // Infinite loop
  // assert inv(A) != E;
  // assert inv(B) != E;
}