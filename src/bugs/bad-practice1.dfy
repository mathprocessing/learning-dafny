// Writing tests like `forall i, j` and `forall j, i` is bad practice
// Because it's a just permutation of variables and if developer we don't know equivalent (i, j) to (j, i)
// in more complicated we need to test all possible permutations all time...
// But stop! Why we are learn about mathematical proofs? Induction? We must use it in practice of development!

// Source: https://github.com/dafny-lang/dafny/blob/495e7d2667a8768590feafc3c99562112e751152/Test/dafny0/NonGhostQuantifiers.dfy
// Figuring out the following bounds requires understanding transitivity. Very cool!
function method IsSorted0(s: seq<int>): bool
{
  (forall i, j :: 0 <= i && i < j && j < |s| ==> s[i] <= s[j])
}
function method IsSorted1(s: seq<int>): bool
{
  (forall j, i :: 0 <= i && i < j && j < |s| ==> s[i] <= s[j])
}