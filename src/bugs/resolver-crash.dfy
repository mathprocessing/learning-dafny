// https://github.com/dafny-lang/dafny/issues/2146
// trait Computation<T> {
//   function run(): T
// }

// class Stop<T> extends Computation<T> {
//   var value: T;

//   constructor(value: T) {
//     this.value := value;
//   }

//   function run(): T { value }
// }

// class Stop<T> extends Computation<T> {
//   var value: T;

//   constructor(value: T) {
//     this.value := value;
//   }

//   function run(): T { value }
// }