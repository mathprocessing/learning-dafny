predicate P(k: int)
predicate Q(k: int)

method test_forall(x: int, y: int)
{
  // var k:nat :| true;
  // if (y != k * x ==> k == 0) {
  //   assert k != 0 ==> y == k * x;
  // }

  // if forall k: nat :: y != k * x ==> k == 0 {
  //   assert forall k:nat :: k != 0 ==> y == k * x;
  // }

  if !(forall k: nat :: y != k * x ==> k == 0) { // Ok
    assert !(forall k:nat :: k != 0 ==> y == k * x); // Ok
  }

  // if !(forall k: nat :: !(y != k * x ==> k == 0)) {
  //   assert !(forall k:nat :: !(k != 0 ==> y == k * x));
  // }

  /**
  not (P ==> Q)
  (P ==> Q) ==> false
  !(!P || P && Q)
  P && !(P && Q)
  P && !Q
  
  */ 

  // P: y != k * x
  // Q: k == 0
  

  // if !forall k: nat :: (y != k * x && k != 0) {
  //   assert !forall k:nat :: (k != 0 && y != k * x);
  // }


}

method test_forall_2(x: int, y: int)
{
  if !forall k :: (y != k * x && k != 0) { // Ok
    assert !forall k :: (y != k * x && k != 0); // Ok
  }
}

method test_forall_3(x: int, y: int)
{
  if !forall k :: (P(k) && Q(k)) { // Ok
    assert !forall k :: (Q(k) && P(k)); // Ok
  }
}

method test_exists_3(x: int, y: int)
{
  if exists k :: (P(k) && Q(k)) { // Ok
    assert exists k :: (Q(k) && P(k)); // Ok
  }
}

method test_exists_3_2(x: int, y: int)
{
  if exists k :: P(k) && Q(k) { // Ok
    assert exists k :: Q(k) && P(k); // Ok
  }
}

method test_forall_4(x: int, y: int)
{
  if !(forall k :: !(y != k * x ==> k == 0)) { // Ok
    assert !(forall k :: !(k != 0 ==> y == k * x)); // Failed: assertion might not hold
  }
}

method test_forall_4_min(n: int)
{
  if !(forall k :: !(n == 1 ==> k == 0)) { // Ok
    assert !(forall k :: !(k != 0 ==> n != 1)); // Failed: assertion might not hold
  }
}

method test_forall_4_min_2(n: int)
{
  if * {
    forall k: nat {
      if (n == 1 ==> k == 0) {
        assert k != 0 ==> n != 1;
      }
    }
  } else {
    if !(forall k :: n == 1 ==> k == 0) { // Ok
      assert !(forall k :: k != 0 ==> n != 1); // Failed: assertion might not hold
    }
  }
}

method test_forall_4_min_3(n: int)
{
  if * {
    forall k {
      if (n == 1 ==> k == 0) {
        assert !(k == 0) ==> !(n == 1);
      }
    }
  } else {
    if !(forall k :: n == 1 ==> k == 0) { // Ok
      assert !(forall k :: !(k == 0) ==> !(n == 1)); // Failed: assertion might not hold
    }
  }
}

method test_exists_4(x: int, y: int)
{
  if exists k :: !(y != k * x ==> k == 0) { // Ok
    assert exists k :: !(k != 0 ==> y == k * x); // OK
  }
}

method test_forall_5(x: int, y: int)
{
  if !forall k :: (y != k * x && k != 0) { // Ok
    assert !forall k :: (k != 0 && y != k * x); // Failed: assertion might not hold
  }
}



