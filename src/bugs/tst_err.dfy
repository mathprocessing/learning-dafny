datatype Set<T(!new)> = Empty | Rest(head: T, tail: Set) {
  function add(e: T): Set<T> {
    if this.has(e) then this
    else Rest(e, this)
  }

  predicate {:verify false} has(e: T) {
    match this
      case Empty => false
      case Rest(e', s') => if e == e' then true else s'.has(e)
  }

  function {:verify false} sub(e: T): Set<T> {
    if this.has(e)
    then 
      if this.head == e
      then this.tail
      else Rest(this.head, this.tail.sub(e))
    else
      this
  }

  predicate le(b: Set<T>) {
    forall e:T | this.has(e) :: b.has(e)
  }

  predicate lt(b: Set<T>) {
    this.le(b) && !this.eq(b)
  }

  predicate {:opaque} eq(b: Set<T>) {
    this.le(b) && b.le(this)
  }

  lemma {:induction false} eq_self()
  ensures this.eq(this) { reveal eq(); }

  lemma {:induction false} eq_symm(b: Set<T>)
  ensures this.eq(b) == b.eq(this) { reveal eq(); }

  lemma {:induction false} lt_cases(b: Set<T>)
  ensures this.lt(b) || !this.le(b) || this.eq(b)
  {}
}

lemma eq_to_eq<T(!new)>(a: Set<T>, b: Set<T>)
requires a == b
ensures a.eq(b)
{
  assert a.eq(a) by { a.eq_self(); }
}

method testSet2()
{
  reveal Set.eq();
  assert Empty.sub(0) == Empty;
  assert Empty.sub(0).eq(Empty);
  var x := Empty.add(1).add(2);
  var y := Empty.add(2).add(1);
  assert x.eq(y);
}