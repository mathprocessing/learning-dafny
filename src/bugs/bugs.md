# Exists behaviour
https://github.com/dafny-lang/dafny/discussions/1994?sort=new
```
method test(){
    var b:=1;
    assert exists a | 0<= a <2 :: a == b;
}
```
expected result: Assertion should be satisfied and not saying that it might not hold.