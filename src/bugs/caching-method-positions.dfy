type Digit = x: int | 0 <= x <= 9

// Try to move comment from position 1 to position 2, and you see bad highlighting of
// counterexamples (press F7) and read underlines in vscode

predicate puzzle(x: Digit, y: Digit)
{
  x + y >= 12 && x * y <= 27
}

/** Comment Position 2
If you have idea how to elegantly solve some puzzle you can express
this by if/while statements in Dafny.
*/
method puzzle_proof_1(x: Digit, y: Digit)
{
  // Press F7 to show two solutions
  assume puzzle(x, y);
  if x < y {
    assert false; // x = 3, y = 9
  } else {
    assert false; // x = 9, y = 3
  }
}

method puzzle_proof_2(x: Digit, y: Digit)
{
  // Press F7 to show two solutions
  assume puzzle(x, y);
  if x < y {
    assert false; // x = 3, y = 9
  } else {
    assert false; // x = 9, y = 3
  }
}

/** Comment Position 1
If you have idea how to elegantly solve some puzzle you can express
this by if/while statements in Dafny.
*/

method puzzle_while_idea()
{

}