/**
This is not a bug technically, this is just wrong tools (SAT-solvers) usage
How to solve this:
* Need more info about process of solving in SAT-solvers
* Print possible actions on each state
* Print graph of states, properties of graph, etc...
* State can be setted from dafny for debug purposes and to show why some proof don't verifies
* In general: Show level-2 proof Q that given level-1 proof P can't be verified with given limit of calculations (steps, branches, actions, rewrites, ...)

If this impossible you must create a brand new (SAT-solver or rewrite system or ...) that holds properties above
 */
lemma ModulusBug(n: nat)
  requires n < 9
{
  // Note: After changing cache mode vscode window is reloaded

  // Setup: caching Advanced
  // assert (n * (n+1)) % 2 == 0; // maybe fails, maybe OK
  // Sometimes checks (first run), sometimes fails and show counterexample `n:int = 11` or `n:int = 17`
  // Ok, Let's substitute n = 11
  // assert (11 * (11+1)) % 2 == 0; // Verification Succeeded
  // assert (17 * (17+1)) % 2 == 0; // Verification Succeeded

  // Setup: caching Basic
  // assert (n * (n+1)) % 2 == 0; // always Verification Succeeded
  // assert (11 * (11+1)) % 2 == 0; // Verification Succeeded
  // assert (17 * (17+1)) % 2 == 0; // Verification Succeeded

  // Setup: caching Basic
  // assert (n * (n+1)) % 2 == 0; // Verification Succeeded if in `requires n < k`, k <= 9
  // assert (n * (n+1)) % 2 != 0; // always Verification Succeeded
  // assert 1 == 1; // When you uncomment this verification of `(n * (n+1)) % 2 == 0` fails
  // assert (11 * (11+1)) % 2 == 0; // Verification Succeeded
  // assert (17 * (17+1)) % 2 == 0; // Verification Succeeded
}

lemma {:induction k} modulusProp (n: nat, k : nat)
  requires 2 * n == k
  ensures n * (n + 1) % 2 == 0
{
  assert n == 0 ==> n * (n + 1) % 2 == 0;
  assert n * (n + 1) % 2 == 0 ==> (n + 2) * (n + 3) % 2 == 0;
}