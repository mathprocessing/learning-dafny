function {:verify false} max(a: nat, b: nat): nat
  // ensures forall m: nat | max(a, b) == m :: (m == a || m == b)
  ensures max(a, b) == a || max(a, b) == 0
{
  if a < b then b else a
}

method test_max(a: nat, b: nat)
{
  // `assume` is like `requires` but can be inside a method body
  assume a < b;
  assert max(a, b) == a;
    // You can see that function body and spec
  // (ensures max(a, b) == a || max(a, b) == 0) on same level of scope
  assert false; // Because definition of max contradicts with spec
  // TODO Feature: It's better if Dafny can automatically check proving of false.
  // Because it's not obvious to user that in some cases false provable.
  // Possible solution: warning user that false can be proved in concrete method.
}

method test_lmax(a: nat, b: nat)
{
    // requires also can be indide body but as definition of lambdas
  var lmax := (a: nat, b) requires a <= 1 => if a < b then b else a;
  // first argument of lmax must be equal to 0, because `requires a == 0`
  assume a <= 1;
  assert (lmax(1, b) == b) <==> (b >= 1);
  assert a == 1 ==> lmax(1, b) == b <==> a == 1 ==> b >= 1;

  assert (lmax(0, b) == b && lmax(1, b) == b) <==> (b >= 1);

  assert lmax(a, b) == b <==> (lmax(0, b) == b && (a == 1 ==> lmax(1, b) == b));

  // Example of generalization
  // Start state:
  assert lmax(a, b) == b <==> (a == 1 ==> b >= 1);
  // Finish state:
  assert lmax(a, b) == b <==> (b >= a);
  // Process
  calc <==> {
    (a == 1 ==> b >= 1);
    (a > 0 ==> b >= 1);
    (a > 0 ==> b >= a);
    (a >= 0 ==> b >= a);
    (true ==> b >= a);
    (b >= a);
  }
}

