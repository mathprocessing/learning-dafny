datatype Nat = O | S(prev: Nat)
datatype Option<T> = None | Some(T)

function method Prev(n: Nat): Option<Nat>
{
  match n
    case O => None
    case S(m) => Some(m)
}

// I make mistake `Plus(n: Nat, m: Nat)` and decreases fails, don't make such mistake! :)
function method Plus(m: Nat, n: Nat): Nat
  decreases n
{
  match n
    case O => m                  // A1(m)
    case S(n') => S(Plus(m, n')) // A2(n)
}

function method Even(n: Nat): bool
{
  match n
    case O => true
    case S(O) => false
    case S(S(n')) => Even(n')
}

method test_plus()
{
  var zero := O;
  var one := S(zero);
  var two := S(one);
  var three := S(two);
  assert Plus(one, two) == three;
  // Just A1 from constructor (definition)
  assert forall m: Nat :: Plus(m, O) == m;
  // Just A2 from constructor (definition)
  assert forall m, n: Nat :: Plus(m, S(n)) == S(Plus(m, n));
}

// Automatic induction works fine but we can do it manually
lemma {:induction false} zero_plus(m: Nat)
  ensures Plus(O, m) == m
  decreases m
{
  if m == O {
    assert Plus(m, O) == O;
  } else {
    zero_plus(m.prev);
  }
}

// Use match
lemma {:induction false} zero_plus'(m: Nat)
  ensures Plus(O, m) == m
  decreases m
{
  match m
    case O => { /* Trivial case */ }
    case S(m') => zero_plus'(m');
}

method add(x: Nat, y: Nat) returns (r: Nat)
{
  r := Plus(x, y);
}

/**
zero_plus: Plus 0 n == n
  Proof:
  // Base case
  assert Plus(O, O) == O; // by A1(m = 0)
  // Induction hypothesis
  assert forall k: Nat :: Plus(O, k) == k ==> Plus(O, S(k)) == S(k);
 */

lemma {:induction x, y} plus_comm(x: Nat, y: Nat)
ensures Plus(x, y) == Plus(y, x)
decreases x, y
{
  if x == y {} else {
    if x == O {
      assert Plus(O, y) == Plus(y, O) by {
        assert Plus(O, y) == y by { zero_plus(y); }
        assert Plus(y, O) == y; // by definition
      }
    } else {
      assert x != y;
      if y == O {} else {
        assert y != O;
        // 3 cells and each "less than" target cell, i.e. 3 induction hypotheses
        assert Plus(x.prev, y) == Plus(y, x.prev) by { plus_comm(x.prev, y); }
        assert Plus(x, y.prev) == Plus(y.prev, x) by { plus_comm(x, y.prev); }
        assert Plus(x.prev, y.prev) == Plus(y.prev, x.prev) by { plus_comm(x.prev, y.prev); }
        
        // subgoal 1: glue states to some equivalence classes
        /*        
        Derive new hyps:
        plus(x, y + 1) = 1 + plus(x, y) = plus(y + 1, x)
        plus(y, x + 1) = 1 + plus(y, x) = plus(x + 1, y)
        */
        // subgoal 2: B == C
        // from 3) we have H: plus(x + 1, y) == plus(y + 1, x)  ~ B == C
        assert Plus(x.prev, y) == Plus(y.prev, x) by {
          assert Plus(x.prev, y.prev) == Plus(y.prev, x.prev); // ih 3)
          assert Plus(x.prev, y) == S(Plus(x.prev, y.prev)); // by def (succ_right)
          assert Plus(y.prev, x) == S(Plus(y.prev, x.prev)); // by def (succ_right)
        }

        // subgoal 3: T_1 = S(B), T_2 = S(C) ==> T_1 == T_2 ==> Qed
        /*
        Use plus(x + 1, y + 1) = 1 + plus(x + 1, y)   ~ T_1 = S(B)
        Use plus(y + 1, x + 1) = 1 + plus(y + 1, x)   ~ T_2 = S(C)
        Now goal is: |- plus(x + 1, y) == plus(y + 1, x)  ~ T_1 == T_2 ~ T_comm ==> Qed.
        */
        assert Plus(x, y) == Plus(y, x) by {
          assert Plus(x.prev, y) == Plus(y.prev, x); // from previous subgoal
          assert Plus(x, y) == S(Plus(x, y.prev)); // by def (succ_right)
          assert Plus(y, x) == S(Plus(y, x.prev)); // by def (succ_right)
        }

        // Try 1:
        // assert Plus(x.prev, y) == S(Plus(x.prev, y.prev)) by {
        //   assert Plus(x.prev, S(y.prev)) == S(Plus(x.prev, y.prev)); // by def
        // }
        // assert Plus(y, x.prev) == S(Plus(x.prev, y.prev));
        //   assume Plus(x.prev, y.prev) == Plus(y.prev, x.prev);
        // assert Plus(y, x.prev) == S(Plus(y.prev, x.prev));
        // assert Plus(y, x.prev) == Plus(y.prev, x);

        // assert Plus(y, x.prev) == Plus(y.prev, x);
        // assert Plus(y, x) == S(Plus(y, x.prev)); // main
        // assert Plus(y.prev, x) == Plus(x, y.prev) by {
        //   plus_comm(y.prev, x);
        // }
      }
    }
  }
}

/**
1 2
0 1

  -x   T = target
-x-y  -y

Let's denote it
C T
A B

4th cell (x+1, y+1) connected to 3 cells [(x+1, y), (x, y+1), (x, y)] = 3 to 1 link

Induction pronciple in 2D grid:
If we have triangle of already TRUE hypothesis set we can extended it to right-up corner by one step
$
* $
* * $
...

If we already have property P and "3 to 1 link i.e. ih" then we can derive next diagonal of triangle.
If we can derive next diagonal of triangle then we can use 1D principle of induction.

Prove: plus(x + 1, y + 1) == plus(y + 1, x + 1)
1) have plus(x, y + 1) == plus(y + 1, x) by ih (decreases)  ~ C_comm ~ C_1 == C_2
2) have plus(x + 1, y) == plus(y, x + 1) by ih (decreases)  ~ B_comm ~ B_1 == B_2
3) have plus(x, y) == plus(y, x)         by ih (decreases)  ~ A_comm ~ A_1 == A_2

A is an equivalence class {A_1, A_2}
B is an equivalence class {B_1, B_2}
C is an equivalence class {C_1, C_2}

Derive new hyps:
plus(x, y + 1) = 1 + plus(x, y) = plus(y + 1, x)
plus(y, x + 1) = 1 + plus(y, x) = plus(x + 1, y)

from 3) we have H: plus(x + 1, y) == plus(y + 1, x)  ~ B == C

Change goal:
Use plus(x + 1, y + 1) = 1 + plus(x + 1, y)   ~ T_1 = S(B)
Use plus(y + 1, x + 1) = 1 + plus(y + 1, x)   ~ T_2 = S(C)
Now goal is: |- plus(x + 1, y) == plus(y + 1, x)  ~ T_1 == T_2 ~ T_comm ==> Qed.

Additional comments:
WISH: In general when we already setup all induction proofs, we might have problem:
* Proof of lemma L still exists but that lemma can't be proved only using strong induction pronciple

Possible workaround:
To solve this we may implement some engine to:
* write proofs in all directions
  What "direction" means in that context?
  Direction - Single proof step, action
* find hyps in all directions
  Direction - Single step of relax procedure
*/

lemma {:induction x, y} plus_comm_match(x: Nat, y: Nat)
ensures Plus(x, y) == Plus(y, x)
decreases x, y
{
  match (x, y)
    case (O, _) => { assert Plus(O, y) == Plus(y, O); }
    case (_, O) => { assert Plus(x, O) == Plus(O, x); }
    case (S(px), S(py)) =>
    {
      // First step
      assert Plus(y, px) == Plus(x, py) by { // H
        calc == {
          Plus(y, px);
          {
            assert Plus(px, y) == Plus(y, px) by { plus_comm_match(px, y); } // ih #1
          }
          Plus(px, y);
          // by def
          S(Plus(px, py));
          {
            assert Plus(px, py) == Plus(py, px) by { plus_comm_match(px, py); } // ih #3
          }
          S(Plus(py, px));
          // by def
          Plus(py, x);
          {
            assert Plus(x, py) == Plus(py, x) by { plus_comm_match(x, py); } // ih #2
          }
          Plus(x, py);
        }
      }
      // Second step
      assert Plus(x, y) == Plus(y, x) by {
        calc == {
          Plus(x, y);
          // by def
          S(Plus(x, py));
          {
            assert Plus(y, px) == Plus(x, py); // by rewrite H
          }
          S(Plus(y, px));
          // by def
          Plus(y, x);
        }
      }
    }
}

lemma {:induction false} succ_left(x: Nat, y: Nat)
ensures Plus(S(x), y) == S(Plus(x, y))
{
  calc == {
    Plus(S(x), y);
      { plus_comm(S(x), y); }
    Plus(y, S(x));
    // by def (succ_right)
    S(Plus(y, x));
    // `plus_comm(y, x);` is equivalent, but how to restrict Dafny to use only one particular direction if rewrite from {->, <-}?
      { plus_comm(x, y); } 
    S(Plus(x, y));
  }
}

/**
Prove: (x + y) + z == x + (y + z)
Base case x = 0:
  (0 + y) + z == 0 + (y + z)
  y + z == y + z by refl

Claim: we can prove assoc with induction on single variable x.
Induction case:
ih: (px + y) + z == px + (y + z)
|- (x + y) + z == x + (y + z)
Use px + y == y + px
apply `S` to lhs and rhs of ih
ih: S ((y + px) + z) == S (px + (y + z))
                                  by pcomm(y + px, z) and pcomm(px, y + z)
ih: S (z + (y + px)) == S ((y + z) + px)
                                  by succ_right
ih: z + S (y + px) == (y + z) + S px
                                  by succ_right
ih: z + (y + S px) == (y + z) + S px
                                  by pcomm(z, y + x) and pcomm(y + z, x)
ih: (y + x) + z == x + (y + z)
                                  by pcomm(x, y)
ih: (x + y) + z == x + (y + z) Qed
*/
lemma {:induction x} plus_assoc(x: Nat, y: Nat, z: Nat)
ensures Plus(Plus(x, y), z) == Plus(x, Plus(y, z))
decreases x
{
  match x
  case O => {
    // Goal:
    assert Plus(Plus(O, y), z) == Plus(O, Plus(y, z)) by {
      assert Plus(O, y) == y                   by { zero_plus(y); }
      assert Plus(O, Plus(y, z)) == Plus(y, z) by { zero_plus(Plus(y, z)); }
    }
  }
  case S(px) => {
    assert S(Plus(Plus(px, y), z)) == S(Plus(px, Plus(y, z))) by { // H
      // x == y ==> S x == S y from f_equal
      assert Plus(Plus(px, y), z) == Plus(px, Plus(y, z)) by {
        plus_assoc(px, y, z);
      }
    }
    assert Plus(x, Plus(y, z)) == Plus(Plus(x, y), z) by {
      calc == {
        Plus(x, Plus(y, z));
          { succ_left(px, Plus(y, z)); }
        S(Plus(px, Plus(y, z)));
        // by H
        S(Plus(Plus(px, y), z));
          { succ_left(Plus(px, y), z); }
        Plus(S(Plus(px, y)), z);
          { succ_left(px, y); }
        Plus(Plus(S(px), y), z);
        Plus(Plus(x, y), z);
      }
    }
  }
}

function {:opaque} Mod(x: Nat, y: Nat): Nat
requires y != O
decreases x
{
  match x
    case O => O
    case S(px) => if Mod(px, y) == y.prev then O else S(Mod(px, y))
}

predicate Lt(x: Nat, y: Nat)
ensures Lt(x, y) ==> y != O // helper post-condition for mod_lt
decreases x
{
  match x
    case O => y != O
    case S(px) => if x == y then false else Lt(px, y) // (x != y && x - 1 < y) <==> x < y
}

lemma {:induction false} lt_def2(x: Nat, y: Nat)
ensures (S(x) != y && Lt(x, y)) <==> Lt(S(x), y)
{}

lemma lt_succ_right_simple(x: Nat)
ensures Lt(x, S(x))
{
  match x
    case O => {}
    case S(px) => {
      assert (S(x.prev) != S(x) && Lt(x.prev, S(x))) <==> Lt(S(x.prev), S(x)) by {
        lt_def2(x.prev, S(x));
      }
      assert (x != S(x) && Lt(x.prev, S(x))) <==> Lt(x, S(x));
      assert Lt(x.prev, S(x)) <==> Lt(x, S(x)) by {
        assert x != S(x);
      }
      assert Lt(x.prev, x) by {
        lt_succ_right_simple(x.prev);
      }
      assert Lt(x.prev, x) == Lt(x.prev, S(x)) by {
        // How to prove that?
      }
    }
}

lemma mod_cases(x: Nat, y: Nat)
requires y != O
ensures if Lt(x, y) then Mod(x, y) == x else if x == y then Mod(x, y) == O else true

lemma {:induction x} lt_imp_succ_right(x: Nat, y: Nat)
ensures Lt(x, y) ==> Lt(x, S(y))
{
  match y
    case O => {}
    case S(py) => {
      if Lt(x, y) {
        assert Lt(x, S(y)); // How to prove this? z3 seems to be ahead from me :)
      }
    }
}

lemma lt_succ_right(x: Nat)
ensures Lt(x, S(x))
decreases x
{
  match x
    case O => {}
    case S(px) => {
      assert Lt(px, x) by {
        lt_succ_right(px);
      }
      // Goal
      assert x != S(x);
      assert Lt(px, S(x)) by {
        lt_imp_succ_right(px, x);
      }
    }
}

lemma mod_lt(x: Nat, y: Nat)
requires Lt(x, y)
ensures Mod(x, y) == x

lemma mod_self(x: Nat)
requires x != O
ensures Mod(x, x) == O
{
  match x.prev
    case O => { assert Mod(S(O), S(O)) == O by { reveal Mod(); } }
    case S(px) => {
      assert Mod(x.prev, x.prev) == O by {
        mod_self(x.prev);
      }
      assert if Mod(x.prev, x) == x.prev then Mod(x, x) == O else Mod(x, x) == S(Mod(x.prev, x)) by {
        reveal Mod();
      }
      if Mod(x.prev, x) == x.prev {
        assert Mod(x, x) == O; // by def
      } else {
        assert Mod(x, x) == S(Mod(x.prev, x));
        assert Mod(x.prev, x) != x.prev;
        assert Mod(x.prev, x.prev) == O;
        // Can we prove from this 3 facts `false`?
        assert Mod(x.prev, x) == x.prev by {
          assert Lt(x.prev, x) by {
            lt_succ_right(x.prev);
          }
          mod_lt(x.prev, x);
        }
        assert false;
        // Can we prove `false` by another way? (without mod_lt, lt_succ_right)
      }
    }
}

method testMod()
{
  var one := S(O);
  var two := S(one);
  var three := S(two);
  var four := S(three);
  var five := S(four);
  if * { // Create local scope for assertions
    reveal Mod();
    assert Mod(one, one) == O;
    assert Mod(three, one) == O;
    assert Mod(three, two) == one;
    assert Mod(four, four) == O;
    assert Mod(five, four) == one;
    // Added non-trivial case
    assert Mod(five, two) == one;
  }
  assert Mod(three, four) == three by {
    mod_cases(three, four);
  }
}
































/*
predicate Q(n: Nat)

predicate P(n: Nat)
  decreases S(n)
{
  match n
    case O => Q(n) // Action: change n -> O to see difference
    case S(m) => P(m) || Q(m)
}

// If we disable induction: we can't prove `P(n) <== Q(O)` but can `P(n) ==> P(S(n))`
lemma {:induction false} P_ind_hyp(n: Nat)
  ensures P(n) ==> P(S(n))
{
  // assume Q(O);
  // assert P(O) ==> P(S(O));
  // assert P(S(O)) ==> P(S(S(O)));
  // assert P(S(S(O))) ==> P(S(S(S(O))));
  // ...
}

method test()
{
  var x := O;
  assert Prev(x) == None;
  var y := S(S(S(O)));
  assert Prev(y) == Some(S(S(O)));
  assert y.prev == S(S(O));

}
*/