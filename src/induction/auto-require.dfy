// See DafnyRef.pdf Page 206
// autoReq boolExpr

function f(x: int): bool
  requires x > 3
{
  x > 7
}

// Should succeed thanks to auto_reqs
function {:autoReq} g(y: int, b: bool): bool
{
  if b then f(y + 2) else f(2 * y)
}

function g_manually(y: int, b: bool): bool
  requires if b then y + 2 > 3 else 2 * y > 3
{
  if b then f(y + 2) else f(2*y)
}