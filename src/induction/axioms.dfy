// See /py-helpers/axiom.py

function fib(n: nat): nat
{
  if n <= 1 then n else fib(n - 2) + fib(n - 1)
}

lemma {:axiom} example(n: nat, m: nat)
  ensures (n * m) % 2 == 0 <==> n % 2 == 0 || m % 2 == 0
{

}