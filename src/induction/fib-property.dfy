// See DafnyRef.pdf Page 224
function fib(n: nat): nat
{
  if n <= 1 then n else fib(n - 2) + fib(n - 1)
}


lemma FibProperty(n: nat)
  ensures fib(n) % 2 == 0 <==> n % 3 == 0
{
  // if n < 2 {} else {
  //   // FibProperty(n-2); FibProperty(n-1);
  //   FibProperty(n-1); // OK, it's more simpler but this proof is really correct?
  // }
  // Of course dafny can prove this without if... but what if we want to
  // to control such behaviour?
}

// Proofs about Extreme Predicates

least predicate g(x: int) { x == 0 || g(x-2) }

lemma EvenNat(x: int)
  requires g(x)
  ensures 0 <= x && x % 2 == 0
{
  var k: ORDINAL :| g#[k](x);
  EvenNatAux(k, x);
}

lemma EvenNatAux(k: ORDINAL, x: int)
  requires g#[k](x)
  ensures 0 <= x && x % 2 == 0
{
  if x == 0 {} else { EvenNatAux(k-1, x-2); } // Error: ORDINAL subtraction might underflow a limit ordinal (that is, RHS might be too large)
}