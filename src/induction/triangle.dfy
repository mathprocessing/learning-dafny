function SumFast(n: nat): nat
{
  n * (n + 1) / 2
}

function Sum(n: int): int
  requires n >= 0
  decreases n
{
  if n <= 0 then 0 else n + Sum(n - 1)
}

function Sum2(n: int): int
  requires n >= 0
  decreases n
{
  if n <= 1 then n else n + Sum(n - 1)
}

method TriangleNumber(N: int) returns (t: int)
  requires 0 <= N
  ensures t == SumFast(N)
  // ensures Sum(N) == SumFast(N) // Failed
{
  t := 0;
  var n := 0;
  while n < N
    invariant 0 <= n <= N
    invariant t == n*(n+1) / 2
    decreases N - n
  {
    n := n + 1;
    t := t + n;
  }
  // just after end of loop `n < N` not holds.
  assert n == N;
}

lemma {:induction n} Sum_SumFast_eq(n: nat)
  ensures Sum(n) == SumFast(n)
{
  // Auto-proved
}

lemma {:induction n} Sum_Sum2_eq(n: nat)
  ensures Sum2(n) == Sum(n)
{
  // Auto-proved
}

// lemma {:verify false} Sum2_SumFast_eq(n: nat)
//   ensures Sum2(n) == SumFast(n)
// {
//   // Failed to prove automatically
//   calc == {
//     Sum2(n) == Sum(n);
//     Sum(n) == SumFast(n);
//   }
// }

method test_induction(m: nat)
{
  var P := (n: nat) => Sum2(n) == SumFast(n);
  // Base case
  assert P(0);
  // Induction step
  assert forall n: nat :: P(n) ==> P(n + 1);
}

method test_triange()
{
  var n := TriangleNumber(3);
  assert n == 6;
  assert Sum(3) == 6;
}