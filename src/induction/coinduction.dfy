codatatype Stream<T> = SNil | SCons(head: T, tail: Stream)

function Up(n: int): Stream<int> { SCons(n, Up(n+1)) }

function FivesUp(n: int): Stream<int>
  decreases 4 - (n - 1) % 5;
{
  if n % 5 == 0 then
    SCons(n, FivesUp(n+1))
  else
    FivesUp(n+1)
}

/**
Stream is a co-inductive datatype whose values are possibly infinite lists.
Function `Up` returns a stream consisting of all integers upwards of `n` and
`FivesUp` returns a stream consisting of all multiplies of 5 upwards of `n`.

The self-call in Up and the FIRST self-call in FivesUp sit in productive positions
and are therefore classified as co-recursive calls, exempt from termination checks.
 */

/**
forall x | P(x) { Lemma(x); }

is used to invoke `Lemma(x)` on all `x` for which `P(x)` holds.
If `Lemma(x)` ensures `Q(x)`, then the forall statement establishes

forall x :: P(x) ==> Q(x)
*/