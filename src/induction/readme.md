### Compiled example of `Even(n: Nat): bool` function
```py
  def Even(n):
    while True:
        with _dafny.label():
            source2_ = n
            if source2_.is_O:
                return True
            elif True:
                _4___mcc_h0_ = source2_.prev
                source3_ = _4___mcc_h0_
                if source3_.is_O:
                    return False
                elif True:
                    _5___mcc_h1_ = source3_.prev
                    _6_n_k_ = _5___mcc_h1_
                    in0_ = _6_n_k_
                    n = in0_
                    _dafny._tail_call()
            break
```

Simplified:
```py
def Even(n):
  while True:
    with _dafny.label():
      if n.is_O: # Even(zero) == true
        return True
      else:
        if n.prev.is_O: # Even(one) == false
          return False
        else:
          n = n.prev.prev # Even(n) == Even(n - 2), n decreases
          _dafny._tail_call()
      break # Carefully think about break, this break must be in with context
```

## Other methods
```py
# Originals
def Prev(n):
    source0_ = n
    if source0_.is_O:
        return module_.Option_None()
    elif True:
        _0___mcc_h0_ = source0_.prev
        _1_m_ = _0___mcc_h0_
        return module_.Option_Some(_1_m_)


def Plus(m, n):
    source1_ = n
    if source1_.is_O:
        return m
    elif True:
        _2___mcc_h0_ = source1_.prev
        _3_n_k_ = _2___mcc_h0_
        return module_.Nat_S(module_.default__.Plus(m, _3_n_k_))


def add(x, y):
    r: module_.Nat = module_.Nat_O()
    r = module_.default__.Plus(x, y)
    return r


# Simplified
def Prev(n):
    if n.is_O:
      # Prev(zero) == None
      return module_.Option_None() 
    else:
      # Prev(n) = n - 1
      return module_.Option_Some(n.prev)


def Plus(m, n):
    if n.is_O:
      # match case 1: Plus(m, 0) == m
      return m
    else:
      # match case 2: Plus(m, n) == Succ(Plus(m, n - 1))
      return module_.Nat_S(module_.default__.Plus(m, n.prev))


def add(x, y):
    r: module_.Nat = module_.Nat_O()
    # add(x, y) == Plus(x, y), just simple call of another method with same arguments
    r = module_.default__.Plus(x, y)
    return r
```