datatype List<A> = Cons(A, List<A>) | Nil // | Error
datatype Expr = And(Expr, Expr) | Or(Expr, Expr) | Not(Expr) | Const(bool)
// see to more examples how to use match:
// https://github.com/dafny-lang/dafny/blob/master/Test/hofs/Fold.dfy

// We can define lambda functions in Dafny as constants
const equal := (e1: Expr, e2: Expr) => eval(e1) == eval(e2)

function method eval(e: Expr): bool
{
  match e
    case And(e1, e2) => eval(e1) && eval(e2)
    case Or(e1, e2) => eval(e1) || eval(e2)
    case Not(e1) => !eval(e1)
    case Const(e1) => e1
}

predicate all_equal(es: List<Expr>)
{
  match es 
    case Cons(e1, Cons(e2, es')) => equal(e1, e2) && all_equal(es')
    case Cons(e1, Nil) => true
    case Nil => true
}


method test_eval()
{
  assert equal(And(Const(true), Const(true)), Const(true));
}

predicate less_eq(a: bool, b: bool)
{
  a ==> b
}

method test_less_eq(x: bool)
{
  assert less_eq(false, x);
  assert less_eq(true, x) == x;
}

predicate sorted(s: seq<bool>)
{
  if |s| < 2 then
    true
  else
    if less_eq(s[0], s[1]) then
      sorted(s[1..])
    else 
      false
}

function method tail(xs: List<bool>): List<bool>
  requires xs != Nil
{
  match xs
    case Cons(_, xs') => xs'
    // case Error => Error
    // case Nil => Error
}

function method length_method(xs: List<bool>): nat
{
  match xs
    case Nil => 0
    case Cons(_, xs') => 1 + length_method(xs')
}

// TODO: Don't know how to prove termination
// 1. Using length_method function is very strange
// 2. How to prove without using it?
function length(xs: List<bool>): nat
  decreases length_method(xs)
{
  if xs == Nil then
    0
  else
    1 + length(tail(xs))
}

predicate lsorted(xs: List<bool>)
{
  if length(xs) < 2 then
    true
  else
    false
}

method main()
{
  var left: List<bool> := Cons(false, Nil);
  var right: List<bool> := Nil;
  assert left != right;
}
