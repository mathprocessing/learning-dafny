include "lib.dfy"

method TestBrickCenter()
{
  assume width == 25;
  assume width == height;
  var b := Brick(3, 5, 2, 3);
  if * {
    assert b.center.0 == 4;
    assert b.center.1 == 6;
  } else {
    assert b.center == (4, 6);
  }
}