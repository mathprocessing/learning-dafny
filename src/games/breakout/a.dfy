function {:opaque} abs(x: int): nat {
  if x < 0 then -x else x
}

lemma Abs1(x: int)
ensures abs(x) == x || abs(x) == -x
{ reveal abs(); }

lemma AbsPos(x: int)
ensures abs(x) >= 0
{} // We don't need to reveal because abs have return type nat <=> output_value >= 0

lemma AbsIsMax(x: int)
ensures x <= abs(x)
{ reveal abs(); }

lemma AbsSymm(x: int)
ensures abs(-x) == abs(x)
{ reveal abs(); }

lemma AbsProp2(x: int, y: int)
ensures abs(x + y) <= abs(x) + abs(y)
decreases -(x + y)
{
  if x + y >= 0 {
    assert abs(x + y) == x + y by { reveal abs(); }
    assert x <= abs(x) by { AbsIsMax(x); }
    assert y <= abs(y) by { AbsIsMax(y); }
    assert x + y <= abs(x) + abs(y);
  } else {
    // assert abs(x + y) == (-x) + (-y) by { reveal abs(); }
    // assert abs((-x) + (-y)) == (-x) + (-y); from abs(c) == abs(-c)
    assert abs(-x - y) <= abs(-x) + abs(-y) by { AbsProp2(-x, -y); }
    assert abs(-x-y) == abs(x+y) by { AbsSymm(x+y); }
    assert abs(-x) == abs(x) by { AbsSymm(x); }
    assert abs(-y) == abs(y) by { AbsSymm(y); }
  }
}

method testAbs()
{
  reveal abs();
  assert abs(0) == 0;
  assert abs(2) == 2;
  assert abs(-2) == 2;
}