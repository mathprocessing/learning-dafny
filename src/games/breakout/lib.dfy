// Breakout
type time = nat
type pos = x: int | x >= 1 witness 1

const width: pos
const height: pos

type tx = x: nat | x < width witness 0
type ty = x: nat | x < height witness 0

datatype Ball = Ball(x: tx, y: ty, vx: int, vy: int)
datatype Brick = Brick(x: tx, y: ty, w: tx, h: ty)
{
  const center := ((2 * x + w) / 2, (2 * y + h) / 2)

  predicate intersects(b: Brick)
  {
    true // TODO
  }
}

// Bricks can be modelled as:
// 1. 2d grid + bit mask (0 - brick destroyed, 1 - brick exists)
// 2. Set of rectangles

datatype Game = Game(ball: Ball, Bricks: seq<Brick>)
{
  const levelCompleteTime: time

  // Bricks are valid type if it don't intesects with each other
  lemma {:axiom} BricksNotIntersects()
  ensures forall i, j | 0 <= i < j < |this.Bricks| :: this.Bricks[i].intersects(this.Bricks[j])
}

// Minimimal time that ball need to destroy one brick
const MinTimeDestroyBrick: time

// We can compute upper bound for levelCompleteTime 
lemma UpperBoundLevelCompelete(g: Game)
ensures g.levelCompleteTime <= MinTimeDestroyBrick * |g.Bricks|

// Critics of verification software: Syntax error `<=` | `<` we can also describe using metamodel (formal spec) of Dafny language and finally replace A <= B with "fuzzy" statement OrederedList{A <= B, A < B, ...}

