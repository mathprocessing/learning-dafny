/*
Assume we have already derived hypothesis H:
H: x + y != 0
Then we can "back to the past": What a process `P` can create it? What properties have that `P`?
Possible variants:
* Process only use "single sets" as state.
  We have Lemma x = {} = y.
  Proof:
    If set(x) = {a}:
      HC: forall x :: Context.have("x = a") ==> Context.have("y != -a")
      From `HC` we can conclude that no possible solutions exists other than x is empty set.
* Process uses only "single sets" + pairs like {(a, b), (a2, b2), ...}

*/

const xState: set<int>
const yState: set<int>

datatype Expr = X | Y // Variables
  | Bool(bval: bool) | Int(ival: int) // Constants
  | Eq(left: Expr, right: Expr) // Functions with arity 2
  | Neq(left: Expr, right: Expr)
  | And(left: Expr, right: Expr)

const Context := And(Eq(X, Int(1)), Neq(X, Int(1)))

function relax(c: Expr): Expr

lemma RelaxSimple1()
ensures relax(Context) == Bool(false)