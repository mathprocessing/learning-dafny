function {:induction false} {:opaque} abs(x: int): (r: nat)
ensures x >= 0 ==> r == x // Expose only positive (trivial) case
{
  if x >= 0 then x else (-x)
}

// We do this because we want more detailed proofs, of course we can automate it
lemma AbsNeg(x: int)
requires x <= 0 // `x < 0` extended to `x <= 0` to increase automation
ensures abs(x) == -x
{
  reveal abs();
}

method test() {
  assert abs(3) == 3;
  assert abs(0) == 0;
  assert abs(-1) == 1 by { AbsNeg(-1); } 
}

// |x| + |y| >= |x + y|
// Short proof
lemma equation_short_automated_proof(x: int, y: int)
ensures abs(x) + abs(y) >= abs(x + y)
{
  reveal abs();
}

// |x| + |y| >= |x + y|
lemma equation(x: int, y: int)
ensures abs(x) + abs(y) >= abs(x + y)
{
  if y < -x {
    assert x + y < 0;
    assert abs(x + y) == -(x + y) by { AbsNeg(x + y); }
    if x >= 0 {
      assert y < 0;
      assert abs(y) == -y by { AbsNeg(y); }
    } else {
      assert x < 0;
      assert abs(y) >= abs(y - abs(x)) - abs(x) by {
        if y - abs(x) >= 0 {
          assert abs(y) >= y - 2 * abs(x) by {
            assert y >= 0;
            assert abs(x) >= 0;
            assert y >= y - 2 * abs(x);
          }
        } else {
          assert abs(y) >= -y by { if y < 0 { AbsNeg(y); } }
          assert abs(y - abs(x)) == -(y - abs(x)) by { AbsNeg(y - abs(x)); }
        }
      }
      assert abs(x) + abs(y) >= abs(x + y) by {
        assert -x == abs(x) by { AbsNeg(x); }
      }
    }
  }
}

lemma AbsIdempotence(x: int)
ensures abs(abs(x)) == abs(x)
{
  var z := abs(x);
  assert z >= 0; // from abs return type
  assert abs(z) == z; // from abs positive case
}

function abs'(x: int): int

lemma Abs'PositiveCase(x: int)
ensures x >= 0 ==> abs'(x) == x

lemma Abs'ReturnType(x: int)
ensures abs'(x) >= 0

// Detailed proof
lemma AbsIdempotence'(x: int)
ensures abs'(abs'(x)) == abs'(x)
{
  var z := abs'(x);
  assert z >= 0 by {
    // from abs return type
    Abs'ReturnType(x);
  }
  assert abs'(z) == z by {
    // from abs positive case
    Abs'PositiveCase(z);
  }
}