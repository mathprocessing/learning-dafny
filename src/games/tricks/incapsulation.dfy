const chars := "ABC"

function {:opaque} head(s: string): char
requires |s| > 0
{
  s[0]
}

function {:opaque} tail(s: string): string
requires |s| > 0
{
  s[1..]
}

lemma HeadTail(s: string)
requires |s| > 0
ensures [head(s)] + tail(s) == s

method test()
{
  var x := chars[0];
  assert x == 'A';
  assert x in chars;
  assert "A" + "BC" == chars;
  assert [head(chars)] + tail(chars) == chars by {
    if * {
      reveal head(), tail(); // Without incapsulation we use `opaque`
    } else {
      HeadTail(chars); // Incapsulation works: we can define new interfaces
      // Tricks like that can help to verify very big projects (games, simulators, generators, solvers and etc.)
    }
  }
}