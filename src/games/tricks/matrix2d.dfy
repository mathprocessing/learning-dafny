predicate P<T>(m: nat, s: seq<seq<T>>): (res: bool)
// ensures s == [[]] ==> exists m: nat :: res
{
  forall s' | s' in s :: |s'| == m
}

// This part of try to prove witness `[[]]` for type `M2d<T>`
lemma H<T> (s: seq<seq<T>>)
ensures s == [[]] ==> exists m: nat :: P(m, s)
{
  if s == [[]] {
    assert P(0, s);
  }
}

type M2d<T> = s: seq<seq<T>> | exists m :: P(m, s)
  witness *

lemma test (m: M2d) {
  assume |m| == 2;
  assert |m[0]| == |m[1]|;
}

lemma test2(m: M2d<int>) {
  assume m == [[0, 1], [2, 3, 4]];
  assert false;
}

predicate isSquareMatrix<T>(m: M2d<T>) {
  |m| > 0 ==> |m| == |m[0]|
}

/*
det
| a b |
| c d | = a * d - b * c
*/
function {:fuel 2} det(m: M2d<int>): int
requires |m| > 0
requires isSquareMatrix(m)
requires |m| <= 3 // In future version of det we can increase it, or we can use refinement?
// decreases |m|
{
  if |m| == 1 then m[0][0] else
  if |m| == 2 then m[0][0] * m[1][1] - m[0][1] * m[1][0] else
  var diagpos := m[0][0] * m[1][1] * m[2][2] 
               + m[0][1] * m[1][2] * m[2][0]
               + m[0][2] * m[1][0] * m[2][1];
  var diagneg := m[0][0] * m[1][2] * m[2][1]
              + m[0][1] * m[1][0] * m[2][2]
              + m[0][2] * m[1][1] * m[2][0];
   diagpos - diagneg
  // m[0][0] * det([m[1][1..], m[2][1..]]) + 0
}

type msq = m: M2d<int> | isSquareMatrix(m) witness *

lemma checkDetFormulaForSizeThree(m: msq)
requires |m| == 3
{
  var diagpos := m[0][0] * m[1][1] * m[2][2] 
               + m[0][1] * m[1][2] * m[2][0]
               + m[0][2] * m[1][0] * m[2][1];
  var diagneg := m[0][0] * m[1][2] * m[2][1]
              + m[0][1] * m[1][0] * m[2][2]
              + m[0][2] * m[1][1] * m[2][0];
  assert det(m) == diagpos - diagneg;
}

lemma testSizeThreeByVars(m: msq)
requires |m| == 3
{
  var (a, b, c) := (m[0][0], m[0][1], m[0][2]);
  var (p, q, r) := (m[1][0], m[1][1], m[1][2]);
  var (x, y, z) := (m[2][0], m[2][1], m[2][2]);
  var expr := a*q*z+b*r*x+c*p*y-(a*r*y+b*p*z+c*q*x);
  assert det(m) == expr;
}

lemma test3(m: M2d<int>) {
  assume m == [
    [1, 2]
  , [3, 4]
  ];
  // assert false; // Danger: not proves automatically but it can be proved with help...
  assert det(m) == -2;
  assert diagleft(m) == [1, 4];
  assert diagright(m) == [2, 3];
}

function diagleftfunc(m: msq, i: nat): int
requires i < |m|
{
  m[i][i]
}

function diagleft(m: msq): seq<int>
{
  seq(|m|, (i: nat) => assume i < |m|; diagleftfunc(m, i))
}

function diagright(m: msq): seq<int>
{
  seq(|m|, (i: nat) => assume i < |m|; m[i][|m|-i-1])
}

//  Two diagonals filled with zeros ==> det = 0
lemma TwoDiagonalsWithZeros(m: msq)
requires 0 < |m| <= 3
requires forall i | 0 <= i < |m| :: diagleft(m)[i] == 0
requires forall i | 0 <= i < |m| :: diagright(m)[i] == 0
ensures det(m) == 0
{
  // var x := seq(3, i => i);
  // assert x == [0, 1, 2];
  if |m| < 2 {
    assert diagleft(m) ==  diagright(m) == [m[0][0]] == [0];
    assert det(m) == 0;
  } else if |m| == 2 {
    assert det(m) == 0 by {
      assert diagleft(m) == [0, 0];
      assert diagright(m) == [0, 0];
      assert diagleft(m) == [m[0][0], m[1][1]];
      assert diagright(m) == [m[0][1], m[1][0]];
    }
  } else if |m| == 3 {
    assert |diagleft(m)| == |diagright(m)| == 3;
    // by { assume false; } can be used to add labels H1 and H2
    assert H1: diagleft(m) == [0, 0, 0];
    assert H2: diagright(m) == [0, 0, 0];

    assert det(m) == 0 by {
      var (a, b, c) := (m[0][0], m[0][1], m[0][2]);
      var (p, q, r) := (m[1][0], m[1][1], m[1][2]);
      var (x, y, z) := (m[2][0], m[2][1], m[2][2]);
      assert diagleft(m) == [m[0][0], m[1][1], m[2][2]];
      assert diagright(m) == [m[0][2], m[1][1], m[2][0]];
      reveal H1, H2;
      var expr := a*q*z+b*r*x+c*p*y-(a*r*y+b*p*z+c*q*x);
      assert a*q*z == c*q*x == 0;
    }
  }
}

/*

Det properties:
1. Up right triangle filled with zeros ==> det = mul of diagonal elements
| a 0 0 |
| * b 0 | = a * b * c
| * * c |

2. Two diagonals filled with zeros ==> det = 0
| 0 * 0 |
| * 0 * | = 0
| 0 * 0 |

*/