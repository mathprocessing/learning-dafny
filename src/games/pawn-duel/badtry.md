// lemma toNumBij(s: state)
// ensures forall k: nat :: toNum(s) == k ==> (!exists s' :: toNum(s') == k)

// function NextSet(s: state): set<nat>
// {
//   // set u: state | u.0 <= 6 && u.1 <= 6 && u.2 <= 6 && Next(s, u) :: u
//   // set u: state | 0 <= u.0 <= 6 && 0 <= u.1 <= 6 && 0 <= u.2 <= 6 && Next(s, u) :: u
  
//   // assume forall s: state :: (forall k: nat :: toNum(s) == k ==> (!exists s' :: toNum(s') == k));
//   // var S := set u: state | Next(s, u) && (toNum(u) == 1) :: toNum(u);
//   S
// }