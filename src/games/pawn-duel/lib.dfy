/**
(0, 0, 0) -> no actions possible -> loss, Pwin = 0
(0, 0, >=1) -> {
  (0, 0, 0) optimal next state
  (0, 0, >=1),
}
Pwin = max(1 - f(0, 0, 0), 1 - f(0, 0, >=1)) = max(1 - loss, 1 - *) = max(1, 0..1) = 1

(0, 1, 1): ? -> (0, 0, 1): win
? = lose because for all possible actions from (0, 1, 1) only possible outcome for opponent is win

*/

type num = x: int | 0 <= x <= 6
type flag = x: int | x == 0 || x == 1
type state = (num, num, num)

function f(s: state): flag

lemma {:axiom} ZeroBoard()
ensures f((0, 0, 0)) == 0 // 0 means lose

// predicate DecCase(cur: state, next: state, diff: num, i: int, j: int, k: int)
// requires 0 <= i < j <= 2
// requires k != i && k != j && 0 <= k <= 2
// {
//   if cur.i == next.i && cur.j == next.j then // Error: member 'i' does not exist in datatype '_tuple#3' ==> we must use sequences?
//     0 <= cur.k - next.k <= diff
//   else true
// }

function Dec(cur: state, next: state, diff: num): bool {
  // Note: This lemma must don't include null moves, otherwise we don't need it
  if cur.0 == next.0 && cur.1 == next.1 then // move pawn 0
    0 <= cur.2 - next.2 <= diff
  else if cur.0 == next.0 && cur.2 == next.2 then // move pawn 1
    0 <= cur.1 - next.1 <= diff
  else if cur.1 == next.1 && cur.2 == next.2 then // move pawn 2
    0 <= cur.0 - next.0 <= diff
  else false
}

/**
We can define List of states as possbile moves from current, but want if we select other way:
  Just define function `Next` + properties for it

But this requires proof: "set of lemmas that we have (definition of next) is enough to calculate it"
*/
function Next(now: state, after: state): bool {
  if now == after then false // No null moves
  else if now == (6, 6, 6) then Dec(now, after, 3)
  else Dec(now, after, 6)
}

function toNum(s: state): nat
{
  s.0 * 49 + s.1 * 7 + s.2
}

// Let's use sequences instead of sets (FUCK AXIOMS OF SETS IN DAFNY ...)
// function NextListAux(now: state, n: nat, xs: seq<state>): seq<state>
// {
//   var after :| toNum(after) == n + 1;
//   if Next(now, after) then xs + [after]
//   else xs
// }

lemma NextCmp(now: state, after: state)
requires Next(now, after)
ensures now.0 >= after.0 && now.1 >= after.1 && now.2 >= after.2
{}

lemma TestF()
{
  assert f((0, 0, 1)) == 1 by {
    // assert NextSet((0, 0, 1)) == {(0, 0, 0)};
  }
}