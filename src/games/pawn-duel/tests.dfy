include "./lib.dfy"

lemma TestNext()
{
  assert !Next((6,6,6), (6,6,6));
  assert !Next((6,6,6), (6,6,2));
  assert !Next((6,6,6), (6,6,1));
  assert !Next((6,6,6), (6,1,6));
  assert !Next((6,6,6), (1,6,6));
  assert !Next((6,6,6), (6,6,0));
  assert !Next((6,6,6), (5,5,6));
  assert !Next((6,6,6), (5,5,5));

  assert Next((6,6,6), (6,6,5));
  assert Next((6,6,6), (5,6,6));
  assert Next((6,6,6), (6,6,4));
  assert Next((6,6,6), (6,6,3));

  assert Next((0,0,1), (0,0,0));
  assert !Next((0,0,1), (0,1,1));
}