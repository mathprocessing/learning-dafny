/*

case 1: 0 < x < 1
case 2: 1 < x < 2

value{absolute error}

1.414{1} - 1 * 2
0.828{2} * 2
1.656{4} - 1 * 2
1.312{8} - 1 * 2
0.624{16} * 2
1.248{32} - 1 * 2
0.496{64} * 2
0.992{128} = [0.992 .. 1.120] ==> output = {0, 1} = '*'
...


sqrt(2) = "1.0110100*..."

Method 1:
(a + b) ^ 2 = a^2 + 2ab + b^2

Given: x = a^2
2 - a^2 = 2ab + b^2


We must to compare:
1) 1.0010^2 <> 2
2) 1.0011^2 <> 2

i.e. 
1) (a + 0 * 2^(-n))^2 <> 2
2) (a + 1 * 2^(-n))^2 <> 2



(1.001 + b)^2 = 2
1.001 + b = 2


1.0 ^ 2 obviouskly (we know it already) < 2

We don't know
1.1 ^ 2 <> 2?

1.1 * 1.1 = 1.1 + 0.11 = 10.01 > 2 ==> don't change `a`


1.00 ^ 2 = 1.0000
1.01 ^ 2 = 1.1001
1.0

*/

type flag = x: int | 0 <= x <= 1 witness 1
type bin = seq<flag>

// method 1 (uses bit shifting `>>` to speed up calculations)
// function sqrt2(): bin {
//   var a := 1.0;
//   var x := a * a; // we store a^2 to x
//   assert x < 2.0; // it always holds
//   if (a + delta) ^ 2 < 2.0 {
//     assert (a + delta) ^ 2 
//     == x + 2 * a * delta 
//     //     ^^^^^^^^^^^^^ can be calculated by bit shift `>>`
//     + delta ^ 2;
//     // Can't change anything in our comparison with 2.0. we just ignore it
//     // because delta = s^(-n) where n is natural number

//     // we don't change `x`, because we do not change `a`
//   } else {

//   }
// }

type pos = r: real | r >= 0.0

function sq(x: real): pos { x * x }

function sqrt(x: pos): (r: pos)
requires x >= 0.0
ensures r >= 0.0
ensures forall a: pos | a < r :: a * a < x
ensures forall a: pos | a > r :: a * a > x

lemma sqrtGt(a: pos, x: pos)
requires a > sqrt(x)
ensures sq(a) > x

const SQRT2 := sqrt(2.0)

method sqrt2_some_iteration() returns (res: pos) {
  var a: pos;
  var d: pos;
  assert a * a < 2.0 by { assume a < SQRT2; }
  assert sq(a + 2.0 * d) > 2.0 by { 
    assume a + 2.0 * d > SQRT2; 
    sqrtGt(a + 2.0 * d, 2.0);
  }

  // d == 0.001
  // 1.011 ?<> sqrt2
  // a + d ?<> sqrt2
  var x := a * a; // we store a^2 to x
}


/*
Method 2 `sqrt(2) - 1`:
(1 + a)^2 = 2
1 + 2a + a^2 = 2
2a + a^2 = 1
a(a+2) = 1
0.414 * 2.414 = 1

"0.xyz..." * "10.xyz..." = "1.000..."

"x.yz..." + "0.xyz..."^2 = "1.000..."
==> x == 0

"0.yz..." + "0.0yz..."^2 = "1.000..."

     [0,    y,    z]
*    [0,    y,    z]
--------------------
  [0, 0,  y*y,  y*z]
+ [0, 0,    0,  y*z]

[y, z] + [0, 0, y*y, y*z]

*/