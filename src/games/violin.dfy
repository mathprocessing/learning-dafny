/*
VIOLIN
VIOLIN
+VIOLA
------
SONATA
+ TRIO

*/

type dig = x: int | 0 <= x <= 9 witness 0
type num = s: seq<dig> | |s| > 0 witness [0]
const V: dig
const I: dig
const O: dig
const L: dig
const N: dig
const A: dig
const S: dig
const T: dig
const R: dig

function toNumber(s: num): nat
{
  if |s| == 1 then
  s[0] else
  s[|s|-1] + 10 * toNumber(s[..|s|-1])
}

function fromNumber(n: nat): num
{
  if n <= 9 then [n] else
  fromNumber(n / 10) + [n % 10]
}

method testToNumber()
{
  assert toNumber([1,2,3]) == 123;
  assert toNumber([9]) == 9;
  assert fromNumber(123) == [1,2,3];
  assert fromNumber(9) == [9];
}

function last(a: num): dig
{
  a[|a| - 1]
}

function add(a: num, b: num): num
requires |a| == |b|
{
  fromNumber(toNumber(a) + toNumber(b))
}
type flag = x: int | 0 <= x <= 1 witness 1

function add'(a: num, b: num, c: flag): num
requires |a| == |b|
{
  // [a1, a2, a3]
  // [b1, b2, b3]
  // +
  // [a1+b1+carry2, a2+b2+carry3, a3+b3 % 10]
  var u := last(a)+last(b)+c;
  if |a| == 1 then 
    if u <= 9 then 
      [u]
    else
      [u / 10, u % 10]
  else
  add'(a[..|a|-1], b[..|b|-1], u / 10) + [u % 10]
}

lemma {:induction false} AddDefEquiv(a: num, b: num)
requires |a| == |b|
ensures add(a, b) == add'(a, b, 0)
{
  if |a| == 1 {
    var z := a[0] + b[0] as nat;
    assert H: toNumber(a) + toNumber(b) == z;
    if z <= 9 {
      assert add'(a, b, 0) == [z];
      assert add(a, b) == [z] by {
        reveal H;
        assert fromNumber(z) == [z];
      }
    } else {
      assert z <= 18;
      assert add'(a, b, 0) == [1, z % 10];
      assert add(a, b) == [1, z % 10] by {
        reveal H;
        assert fromNumber(z) == [1, z % 10];
      }
    }
  } else {
    var L := |a|;
    var a' := a[..L-1];
    var b' := b[..L-1];
    assert add(a', b') == add'(a', b', 0) by {
      AddDefEquiv(a', b');
    }
    var z := last(a) + last(b);
    calc == {
      add(a, b);
      fromNumber(toNumber(a') + toNumber(b') + z / 10) + [z % 10];
      { assume false; }
      add'(a', b', z / 10) + [z % 10];
      add'(a, b, 0);
    }
  }
}

method test()
{
  assert add'([4, 5], [1, 1], 0) == [5, 6];
  assert add'([9], [1], 0) == [1, 0];
  assert add'([9, 9], [0, 1], 0) == [1, 0, 0];
  assert add'([9, 9], [0, 1], 1) == [1, 0, 1];
  assert add([9, 9], [0, 1]) == add'([9, 9], [0, 1], 0);
}

method test_general(x: dig, y: dig)
ensures add([x, y], [1, 1]) == add'([x, y], [1, 1], 0)
{
  assert add([x], [y]) == add'([x], [y], 0) by {
    assert add([9, 9], [0, 1]) == add'([9, 9], [0, 1], 0);
  }

  if x * 10 + y <= 88 {
    assert x <= 8;
    assert x == 8 ==> y <= 8;
    if y <= 8 {
      assert add'([x, y], [1, 1], 0) == [x + 1, y + 1] by {
        calc == {
          add'([x, y], [1, 1], 0);
          add'([x], [1], (y + 1) / 10) + [(y + 1) % 10];
          { assert (y + 1) % 10 == y + 1;
            assert (y + 1) / 10 == 0; }
          add'([x], [1], 0) + [y + 1];
          { assert add'([x], [1], 0) == [x + 1]; }
          [x + 1, y + 1];
        }
      }
      assert add([x, y], [1, 1]) == [x + 1, y + 1] by {
        calc == {
          add([x, y], [1, 1]);
          {
            assert toNumber([1, 1]) == 11;
            assert toNumber([x, y]) == x * 10 + y;
            assert 11 + x*10 + y == (x+1) * 10 + (y + 1);
          }
          fromNumber((x+1) * 10 + (y + 1));
          [x + 1, y + 1];
        }
      }
    } else {
      calc == {
        add'([x, y], [1, 1], 0);
        { assert y == 9; }
        add'([x, 9], [1, 1], 0);
        [x + 2, 0];
      }
      calc == {
        add([x, y], [1, 1]);
        add([x, 9], [1, 1]);
        {
          assert toNumber([1, 1]) == 11;
          assert toNumber([x, 9]) == x * 10 + 9;
        }
        fromNumber((x+2) * 10);
        // Additional steps (Dafny don't need it)
          // By def
          fromNumber(((x+2) * 10) / 10) + [((x+2) * 10) % 10];
          { assert ((x+2) * 10) / 10 == x + 2;
            assert ((x+2) * 10) % 10 == 0; }
          fromNumber(x + 2) + [0];
          {
            assert fromNumber(x + 2) == [x + 2];
          }
        [x + 2, 0];
      }
    }
    
  } else {
    assert x * 10 + y >= 89;
    assert x >= 8;
    // assume false;
  }

}

