/*
3x3x3 cube have 3 Surfaces:
1.
012 x
* * 0
**  1 
* * 2
    y
2.
***
* *
***

3.
***
 *
 *

Rules:
  * All mini-cubes connected(glued) by it surfaces.
  * 3x3x3 contatins only 16 cubes.

Goal: draw surface that oposite to O (second).
*/

type num = x: int | 0 <= x <= 2

function f(x: num, y: num, z: num): bool

// Let's fix letter K (Note: We can begin to solve by fixing any of 3 letters K, O, T)
lemma {:axiom} letterK(x: num, y: num)
ensures if (x == 1 && y != 1) || (x == 2 && y == 1) then !f(x, y, 0) else f(x, y, 0)

// declare letter O, it can be in any of 6 surfaces (how to express this in Dafny? We want short way...)
// O is symmetrical, orientation don't play any role
// All positions = 6 surfaces * 1 orientation = 6 combinations of O (central + rotational symmetry)

// declare letter T, but T can be oriented 4 times. 
// All positions = 6 surfaces * 4 orientations = 24 combinations of T (axis symmetry)

// If we select as first other letter then we must calculate combinations to letter K:
// All positions = 6 surfaces * 8 orientations = 48 combinations of K (no symmetry)


// function cubeProjection()

/**
Pseudo code

const K := [
  [1, 0, 1],
  [1, 1, 0],
  [1, 0, 1],
]

const T := [
  [1, 1, 1],
  [0, 1, 0],
  [0, 1, 0],
]

const O := [
  [1, 1, 1],
  [1, 0, 1],
  [1, 1, 1],
]

planeZ(0) ~ equation z == 0

lemma letterK'()
ensures planeZ(0) == K

const anyPlane := planeX(0) || planeX(2) || planeY(0) || planeY(2) || planeZ(0) || planeZ(2)

lemma letterO'()
ensures anyPlane == O

lemma letterT'()
ensures anyPlaneAnyRotation() == T


1. Why it so hard?
  May be we need some symmetry group for cube, surfaces. And after everything becomes easy.
  Cube have 6 surfaces(squares), square have 4 rotations * 2 mirrors = 8 positions of image on square (Dihedral group D8)

  How to calculate Dihedral group in Dafny? How to define it assuming that it already calculated?

      0 deg   90 deg   180 deg  270 deg
D8 = {id,     rot1,    rot2,    rot3, 
      mirror, rotmir1, rotmir2, rotmir3}

We can desribe any lettersby this construction:
exists rotationOfCube :: rotate(cube, rotationOfCube) == K
exists rotationOfCube :: rotate(cube, rotationOfCube) == O
exists rotationOfCube :: rotate(cube, rotationOfCube) == T
+ axioms
1. all mini-cubes are connected
  ones(neighbours(point)) >= 1
2. 3x3x3 contatins only 16 cubes.
  sum(allPoints) == 16
*/

datatype Point = P(x: num, y: num, z: num)

// const allPoints := [P(0, 0, 0), P(0, 0, 1), P(0, 0, 2), P(0, 1, 0), P(0, 1, 1), P(0, 1, 2)] TOO LONG!


