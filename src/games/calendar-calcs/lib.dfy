/**
Monday
Tuesday
Wednesday
Thursday
Friday
Saturday
Sunday

TODO: How to check that words above don't have misspellings? (Using open source)
* Using statistics over internet, over books?
* Or use dictionary that already proven to be correct?
* Idea: use open OCR libraries to prove some facts from images database.
  It can be useful to automate a lot of work with words(why useful?), think about: "What if we need multiple languages in docs?" it really can be annoying to translate, support and verify that all logical and grammatic stuff are correct.
*/

datatype Week = Mon | Tue | Wed | Thu | Fri | Sat | Sun
{
  function toString(): string {
    match this
      case Mon => "Monday"
      case Tue => "Tuesday"
      case Wed => "Wednesday"
      case Thu => "Thursday"
      case Fri => "Friday"
      case Sat => "Saturday"
      case Sun => "Sunday"
  }

  function toNat(): nat {
    match this
      case Mon => 0
      case Tue => 1
      case Wed => 2
      case Thu => 3
      case Fri => 4
      case Sat => 5
      case Sun => 6
  }
}
datatype Month = Jan | Feb | Mar | Apr | May | Jun | Jul | Aug | Sep | Oct | Nov | Dec
// TODO: setup auto checking for |Month| == 12, |Week| == 7
{
  function toString(): string {
    match this
      case Jan => "January"
      case Feb => "February"
      case Mar => "March"
      case Apr => "April"
      case May => "May"
      case Jun => "June"
      case Jul => "July"
      case Aug => "August"
      case Sep => "September"
      case Oct => "October"
      case Nov => "November"
      case Dec => "December"
  }

  function toNat(): nat {
    match this
      case Jan => 0
      case Feb => 1
      case Mar => 2
      case Apr => 3
      case May => 4
      case Jun => 5
      case Jul => 6
      case Aug => 7
      case Sep => 8
      case Oct => 9
      case Nov => 10
      case Dec => 11
  }
}

// Or we just define a function (maybe it better, short and obvious approach but what if anyone want to create list? We must check that Dafny work well with it)
// I.e. we can prove that some approaches perfectly equivalent to each other without drawbacks, unoptimizations, reducing of calculation speed, memory consumption,
// code obviousness, code style, ...
// maybe better if we store tis as map { Jan => 31, Feb => 28, ... }
// Or map as expression: `map { Jan => 31, Feb => if IsLeapYear then 29 else 28, ... }`
const daysInMonth := [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
const currentYear := 2022
const MonthEnumCapacity := 12 // This can be calculated automatically from definition of `datatype Month`

lemma test()
{
  assert Sat.toString() == "Saturday";
  assert Aug.toString() == "August";
  assert |Dec.toString()| == 8;
  // TODO: how to check
  // sum of all letters in all month == ?
  // * We need to store list of month names?
  // monthNames = ["January", "February", ...]
  // * Or we can just use function toNat :: Month -> nat
  assert |daysInMonth| == MonthEnumCapacity; // == |Month|
}

/**
Leap Year Algorithm:
Every year that is exactly divisible by 4 is a leap year, except for years that are exactly divisible by 100, but these centurial years are leap years, if they are exactly divisible by 400.

 */