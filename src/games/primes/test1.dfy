include "./lib.dfy"

method test()
{
  assert divisor(2, 4) by { ModAuto(); }
  assert !divisor(3, 10) by { ModAuto(); }
  assert divisor(10, 100) by { ModAuto(); }
}

method testPrimes()
{
  assert !isPrime(0);
  assert !isPrime(1);
  assert isPrime(2);
  assert isPrime(3) by { ModAuto(); }
  assert !isPrime(4) by { ModAuto(); }
  assert isPrime(5) by { ModAuto(); }
  assert !isPrime(6) by { ModAuto(); }
  assert isPrime(7) by { ModAuto(); }
  assert !isPrime(8) by { ModAuto(); }
  assert !isPrime(9) by { ModAuto(); }
}

method testDivisors()
{
  assert divisors(3) == {1, 3} by { ModAuto(); }
  assert divisors(8) == {1, 2, 4, 8} by { ModAuto(); }
}

method testDivisorsDetailed()
{
  assert divisors(3) == {1, 3} by {
    assert divisor(1, 3) by { DivisorOne(3); }
    assert !divisor(2, 3) by {
      assert mod(3, 2) == 1 by {
        ModDefStep(3, 2);
        ModDefBase(1, 2);
      }
    }
    assert divisor(3, 3) by { DivisorSelf(3); }
  }
  assert divisors(8) == {1, 2, 4, 8} by {
    assert divisor(1, 8) by { DivisorOne(8); }
    assert divisor(2, 8) by { ModDefStep(8, 2); ModDefStep(6, 2); ModDefStep(4, 2); ModSelf(2); }
    assert divisor(4, 8) by { DivSymm(2, 8); } // `DivSymm` used to check that "/" work well, but actually we have crash documented in "test2-div-crash.dfy"
    assert divisor(8, 8) by { DivSymm(1, 8); } // After that crash we change "/" to `div` function that defined using `-` operator
    assert !divisor(3, 8) by { ModDefStep(8, 3); ModDefStep(5, 3); ModDefBase(2, 3); }
    assert !divisor(5, 8) by { ModDefStep(8, 5); ModDefBase(3, 5); }
    assert !divisor(6, 8) by { ModDefStep(8, 6); ModDefBase(2, 6); }
    assert !divisor(7, 8) by { ModDefStep(8, 7); ModDefBase(1, 7); }
  }
}