include "../common.dfy"
include "../mod-func/lib.dfy"

function div(x: nat, y: pos): nat
{
  if x < y then 0
  else 1 + div(x - y, y)
}

type prime = x: int | x >= 1 && isPrime(x) witness 2

predicate isPrime(x: nat): (res: bool)
ensures x == 0 ==> !res
ensures x == 1 ==> !res
ensures x == 2 ==> res
{
  x >= 2 && isPrimeAux(x, 2) // if x >= 2 then isPrimeAux(x, 2) else false
}

predicate isPrimeAux(x: nat, minBound: pos)
decreases x - minBound
{
  if minBound < x then !divisor(minBound, x) && isPrimeAux(x, minBound + 1)
  else true
}

// to eliminate case divisor(_, 0) we use m:pos instead of m:nat
predicate divisor(d: pos, m: pos)
{
  mod(m, d) == 0
}

function divisors(x: pos): set<pos>
{
  set d | 1 <= d <= x && divisor(d, x) :: d
}

lemma DivisorLess(d: pos, m: pos)
ensures m < d ==> !divisor(d, m)
{
  if m < d {
    assert mod(m, d) == m by {
      ModDefBase(m, d);
    }
  }
}

lemma DivisorOne(x: pos)
ensures divisor(1, x)
{
  assert mod(x, 1) == 0 by { ModXOne(x); }
}

lemma DivisorSelf(x: pos)
ensures divisor(x, x)
{
  ModSelf(x);
}

lemma ModDivSymm(x: pos, y: pos)
requires y < x
requires mod(x, y) == 0 // 10 % 2 == 0
ensures mod(x, div(x, y)) == 0 // 10 % (10 / 2) == 0
{
  assert x > y;
  assert div(x, y) >= 1;
  assume false;
}

lemma DivSymm(d: pos, m: pos)
requires d < m
requires divisor(d, m)
ensures divisor(div(m, d), m)
{
  ModDivSymm(m, d);
}