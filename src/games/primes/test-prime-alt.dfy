include "./prime-alt.dfy"

method test0()
{
  assert !isPrimeAlt(0);
}

method test1()
{
  assert !isPrimeAlt(1);
}

method test2()
{
  assert isPrimeAlt(2);
}

method test3()
{
  assert divisors(3) == {1, 3} by { ModAuto(); }
  assert isPrimeAlt(3);
}

method test4()
{
  assert divisors(4) == {1, 2, 4} by { ModAuto(); }
  assert !isPrimeAlt(4);
}

method test5()
{
  assert divisors(5) == {1, 5} by { ModAuto(); }
  assert isPrimeAlt(5);
}

method test6()
{
  assert divisors(6) == {1, 2, 3, 6} by { ModAuto(); }
  assert !isPrimeAlt(6);
}

method test7()
{
  assert divisors(7) == {1, 7} by { ModAuto(); }
  assert isPrimeAlt(7);
}

method test8()
{
  assert divisors(8) == {1, 2, 4, 8} by { ModAuto(); }
  assert !isPrimeAlt(8);
}

method test3'()
{
  ModAuto();
  assert isPrimeAlt(3);
}

method test4'()
{
  assert 2 in divisors(4) by { ModAuto(); } // Why we need  this line?
  assert !isPrimeAlt(4);
}

method test5'()
{
  ModAuto();
  assert isPrimeAlt(5);
}

method test6'()
{
  ModAuto();
  assert 2 in divisors(6);
  assert !isPrimeAlt(6);
}

method test7'()
{
  ModAuto();
  assert isPrimeAlt(7);
}

method test8'()
{
  ModAuto();
  assert 2 in divisors(8);
  assert !isPrimeAlt(8);
}
