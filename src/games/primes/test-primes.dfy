include "./lib.dfy"
include "./prime-alt.dfy"

lemma test(p: prime)
requires p <= 2
ensures p == 2
{
  // Proved automatically
}

lemma test2(n: nat)
requires exists k | k >= 1 :: k * k == n
ensures !isPrime(n)
{
  var k :| k * k == n && k >= 1;
  assert divisor(k, n) by {
    assert mod(k * k, k) == 0 by {
      ModFactor(k, k);
    }
  }
  assert k in divisors(n);
  assert !isPrimeAlt(n);
  assert !isPrime(n) by {
    IsPrimeAltEquivIsPrime(n);
  }
}