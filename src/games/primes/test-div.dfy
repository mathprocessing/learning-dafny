include "./lib.dfy"

method test()
{
  assert div(1, 1) == 1;
  assert div(6, 1) == 6;
  assert div(4, 5) == 0;
  assert div(10, 9) == 1;
}