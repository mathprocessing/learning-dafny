include "./lib.dfy"

lemma ModDivSymm(x: pos, y: pos)
requires x <= y
requires mod(y, x) == 0
ensures mod(x, x / y) == 0 // Sometimes we have "red lined" `/`
// Unhandled exception. System.ArgumentOutOfRangeException: Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection. (Parameter 'count')

lemma DivSymm(d: pos, m: pos)
requires d <= m
requires divisor(d, m)
ensures divisor(m / d, m)
{
  ModDivSymm(d, m);
}