include "./lib.dfy"

predicate isPrimeAlt(x: nat): (res: bool)
{
  x >= 2 && !exists d | 1 < d < x :: d in divisors(x)
}

lemma IsPrimeAltEquivIsPrimeLeft(n: nat)
requires isPrimeAlt(n)
ensures isPrime(n)
{
  assert n >= 2;
  assume false;
}

lemma IsPrimeAltEquivIsPrimeRight(n: nat)
requires isPrime(n)
ensures isPrimeAlt(n)
{
  if n <= 2 {} else {
    // assert if n >= 2 then isPrimeAux(n, 2) else false;
    // assert (n >= 2 ==> isPrimeAux(n, 2)) && (n >= 2);
    assert isPrimeAux(n, 2);
    // IsPrimeAltEquivIsPrimeRight(n - 1);
    assume false;
  }
}

lemma IsPrimeAltEquivIsPrime(n: nat)
ensures isPrimeAlt(n) <==> isPrime(n)
{
  if isPrimeAlt(n) { IsPrimeAltEquivIsPrimeLeft(n); }
  if isPrime(n) { IsPrimeAltEquivIsPrimeRight(n); }
}