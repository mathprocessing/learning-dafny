include "./lib.dfy"

lemma InfinitePrimes(n: nat)
ensures exists i | i > n :: isPrime(i)

// Just for simpler example
lemma {:verify false} InfiniteMod(x: nat, y: pos)
requires mod(x, y) == 0 // How to prove without it? (actually this right proof bevause `forall y :: mod(0, y) == 0` )
ensures exists i | i > x :: mod(i, y) == 0
{
  var i := x + y;
  assert mod(i, y) == 0 by { ModDef(i, y); }
}

lemma ModAddDistr(a: nat, b: nat, d: pos)
ensures mod(a + b, d) == mod(mod(a, d) + mod(b, d), d)
{
  if a > b { assume false; }
  else {
    assert a <= b;
    if a < d {
      assert mod(a + b, d) == mod(mod(a, d) + mod(b, d), d);
      assert mod(a + b, d) == a + mod(b, d);
      // ModDef(0, d);
    } else {
      
      // b := b - a
      // assume mod(a + b - a, d) == mod(a, d) + mod(b - a, d);
      // assert mod(b, d) == mod(a, d) + mod(b - a, d);
      // // a := a - b
      // assume mod(a + b - b, d) == mod(a - b, d) + mod(b, d);
      // Simultaniosly

      // a := a - d && b := b - d

      assume mod(a - d + b - d, d) == mod(a - d, d) + mod(b - d, d);
      assert mod((a + b) - 2*d, d) == mod(a, d) + mod(b, d);
      assert mod(a + b, d) == mod(a, d) + mod(b, d);
    }
  }

}

lemma {:verify false} InfiniteModStrict(x: nat, y: pos)
ensures exists i | i > x :: mod(i, y) == 0
{
  if x < y {
    var i := y;
    assert mod(i, y) == 0 by { ModSelf(y); }
  } else {
    var i := x + (y - mod(x, y));
    assert i > x by {
      assert mod(x, y) < y by { ModLtY(x, y); }
    }
    calc <==> {
      mod(x + (y - mod(x, y)), y) == 0;
      mod(x + y - mod(x, y), y) == 0;
        { assume false; }
      mod(x, y) + mod(y, y) - mod(mod(x, y), y) == 0;
        { assert mod(y, y) == 0 by { ModSelf(y); } }
      mod(x, y) - mod(mod(x, y), y) == 0;
        { ModTwice(x, y); }
      mod(x, y) - mod(x, y) == 0; // by arith
    }
    assert mod(i, y) == 0;
  }
}