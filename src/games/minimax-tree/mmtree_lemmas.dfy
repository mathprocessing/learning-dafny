include "./mmtree.dfy"

lemma MinMaxEqImpAllEqBack(s: seq<int>)
requires |s| > 0
requires forall x | x in s :: x == lmin(s)
ensures lmin(s) == lmax(s)
decreases |s|
{
  /*
  H: forall x | x in s :: x == lmin(s)
  H(s[0]): s[0] == lmin(s)


  H2: forall x | x in s[1..] :: x == lmin(s[1..]) from {
    calc <==> {
      forall x | x in s[1..] :: x == lmin(s);
      // use def
      forall x | x in s[1..] :: x == min(s[0], lmin(s[1..]));
      Qed by lmin(s[1..]) == min(s[0], lmin(s[1..])) {
        assert s[0] >= lmin(s[1..]) {
          
        }
      }
      
    }

  }
  ih: 
    ==> lmin(s[1..]) == lmax(s[1..])

  Goal: lmin(s) == lmax(s)
  Goal: min(s[0], lmin(s[1..])) == max(s[0], lmax(s[1..]))



  */

  // if |s| > 1 {
  //   assert ih: lmin(s[1..]) == lmax(s[1..]) by {
  //     assume forall x | x in s[1..] :: x == lmin(s[1..]);
  //   }
  //   calc == {
  //     lmin(s);
  //     min(s[0], lmin(s[1..]));
  //     {
  //       reveal ih;
  //     }
  //     min(s[0], lmax(s[1..]));
  //     max(s[0], lmax(s[1..]));
  //     lmax(s);
  //   }
  //   // assert lmin(s[1..]) == lmin(s);
  // }
  
}