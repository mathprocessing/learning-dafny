/*
Minimax tree:

basic Alpha-beta prunning

root - ? 
     - 5
     - (<= 4) - (can't prune, we already calculated this branch)
              - 4 (new info)
              - ? (we can prune this branch by `new info` + def of mmtree)


How to model mmtree?

1. Use datatype
2. Define function: path -> score
  where type path = seq<Action>
*/

datatype Action = A | B | C
type Score = nat

function min(x: int, y: int): int { if x < y then x else y }
function max(x: int, y: int): int { if x > y then x else y }

function lmin(s: seq<int>): int
requires |s| > 0 { if |s| == 1 then s[0] else min(s[0], lmin(s[1..])) }

function lmax(s: seq<int>): int
requires |s| > 0 { if |s| == 1 then s[0] else max(s[0], lmax(s[1..])) }

function f(a: seq<Action>): Score

type nonemptyseq = s: seq<int> | |s| > 0 witness [0]

lemma head_tail(s: seq<int>, P: int -> bool)
requires |s| > 0
requires forall x | x in s[1..] :: P(x)
requires P(s[0])
ensures forall x | x in s :: P(x)
{}

lemma {:induction false} AllGreaterMin_long_proof(s: seq<int>)
ensures forall x | x in s :: x >= lmin(s)
decreases |s|
{
  if |s| <= 1 {} else {
    if s[0] >= lmin(s[1..]) {
      // We use trick with lambda function `P`
      // to avoid indexing of list `s` (forall i | 0 <= i < |s| :: s[i] bla bla)
      var P := (x: int) => x >= lmin(s[1..]);
      assert forall x | x in s :: x >= lmin(s[1..]) by {
        // assume forall x | x in s[1..] :: x >= lmin(s[1..]);
        // assume s[0] >= lmin(s[1..]);
        // head_tail(s, (x, s) => x >= lmin(s[1..]));
        assert forall x | x in s[1..] :: P(x) by {
          AllGreaterMin_long_proof(s[1..]);
        }
        assert P(s[0]) by { // rewrite same thing using `P`
          assert s[0] >= lmin(s[1..]);
        }
        assert forall x | x in s :: P(x); // Dafny need this helper assert
      }
    } else {
      assert s[0] < lmin(s[1..]);
      assert s[0] == lmin(s);
      assert lmin(s) == min(s[0], lmin(s[1..])); // by def
      var P := (x: int) => x >= s[0];
      assert Goal: forall x | x in s :: x >= lmin(s) by {
        assert forall x | x in s :: x >= s[0] by {
          // assert forall x | x in s[1..] :: x >= lmin(s[1..]);
          // assert forall x | x in s[1..] :: x >= s[0];
          assert forall x | x in s[1..] :: P(x) by {
            AllGreaterMin_long_proof(s[1..]);
          }
          // assert P(s[0]); // by le_refl
          assert forall x | x in s :: P(x);
        }
      }
      reveal Goal;
    }
    calc {
      lmin(s);
      ==
      min(s[0], lmin(s[1..]));
      <=
      lmin(s[1..]);
    }
  }
}

lemma {:induction false} AllGreaterMin(s: seq<int>)
ensures forall x | x in s :: x >= lmin(s)
decreases |s|
{
  if |s| <= 1 {} else {
    var P;
    if s[0] >= lmin(s[1..]) {
       P := (x: int) => x >= lmin(s[1..]);
    } else {
      P := (x: int) => x >= s[0];
    }
    assert forall x | x in s[1..] :: P(x) by {
      AllGreaterMin(s[1..]);
    }
    assert forall x | x in s :: P(x);
  }
}

lemma {:induction s} AllLessMax(s: seq<int>)
ensures forall x | x in s :: x <= lmax(s)
{
  if |s| > 1 {
    // var P;
    if s[0] <= lmax(s[1..]) {
      // P := (x: int) => x <= lmax(s[1..]);

      assert forall x | x in s[1..] :: x <= lmax(s[1..]); // by ih
      assert forall x | x in s :: x <= lmax(s[1..]) by {
        // forall x | x in s ensures x <= lmax(s[1..]) {
        //   assert s[0] <= lmax(s[1..]);
        //   assert lmax(s[1..]) >= lmax(s);
        // }
        assume false; // TODO prove this without `var` declaration and indexing
      }
    } else {
      // P := (x: int) => x <= s[0];
      assert forall x | x in s[1..] :: x <= s[0]; // by ih
      assert forall x | x in s :: x <= s[0] by {
        assume false; // TODO prove this without `var` declaration and indexing
      }
    }
    // assert forall x | x in s[1..] :: P(x); // by ih
    // assert forall x | x in s :: P(x);

  }
}

lemma {:induction false} MinMaxEqImpAllEq(s: seq<int>)
requires |s| > 0
requires lmin(s) == lmax(s)
ensures forall x | x in s :: x == lmin(s)
{
  AllGreaterMin(s);
  AllLessMax(s);
}