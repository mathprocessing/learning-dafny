include "./mmtree.dfy"

method {:verify false} testlmm()
{
  assert lmax([5,6,7]) == 7;
  assert lmax([3,6,5]) == 6;
  assert lmin([3,6]) == 3;
}

function min3(x: int, y: int, z: int): int { min(x, min(y, z)) }
function max3(x: int, y: int, z: int): int { max(x, max(y, z)) }

lemma {:axiom} PlayerOneMax()
ensures f([]) == max3(f([A]), f([B]), f([C]))

lemma {:axiom} PlayerTwoMin()
ensures forall x :: f([x]) == min3(f([x, A]), f([x, B]), f([x, C]))



// Not strict
predicate isOptimal(x: Action)
{
  f([x]) >= f([A]) && f([x]) >= f([B]) && f([x]) >= f([C]) // Works
}

predicate isOptimal_2(x: Action)
{
  forall y: Action :: f([x]) >= f([y]) // Not works, even with triggers
  // But if we use helper lambdas like `var h := y => y == A`, it works
}

predicate geScore(x: Action, s: Score) {f([x]) >= s}

predicate isOptimal_3(x: Action)
{
  forall y: Action :: geScore(x, f([y]))
}

// Strict optimality
predicate isStrictlyOptimal(x: Action)
{
  forall y | y != x :: f([x]) > f([y])
}

// Hmm... Why `isStrictlyOptimal` don't need helper?
// May be `y != x` change the game... Let's try to add some obvious expressions
predicate isOptimal_4(x: Action)
{
  // forall y :: f([x]) >= f([y]) // not works :(
  // forall y | true :: f([x]) >= f([y]) // also not works :(
  forall y | y == y :: f([x]) >= f([y]) // Works!!! Why? May be we already include helper inside a forall statement i.e. `y => y == A` is almost equivalent to `forall y | y == y` in z3
}

predicate canPrune'(path: seq<Action>)
requires |path| == 1 // this works only for len = 1 because wrong defined
{
  exists otherPath | |path| == |otherPath| && path != otherPath :: f(otherPath) >= f(path)
}

predicate isEven(n: nat) { 
  // if n == 0 then true else !isEven(n - 1)
  n > 0 ==> !isEven(n - 1)
}

// We require odd length because we prune only (maximal) first player branches
predicate canPruneMax(path: seq<Action>)
requires !isEven(|path|)
{
  exists betterAction :: f(path[(|path|-1):=betterAction]) >= f(path)
}

method AlphaBetaStrictOptimal()
{
  // we can move witness away from assert body and up in scope
  // var helper := y => y == A; 
  PlayerOneMax();
  PlayerTwoMin();
  if f([A]) == 5 && f([B, C]) == 4 {
    // we can prune branch [B, ?, ?, ...]
    assert canPrune'([B]) by {
      // Dafny don't need this assert
      // assert f([A]) > f([B]) by { assert f([B]) <= 4; }
    }
    // assert canPrune'([A]); // Can't be proved as expected
    // assert canPrune'([C]); // Can't be proved as expected

    assert canPruneMax([B]) by {
      assert f([A]) >= f([B]);
      var path := [B];
      var betterAction := A;
      var otherPath := path[(|path|-1):=betterAction];
      assert otherPath == [A];
      assert f(path[(|path|-1):=betterAction]) >= f(path);
    }

    if f([C]) > 5 {
      assert isStrictlyOptimal(C);
    } else {
      assert isOptimal_4(A);
      assert f([C]) != 5 ==> isStrictlyOptimal(A);
    }
  }
}

method {:verify false} AlphaBetaSimple()
{
  // 1-st player turn --> perfect player takes action with maximum score
  PlayerOneMax();
  // 2-st player turn --> perfect player takes action with minimum score
  PlayerTwoMin();
  if f([A]) == 5 && f([B, C]) == 4 {
    // we can prune branch [B, ?, ?, ...]
    assert f([A]) > f([B]) by {
      assert f([B]) <= 4;
    }
    // and root score:
    var rootScore := f([]);
    assert rootScore == max(f([A]), f([C])) == max(5, f([C]));
    // Optimal move
    if case true => {
      if f([C]) >= 5 {
        assert isOptimal(C);
      } else {
        assert isOptimal(A);
      }
    } case true => {
      if f([C]) >= 5 {
        // assert isOptimal_2(C); // fails
      } else {
        // assert isOptimal_2(A); // fails
      }
    } case true => {
      if geScore(C, 5) { // f([C]) >= 5
        // assert isOptimal_3(C); // fails
      } else {
        // assert isOptimal_3(A); // fails
      }
    } case true => {
      // Now we try to help z3
      if f([C]) >= 5 {
        assert isOptimal_2(C) by {
          // Way 1:
          // var helper := (y: Action) => y == A; // Works
          // Way 2:
          forall y ensures f([C]) >= f([y]) {
            if y == A { /* assert f([C]) >= f([y]); */ }
          }
        }
      } else {
        assert isOptimal_2(A) by {
          var helper := (y: Action) => y == A; // Works
          // var helper := (y: Action) => y == B; // Works
          // var helper := (y: Action) => y == C; // Works
        }
      }
    }

  }
}

// method test()
// {
//   // 1-st player turn --> perfect player takes action with maximum score
//   assume f([]) == lmax([f([A]), f([B]), f([C])]);
//   // 2-st player turn --> perfect player takes action with minimum score
//   assume forall x :: f([x]) == lmin([f([x, A]), f([x, B]), f([x, C])]);
//   if f([A]) == 5 && f([B, C]) == 4 {
//     assert f([B]) <= 4;
//   }
// }