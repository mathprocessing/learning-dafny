from utils import log2int, sumeq

# Globally defined storage for memoization values if `v` function
V = dict()

def v(delta, eggs) -> "Option<nat>":
  """
  v = Minimal number of throws in current state(delta, eggs).
  delta - characterises uncertanty of h at the beginning.
  eggs - number of eggs that we have at the beginning of experiment.

  Axiom DeltaZero: v(0, eggs) == 0
  Axiom EggsZero: delta > 0 ==> v(delta, 0) == None

  Lemma EggOne: v(delta, 1) == delta
  >>> [v(delta, 1) for delta in range(5)]
  [0, 1, 2, 3, 4]

  Lemma EggsTwo: delta >= 1 ==> v(delta, 2) == sumeq(delta - 1) + 1
  >>> [(v(delta, 2), sumeq(delta - 1) + 1) for delta in range(1, 6)]
  [(1, 1), (2, 2), (2, 2), (3, 3), (3, 3)]

  Another formulation:
    Lemma EggsTwo:
      forall n: nat, k | 1 <= k <= n :: v(n*(n-1) // 2 + k, 2) == n
  """
  if delta == 0:
    # h = {0}, problem already solve ==> user need do 0 actions(0 throws)
    return 0
  if eggs == 0:
    assert delta >= 1
    # h not determined and we can't make new tries ==> problem not solvable
    return None
  # if eggs == 2:
  #   # n(n-1) / 2 = delta - {1..n}
  #   # sumeq: 5 -> 2, 6 -> 3
  #   # v(6..8, 2) = 3, 4, 4
  #   # s = sumeq(delta - 1)
  #   # dx = delta - 1 - s*(s-1) // 2
  #   # assert dx >= 0
  #   return sumeq(delta - 1) + 1
  m = V.get((delta, eggs))
  if m is not None:
    if m == -1: # decode None from dict storage
      return None
    return m
  assert m is None
  minval = None
  for flr in range(1, delta + 1):
    fval = f(delta, eggs, flr)
    if fval is None:
      continue
    assert fval is not None
    # but minval in {None, nat}
    if minval is None or minval > fval:
      minval = fval
  # we must encode `None` somehow, let encoded(None) = -1
  if minval is None:
    V[(delta, eggs)] = -1
  else:
    V[(delta, eggs)] = minval
  return minval # wrong identation of return can lead to errors

def f(delta, eggs, flr) -> "Option<nat>":
  """
  f = Minimal number of throws when floor (flr) is fixed.
  `None` means problem not solvable i.e. first_action_set = empty_set
  Also `None` can be interpreted as infinite number of throws(actions): None == +INF
    Why? Because we can't do infinite number of throws ==> `+INF throws` equivalent to empty_set
                                                                         ^^^^^^^^^^ TODO: learn about this type of equivalence
  """
  if delta == 0:
    # problem solved, no actions needed
    return 0
  if eggs == 0:
    # problem can't be solved if user can't throw, but problem can be already solved only if delta == 0, now delta > 0
    return None
  assert flr > 0, "zero floor"
  assert delta - flr >= 0, "negative delta - flr"
  v1 = v(delta - flr, eggs)
  v2 = v(flr - 1, eggs - 1)
  if v1 is None or v2 is None:
    return None
  return 1 + max(v1, v2)

def hsetstr(delta):
  s = f"{', '.join(map(str, range(delta + 1)))}"
  s2 = f"delta = {delta} ==> h in " + '{' + s + '}'
  return s2

def optaction(delta, eggs):
  """

  >>> d = 0
  >>> print(hsetstr(d))
  delta = 0 ==> h in {0}
  
  h is determined (have only one possible value)
  i.e. problem already solved, no actions needed
  >>> [optaction(d, e) for e in range(0, 3)]
  [None, None, None]

  >>> d = 1
  >>> print(hsetstr(d))
  delta = 1 ==> h in {0, 1}

  First action in strategy: just one throw from 1st floor if you have eggs
  Note: only one(this) strategy possible
  >>> [optaction(d, e) for e in range(0, 6)]
  [None, 1, 1, 1, 1, 1]

  >>> d = 2
  >>> print(hsetstr(d))
  delta = 2 ==> h in {0, 1, 2}

  """

def showeqs(delta, eggs, flrmin, flrmax):
  for flr in range(flrmin, flrmax + 1):
    v1 = v(delta - flr, eggs)
    v2 = v(flr - 1, eggs - 1)
    anynone = v1 is None or v2 is None
    m = None if anynone else 1 + max(v1, v2)
    s = f'f({delta}, {eggs}, {flr}) = '\
    f'{m} = 1 + max({v1}, {v2}) = '\
    f'1 + max(v({delta - flr}, {eggs}), v({flr - 1}, {eggs - 1}))'
    print(s)

def showv(left, right, eggs):
  for d in range(left, right + 1):
    print(f'v({d}, {eggs}) =', v(d, eggs))

def showveggs(left, right, delta):
  for eggs in range(left, right + 1):
    print(f'v({delta}, {eggs}) =', v(delta, eggs))

def test1():
  """
  if eggs == 0 then no actions possible
    i.e. number of throws can't be determined
  >>> assert v(1, 0) is None

  possible values for egg h: is only one i.e. 
    Generally: lemma HUncertaintyIsSuccOfDelta: size(h_set) == delta + 1
    where in current case: h_set = {0}
  minimal number of throws = 0
  >>> assert v(0, 1) == 0

  if delta == 1 h can have two values: h in {0, 1}
  we need at least one throw to determine h value
  >>> assert v(1, 1) == 1

  number of eggs can't change this number, because lemma vMonotonic: v(delta, eggs) >= v(delta, eggs + 1)
    + some things why v(1, eggs) can't be just equal to zero.
  >>> assert v(1, 2) == 1
  >>> assert v(1, 3) == 1
  >>> assert v(1, 10) == 1

  h in {0, 1, 2}
  >>> assert v(2, 2) == 2

  >>> f(3, 2, 1), f(3, 2, 2), f(3, 2, 3)
  (3, 2, 3)

  Check optimal action selection lemma:
    v(3, 2) = 2 = min(3, 2, 3) = min(f(3, 2, 1), f(3, 2, 2), f(3, 2, 3))

  >>> assert v(3, 2) == 2
  >>> assert v(4, 2) == 3
  >>> assert v(5, 2) == 3

  >>> showeqs(delta=5, eggs=2, flrmin=1, flrmax=5)
  f(5, 2, 1) = 4 = 1 + max(3, 0) = 1 + max(v(4, 2), v(0, 1))
  f(5, 2, 2) = 3 = 1 + max(2, 1) = 1 + max(v(3, 2), v(1, 1))
  f(5, 2, 3) = 3 = 1 + max(2, 2) = 1 + max(v(2, 2), v(2, 1))
  f(5, 2, 4) = 4 = 1 + max(1, 3) = 1 + max(v(1, 2), v(3, 1))
  f(5, 2, 5) = 5 = 1 + max(0, 4) = 1 + max(v(0, 2), v(4, 1))

  Let's invent lemma vMinBound: v(d, e) >= 1 + log2(d)
    >>> showveggs(1, 7, delta=63)
    v(63, 1) = 63
    v(63, 2) = 11
    v(63, 3) = 7
    v(63, 4) = 7
    v(63, 5) = 7
    v(63, 6) = 6
    v(63, 7) = 6

    >>> assert log2int(63) == 5

    >>> showveggs(1, 7, delta=64)
    v(64, 1) = 64
    v(64, 2) = 11
    v(64, 3) = 8
    v(64, 4) = 7
    v(64, 5) = 7
    v(64, 6) = 7
    v(64, 7) = 7

    >>> assert log2int(64) == 6

    lemma vMinBound: v(d, e) >= 1 + log2(d)
    for this and f(d, e, _) >= v(d, e)
      we have:
      f(d, e, _) >= 1 + log2(d)

    Also we have vMonotonic lemma:
      v(d, e + 1) <= v(d, e)
      because more eggs implies less or same number of throws
  """

if __name__ == '__main__':
  import doctest
  doctest.testmod(optionflags=doctest.FAIL_FAST)