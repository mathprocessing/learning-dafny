def log2int(x) -> int:
  """
  >>> {i: log2int(i) for i in range(1, 5)}
  {1: 0, 2: 1, 3: 1, 4: 2}

  >>> log2int(0.5)
  -1
  >>> log2int(0.25)
  -2
  >>> log2int(0.3)
  -1
  """
  assert x >= 0
  if x == 1:
    return 0
  flipped = False
  if x < 1:
    flipped = True
    x = 1 / x
  counter = 0
  assert x > 1
  while x > 1:
    x = x / 2
    counter += 1
  if x < 1:
    counter -= 1
  if flipped:
    return -counter
  else:
    return counter

def sumeq(x: "nat"):
  """
  This function in reverse for sum of arithmetic progression: [1, 1 + 2, 1 + 2 + 3, ...]

  Lemma: sumeq(x)*(sumeq(x) + 1) // 2 >= x

  >>> {i: sumeq(i) for i in range(7)}
  {0: 0, 1: 1, 2: 1, 3: 2, 4: 2, 5: 2, 6: 3}
  """
  assert x >= 0
  i = 0
  while x >= 0:
    i += 1
    assert i > 0 # to prove termination of loop, this assert can help even if someone change this code
    x -= i
  return i - 1

if __name__ == '__main__':
  import doctest
  doctest.testmod(optionflags=doctest.FAIL_FAST)