# from mylib import add # OK

# from .mylib import add # wrong
# import eggs # ModuleNotFoundError: No module named 'eggs'
# from eggs import add # ModuleNotFoundError: No module named 'eggs'
# from . import add # ImportError: cannot import name 'add' from '__main__' (testinner.py)

print(add(1, 42))