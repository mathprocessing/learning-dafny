include "./lib.dfy"

lemma DiagLemma(floor: pos)
ensures f(floor, 2, floor) == floor
{
  var eggs := 2;
  calc == {
    f(floor, eggs, floor);
    { MaxStepAuto(); }
    1 + max(v(0, eggs), v(floor - 1, eggs - 1));
    { ZeroDelta(eggs); }
    1 + v(floor - 1, eggs - 1);
    { OneEgg(floor - 1); }
    floor;
  }
}

lemma DeltaLtEggs(delta: nat, eggs: pos)
requires delta <= eggs
ensures v(delta, eggs) == v(delta, eggs - 1)
decreases delta
{
  if delta == 0 {
    assert v(0, eggs) == v(0, eggs - 1) by { ZeroDelta(eggs); ZeroDelta(eggs - 1); }
  } else if delta == 1 {
    assert v(1, eggs) == v(1, eggs - 1) by { }
  } else {
    assert v(delta - 1, eggs) == v(delta - 1, eggs - 1) by { DeltaLtEggs(delta - 1, eggs); }
    calc {
      v(delta - 1, eggs);
      { OptimalAction(delta - 1, eggs); }
      minrange(0, delta - 1, (i: pos) => f(delta - 1, eggs, i));
    }
    assume false;
  }
}

lemma FloorOne(delta: pos, eggs: pos)
ensures f(delta, eggs, 1) == 1 + v(delta - 1, eggs)
{
  calc == {
    f(delta, eggs, 1);
      { MaxStep(delta, eggs, 1); }
    1 + max(v(delta - 1, eggs), v(0, eggs - 1));
      { ZeroDelta(eggs - 1); }
    1 + v(delta - 1, eggs);
  }
}