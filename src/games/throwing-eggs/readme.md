# Problem
Есть N идентичных друг другу хрупких яйц и здание в 100 этажей.
Можно подмиматься на любой этаж и бросать оттуда яйцо, но проблема: количество яиц ограничено.

Задача: Написать алгоритм, который позволяет определить хрупкость яйца в этажах (единицах измерения) за минимальное число бросков.

# Solution
Решение:
Определим функцию f:
* `hmin <= h <= hmax`
* `h` - номер этажа, при броске с которого яйцо уцелеет, но с `h + 1` уже разобъётся
* `eggs` - кол-во оставшихся яиц

`throws(hmin, hmax, eggs, throw_height)`

Properties:
```text
if hmin == hmax:
  throws(hmin, hmax, eggs) = 0 // Problem solved

```

# Table
```
if h >= 5:
  throws(0, 100, eggs=1, action=5) --> eggs-=0 --> knownH >= 5


f(0, 100, 1) = 100 throws in worst case

delta = hmax - hmin
f(0, 0, *) = 0 <=> f(delta=0, eggs) = 0

-- f(eggs, action_throw_from_Nth_floor) --

player selection => use min function
f(eggs) = min(f(eggs, 1), f(eggs, 2), ..., f(eggs, 100))

observation of new information => use max function
egg squashes ==> eggs -= 1; info.push({h < floor})
  i.e. hmax := floor - 1

otherwise ==> info.push({h >= floor})
  i.e. hmin := floor

f(eggs, floor) = max(f(floor - 1, eggs - 1))


f(delta=0, eggs) = 0


```
# Table for 2 eggs
```text
          5 | 5 floor
        4 4 | 4
      3 3 3 | 3
    2 2 3 3 | 2
  1 2 3 3 4 | 1
-------------
0 1 2 3 4 5 delta

v(1, 2) = 1 = minl([1])
v(2, 2) = 2 = minl([2, 2])
v(3, 2) = 2 = minl([3, 2, 3])
v(4, 2) = 3 = minl([3, 3, 3, 4])
v(5, 2) = 3 = minl([4, 3, 3, 4, 5])
```

# Axioms
* ZeroDelta: `v(0, eggs) = 0`
* OneEgg: `v(n, 1) = n`
* RecursionStep:
  `requires eggs > 0`
  `ensures f(delta, eggs, floor) = 1 + max(v(delta - floor), v(floor - 1, eggs - 1))`
* OptimalAction: `v(delta, edgs) = minrange(1, delta, floor => f(delta, eggs, floor))`

# Theorems
* vMonotonic: `if delta <= eggs then v(delta, eggs) = v(delta, eggs - 1)`
  How to create:
  1. `v(1, 2) = min(f(1, 2, 1)) = f(1, 2, 1) = 1`
  2. Think about why `v(1, 2) = v(1, 1)`?
* TwoEggs: `v(delta, 2) = ...`

# Energy minimization
From this problem we can generalize:
* if strategy is optimal then
    linear combnation of number of actions is constant (or equal zero after reducing symmetries)
    i.e.
    count(coefA * nactionsA, coefB * nactionsB, ...) = 0

* Each action excuation consumes some energy (in that task E(throw) = 1)
  our goal may be "minimize energy consumption" in some sequence of actions `[A, B, ...]`

* In current problem: we can define energy as `E = distance(lastThrow, currentThrow)`
  and prove that solution do not changes.
  i.e. `solutions_set(E(throw) = 1) <=> solutions_set(E(throw) = distance(...))`