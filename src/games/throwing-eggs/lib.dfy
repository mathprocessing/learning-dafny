
type pos = x: int | x > 0 witness 1

// function minrange(left, right, )

function min(x: int, y: int): int { if x < y then x else y }
function max(x: int, y: int): int { if x < y then y else x }

function lmin(xs: seq<int>): int
requires xs != []
{
  if |xs| == 1 then xs[0]
  else min(xs[0], lmin(xs[1..]))
}

lemma lminABC(a: int, b: int, c: int) // WISH: This must be provable automatically
ensures lmin([a, b, c]) == min(a, min(b, c))
{
  calc == {
    lmin([a, b, c]);
    min(a, lmin([b, c]));
    min(a, min(b, c));
  }
}

function minrange(left: int, right: int, func: int -> int): int
requires left <= right
decreases right - left
{
  if left == right then func(left)
  else min(func(left), minrange(left + 1, right, func))
}

// We can make special symbol/contant INF by just defining it `const inf: int; lemma infdef: forall n :: n <= inf`
const inf := 1000

function v(delta: nat, eggs: nat): nat
// ensures delta == 0 ==> res == 0
// ensures eggs == 1 ==> res == delta

lemma vObvious()
ensures forall eggs: nat :: v(0, eggs) == 0
ensures forall delta: nat :: v(delta, 1) == delta

function f(delta: nat, eggs: nat, action: pos): nat

lemma ZeroEggs(delta: nat)
ensures v(delta, 0) == inf

lemma ZeroDelta(eggs: nat)
ensures v(0, eggs) == 0

lemma OneEgg(delta: nat)
ensures v(delta, 1) == delta

lemma OptimalAction(delta: nat, eggs: nat)
requires delta >= 1
ensures v(delta, eggs) == minrange(1, delta,  floor => assume floor >= 1; f(delta, eggs, floor))

lemma MaxStep(delta: nat, eggs: pos, floor: pos)
requires floor <= delta
ensures f(delta, eggs, floor) == 1 + max(v(delta - floor, eggs), v(floor - 1, eggs - 1))

lemma {:verify false} MaxStepAuto()
ensures forall delta: nat, eggs: pos, floor: pos | floor <= delta :: f(delta, eggs, floor) == 1 + max(v(delta - floor, eggs), v(floor - 1, eggs - 1))
{
  forall delta: nat, eggs: pos, floor: pos | floor <= delta ensures f(delta, eggs, floor) == 1 + max(v(delta - floor, eggs), v(floor - 1, eggs - 1))
  {
    MaxStep(delta, eggs, floor);
  }
}

method test(eggs: nat)
{
  assume v(5, eggs) == lmin([f(5, eggs, 1), f(5, eggs, 2), f(5, eggs, 3), f(5, eggs, 4), f(5, eggs, 5)]);
  if eggs == 0 {
    // if delta = 0 ==> problem solved
    // if delta > 0 && eggs = 0 ==> problem not solved
    assert v(5, eggs) == inf by { ZeroEggs(5); }
  } else {
    assert f(5, eggs, 2) == 1 + max(v(3, eggs), v(1, eggs - 1)) by {
      MaxStep(5, eggs, 2);
    }
  }
}

lemma vTable()
ensures v(1, 2) == 1
ensures v(2, 2) == 2
ensures v(3, 2) == 2
ensures v(4, 2) == 3
ensures v(5, 2) == 3
{
  assert v(1, 2) == 1 by {
    calc == {
      v(1, 2);
        { OptimalAction(1, 2); }
      f(1, 2, 1);
        { MaxStep(1, 2, 1); vObvious(); }
      1;
    }
  }
  assert v(2, 2) == 2 by {
    calc == {
      v(2, 2);
        { OptimalAction(2, 2); }
      min(f(2, 2, 1), f(2, 2, 2));
      // Can we prove that f(2, 2, 1) == f(2, 2, 2) without calculating exact values?
      {
        MaxStepAuto();
        vObvious();
        assert f(2, 2, 1) == 2;
        assert f(2, 2, 2) == 2;
      }
      2;
    }
  }
  assert v(3, 2) == 2 by {
    calc == {
      v(3, 2);
        { OptimalAction(3, 2); forall a, b, c { lminABC(a, b, c); } }
      lmin([f(3, 2, 1), f(3, 2, 2), f(3, 2, 3)]);
      {
        assert f(3, 2, 1) == 3 by {
          calc == {
            f(3, 2, 1);
              // { MaxStepAuto(); } // Note: we can partially use automation! This idea helps to write detailed proofs with less effort but with fixed set of lemmas: "what lemmas we use when we write that current proof?".
            // 1 + max(v(3 - 1, 2), v(1, 1)); // Bad news: this still provable but we don't want that i.e. we must disable automation :(
              // How to fix this? 
            // 1. Restrict z3 to use only one time MaxStep lemma (set fuel=1 or something like that)
            // Or 
            // 2. Create variable for list of arguments `var args := [delta=3, eggs=2, floor=1]`
            //    And then use it: `MaxStep.call(args)`
            // { MaxStep(3, 2, 1); }
            // 1 + max(v(3 - 1, 2), v(1, 1)); // still provable, i.e. we must FULLY disable automation i.e. clear ensures in definition of `v`
            // We moved this clauses to lemma `vObvious`
            //   { MaxStep(3, 2, 1); }
            // 1 + max(v(3 - 1, 2), v(1, 1)); // not proves, ok
              { MaxStep(3, 2, 1); }
            1 + max(v(3 - 1, 2), v(1 - 1, 1)); // OK
            1 + max(v(2, 2), v(0, 1));
            { vObvious(); }
            3;
          }
        }
        assert f(3, 2, 2) == 2 by {
          calc == {
            f(3, 2, 2);
            { MaxStep(3, 2, 2); }
            1 + max(v(1, 2), v(1, 1));
            { vObvious(); } // WITH: dafny must guarantee us that path of proof is minimal, i.e. just `{}` don't works. (I already saw about that earlier)
            2;
          }
        }
        assert f(3, 2, 3) == 3 by { MaxStepAuto(); vObvious(); }
        assert lmin([3, 2, 3]) == 2;
      }
      2;
    }
  }
  assert v(4, 2) == 3 by {
    MaxStepAuto(); vObvious();
    assert f(4, 2, 1) == 3;
    assert f(4, 2, 2) == 3;
    assert f(4, 2, 3) == 3;
    assert f(4, 2, 4) == 4;
    OptimalAction(4, 2);
  }
  assert v(5, 2) == 3 by {
    MaxStepAuto(); vObvious();
    assert f(5, 2, 1) == 4;
    assert f(5, 2, 2) == 3;
    assert f(5, 2, 3) == 3;
    assert f(5, 2, 4) == 4;
    assert f(5, 2, 5) == 5;
    OptimalAction(5, 2);
  }
}