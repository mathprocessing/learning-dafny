include "./lib.dfy"

lemma test1(x: pos)
ensures mod(x, 2 * x - 1) == (if x == 1 then 0 else x)
{
  if x == 1 {
    ModSelf(1);
  } else {
    ModDefBase(x, 2 * x - 1);
  }
  assert h1: mod(1, 1) == 0 by { ModAuto(); }
  assert h2: mod(2, 3) == 2 by { ModAuto(); }
  assert h3: mod(3, 5) == 3 by { ModAuto(); }
  assert h4: mod(4, 7) == 4 by { ModAuto(); }
}