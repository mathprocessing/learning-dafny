include "../common.dfy"

const mod: (nat, pos) -> nat

lemma {:axiom} ModDef(x: nat, y: pos)
ensures mod(x, y) == (if x < y then x else mod(x - y, y))

// For more precise proofs:
lemma ModDefBase(x: nat, y: pos)
requires x < y
ensures mod(x, y) == x
{ ModDef(x, y); }

lemma ModDefStep(x: nat, y: pos)
requires x >= y
ensures mod(x, y) == mod(x - y, y)
{ ModDef(x, y); }

/**
Table of values:
mod(x, y)
  x 0 1 2 3 4 5 6 7 8 9 10
y
0   - - - - - -
1   0 0 0 0 0 0 0 0 0 0 0
2   0 1 0 1 0 1 0 1 0 1 0
3   0 1 2 0 1 2 0 1 2 0 1
4   0 1 2 3 0 1 2 3 0 1 2
5   0 1 2 3 4 0 1 2 3 4 0
6   0 1 2 3 4 5 0 1 2 3 4
7   0 1 2 3 4 5 6 0 1 2 3
8   0 1 2 3 4 5 6 7 0 1 2
9   0 1 2 3 4 5 6 7 8 0 1
10  0 1 2 3 4 5 6 7 8 9 0

1.
SUM(i := 0 to x, j := 1 to x) of mod(i, j) <= ? <= (x - 1)^3
  Can we improve (x - 1)^3?
  Hint: use `mod(x, y) <= min(x, y - 1)`
2.
We can convert all mod function to sequence by using this trick:
0 1 8 9
3 2 7 ...
4 5 6
Or
0 1 5 6
2 4 7
3 8
9 ...

modseq = [0, 0, 1, 0, 0, 1, 2, 0, 0, 0, 1, 0, 3, 2, 1, 0, ...]
3.

*/

lemma ModUpperBound(x: nat, y: pos)
ensures mod(x, y) <= min(x, y - 1)
{
  ModLeX(x, y); // WISH: to check that this proof (and defnitions around) are correct we need auto-reducer for proofs
  ModLtY(x, y); // For example it can just comment out one of this lines and possibility of proving, it must be not possible to minimize
  // In general use case: proof can be marked using attribute {:not-reducible}, that means "proof is minimal in terms of current context"
  // We need "in terms of current context" because in general proof can be minimized but that fact may depend on definitions around.
  // I.e. we may define some function `CanBeMinimized(context, proof) -> bool`
}

lemma ModLeX(x: nat, y: pos)
ensures mod(x, y) <= x
decreases x
{
  if x < y {
    ModDefBase(x, y);
  } else {
    ModLeX(x - y, y);
    ModDefStep(x, y);
  }
}

lemma ModAdd(x: nat, y: nat, z: pos)
ensures mod(x + y, z) == mod(mod(x, z) + mod(y, z), z)

lemma ModTwice(x: nat, y: pos)
ensures mod(mod(x, y), y) == mod(x, y)
// Can be generalized using `iterate`:
// iterate(x, y, mod, 0) == x
// iterate(x, y, mod, 1) == mod(x, y)
// iterate(x, y, mod, 2) == mod(mod(x, y), y)
// ...
// ensures n >= 1 ==> iterate(x, y, mod, n) == iterate(x, y, mod, 1)
{
  assert mod(x, y) < y by { ModLtY(x, y); }
  ModDefBase(mod(x, y), y);
}

lemma ModDefStepRev(x: nat, y: pos)
ensures mod(x, y) == mod(x + y, y)
{
  /*
  mod(3, 1) == mod(4, 1)
  mod(3, 2) == mod(5, 2)
  mod(3, 3) == mod(6, 3)
  ...
  mod(3, y) == mod(3 + y, y)
  ...
  mod(x, y) == mod(x + y, y)
  */
  ModDefStep(x + y, y);
}

lemma ModLinear(x: nat, a: nat, b: nat)
// ensures mod(a * x + b, x) == ?

lemma ModSub1(x: pos)
{
  /*
  mod(2 * x + 2, x) == ?
  mod(2 * (x + 1), x) == ?
    How to connect it? Maybe using: mod(?, x) related to mod(?, x-1)
  mod(2 * (x + 1), x + 1) == 0
  */
  /*
  mod(x, y) - mod(x, y + 1) == ?
    x < y     ==> ? == 0
    x < 2 * y ==> ? == 1
    ...

  abs(mod(x, y) - mod(x, y + 1)) == ?
  */
}

lemma ModDiag(x: nat) // WISH: If we have `x: pos` dafny extension seggests: "`x: pos` can be extended to `x: nat`"
ensures mod(2 * x + 1, x + 1) == x;
{
  assert mod(1, 1) == 0 by { ModAuto(); }
  assert mod(3, 2) == 1 by { ModAuto(); }
  assert mod(5, 3) == 2 by { ModAuto(); }
  ModDefStep(2 * x + 1, x + 1);
  ModDefBase(x, x + 1);
  assert mod(2 * x + 1, x + 1) == mod(x, x + 1) == x;
}

lemma ModFactor(c: nat, x: pos)
ensures mod(c * x, x) == 0
decreases c
{
  if c == 0 {
    ModZeroY(x);
  } else {
    calc == {
      mod(c * x, x);
        { 
          // assert c * x >= x;
          // ModDef(c * x, x);
          // We can replace this TWO lines by ONE:
          ModDefStep(c * x, x);
        }
      mod(c * x - x, x);
        { assert c * x - x == (c - 1) * x; } // by arith
      mod((c - 1) * x, x);
        { ModFactor(c - 1, x); }
      0;
    }
  }
}

lemma ModXOne(x: nat)
ensures mod(x, 1) == 0
{
  if x == 0 {
    ModDefBase(0, 1);
  } else {
    ModXOne(x - 1);
    ModDefStep(x, 1);
  }
}

lemma ModZeroY(y: pos)
ensures mod(0, y) == 0
{
  ModDef(0, y);
}

lemma ModOneY(y: pos)
ensures mod(1, y) == (if y == 1 then 0 else 1)
{
  if case true => {
    // Proof variant 1
    ModDef(1, y);
    ModDefBase(0, y); // WISH: Auto reduce `ModDef(0, y);` to `ModDefBase(0, y);`. I.e. we name it "Detalization of proof"
  } case true => {
    // Proof variant 2
    if 1 < y {
      ModDefBase(1, y);
    } else {
      assert y == 1;
      ModDefStep(1, y);
      ModDefBase(0, y);
    }
  }
}

lemma ModSelf(x: pos)
ensures mod(x, x) == 0
{
  ModDefStep(x, x);
  ModDefBase(0, x);
}

lemma ModLtY(x: nat, y: pos)
ensures mod(x, y) < y
decreases x
{
  ModDef(x, y);
  if x >= y {
    assert mod(x - y, y) < y by { ModLtY(x - y, y); }
  }
}

// Little automation
lemma ModAuto()
// ensures forall x: nat, y: pos :: mod(x, y) == if x < y then x else mod(x - y, y)
ensures forall x: nat, y: pos {:trigger mod(x, y)} | x < y :: mod(x, y) == x
ensures forall x: nat, y: pos {:trigger mod(x, y)} {:trigger mod(x - y, y)} | x >= y :: mod(x, y) == mod(x - y, y)
// ensures forall y: pos :: mod(0, y) == 0 // Bonus: We don't need this if we use {:trigger ...} in lines above
// ensures forall x: nat :: mod(x, 1) == 0 // Bonus: We don't need this if we use {:trigger ...} in lines above
ensures forall x: pos :: mod(x, x) == 0
{
  forall x { ModSelf(x); }
  forall x: nat, y: pos | x < y ensures mod(x, y) == x { ModDefBase(x, y); }
  forall x: nat, y: pos | x >= y ensures mod(x, y) == mod(x - y, y) { ModDefStep(x, y); }
}