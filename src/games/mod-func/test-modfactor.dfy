include "./lib.dfy"

lemma testmodfactor3(c: pos)
ensures mod(c * c * c, c) == 0
{
  ModFactor(c * c, c);
}

lemma testmodfactor2(c: pos)
ensures mod(c * c, c) == 0
{
  ModFactor(c, c);
}

lemma testmodfactor(c: nat, x: pos)
ensures mod(c * x + c, x + 1) == 0
{
  ModFactor(c, x + 1);
}