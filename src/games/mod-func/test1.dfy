include "./lib.dfy"

method test()
{
  // Manual instantiation
  ModOne(0); ModOne(1); ModOne(2); ModOne(3); ModOne(4); ModOne(5);
  ModSelf(1); ModSelf(2); ModSelf(3); ModSelf(4); ModSelf(5);
  ModBound(0, 1); ModBound(0, 2); ModBound(0, 3); ModBound(0, 4); ModBound(0, 5);
  ModBound(1, 1); ModBound(1, 2); ModBound(1, 3); ModBound(1, 4); ModBound(1, 5);
  ModBound(2, 1); ModBound(2, 2); ModBound(2, 3); ModBound(2, 4); ModBound(2, 5);
  ModBound(3, 1); ModBound(3, 2); ModBound(3, 3); ModBound(3, 4); ModBound(3, 5);
  ModBound(4, 1); ModBound(4, 2); ModBound(4, 3); ModBound(4, 4); ModBound(4, 5);
  
  assert mod(2, 1) == 0;
  assert mod(3, 2) == 1; // How to prove that we can't prove that even if we instanciate all lemmas fully (infinite times)?
  // We can write in prover fact like that:
  // assert IsNotAchievable(State: [mod(3, 2) in {1}])
  
}