include "./lib.dfy"

method test()
{
  if case true => {
    assert mod(1, 1) == 0 by { ModAuto(); }
  } case true => {
    assert mod(3, 2) == 1 by { ModAuto(); }
  } case true => {
    assert mod(5, 3) == 2 by { ModAuto(); }
  } case true => {
    assert mod(7, 10) == 7 by { ModAuto(); }
  }
}

method test2()
{
  assert mod(10, 1) == 0 by { ModAuto(); }
  assert mod(10, 2) == 0 by { ModAuto(); }
  assert mod(10, 3) == 1 by { ModAuto(); }
}

method test3()
{
  assert mod(2, 2) == mod(1 + 1, 1) == mod(10, 5) by { ModAuto(); }
}

method test4()
{
  assert mod(2000, 1000) == 0 by { ModAuto(); }
}

method test5()
{
  assert mod(2000, 999) == 2 by { ModAuto(); }
}