include "./lib.dfy"

lemma H(x: nat, y: nat, a: pos, b: pos)
ensures mod(x, a) * mod(y, b) == mod(x * y, a * b)

lemma ModMul(x: nat, a: pos, b: pos)
requires mod(x, a) == 0
requires mod(x, b) == 0
ensures mod(x, a * b) == 0
{
  // Thinking step 1.
  assert a * b > 0;
  // Thinking step 2.
  assert mod(x, a) * mod(x, b) == mod(x * x, a * b) by {
    H(x, x, a, b);
  }
}

lemma ModTwoThreeImpSix(x: nat)
requires mod(x, 2) == 0
requires mod(x, 3) == 0
ensures mod(x, 6) == 0
decreases x
{
  if case true => {
    // Proof by using more general lemma
    ModMul(x, 2, 3);
  } case true => {
    // Proof by induction
    if x < 6 {
      ModZeroY(6);
      ModAuto();
    } else {
      /*
      Pre := mod(x, 2) == 0 && mod(x, 3) == 0
      If we prove Pre==false for [x+1, x+2, .., x+5] then we just use fact `mod(x, 6) == 0 ==> mod(x + 6, 6) == 0`
      */
      assert mod(x - 6, 6) == 0 by {
        assert mod(x - 6, 2) == 0 by {
          ModDefStep(x, 2);
          ModDefStep(x - 2, 2);
          ModDefStep(x - 4, 2);
        }
        assert mod(x - 6, 3) == 0 by {
          ModDefStep(x, 3);
          ModDefStep(x - 3, 3);
        }
        ModTwoThreeImpSix(x - 6);
      }
      ModDefStep(x, 6);
    }
  }
}