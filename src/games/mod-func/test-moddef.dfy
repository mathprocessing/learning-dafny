include "./lib.dfy"

method testDef()
ensures mod(3, 2) == 1
{
  calc == {
    mod(3, 2);
    { ModDef(3, 2); }
    mod(1, 2);
    { ModDef(1, 2); }
    1;
  }
}

method testDef2()
ensures mod(0, 1) == 0
{
  ModDef(0, 1);
}

method testDef3()
ensures mod(1, 1) == 0
{
  ModDef(1, 1);
  ModDef(0, 1);
}

method testDef4()
ensures mod(10, 3) == 1
{
  ModDef(10, 3);
  ModDef(7, 3);
  ModDef(4, 3);
  ModDef(1, 3);
}