def floor(x):
  return x - int(x)

def tobin_frac(x, n=10):
  """
  >>> tobin_frac(0.0)
  ''
  >>> tobin_frac(0.99999)
  '111111111'
  >>> tobin_frac(0.53125)
  '10001'
  >>> tobin_frac(0.5)
  '1'
  >>> tobin_frac(0.25)
  '01'
  >>> tobin_frac(0.125)
  '001'
  >>> tobin_frac(0.0625)
  '0001'
  >>> tobin_frac(0.414)
  '011010011'
  """
  assert x >= 0 and x < 1, "Wrong input"
  res = ''
  i, lastone = 0, 0
  while i < n and x >= 0:
    if x < 1:
      res = res + '0'
    else:
      res = res + '1'
      x = x - 1
      lastone = i
    x *= 2
    i += 1
  if lastone == 0:
    return ''
  return res[1:(lastone+1)]

def tobin(x, n=10):
  """
  
  >>> tobin(1)
  '1'
  >>> tobin(2)
  '10'
  >>> tobin(5)
  '101'
  >>> tobin(5.5)
  '101.1'
  >>> tobin(8.125)
  '1000.001'
  >>> tobin(0.75)
  '0.11'
  >>> tobin(0.125)
  '0.001'
  """
  right = tobin_frac(floor(x), n)
  left = ''
  x = int(x)
  while x >= 1:
    r = x % 2
    assert r == 0 or r == 1
    left = str(r) + left
    x = x // 2
  if len(left) == 0:
    left = '0'
  if len(right) > 0:
    return left + '.' + right
  else:
    return left

a = 1.25 # 1.01
d = 0.125 # 0.001
if (a + d) ** 2 > 2.0:
  a = a + d
else:
  pass

print(tobin(a), tobin(d))


if __name__ == '__main__':
  import doctest
  doctest.testmod(optionflags=doctest.FAIL_FAST)
