# Using if exists
I do not carefully understand what's goning on in this logic:
```dfy
lemma MinAirCells(g: game, r: rect)
ensures g.numCells(Air, r) >= 2
{
  if exists r': rect :: g.numCells(Air, r') == 0 {
    var start := g.plpos(0);
    assert g.cell(start) == Air by { g.PlayerAlwaysInAir(0); }
    // requires forall r: rect :: numCells(c, r) == 0
    // ensures forall pos: point :: cell(pos) != c
    assert g.cell(start) != Air by {
      assume r.haveInside(start);
      g.NumCellsZero(Air, start, r);
    }

  } if g.numCells(Air, r) == 1 {
    assume false;
  }
}
```
When we can use construction `if exists`?

TODO: Add examples of using `if exists` from github.