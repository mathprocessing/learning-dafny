/*
How to define a grid?
  - Define a function from (x, y) to cell type

How to define a path?
  - ...

How to manage connections between cells? Path - is a chain of connections.

PLayer?
  - define `player_pos(time: nat): Point`

  Time changes from 0 (start game) to some t_end (end of the game)
*/

function abs(x: int): nat { if x < 0 then -x else x }

// Manhattan distance
function dist(p: point, q: point): nat {
  abs(p.x - q.x) + abs(p.y - q.y)
}

// Firstly try to define sokoban without boxes
datatype point = Point(x: int, y: int)

datatype notRestrictedRect = Rect(topleft: point, bottomright: point) {
  predicate haveInside(p: point) {
    this.topleft.x <= p.x &&
    this.topleft.y <= p.y &&
    this.bottomright.x >= p.x &&
    this.bottomright.y >= p.y
  }
}
type rect = r: notRestrictedRect | r.topleft.x <= r.bottomright.x && r.topleft.y <= r.bottomright.y witness Rect(Point(0, 0), Point(0, 0))

datatype celltype = Air | Wall

datatype game = Game {
  function cell(p: point): celltype

  /*
  Problem: using just numCells can "think" only about rectangles, but this not helps to define it recursively
    ****    ***
    **** -> *** -> ...
    ****    *** 

  We must think about rect + some one end row that can be not fully filled with cells:
    **** -> **** -> **** -> ... -> **** -> **** -> ...
    **** -> **** -> **** -> ... -> **** -> ***_ -> ...
    **** -> ***_ -> **__ -> ...
  */
  function numCellsAux(c: celltype, r: rect, columnIndex: nat): nat

  // Number of all cells with fixed type in some box
  // We define r: rect because rect may be infinite and numCells = +inf, but type `nat` can't include `+inf`
  function numCells(c: celltype, r: rect): nat
  // {
  //   if r.topleft == r.bottomright then 0 // Empty rect
  //   else
  // }

  predicate insideRect()

  lemma NumCellsZero(c: celltype, pos: point, r: rect)
  requires numCells(c, r) == 0
  requires r.haveInside(pos)
  ensures cell(pos) != c

  lemma NumCellsOne(c: celltype)
  requires forall r: rect :: numCells(c, r) == 1
  ensures exists pos: point :: cell(pos) == c && (forall pos2: point | pos2 != pos :: cell(pos2) != c)

  function plpos(time: nat): point

  // Player always (at only timestep) placed on the empty cell (air)
  lemma PlayerAlwaysInAir(time: nat)
  ensures this.cell(plpos(time)) == Air

  // Defined difference between two states: state(t) and state(t + 1)
  // What player can do:
  // 1. Move from Air to Air
  // 2. Move from Air to box + move box from Air to Air
  // Note: This can be expressed as law: number of objects in fixed set of cells are constant (law of mass conservation)
  //
  // State includes:
  // * Player position
  // * Box positions
  lemma {:induction false} CorrectTimeStep(t: nat)
  ensures var p := this.plpos(t);
          this.plpos(t + 1) == Point(p.x - 1, p.y) 
       || this.plpos(t + 1) == Point(p.x + 1, p.y)
       || this.plpos(t + 1) == Point(p.x, p.y - 1)
       || this.plpos(t + 1) == Point(p.x, p.y + 1)
  {
    CorrectTimeStep'(t);
  }

  lemma {:axiom} CorrectTimeStep'(t: nat)
  ensures dist(this.plpos(t), this.plpos(t + 1)) == 1
}

method Main()
{
  var g := Game;

  assume g.plpos(0) == Point(0, 0);
  assert g.cell(Point(0, 0)) == Air by { g.PlayerAlwaysInAir(0); }
  assert g.plpos(1) == Point(-1, 0) || g.plpos(1) == Point(1, 0) || g.plpos(1) == Point(0, -1) || g.plpos(1) == Point(0, 1) by {
    g.CorrectTimeStep(0);
  }
}

// If we try to minimize number of Air cells we end up with lemma:
// MinimalAirCells == 2
//
// Player can move just [left, right, left, right, ...] infinitely
// How to express this in Dafny?
// Possible action sequence = [L, R, L, R, ...]
// i.e. 
// f: time -> Action =
//   if even(time) then L else R
// 
// And proof of that is possible (correct):
// if playerAction = f then
//   only two cells can be Air
lemma MinAirCells(g: game, gamerect: rect)
// player always in game board, or we can think about this like
// player can't reach some points outside with dist(playerstart, p) >= timeOfGameEnd
// Note: timeOfGameEnd = PlayerMovesInLevel
requires forall t: nat :: gamerect.haveInside(g.plpos(t)) 
ensures g.numCells(Air, gamerect) >= 2
{
  if g.numCells(Air, gamerect) == 0 {
    var start := g.plpos(0);
    assert g.cell(start) == Air by { g.PlayerAlwaysInAir(0); }
    // requires forall r: rect :: numCells(c, r) == 0
    // ensures forall pos: point :: cell(pos) != c
    assert g.cell(start) != Air by {
      g.NumCellsZero(Air, start, gamerect);
    }

  } if g.numCells(Air, gamerect) == 1 {
    assume false;
  }
}