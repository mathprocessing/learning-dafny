/*
State:
* Robot can be ({with battery in load, empty load}, {battery in manipulator, empty}), i.e. 2 * 2 = 4 states
  i.e. load = 0 | 1
       manip = 0 | 1

Actions:
* Robot can move in 1d space
* Robot can grab/drop battery

Goal: 
  Using batteries around move to maximal distance from stating point
  Find optimal algorithm:
    do: State -> Action
  Find utility function:
    U: State -> distance
    Q: (State, Action) -> distance
*/

// Assume that time is discrete and >= 0
predicate batteryOnGround(pos: int, time: nat)

//-------------------------
// If we model Bot as datatype

// Let's assume that we in discrete space and set type of position in space: int
// datatype Bot = Bot(pos: int, load: bool, manip: bool) {
//   function Move(dx: int): Bot {
//     Bot(this.pos + dx, this.load, this.manip)
//   }

//   function Grab(time: nat): Bot
//   requires !this.manip // Empty manipulator
//   requires batteryOnGround(this.pos, time) // Near Bot exists some battery
//   ensures !batteryOnGround(this.pos, time + 1) // After grab battery don't exists on ground
//   {
//     Bot(this.pos, this.load, true) // Return bot state with filled manipulator
//   }

//   function Drop(time: nat): Bot
//   requires this.manip
//   requires !batteryOnGround(this.pos)
//   ensures batteryOnGround(this.pos)
//   {
//     Bot(this.pos, this.load, false)
//   }

// }

//-------------------------
// If we model Bot using U function

/*
* Problem if we just use U(state) we can't guarantee that this state is reachable.
  Try: Use U(actionseq)
  U(actionseq = [Grab, Drop, Move(dx), ...])

  This two possibilities are equivalent?
    If we can't do some action (for example [Grab, Grab]) then can assume:
    1. if we can't ==> do nothing 
      i.e. [Grab, Grab] ~ [Grab, DoNothing] ~ [Grab]
    2. This just can't be possible, i.e. that state just not reachable i.e.
      [Grab, Grab] have incorrect type.
*/

