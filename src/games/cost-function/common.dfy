function abs(x: int): nat { if x >= 0 then x else -x }

// Можно определить тип Point не стен т.е. с !isWall(p), но тогда можно допустить ошибку в коде и указать не тот тип
// Чтобы избежать этого можно "перевернуть" ограничения на типы:
// В начале определить noWallPoint, и  уже после его отменять по необходимости. Но как это сделать в Dafny?
datatype Point = Point(x: int, y: int)
{
  function add(p: Point): Point
  {
    Point(this.x + p.x, this.y + p.y)
  }

  function sub(p: Point): Point
  {
    Point(this.x - p.x, this.y - p.y)
  }
}

function cost(src: Point, dest: Point): nat

function distance(p1: Point, p2: Point): nat
{
  abs(p2.x - p1.x) + abs(p2.y - p1.y)
}

predicate isWall(p: Point)

// Note: we must prove that middle point is not a wall, but if no walls on the map we just ignore it or use some axiom `forall p :: !isWall(p)`.
lemma {:axiom} CostIntermediate(start: Point, middle: Point, finish: Point)
requires !isWall(middle)
ensures cost(start, finish) <= cost(start, middle) + cost(middle, finish)

lemma {:axiom} CostSymm(start: Point, finish: Point)
ensures cost(start, finish) == cost(finish, start)

lemma CostSelf(p: Point)
ensures cost(p, p) == 0

lemma CostNear(p: Point, near: Point)
requires distance(p, near) == 1
ensures cost(p, near) == 1