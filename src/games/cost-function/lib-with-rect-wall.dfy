/*
Cost function in infinite 2d discrete space.

With one rectangle as wall.

*/

include "./common.dfy"

const rectTopLeft: Point
const rectBottomRight: Point

// Now one wall exist on the map, but only one rectanhle wall
lemma {:axiom} rectangleWall(p: Point)
ensures rectTopLeft.x <= p.x <= rectBottomRight.x && rectTopLeft.y <= p.y <= rectBottomRight.y ==> isWall(p)
