/*
Cost function in infinite 2d discrete space.

No walls on map.

*/

include "./common.dfy"

// Represent simple fact: no walls exists on 2d map
lemma {:axiom} noWalls(p: Point)
ensures !isWall(p)

// A* heuristic - Manhattan distance
lemma CostDistance(start: Point, finish: Point)
ensures cost(start, finish) >= distance(start, finish)

lemma ExampleDistTwo(start: Point, finish: Point)
requires distance(start, finish) == 2
{
  if start.x == finish.x && start.y < finish.y {
    /*
    . S .
    . m .
    . F .
    */
    assert cost(start, finish) >= 2 by { CostDistance(start, finish); }
    var middle := Point(start.x, start.y + 1);
    assert cost(start, finish) == 2 by {
      assert cost(start, middle) == 1 by { CostNear(start, middle); }
      assert cost(middle, finish) == 1 by { CostNear(middle, finish); }
      noWalls(middle);
      CostIntermediate(start, middle, finish);
    }
  }
}

lemma GetMiddlePoint(start: Point, finish: Point) returns (p: Point)
requires distance(start, finish) == 2
ensures distance(start, p) == distance(p, finish) == 1
{
  if start.x == finish.x || start.y == finish.y {
    // In case: S m F - Just average point
    p := Point((start.x + finish.x) / 2, (start.y + finish.y) / 2);
  } else {
    // In diagonal case:
    // 1) S m  2) S
    //      F     m F
    // We have two possibilities to select middle point, let's choose gorizontal one.
    if finish.x > start.x {
      // We only add or subtract one from `start.x` because we choose gorizontal position of `p` relative to `start`.
      p := Point(start.x + 1, start.y);
    } else {
      p := Point(start.x - 1, start.y);
    }
  }
}

lemma ExampleDistTwoMiddleNotExplicit(start: Point, finish: Point)
requires distance(start, finish) == 2
ensures cost(start, finish) == 2
{
  assert cost(start, finish) >= 2 by { CostDistance(start, finish); }
  // assert exists p :: distance(start, p) == distance(p, finish) == 1 by {
  // }
  // Error: cannot establish the existence of LHS values that satisfy the such-that predicate
  // var middle :| distance(start, middle) == distance(middle, finish) == 1;
  // We can use construction: "lemma + return value"
  var middle := GetMiddlePoint(start, finish);
  assert cost(start, finish) == 2 by {
    assert cost(start, middle) == 1 by { CostNear(start, middle); }
    assert cost(middle, finish) == 1 by { CostNear(middle, finish); }
    noWalls(middle);
    CostIntermediate(start, middle, finish);
  }
}