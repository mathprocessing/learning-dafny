19.08.2021

Здравствуйте, Павел! 
...
Мир не изменяется, он всегда одинаково сложен, изменяется лишь наше представление о нём.


Возможности изучения:
1. Системы авто-докательства теорем (Lean mathlib, COQ, ...)
2. Глубинное понимание теории групп, доказательств
3. Разные задачки
   * Проблема дивана - максимизация площади фигуры, перемещаемой по фиксированной формы коридору
     <https://en.wikipedia.org/wiki/Moving_sofa_problem>
   * Алгоритм построения радиационной карты местности (интеллектуальный агент - движущийся робот, оснащенный счетчиком гейгера)
   * "А все таки вдруг количество кучек существует?": Построить функцию, которая возвращет количество кучек для данного * * множества точек на плоскости (применив только чистую теорию вероятности, т.е. найти наипростейшее решение)
   * Найти полное решение игры "Выключи свет" для всех начальных состояний (для 3x3, 7x7, ... (4*n-1)x(4*n-1) решения однозначны)
      + Правила игры: Имеется квадратное поле, на котором включено часть лампочек, лампочка - это бит вкл./выкл, найти накратчайшую послдовательность действий позволяющую выключить все лампочки если вы можете применять операцию xor только одновременно к 5 лампочкам, в форме крестика:
       o
      ooo
       o
   * Взглянуть на RTS стратегии используя чисто переборный теоретико-игровой подход (найти оптимальное действие для каждого состояния, без применения алгоритмов машиного обучения)
   * Концепция оптимальной битвы для дискретных игр
     + Задача вычислить битву как переменную в уравнении, т.е. последовательность состояний (битва это как партия в шахматах) (сейчас есть возможность используя например Stockfish вычислить только в одну сторону)
     + Обратный анализ: Как оптимально прокрутить время вспять в шахматной партии
     
4. Заготовки теорий
   * Реляционная модель лямбда исчисления (возможность решать уравнения с лямбда-термами)
   * Blurred learning - Концепция алгоритмов машинного обучения, главным образом основанная на идее: Посмотри на окружащую среду как на размытую картинку множества действий в порядке уменьшения размытия, чтобы не "закопаться в комбинациях" (обойти комбинаторный взрыв)
   * Рефлексивная математика - концепция мышления со следующими свойствами
     + Ключевая идея: РМ должна описывать такие объекты, которые позволяют вычислять РМ, т.е. изучает объекты, которые содержат полную или частичную информацию о том как вычислять объекты того же типа.
     Суть рефлексивности - это способность объекта вычислить части самого себя.
     Объект может считаться рефлексивным даже если содержит неполную информацию о методе вычисления самого себя или своей части, и даже если вероятность успешного применения метода, стратегии вычисления мала.
     Видимо тут нужен какой-то порог вероятности, чтобы отделить слабо-рефлексивные объекты от сильно-рефлексивных.
     + Все окружающие объекты для которых есть отображение в некоторую модель являются вычислительными, иначе потенциально вычислительными
     + На каждый процесс вычесления можно смотреть как на путь в графе состояний (применять популярный алгоритм поиска пути на графах A* например)
     + Процесс вычисления есть процесс доказательства и наоборот (вычисление как рассмотрение ветвей: Пусть переменная типа T имеет значение (подмножество значений) Value, тогда ...)
     + 
     
    * Теория ограничений - отражает абстрактный взгляд на перебор вариантов, главным образом направлена на автоматизацию поиска решений функциональных уравнений, решение задач поиска, оптимизации
      + Ключевая идея: Любой объект есть пара: {граница, ядро}
      + две переменные могут ограничивать друга друга уменьшая количество возможных пар (кортежей в общем случае).
      + Свойства ограничений тоже переменные, ограничения на их свойства могут описывать другие ограничения второго уровня
      + Не простейший случай: два объекта ограничены третьим, который описывает то как именно они ограничены, но, в свою очередь, можно взглянуть на эти 3 объекта целиком как на кортеж, который тоже принимает далеко не всевозможные значения - значит существует 4-й объект, который ограничивает эти 3, но можно взглянуть на эти 4 объекта целиком значит есть 5-й объект и т.д.
        Выходит множество объектов бесконечно, даже в случае когда рассматривается конечное кол-во переменных (объектов 1-го уровня)
      + Простейший случай: ограничение настолько сильное, что переменная становится константой
      + Существуют уровни логики: 
        1. уровень - перебор переменных
        2. уровень - перебор функций, методов, алгоритмов (если найдены способствуют более быстрому перебору переменных)
        3. уровень - перебор объетов, которые способствуют эффективному перебору объектов 2 уровня
        ...
        N. уровень - видимо перебор объетов, которые способствуют эффективному перебору объектов N-1 уровня
        NOTE: один и тот объект может относится к нескольким уровням (к 1 и 2 например), Пример: решение задачи "Выключи свет"
      + NOTE: есть возможность присвоить другие определения уровням (это действительно трудно предугадать)
        * уровень 1 - это группа, либо множество с несколькими не только бинарными операциями
        * уровень 2 - это произведение групп
        * уровень 3 - не знаю :)
      + Теорема есть пара (ограничение B, отображение f) которое "распыляет" (используя ограничение уровня 2) его на некотором множестве (типе данных), используя конструктор этого типа данных
      
     * Теория паззлов - детализирует теорию ограничений на примере головоломок
       + Граница всегда состоит из непохожих друг на друга частей
       + Оптимальный указатель перебора: указывает на то свойство, которое необходимо перебирать в первую очередь для минимизации затрат энергии на поиск решения
       + Декартово произведение - ключевая операция, позволяющая размещать переменные "рядом"
       + Пример: Утверждение 1: натуральные числа A и B одинаковой четности.
          Ограничение(формула, теорема, гипотеза) - Утверждение 1
          Граница - свойство четности.
          Тело A (Тип переменной) - Множество натуральных чисел
          Тело B (Тип переменной) - Множество натуральных чисел
          


О новопридуманных "операторах"(оператор в данном значении это, например, некоторый черный ящик, который "ограничивает" пространство переменныхб данных вокруг себя):
* Пространство должно схлопываться (падает кол-во сущностей, очень много вещей можно объяснить одним словом и не потерять сути) если теория применяется успешно, если не схлопывается значит есть повод задуматься верно ли формализованы/определены начала теории
* Пример схлопывания:
  + До применения операторов:
    Используем стандартные тестирующе фрейворки (Jest, mocha и т.д.)
    Они работают в одну сторону - т.е. это просто код который можно запустить, но нельзя распарсить как теоремы, лямбда-термы
  + После
    Далее можно применить оператор FunToRel - который все one way function преобразует в two way function
    и у нас должна возникнуть возможность вычислять в обратную сторону.
    НО это не обязательно мы просто может решать это как уравнение в любую сторону с любой стартовой точки
    Стартовая точка есть "Оптимальный указатель перебора" см. выше определение
    На практике указатель не обязательно будет строго оптимальным, но суть вычисления вторичных (уровня 2) вычислительных объектов в том и заключается, что они могут помочь сделать указатель оптимальным, т.е. ради этого мы их и вычисляем!
  * Еще пример "оператора" MakeQuantum:
    Пусть имеются правила некоторой настольной игры Game, тогда `MakeQuantum(Game)` это её квантовая версия.
    Стоп, скажете вы, MakeQuantum это же обычная функция(алгоритм) зачем для этого придумывать новое слово "оператор"?
    Верно, отвечу я, но что если такая функция будет неопределена "до последнего", но тем не менее она должна запускаться?
    Кроме того её не всегда легко продолжить на другие значения аргумента (задача машинного обучения).
    Вообщем можно слово "оператор" заменить на модель машинного обучения, с той оговоркой, что можно вместо машинного обучения использовать системы доказательства теорем. Т.е. оператор - это модель какого-либо отношения между объектами + она может работать даже когда вычислена частично, в отличие от стандартных приемов
    машинного обучения.
    Почему мне (интуитивно) хочется выделить это в отдельно понятие, видимо у понятия "оператор" есть еще несколько свойств, которые не описаны.
    Видимо чтобы их пролить на них свет некоторое множество конкретных, специфических задач.
    

Варианты ответов на вопрос "Почему так трудно вычислить объекты 2 уровня?":
## Оптимистичные ответы:
  * Возможно мы не привыкли к этому нужно просто попрововать :)
  * 
## Реалистичные

    
    
# О формате для науки
Нецензурированная мысль:
Универсальный формат для сохранения исследований либо незакончнных исследовании либо идеи исследовании либо варианты теорем, гипотез вычисленные человеком или алгоритмом не важно это типа как jpeg, только для логики?

Математика - это уже ФОРМАТ! (а также и язык выражения мыслей) Но без отрицательных ошибочных ветвей. (мысли о мыслях или мысли о том как могли рождаться мысли, каков путь их становления на рельсы логики)
отрицательные ошибочные ветви нужны потому что:
  * Рассмотрим сюжет где исследователь А отправил состояние своей работы исследователю Б.
  * Если в состоянии не будет указаны ошибочные ветви (неверные способы проведения доказательств, или просто уже рассмотренные идеи, которые точно не стоит рассматривать 2 раза) исследователь Б может работать не оптимально, просто начнет повторяться
    Важно: исследователями как А так и Б может быть программа, которая затратить лишнее количество ресурсов, либо вообще не закончит работу, так отсутствуют предыдущий отрицательный опыт (только положительного недостаточно)
    * Есть еще более очевидный шуточный пример:
      Один сумасшедший интернет-провайдер решил что не будет лгать своим клиентам и перестал передавать нули, теперь по кабелю идут только единицы - истина в чистом виде
  

## Математику можно представить как зерна и паутинку
Увидели мы интересное уравнение, но сами догадаться до него не смогли, слишком сложно (например Уравнение Навье-Стокса)
Какие-то простые факты об уравнении получается выводить самостоятельно (окружающую зерно паутинку)
Далее паутинка перестает расти, видимо нехватает верных фактов (теорем, лемм) или почти верных (гипотез, аналогий, отрицательного опыта использования аналогий и т.д.) для дальнейшего продвижения. Возникает необходимость в следующем зерне и т.д.

Предположим что такой формат уже существует. Как им пользоваться?
* Пусть внутри будет какой-то граф + информация дополняющая его
* Варианты определения в порядке убывания логичности: "Вершина - это "
  + состояние исследования
  + понятие
  + теорема
  + другое ребро, может другого графа или даже того же самого (странно как то всё это)
* Варианты определения: "Ребро - это "
  + лемма
  + связь, relation
  + действие
    * Предположение (обычный ход логики "вперед по времени")
    * Обратный анализ (что-то очень захватывающее и впечатляющее! Но тоже состоящее из предположений, но почему оно должно быть выделено в отдельную группу? Может этого вообще не должно быть? Такое может быть!)
      Может оно должно отвечать на вопрос:
        * Как система развивалась до того как она должна выдать оптимальный результат?
        * Главная идея уважение к истокам, к первым некрасивым попыткам создания чего-либо, историческое восстановление прошлого, расследование, каким мог быть первый коммит Л. Эйлера? Если он формулировал/доказывал теоремы?
        - Исходя из "Главной идеи" нужно сначала изучить труды эйлера и как-то их оцифровать, используя Coq, Lean4, самопиные утилиты
        - После подумать что является движущим фактором вообще любом математическом (и не только) исследовании.
          Варианты:
          * Умение сжимать ифнормацию (из данных получать леммы, правила)
          * Умение группировать информацию по сходным свйоствам (помогать уменьшить время обработки данных)
          * ...
* Варианты функциональных свойств графа:
  + вершина как тип, ребро как уравнение
  + ребро может быть и ребром и вершиной одновременно (иметь отображение в вершину)
  
Уровни обобщенности:
Самый первый уровень медленно обрабатывается машиной и человеком (что-то похожее связи между понятиями, аналогии как функции частично определенным типом данных)
Следующие уровни каждый все легче и легче обрабатывается машиной и медленней человеком. (четкие формальные факты)


Идея рожденная в следствии анализа процедуры стихосложения:
Мы можем сочетать функции (операторы) находящиеся в разных группах f и g (максимально удаленных по смыслу или по-какой-либо метрике) 
  для того чтобы найти новую группу функций, которая включает не только f и g но и другие интересные функции, 
  необыкновеность которых пропорциональна смысловому расстоянию между группами F и G.

Часто рождает морфизм между структурами, которые имеют разное клочество элементов, вершин.
Пример: в строке три слова, но смысловых понятий 5. Или наоборот смысловых единиц меньше чем слов, но они снабжены замысловатыми связями,
  настолько насколько их необходимо добавить для достижения оптимальности решения.
Приведенный выше пример можно упростить до фразы: "Не воспринимай слова буквально"
А можно пройти путь обратно от упрощенной фразы до развернутой теоретической единицы. (Потенциальной части какой-либо теории)
Этот путь обратно может напоминать операцию разархивирования, (чем же?)тем что количество символов формально увеличивается.

Когда алгоритм (код) выдает exception, то это аналогично тому когда человек встречает нелогичное сочетание слов,
человеку в таком случае приходится расширять группу операторов, пока данное словосочетание не покажется логичным,
можно предпоожить что интепретатор кода (например python) в таком случае можно также расширять множество свои действий:
* Доставать из базы дополонительные теоремы, чтобы оценить состояние (перевести оценку из неопределенной в более однозначную, это может быть итеративным процессом)
* Вычислять объекты, которые могут помочь осуществить оценку состояния

Т.е. самое главное:
1. Осуществить оценку состояния
2. Вычислить множество возможных действий (в дискретных играх типа шахмат, его вычислять не нужно, оно уже дано в правилах игры, наша же ситуация аналогична игре в которой правила "размыты")
 + Сформировать наиболее полезное множество мутирующих функций (симметрии, изоморфизмы, индуктивные конструкторы функций)

Что такое "размытые" правила?
Возможные взамоисключающие друг друга ответы:
* интервальная оценка для одномерного пространства: (min <= x <= max), для двумерного: система неравеств, описывающая фигуру на плоскости (или двумерно многообразии), для N-мерного: ...
* транзитивный граф, где вершиной является утверждение, позволяющее выразить неопределенность значения переменной. Утверждения отсортированы в порядке важности, линейный отсортированный список обобщён до транзитивного графа (см. Топологическая сортировка).



Алгебра указателей:
Копирование - это когда два указателя указывают на один объект
Может быть существует некоторая алгебра указателей, с помощью которой можно выразить понятия родственные копированию?
инверсии: 0100 -> 1011
отражения: 0100 -> 0010

Пример который рассматривался для вывода гипотезы:
Стоит ли использовать разные решения для передних и задних фар автомобиля?
1. Если не доказано что форма влияет на характеристики автомобиля, то лучше использовать один и тот де "код" для всех фар.
2. Если есть контрпример, того чтобы использование одинакового "кода" менее оптимально чем разного, то пытаемся найти разного рода симметрий: 
  * использовать часть кода (код задних фар совпадает с передними лишь частично)
  * использовать произвольные операторы отображения решений
  
  
  
Что такое спираль?
Понятия мможно задавать с помощью конструкторов, но с какого конструктора начать неизвестно.

Начиная рассуждение с одного конструктора можно выйти на другой "с другой стороны"(чего? может быть "алгоритмического простанства").

Случайно найденный конструктор (модель) для какого либо набора примеров данных почти всегда является уникальным, а значит и полезным для изучения.
Потому что по сути он является, множеством предполагаемых теорем, который может найти долго изучая объект.

К примеру рассморим идею спирали:
1. Базовый конструктор это просто параметрическое уравнение спирали.
2. Но выход на другие свойства может быть найден неожиданно, даже если мы не знаем о базовом конструкторе (это фантастическое свойство конструкторов).
  09.11.2022: Т.е. точка входа на модель какого-либо объекта, понятия может быть разной, а это уже вполне очевидно: мы же мыслим по-разному.
3. Например некоторое функциональное уравнение Func + множеств свойств S может являться альтернативным конструктором спирали.
Тогда Func может задавать конструируемый объект неоднозначно, например это лиюо окружность либо многоугольник либо спираль, но добавить множество аксиом S, то останется только один объект - спираль.

Можно определить операцию поиска всевозможных констукторов для данной идеи.
Тут важно подчеркнуть что ко всем конструкторам мы (или эта операция) должны относиться на равных: т.е. принципиальноо не может существовать "любимого" констуктора.
Среди фанстастически огромноо многообразия конструкторов необходимо выделить способ как их упорядочить, хотя бы разбить на группы, возможно стоит использовать Колмогороскую сложность + размытие оценки сложности для того чтобы сложность не была однозначнымы числом, так как это может привести к неверному порядку если будет фиксирован язык (программирования), который исползуется для непосредственного вычисления значения сложности модели.
Когода мы выбираем конкретный интерпретатор (подмножество языка, язык программирования), то по сути уже создаем "любимчиков" среди возможных констукторов, что крайне опасно для понимания происходящего. 
Поэтому сформируем варианты обхода этого нежелательного паттерна поведения:
* Интерспретатор + модель в равной степени неизвестные для нас.
* Нельзя использовать один, а если использовать например N, но максимально различных между собой интерпретатора? Тогда у нас будет несколько оценок сложности и если все приблизительно совпадают значит сложность для данной модели можно вычислить довольно точно, но если они слишком различаются, то данную модель нельзя сравнивать с другими по критерию сложности (или сравнение не несет дополнительной полезной информации).
* Странная идея: модель может являться "осколком" интерпретатора (обратим внимание что интерпретатором может креточный автомат), поэтому наш взор направлен не туда нужно просто вычислить/изучить/натравить друг на друга сами интерпретаторы, а результат этого процесса будет постепенно добывать новую информацию, о том как работать с моделями (сжимать модели)

Вернемся к примеру спирали.
Какими важными свойствами должен обладать "правильный" метод машинного обучения?
Должен с приблизительно (не в 10^100 раз различной) равной вероятностью находить или базовый констуктор, или альтернативные.
Или можно убрать вероятность:
* Пусть имеется параметр alpha который как-нибудь влияет на факт какой именно конструктор будет найден, тогда есть функция f(alpha) которая возвращает подмножество конструкторов.
* Иначе говоряЖ кроме обучающей и тестирующей выборки (что преставляет собой некую разовидность "элементарных" конструкторов) мы должны иметь "локальные теории" (частично-собранные "паззлы", но не все подходят друг к другу) т.е. по сути это есть то самое подмножество конструкторов.

Локальные теории - это подмножества конструкторов, которые могут противоречить друг другу если их не "ослабить".

Операция "ослабление" - то же самое что "ослабить утверждение теоремы X" т.е. более сильная теорема применима в большем количестве случаев.
Или так: олее сильная теорема требует меньше предусловий перед её применением.

* "ослабление" может принимать параметр - на сколько нужно ослабить.
* либо сама теорема может иметь несколько версий ослабления
  -> если это обобщить и перейти к множеству теорем T, тогда
  T будет иметь свой собственный оператор "ослабления", который можно применять к каждой теореме из множества T.
  
Пример логического ветвления и оценки сложности решений на практике:
https://github.com/dart-lang/language/blob/master/working/0125-static-immutability/feature-specification.md

## Alternative syntax 1
Instead of adding constraints, a simpler approach is to add a marker interface Immutable. The property expressed by the constraint T is immutable then becomes expressed by implements Immutable in the case of a class, or T extends Immutable in the case of a type variable T.

## Alternative syntax 2
Instead of adding general constraints, we could expose a dedicated syntax. For example, this proposal from @yjbanov.
```
data Value<data T> extends Scalar<T> {
}

foo<S, data T>(Value<T> v) {
}
```

Как видим из этого примера человеку свойственно использовать сравнения по сложности, используя принцип "проще значит лучше". А также видим насколько важно рассматривать/фиксировать ветви, делать этот процесс наглядным.

Пример самоприменимости:
```dart
class Deprecated {
  /// Message provided to the user when they use the deprecated feature.
  ///
  /// The message should explain how to migrate away from the feature if an
  /// alternative is available, and when the deprecated feature is expected to be
  /// removed.
  final String message;

  /// Create a deprecation annotation which specifies the migration path and
  /// expiration of the annotated feature.
  ///
  /// The [message] argument should be readable by programmers, and should state
  /// an alternative feature (if available) as well as when an annotated feature
  /// is expected to be removed.
  const Deprecated(this.message);

  @Deprecated('Use `message` instead. Will be removed in Dart 3.0.0')
  String get expires => message;

  String toString() => "Deprecated feature: $message";
}
```

Декоратор @Deprecated используется внутри опредления самого себя.


## О минимизации доказательств

Почему процессы происходящие внутри ЭВМ (скорее правильно: типичные для ЭВМ) нам людям кажутся "машинными" чуждыми для восприятия?
- Не потому что использует двоичный код.
- Не потому что быстро может считать.
- Не потому что нужно программировать. (машине нужно досконально и точно давать инструкции)
+ Потому, что мы можем себе позволить такую роскошь как не минимизировать доказательства.
  * за счёт большого кол-ва элементарных операций в секунду.
  * Большая вычислительная скорость не требует точных фактов, в большинстве практических ситуаций достаточно протестировать простые случаи.
  * Большое кол-во памяти позволяет расточительно её использовать (это крайне отрицательно влияет на опыт использования ПО).

Достаточно ли для вычислительной системы правила "минимизировать всё что видишь"?
- Могут существовать объекты, сложность которых необходимо увеличивать для того чтобы достигнуть некоторого барьера, но одновремено с этим, вероятно, потребуется по мере возможности минимизировать результаты применения этих объектов.

## Оператор перехода "наверх"
Переход от 
- переменной
- объекта с фиксированным типом
- состояния (декартово произведение типов), которое можно разрешивать
к разрешиванию докательств о потенциальных свойствах этого объекта назовём переходом на уровень выше: +1 к уровню абстракции/логики.

Достаточно ли оператора "наверх" для вывода идеи "Минимизации доказательва" при уже имеющейся идее минимизации переменной, числа, вектора, матрицы, тензора, функции, отношения, моноида, группы, категории, топологического пространства?

## Соотвествие между топологическим пространством и компонентами транзитивного графа или "Как примудать идею предшествующую данной" или "backwards analysis на примере топологии"
Односторонняя конпонента графа - односторонняя область достижимости (по аналогии с двусвязной компонетой графа)

Соображение: Идея транзитивного графа может помочь "придумать" идею топологического пространства, его свойств, базу и т.д.
Какой ещё способ поможет "придумать" идею (определение) топологического пространства? С чего начать наиболее оптимально?
- Транзитивный граф (рассмотрен) (так как в этой логической ветви уже использовано понятие тр. графа, то следующая ветвь не должна содержать граф как доминирующее понятие)
- Обозначим идею буквой X (Данная ветвь не должна использовать понятие транзитивного графа.)
- ...


Теория связей:
Strong link - 
Weak link -

1. Мы не можем знать состояния объектов пока не присвоим им числа (метки).
   В более общем случае мы должны найти биективное отображение между моделью и реальным состоянием. Например реальными сутками и словами - названиями дней недели.
   Более сложный пример:
   Улица с домами и отсортированный список чисел (номеров домов) в памяти компьтера.
   Вообще есть все множество способов как строго связать состояние реального или виртуального объекта с ячейкой памяти компьютера, и это тоже есть изоморфная связь, если соответствие однозначное, двустороннее f o inv f = id т.е. связь являющееся биекцией.
2. Мы можем знать лишь разницу (difference of states).
  Пример с двумя объектами, связанными связью Strong link:
  Пусть есть связь Xor (A, B)=const тогда если мы ассоциировать сосотояние A с числом 0, то не дает нам иформации о состоянии B. Но вот если мы знаем что состояние A изменилось, то мы не можем обозначить это нулем - только единицей. Соответственно из факта наличия связи и факта изменения состояния A (связь Xor(A(t+1), A(t)) = 1) мы можем сделать вывод что состояние B изменилось:
  сущ. связь = 1 AND изм. A = 1 =>
    xor(изм. A = 1, изм. B = 1) = 0
    => изм. B = 1



Мы можем вводить естественные преобразования(морфизмы, отноошения)
например естествеено из конструктора (succ) натуральных чисел получить
отношение (less_or_equal)

Важно обобщить идею если переменная иммет тип натуральное число и на неё наложено ограничение к примеру x < 2, то она эквиваленстна булевому типу + некоторые естественные изоморфизмы:
обозначим число 0 как x0 если x принимает значение 0
Пусть есть другое число y с аналогичным ограничением:
тогда существует естественный морфизм:
x0 <-> y0
x1 <-> y1
Вожно понимать что если бы x не был натуральным: x in {A, B}, y in {C,D}
, то символ A никак не указывал бы на символ C, в то время как x0 указывает на y0 потому что и там и там есть символ нуля.






What-if system
Свойства:
* Ранняя рефлексивность
  Доступна рефлексивность на раннем этапе разработки, ну или достаточно малая часть системы обладает рефлексивностью

* 

Примеры работы пользователя:
Часто встречается:
Дать определение объекта, пусть  объект G - это группа
Группа - это ... множество аксиом ...
Аксиома - это правило ...

1. Оценка времени работы запланированного вычисления (Обобщая: оценка сверху затрат ресурсов на вычисления)
Дается определение объекта, который необходимо вычислить:
Существует объект F, который обладает свойством S
Определяется текущее состояние вычисления: START_OF_CALCULATION
Цель вычисления: FINISH_OF_CALCULATION
И сам процесс вычисления как ребро графа, соединяющее две эти вершины.
Далее --> система может оценить время работы процесса
И если оценка меньше PROCESS.MAX_TIMEOUT = 1 секунда, то процесс становится доступен для запуска, так как доказано, что он достигнет результата за небольшое (конечное) время

Тем самым достигается высокая определённость в вычислениях(какие-то ветви за счет оценки времени помечаются как неоптимальные и на них расходуется меньше ресурсов), и контроль за вычислениями.

Note: Оценка это тоже выч. процесс, время оценки также может быть оценено с помощью оценочного процесса-2 и так до бесконечности, поэтому сначала необходимо доказать что эта цепочка не бесконечна.

Список возможностей:
1. Даем определением
2. Указываем цель A
3. Получаем мусорные объект + результат
4. Может быть посталена цель улучшить мусорные объекты
  мусорные объекты в свою очередь могут являться объектами более высокого уровня, поэтому можно поставить отдельную цель B их поиска, либо использовать рефлексивность: предположить что B эквиваленстно A.


Тень процесса: Если все действия равновероятны, то множество всевозможных комбинаций образует "тень".
Пример: Дерево вариантов игры клестики-нолики

Факторизация состояния: представление состояние как класс, тогда сущ-ют поля класса - они есть части состояния т.е. результат факторизации состояния

Пример: Множество твердых тел в механике
Вся информация о положении тел в простаранстве - целое состояние
Положение i-го тела есть свойство целого состояния или иначе формулируя - результат факторизации целого состояния.
Note: Где гарантия что нужно систему разбивать именно на N тел, может лучше разбить её на две части, например head, tail или на бесконечное как непрерывный отрезок?

Непрерывный отрезок:
Отсечем часть отрезка получим тоже отрезок, значит определение непрерывного отрезка:
T + T = T
1. Но еще можно учесть что отрезок кончается отдельно от идеи T + T = T.
2. Но еще можно учесть что отрезок кончается вместе с идеей T + T = T.
3. Указать правило что необходимо разделять эти две идеи (важна сама возможность этого, чем этот пример).

Если подумать рефлексивно над тем что выше (Множество твердых тел в механике, упоминание непрерывности отрезка, развитие темы о непрерывном отрезке)
, то можно внедрить понятие CHAIN - цепочка связанных аналогий
A -> B, B -> C, C -> D, ...

инопланетяне называют наверное землю чикабарахта

# о последовательности применений оператора "Increase logic level"
state, actions -> 1-logic state, 1-logic actions -> 2-logic state, 2-logic actions -> ...
Упрощенно: действия над облаками (размытыми объектами) суть аналогии между реальными объектами

Более формально:
Непосредтвенные действия с логическим состоянием n+1-logic state (в системе отсчета n+1-logic) выглядят как аналогии между n-states в системе отсчета n-logic

# Идеология soft proof
Запрещать в данном состоянии использовать метод X для того, чтобы доказать что он важен для данного состояния или множества состояний если есть возможность наследовать информацию в соседние состояния

Рассуждение на уровень выше: Можно пытаться доказать что нельзя расширишь множество соседних состояний больше данного

Прослеживается связь с:
Областью устойчивостью решений задач линейного программирования.
Когда мы находим решение ЗЛП симплекс-методом, то есть возможность найти область изменения коэффициентов неравенств для того, чтобы понять насколько это решение устойчиво к изменению коэффициентов. (Стоит ли пересчивать всё решение если коэффициенты изменили так-то?)


# Теория ограничений
теория дает связь между задачами, которую трудно обнаружить без теории
Полезность теории = разнице множества связей между задачами (мощность разницы) между двумя состояниями: 1. Используем теорию 2. Игнорируем теорию

Конкретный пример:
Можно подумать о минимизации суммарной энергии (интеграла энергии по промежутку времени) затраченной на решение фиксированной задачи Task.
Хороший пример задача о бросании яиц с многоэтажки (100 этажей).
  В этой задаче демострируется как можно оптимально работать с неопределенностью в данных (для стратегии неизвестна хрупкость яйца, но известен интервал значений этой хрупкости hmin..hmax или по транляционной симметрии можно hmin приравнять к нулю 0..delta)
  Цель для алгоритма(что будет когда стратегия завершит свое исполнение): Узнать прочность яйца
  Цель для пользователя (человека, решающего задачу): Найти стратегию (последовательность действий минимизирующих затраты энергии (число бросков)) бросания.
    Пример стратегии когда количество яиц равно одному:
      if eggs == 1 then action_sequence = [1, 2, 3, ..., 99, 100]
        ENERGY = number_of_throws = len(action_sequence) = 100
    Пример стратегии когда количество яиц равно двум:
      if eggs == 2 then action_sequence = [9, 22, 34, 45, 55, 64, 72, 79, 85, 90, 94, 97, 99, 100] =
        = list(reversed([100 - i*(i+1) // 2 for i in range(14)]))
        ENERGY = 14
    Note: эта стратегия "одна из", например можете получить еще если заменить первый элемент в списке 9 на любое число от 10 до 14.
      if eggs == +inf then action_sequence = [32, 48, ] =

# О "математическом" компьютере (как математический маятник - такой же идеальный и нереальный)
Вот бы создать компьютер, который мог бы мысли кратчайшим путем.
Ну хотя бы сделать так, чтобы с каждой итерацией путь его рассуждений мог сокращаться.

Это контрастирует с сегодняшним ПО:
* Программа исполняет всегда по "проторенной дорожке" и никогда не изменяет поведения, так как (если это внедрить в интерпретатор кода) это может привести к ухудшению производительности, не контролируемым тратам ресурсов памяти, возможным зацикливаниям и др.
* Есть направление: дизайн алгоритмов машинного обучения. Но хоть это и работает, такие алгоритмы не дают (или очень сложно дать такие гарантии, невероятно сложно) гарантии, что в конце концов задача T будет решаться кратчайшим путем.