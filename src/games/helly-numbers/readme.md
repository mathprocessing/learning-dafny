Helly numbers of algebraic subsets of R^d and an extension of Doignon's theorem
https://escholarship.org/uc/item/78d6r5wb

## Let's begin with simple examples:
https://www.youtube.com/watch?v=ytwnrs6pj-4
Let s1 and s2 be the any 2 convex sets.
Then we will show that I = intersection(s1, s2) is also a convex set.

Proof:
Let x, y is any two points from set I.
=> x, y in s1 and x, y in s2
Without loss of generality let's work only with s1:
if s1 is convex then exist stright line between x, y
if s2 is convex then also exist same stright line between x, y
L1 inter L2 = L => for x, y in I, exist line L => Q.E.D.