/**
Our model includes only economics from rts game Dune

Harverster can move to spice field from Refinery

Buildings:
* Refinery
* Windtrap

Units:
* Harvester

Actions:
* Build buiding(b)
* Build Unit(u)
* Move unit to position(p)

Cost:
Windtrap - 300 $
Refinery - 400 $
Harvester - 300 $

Rules:
* First harvester in refinery is free.
* Units move to neighbour cell at timedelay = 1 / Unit.speed

*/

type time = nat

datatype point = Point(x: int, y: int)

datatype Building = Windtrap | Refinery
{
  const cost: int
  const pos: point // position of building can't change over time
}

datatype Unit = Harvester
{
  const cost: int // unit cost can't change over time
  const pos: time -> point
}

lemma Costs()
ensures Windtrap.cost == 300
ensures Refinery.cost == 400
ensures Harvester.cost == 300

datatype Object = B(b: Building) | U(u: Unit)

// Action of player
datatype Action = NewBuilding(Building) | NewUnit(Unit) | None

datatype game = Game
{
  const buildings: time -> set<Building>
  const units: time -> set<Unit>
  const action: time -> Action
}

lemma GameInit(g: game)
ensures g.units(0) == {}
ensures g.buildings(0) == {}

lemma BuildStepNone(g: game, t: time)
requires g.action(t) == None
ensures g.units(t) == g.units(t+1) && g.buildings(t) == g.buildings(t+1)

lemma BuildStepNewBuilding(g: game, t: time, b: Building)
requires g.action(t) == NewBuilding(b)
ensures g.units(t) == g.units(t+1) && g.buildings(t+1) - g.buildings(t) == {b}

lemma BuildStepNewUnit(g: game, t: time, u: Unit)
requires g.action(t) == NewUnit(u)
ensures g.units(t+1) - g.units(t) == {u} && g.buildings(t) == g.buildings(t+1)
