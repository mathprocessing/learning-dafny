include "lib.dfy"

method test(g: game) {
  Costs();
  assert Harvester.cost + Windtrap.cost + Refinery.cost == 1000;
  GameInit(g);

  if g.action(0) == None {
    assert |g.units(1)| == 0 by {
      BuildStepNone(g, 0);
    }
  } else if g.action(0) == NewBuilding(Windtrap) {
    assert |g.buildings(1)| == 1 by {
      assert g.buildings(0) == {};
      assert g.buildings(1) == {Windtrap} by { BuildStepNewBuilding(g, 0, Windtrap); }
    }
  }
}