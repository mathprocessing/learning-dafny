# Description
```
Betty:

Have you noticed how popular these "black-knot" toys are? Every store
I've been to while shopping for Holidays is sold out of them, and kids
are basically crying in the streets. Parents are in a panic.
Unfortunately the manufacturing process uses a technology called "Open
Terms," which is patented, so nobody can make competing replacements.

Well, I think I just figured out a way to build these toys using a
different process. Let me run this by you. First, here's a
mathematical formulation of what black-knots do:

  A black-knot has n inputs (numbered 0 to n-1) and n outputs.
  (These are the little holes that kids drop marbles into.) The
  black-knot is a function from each of its n inputs to a pair of
  integers (j, k) where 0 <= j < n and 0 <= k. The number j is the
  output hole that the marble comes out of. The number k is a number
  of "plink" sounds that are produced as the marble rolls unseen
  through the toy.

My manufacturing technique uses only two parts, or "combinators,"
called | and ><. I conjecture that we can reproduce each of the 11
models of the black-knot toy using only these combinators. With my
technique, a toy is just a grid filled with these two combinators.
(The | combinator has width 1 and >< has width 2.) Marbles are dropped
into the top of the grid, with input 0 being the first column, and
input n-1 being the final column. A marble that enters the top of a |
combinator continues into the row below in the same column. A marble
that enters the left side of a >< combinator emerges in the right
column and generates a "plink" sound. A marble that enters the right
side of the >< combinator emerges on the left but does not generate a
sound.

For example:

           012 
           ||| 
           ><| 
           |>< 
           ||| 
 
A marble that goes in column 0 comes out in column 2 with two "plinks". 
A marble that goes in column 1 comes out in column 0 with no "plinks". 
A marble that goes in column 2 comes out in column 1 with no "plinks". 


I expect children will be very picky about their knockoff toys being
observationally equivalent to the 11 official black-knot models. I've
written a simple program which you can use to access the mathematical
descriptions of these toys. Run the bk_specs file in your home
directory. The small ones are easy to reproduce but I'm having trouble
implementing the larger ones--can you help?

 -- Cain

```

# SML code
```sml
(* This code is super crappy.  First, you have to run it in SML/NJ because
 * it uses random number stuff.
 *
 * After you load it, you can generate a puzzle by calling
 * bk_rand_machine (width, height, freq, seed1, seed2)
 * width and height are the width and height of the puzzle.
 * When deciding what to put in a particular place, the generator will
 *  place a >< pair 1/freq of the time.
 * seed1 and seed2 are seeds for the random number generator.
 *)


(* SMLNJ *)

fun bk_rand_machine (width,height,freq,seed1,seed2) =
  let
    val rands = Random.rand (seed1,seed2)
    fun rand () = Random.randNat rands

    fun build_row (0,accum) = implode accum
      | build_row (1,accum) = implode (#"|" :: accum)
      | build_row (n,accum) = 
        if 0 = rand () mod freq then
           build_row (n - 2, #">" :: #"<" :: accum)
        else build_row (n - 1, #"|" :: accum)

    fun create_mach (0,accum) = foldr (fn (s1,s2) => s1 ^ "\n" ^ s2) "\n" accum
      | create_mach (n,accum) = 
        create_mach (n - 1, (build_row (width,nil)) :: accum)
  in
    print (create_mach (height,nil))
  end

```

# My thinkings
```
      0 -> (8, 10)
      1 -> (1, 5)
      2 -> (2, 5)
      3 -> (0, 5)
      4 -> (5, 4)
      5 -> (4, 6)
      6 -> (7, 4)
      7 -> (3, 3)
      8 -> (9, 6)
      9 -> (6, 4)
      
0 -> (3, 4)
1 -> (2, 3)
2 -> (0, 1)
3 -> (1, 1)
=>
0123
><><     0 + 1 = 1 (plink)
><||     1 - 1 = 0
||||     0 + 0 = 0
><><     0 + 1 = 1 (plink)
|><|     1 + 1 = 2 (plink)
||||     2 + 0 = 2
><><     2 + 1 = 3 (plink)  for input 0 game generate 4 plinks
|><|     3 + 0 = 3
0123

Short rules:
  | ~ identity
  > ~ (+1) and generate sound
  < ~ (-1) without sound
  
For example:

 012 
 ||| 
 ><| 
 |>< 
 ||| 
 
A marble that goes in column 0 comes out in column 2 with two "plinks". 
A marble that goes in column 1 comes out in column 0 with no "plinks". 
A marble that goes in column 2 comes out in column 1 with no "plinks". 
 
This black-knot has n=10 inputs, numbered from 0 to 9 and n=10 outputs

(These are little holes that kids drop marbles into.)

The black-knot is the function


i -> (j, k)
0 <= j < n
  in our case: 0 <= j < 10
0 <= k

j is a number of output hole
k is a number of "plink" sounds that are prduced as the marble rolls
  unseen through the toy.
 
Combinators:
1) |    has width 1
2) ><   has width 2

We try to reproduce 11 models of black-knot by only this combinators. 


Rules:
1. A marble that enters the top of a | combinator continues into the row below in the same column. (| ~ identity)
2. A marble that enters the left side of a >< combinator emerges in the right
column and generates a "plink" sound.  ( > ~ (+1) and generate sound )
3. A marble that enters the right
side of the >< combinator emerges on the left but does not generate a
sound. ( < ~ (-1) without sound)
  
Short rules:
  | ~ identity
  > ~ (+1) and generate sound
  < ~ (-1) without sound
```