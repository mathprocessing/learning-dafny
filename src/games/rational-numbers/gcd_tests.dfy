include "./gcd.dfy"

const divisor: (pos, pos) -> bool := Inner.divisor
// Error: ghost fields are allowed only in specification contexts
// To fix this error replace `predicate divisor` by `predicate method divisor`

// predicate divisor(a: pos, b: pos)
// {
//   Inner.divisor(a, b)
// }

lemma GcdOne(n: pos)
ensures gcd(1, n) == 1
{}

lemma GcdMul(a: pos, b: pos, c: pos)
ensures gcd(a * b, a * c) == a * gcd(b, c)
{}

lemma GcdProp(a: pos, b: pos, c: pos)
ensures gcd(a * a, b * c) == 0

lemma GcdComm(a: pos, b: pos)
ensures gcd(a, b) == gcd(b, a)
{}

lemma GcdAssoc(a: pos, b: pos, c: pos)
ensures gcd(gcd(a, b), c) == gcd(a, gcd(b, c))

lemma Gcd(a: pos, b: pos)
requires gcd(a, b) == 1
ensures !exists d :: divisor(d, a) && divisor(d, b)

lemma test()
{
  assert forall n :: gcd(1, n) == 1 by {
    forall k { GcdOne(k); }
  }
  // assert gcd(1, 1) == 1;
}
