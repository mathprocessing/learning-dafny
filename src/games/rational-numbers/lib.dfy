include "./types.dfy"
include "./gcd.dfy"
include "../common.dfy"

import opened Types

datatype rat = r(n: int, d: pos)
{
  function mul(b: rat): rat { r(this.n * b.n, this.d * b.d) }
  function add(b: rat): rat { r(this.n * b.d + this.d * b.n, this.d * b.d) }
  function sub(b: rat): rat { this.add(b.mul(r(-1, 1))) }
  function gcd_(): nat { gcd(abs(this.n), this.d) }
  predicate eq(b: rat) {
    this.gcd_() == b.gcd_()
    //exists k :: b == r(this.n * k, this.d * k)
  }
}