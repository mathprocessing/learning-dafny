include "./lib.dfy"

lemma test()
{
  var x: rat := r(5, 1);
  var y: rat := r(2, 3);
  assert x.mul(y) == r(10, 3);
  assert x.add(y) == r(17, 3);
  assert x.sub(y) == r(13, 3);
}