include "./types.dfy"

import opened Types

function gcd(x: nat, y: nat): nat {
  if x == 0 then y
  else if y == 0 then x
  else if x > y then
    gcd(y, x)
  else if x == y then
    x
  else gcd(x, y - x)
}

module Inner {
  import opened Types
  predicate method divisor(a: pos, b: pos)
}