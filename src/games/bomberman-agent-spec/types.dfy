datatype Expr = B(bool) | I(int) | X | Y | Z
  | And(Expr, Expr)
  | Or(Expr, Expr)
  | Eq(Expr, Expr)

datatype MoveAction = Left | Right | Up | Down | PlantBomb

type GameState = int

// We distinguish two starts:
// 1. Start cell (Player start position)
// 2. Start game (game start state)
const PLayerStart: Cell

const GameStart: GameState

datatype Action = Observe(Expr) | Do(MoveAction)

type Position = (nat, nat)

datatype Cell = Empty | Wall | Block | Bomb(timer: nat)
{
  function getRight(): Cell
  function getLeft(): Cell
  function getUp(): Cell
  function getDown(): Cell
  function nextTick(): Cell
}

datatype Player = Player
{
  const pos: Position
  const floor: Cell
  function move(ma: MoveAction): Player
}
