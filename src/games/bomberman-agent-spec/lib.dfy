/*
Verification + Q-learning = :
* Just verify all Q-learning methods, models, formualas, etc.
* Apply Q-learning to verification in general, searching of lemmas, etc.
* 


Sequence:
Start -> [Observe(..), Move(..), ...]

Philosophical basics of decision making:
* Randomness(throwing coin) <~> Observe new information(action of opponent, god of randomness)

  P - player
  B - bomb
  $ - detroyable block
  # - undestroyable wall

  # #P# #
  #$#$# #
*/

include "./types.dfy"

// 2D physics of player
method testMoveRight()
{
  var player: Player;
  if player.floor.getRight() != Empty {
    // P# >> moveR >> P#
    assert player.move(Right).pos == player.pos;
    // WISH: if we know that this assertion must holds ==> we can conclude "backwards" and calculate possible changes to model
    // From fact "this assertion A must holds" we can derive facts:
    // * What we must have after "requires X" and "ensures Y"
    // * Constraints of types (For example: type T can't have one enum state, like `type Cell = Empty` is not enough to saying about game, having full model of game)
    //   i.e. some functions remains unexpressable if we don't touch|change our model to more complex.
  } else {
    // P- >> moveR >> -P
    assert player.move(Right).pos != player.pos;
    assert player.move(Right).floor == player.floor.getRight();
  }
}

// Bomb physics(throwing, exploding) and timer

const Start: Cell

/* Block physics: 
   * Destroyable by bomb
*/
method testBombExplode()
{
  var now := Start;
  var next := Start.nextTick();
  if now == Bomb(0) {
    // Bomb explodes
    assert next == Empty;
  }
}