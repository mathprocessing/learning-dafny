// Can use tricks/matrix2d

/*
Rules:
* Unique numbers in row
* Unique numbers in column
* Unique numbers in 2x2 box (this rule can be ignored)

0 2 3 1
1 3 0 2
2 0 1 3
3 1 2 0

*/

predicate allUnique(s: seq<nat>): (res: bool)
  ensures |s| <= 1 ==> res // trivial case for better automation
{
  forall i, j | 0 <= i < j < |s| :: s[i] != s[j]
}

function minElem(s: seq<nat>): (res: nat)
  ensures s == [] ==> res == 0
  decreases s
{
  if |s| == 0 then 0
  else var restMin := minElem(s[1..]);
   if s[0] < restMin
     then s[0]
     else restMin
}
// {
//   if |s| == 0 {
//     return 0;
//   } else {
//     var restMin := minElem(s[1..]);
//     if s[0] < restMin {
//       return s[0];
//     } else {
//       return restMin;
//     }
// }

function maxElem(s: seq<nat>): (res: int)
  ensures |s| >= 1 ==> res >= 0
  ensures s == [] ==> res == -1
  decreases s
{
  if |s| == 0 then -1
  else var restMax := maxElem(s[1..]);
    if s[0] > restMax
      then s[0]
      else restMax
}

// version 1
// type perm = s: seq<nat> | allUnique(s) && minElem(s) == 0 && maxElem(s) == |s| - 1 witness []

// version 2
predicate isPermutation(s: seq<nat>): (res: bool)
  ensures |s| == 0 ==> res
{ 
  forall i :: 0 <= i < |s| ==> exists j :: 0 <= j < |s| && s[j] == i
}
type perm = s: seq<nat> | allUnique(s) && isPermutation(s) witness []

type pos = x: int | x > 0 witness 1

lemma PermFollowsMin(s: perm)
ensures minElem(s) == 0
{
  if |s| == 0 {} else {
    assert allUnique(s[1..]);
    assert isPermutation(s[1..]) by { assume false; }
    assert minElem(s[1..]) == 0 by { PermFollowsMin(s[1..]); }
  }
}

// lemma test()
// {
//   assert allUnique([]);
//   assert allUnique([1, 2]);
//   assert allUnique([2, 3, 4]);
// }

lemma test2(p: perm)
{
  if case |p| == 0 => {
    assert p == [];
  } case |p| == 1 => {
    PermFollowsMin(p);
    assert minElem(p) == 0;
    assert p == [0];
  } case |p| >= 2 => {
    // assert p == [0];
  }
}