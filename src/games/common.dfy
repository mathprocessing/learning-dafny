type pos = x: int | x >= 1 witness 1
function min(x: int, y: int): int { if x < y then x else y }
function max(x: int, y: int): int { if x < y then y else x }
function abs(x: int): nat { if x >= 0 then x else -x }