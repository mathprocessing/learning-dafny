module Predicate {

  predicate cond(a: array<int>, start: int, end: int)
  requires 0 <= start <= a.Length
  requires 0 <= end <= a.Length
  reads a
  {
    forall i | start <= i < end :: a[i] == 8 || a[i] == 11
  }

  method P(a: array<int>) returns (result: bool)
    ensures result == cond(a, 0, a.Length)
  {
    var r: bool := true;
    var j: int := 0;
    while j < a.Length
      invariant 0 <= j <= a.Length
      invariant r == cond(a, 0, j)
      decreases a.Length - j
    {
      r := r && (a[j] == 8 || a[j] == 11);
      j := j + 1;
    }
    result := r;
  }

}