module SumElements {
  // We need to implement `sum` and `count`
  method testForall()
  {
    assert forall i | 0 <= i <= 5 :: i*i < 100;
  }

  // function sum(cond: int -> bool): int
  // {
  //   if forall i :: !cond(i) then 0 else
  //   // `sum` can be infinite?
  // }

  function {:verify false} sum(cond: int -> bool, valuef: int -> int, start: int, end: int): int
    decreases end - start
  {
    if end <= start then 0 else
    if cond(start)
      then valuef(start) + sum(cond, valuef, start + 1, end)
      else 0
  }

  method {:verify false} testSum()
  {
    // assert sum(i => 0 <= i <= 5 ==> i % 2 == 0) == 3;
    assert sum(i => i == 0, i => i, 0, 6) == 1 by {
      assert sum(i => i == 0, i => i, 1, 1) == 0;
      var val := 1 + sum(i => i == 0, i => i, 1, 1);
      var c := i => i == 0;
      var fv := (i: int) => i;
      assert sum(c, fv, 0, 1) == val;
    }
  }

  function {:verify false} count(cond: int -> bool, start: int, end: int): int
    decreases end - start
  {
    if end <= start then 0 else
    (if cond(start) then 1 else 0) + count(cond, start + 1, end)
    // Easy mistake: if cond(start) then 1 else 0 + count(cond, start + 1, end)
  }

  function {:verify false} count'(cond: int -> bool, start: int, end: int): int
    decreases end - start
  {
    if end <= start then 0 else
    (if cond(end - 1) then 1 else 0) + count'(cond, start, end - 1)
  }

  // Todo: prove this
  lemma CountDefEquivToCount'(cond: int -> bool, start: int, end: int)
  ensures count(cond, start, end) == count'(cond, start, end)

  lemma CountTrue(start: int, end: int)
  ensures var r := count(_ => true, start, end); if end > start then r == end - start else r == 0
  {
    Count'True(start, end);
    CountDefEquivToCount'(_ => true, start, end);
  }

  lemma {:induction false} Count'True(start: int, end: int)
  ensures var r := count'(_ => true, start, end); if end > start then r == end - start else r == 0
  decreases end - start
  {
    if end > start {
      
      assert count'(_ => true, start, end) == end - start by {
        calc == {
          count'(_ => true, start, end);
          1 + count'(_ => true, start, end - 1);
          { assert count'(_ => true, start, end - 1) == end - start - 1 by { Count'True(start, end - 1); } }
          1 + end - start - 1;
          end - start;
        }
      }
    } else {}
  }

  lemma CountFalse(start: int, end: int)
  ensures count(_ => false, start, end) == 0

  lemma CountConcat(cond: int -> bool, start: int, middle: int, end: int)
  ensures count(cond, start, middle) + count(cond, middle, end) == count(cond, start, end)

  method testCount()
  {
    // assert count(_ => true, 0, 33) == 33; // Maximum holding value: 33
    // If you want to prove other values use lemma `CountTrue`
    assert count(_ => true, 0, 100) == 100 by { CountTrue(0, 100); }
    assert count(_ => true, 0, -100) == 0;
    assert count(_ => false, 0, 100) == 0 by { CountFalse(0, 100); }
    assert count(_ => true, 0, 100) == count(_ => true, 0, 50) + count(_ => true, 50, 100) by { CountConcat(_ => true, 0, 50, 100); }
    assert count(i => i % 2 == 0, 0, 30) == 15;
  }

  method testCount'()
  {
    // assert count'(_ => true, 0, 33) == 33; // Maximum holding value is also: 33
  }
}