/** Video: https://www.youtube.com/watch?v=P2durYFsJSA */

/*
We need Value function for
turn sequence of booleans into a native dafny natural number `nat`
(technically int with condition: n >= 0)

 */

function pow(n: nat): nat
{
  if n == 0
  then 1
  else (2 * pow(n-1))
}

function Value(s: seq<bool>, base: nat): nat
{
  if s == []
    then 0
    else (if s[0]
           then base + Value(s[1..], 2 * base)
           else        Value(s[1..], 2 * base)
         )
}

// Simplification of Inc method in functional style
function inc(s: seq<bool>): seq<bool>
  // ensures |s| > 0 ==> s[0] == false ==> inc(s) == s[0 := true]
  // ensures |s| == 0 ==> inc(s) == [true]
{
  if |s| == 0 then  //     inc([1, ..., 1, 1]) = [0, ..., 0, 0, 1]
    [true] // extend list [true, ..., true, true] -> [false, ..., false, false, true]
  else
    if s[0] == false then
      s[0 := true]
    else
      [false] + inc(s[1..])
}

// a < b <=> not imp(b, a)
predicate bool_lt(a: bool, b: bool)
{
  !(b ==> a)
}

/** Sequence s is increases if forall elements in s, 0 <= i < |s| - 1 ==> s[i] <= s[i+1] */
predicate increases(s: seq<bool>)
{
  if |s| <= 1 then true else // |s| >= 2
    if bool_lt(s[1], s[0]) then false
      else increases(s[1..])
}

function repeat(value: bool, size: nat): seq<bool>
{
  if size == 0 then
    []
  else
    [value] + repeat(value, size - 1)
}

method test_repeat()
{
  assert repeat(true, 3) == [true, true, true];
  assert repeat(false, 0) == [];
}

method test_bool_lt()
{
  assert bool_lt(false, true) == true;
  var some: bool :| true; // equivalent to `forall some : bool :: ...`
  assert bool_lt(true, some) == false;
  // assert forall some : bool :: bool_lt(true, some) == false;

}

method learn_exists_quantifier()
{
  /**
  We can grab typical syntax from Stackoverflow:
  https://stackoverflow.com/questions/42434539/dafny-how-to-use-both-quantifiers-together
  ensures exists i | 0 <= i <= a.length ::
               (forall j | 0 <= j < i ::  a[j] != 0)
            && (forall j | i <= j < a.length ::  a[j] == 0);
  */
  
  // assert exists n | n < 3 :: n + 1 == 2; NOT WORKS!

  // Ok let's move to docs:
  // https://github.com/dafny-lang/dafny/wiki/FAQ
  // @Dafny sometimes reports an error, even on a correct program.


}

method test_increases()
{

  /**
  Source: https://github.com/dafny-lang/dafny/wiki/FAQ

  One common problem is with "extensionality", which refers to the fact that 
    two collections are equal when they have the same elements.
    
  Dafny has trouble knowing when to invoke this principle, 
    but you can encourage it to do so by adding an explicit equality assertion to your program.
   */
  // fill Cache of SAT-SOLVER
  assert [false] == [false];
  assert [false] != [false, false];
  assert [false] != [false, false, false];
  assert [false] != [false, false, false, false];
  assert [false] != [false, false, false, false, false];

  assert forall s: seq<bool> :: |s| <= 2 ==> !increases(s) ==> s == [true, false];

  // Idea:
  assert forall s: seq<bool> :: s == [] ==> increases(s);
  assert forall s: seq<bool> :: s == [true] ==> increases(s);
  assert forall s: seq<bool> :: s == [true, true] ==> increases(s);
  // ...
  // How to express this as general statement (using variable i)?
  // assert forall s: seq<bool>, i: nat :: 0 <= i < |s| ==> s[i] == true ==> increases(s);
  // Line above not works.

  // Let's use repeat function:
  assert increases(repeat(true, 3));
  // assert forall n: nat :: increases(repeat(true, n)); // Fails

  // Base case
  assert increases(repeat(true, 0)); // OK
  // Let's try to prove induction hypothesis
  assert forall n: nat :: increases(repeat(true, n)) ==> increases(repeat(true, n+1)); // OK

  // General case: [x, x, ...]
  // Base case
  assert forall x: bool :: increases(repeat(x, 0)); // OK
  // Induction hypothesis
  assert forall n: nat, x: bool :: increases(repeat(x, n)) ==> increases(repeat(x, n+1)); // OK

  // if sequence is not increases then must exists some subpair of sequence [..., true, false, ...]
  var s1 := [true, false];
  var s2 := [true, true, false];
  // assert exists offset: nat :: (offset <= |s2| - |s1|) && 
  //   (forall i: nat :: i < |s1| ==> s1[i] == s2[i + offset]); // Fails

  // Let's check it manually maybe it have mistakes:
  /**
  exists offset: nat :: (offset <= |s2| - |s1|) && (forall i: nat :: i < |s1| ==> s1[i] == s2[i + offset])
  Can be computed: |s1| == 2 && |s2| == 3
  exists offset: nat :: (offset <= 1) && (forall i: nat :: i < |s1| ==> s1[i] == s2[i + offset])
  (offset == 0) && (forall i: nat :: i < |s1| ==> s1[i] == s2[i + offset])
    || (offset == 1) && (forall i: nat :: i < |s1| ==> s1[i] == s2[i + offset])
  */
  assert forall offset: nat :: ((offset == 0) ==> (forall i: nat :: i < |s1| ==> s1[i] == s2[i + offset]))
    || ((offset == 1) ==> (forall i: nat :: i < |s1| ==> s1[i] == s2[i + offset]));
  assert (forall i: nat :: i < |s1| ==> s1[i] == s2[i]) || (forall i: nat :: i < |s1| ==> s1[i] == s2[i + 1]);
  
   

}

method test_inc()
{

  // assert forall s: seq<bool>, i: nat :: 0 <= i < |s| ==> s[i] == true ==> |s| == 2 ==> s == [true, true];
  
  
  // assert forall s: seq<bool> :: |s| == 1 ==> (s == [true] || s == [false]);
  // assert (s == [true] || s == [false]);


  // assert forall a, b : bool :: !(b ==> a) == !(!a ==> !b);




  assert inc([true]) == [false, true];
  assert inc([true, true]) == [false, false, true];
  assert inc([true, true, true]) == [false, false, false, true];


  var e : seq<bool> :| |e| == 0;
  assert |inc(e)| == 1;

  var e2 : seq<bool> :| |e2| == 1;
  assert |inc(e2)| == 1 || |inc(e2)| == 2;
  // if len(e2) == 1 then we have two cases:
  // 1. e2 == [true] then we conclude inc(e2) == [false, true]
  // 2. e2 == [false] then we conclude inc(e2) == [true]
  // in all cases `last element == true`
  // Let's check it:
  // case 1:
  assert |inc(e2)| == 2 ==> inc(e2) == [false, true];
  // case 2:
  assert |inc(e2)| == 1 ==> inc(e2) == [true];
  // last element == true
  var result := inc(e2);
  assert result[|result|-1] == true;
}

method Inc(s: seq<bool>) returns (r: seq<bool>)
  ensures Value(r, 1) == Value(s, 1) + 1
  // Rustan Leino: It's a full spec? Could we test this spec?
  // Jason Koenig: To test spec we can use assertions in Main method
{
  // we use r := s because we want to modify sequence
  // in other case we might add word `modifies` to spec above
  r := s;
  var i := 0;
  // ghost var is a variable used only for specification
  ghost var incAmt := 1;
  while i < |r|
    invariant 0 <= i <= |r|
    invariant incAmt == pow(i)
    invariant forall k :: 0 <= k < i ==> r[k] == false
    invariant Value(s, 1) + 1 == incAmt + Value(r, 1)
  {
    // flip_bit
    if r[i] == true
    {
      // possible inner states (or view of that set of states with fixed property):
      // fantom_pre_state: r = [..., true, ...], r[i] == true
      // r == [true, ...] || r == [*, true, ...] || r == [*, *, true, ...] || ...
      // i = 0               i = 1                       i = 2
      r := r[i := false]; // must equal to new sequence with changed element
      // fantom_post_state: 
      // r == [false, inc(...)] || r == [*, false, inc(...)] || r == [*, *, false, inc(...)] || ...

      // Взгляд 1: Когда мы пишем цикл, то работаем с потенциально бесконечным множеством состояний и нам остро необходим способ "склейки этих состояний" в один объект конечного размера
      // Взгляд 2: [Теория игр] Есть противник, он выбирает случайное состояние и мы должны отреагировать в наихудшем случае наилучшим образом 
      // (выбрать тело цикла наиболее близкое к оптимальной последователность действий)
    } else { // r[i] == false

      // fantom_state = [.., false] <=>
      // r == [false, ...] || r == [*, false, ...] || r == [*, *, false, ...] || ...
      // i = 0                i = 1                   i = 2
      r := r[i := true];
      return;
    }
    incAmt := incAmt + pow(i);
    i := i + 1;
  }
  if i == |r|
  {
    r := r + [true];
  }
}

method Main()
{
  var v: seq<bool> := []; // Empty sequence
  assert Value(v, 1) == 0;
  v := [true, false];
  // Value([true, false], 1) = 
  // 1 + Value([false], 2) = 
  // 1*1 + 2*0 + Value([], 4) = 
  // 1*1 + 2*0 + 0 = 1 
  //
  // Value(s, 1) = [1 * s[0], 2 * s[1], 4 * s[2], 8 * s[3], ...]
  //   where true <=> 0, false <=> 1
  assert Value(v, 1) == 1;
  v := [];

  // Let's test our spec of Inc method
  v := Inc(v);
  assert Value(v, 1) == 1;
  v := Inc(v);
  assert Value(v, 1) == 2;

  
}

method cache_bad_behaviour()
{
  // var u : seq<bool> :| |u| == 1; // Verifier: cannot establish the existence of LHS values that satisfy the such-that predicate

  // assert inc([]) == [true]; // This line Changed state of SAT-Solver?
  // Asserts stores in SAT-solver cache?

  // Iterations of finding (WHAT CAN CHANGE STATE?)
  // 1. Can't change.
  // var empty1: seq<bool> := [];
  // var empty2: seq<bool> := [];
  // assert empty1 == empty2;
  // 2. Changed.
  // assert [false] == [false];
  // assert [false] != [false, false];
  // var u : seq<bool> :| |u| == 2; // OK!!! Works!!! BUT WHY???
}