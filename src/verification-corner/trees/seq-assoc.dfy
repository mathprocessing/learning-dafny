method testLeftAssoc(x: int, xs: seq<int>)
ensures ([x] + xs)[0] == x
{}

method testRightAssoc(x: int, xs: seq<int>)
ensures (xs + [x])[|xs|] == x
{}

// Rustan: In dafny operator `+` for sequences is left associative but you can prove that
// it a right associative
// How to prove this?
lemma SeqPlusIsAssoc(a: seq, b: seq, c: seq)
ensures (a + b) + c == a + (b + c)
{}