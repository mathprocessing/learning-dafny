include "./lib.dfy"

function toString(lst: seq<Code<char>>): string
{
  if lst == []
    then []
    else 
    // var head: seq<char> := match lst[0]
    //                          case Lf(x) => [x]
    //                          case Nd => [];
    // head + toString(lst[1..]) // maybe this definition hard to enroll, but i don't have example when it harder than line below
    (match lst[0] case Lf(x) => [x] case Nd => []) + toString(lst[1..])
}

method testFlatten()
{
  var t: Tree<char> := 
    Node(
      Node( // t.left
        Leaf('D'), // t.left.left
        Leaf('a') // t.left.right
      ),
      Node( // t.right
        Node( // t.right.left
          Leaf('f'), // t.right.left.left
          Leaf('n') // t.right.left.right
        ), 
        Leaf('y')) // t.right.right
      );

  // Block 1 BEGIN
  var dafnyCode := [Lf('D'), Lf('a'), Nd, Lf('f'), Lf('n'), Nd, Lf('y'), Nd, Nd];
  assert flatten(t) == dafnyCode by {
    calc == {
      flatten(t);
      // by def of flatten
      flatten(t.left) + flatten(t.right) + [Nd];
      calc == { // sub calc statement
        flatten(t.left);
        flatten(Leaf('D')) + flatten(Leaf('a')) + [Nd];
        // use def of faltten
        [Lf('D')] + [Lf('a')] + [Nd];
        // use concatenation property of lists
        [Lf('D'), Lf('a'), Nd];
      }
      [Lf('D'), Lf('a'), Nd] + flatten(t.right) + [Nd];
      calc == {
        flatten(t.right);
        [Lf('f'), Lf('n'), Nd, Lf('y'), Nd];
      }
      [Lf('D'), Lf('a'), Nd] + [Lf('f'), Lf('n'), Nd, Lf('y'), Nd] + [Nd];
      // concatenate all sublists
      [Lf('D'), Lf('a'), Nd, Lf('f'), Lf('n'), Nd, Lf('y'), Nd, Nd];
    }
  }
  // Block 1 END
  // We can convert tree to string
  assert toString(flatten(t)) == "Dafny"; // not holds without `Block 1`
}