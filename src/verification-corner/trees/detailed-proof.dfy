// WISH 1: modify fuel of imported functions like `flatten` in lib.dfy

// We need to redefine this functions because "WITH 1" not realized in Dafny (but can work in future versions)
datatype Tree<T> = Leaf(data: T) | Node(left: Tree<T>, right: Tree<T>)
datatype Code<T> = Lf(data: T) | Nd

function {:opaque} flatten(t: Tree): seq<Code>
decreases t
{
  match t
  case Leaf(x) => [Lf(x)]
  case Node(t1, t2) => flatten(t1) + flatten(t2) + [Nd]
}

function {:opaque} size(t: Tree): nat
decreases t
{
  match t
  case Leaf(x) => 1
  case Node(t1, t2) => 1 + size(t1) + size(t2)
}

// We disable automatic induction and use `{:opaque}` attribute for definitions
lemma {:induction false} FlattenLength<T>(t: Tree<T>)
ensures |flatten(t)| == size(t)
decreases t
{
  match t
  // Base case
  case Leaf(x) => {
    // We need to show how to prove `Goal`
    assert Goal: |flatten(Leaf(x))| == size(Leaf(x)) by {
      calc == {
        |flatten(Leaf(x))|;
        { reveal flatten(); }
        |[Lf(x)]|;
        // by definition of `|seq|` operator in Dafny
        1;
        { reveal size(); }
        size(Leaf(x));
      }
    }
    reveal Goal;
  }
  // Inductive step
  case Node(t1, t2) => {
    // Inductive hypothesis
    // We hide it by using label `IH`
    // Also we can write shorthand for it usung keyword `var`
    var ih := size(t1) == |flatten(t1)| && size(t2) == |flatten(t2)|;
    assert IH: ih by { // by structural induction on datatype `Tree`
      FlattenLength(t1);
      FlattenLength(t2);
    }

    assert Goal: |flatten(Node(t1, t2))| == size(Node(t1, t2)) by {
      // We need this because `the type of `Nd` expression is underspecified`
      var Nd': Code<T> := Nd;
      if case true => {
        // 1-st variant of proof
        calc == {
          size(Node(t1, t2));
          { reveal size(); } // by definition of size
          1 + size(t1) + size(t2);
          { reveal IH; } // that step follows from induction hypothesis
          1 + |flatten(t1)| + |flatten(t2)|;
          |[Nd']| + |flatten(t1)| + |flatten(t2)|;
          |flatten(t1) + flatten(t2) + [Nd]|;
          { reveal flatten(); } // by definition of flatten
          |flatten(Node(t1, t2))|;
        }
      } case true => {
        // 2-nd variant of proof
        calc == {
          size(Node(t1, t2));
          { reveal size(); } // by definition of size
          1 + size(t1) + size(t2);
          { reveal IH; } // that step follows from induction hypothesis
          1 + |flatten(t1)| + |flatten(t2)|; 
          // Note: It's hard to figure out why we replace `1` with `|[Nd]|`.
          // May be we can reverse all steps in calc after this point somehow
          // (by creating new calc statement inside this one or defining new near)
          calc == { // Let's select "by creating new calc statement inside"
            |flatten(Node(t1, t2))|;
            { reveal flatten(); }
            |flatten(t1) + flatten(t2) + [Nd]|;
            |flatten(t1)| + |flatten(t2)| + |[Nd']|;
            1 + |flatten(t1)| + |flatten(t2)|;
          }
          |flatten(Node(t1, t2))|;
        }
      }
    }
    reveal Goal;
  }
}