include "./lib.dfy"

// 18:41 videotime
function raise(cds: seq<Code>, ts: seq<Tree>): seq<Tree>
{
  if cds == []
    then ts
    else raise(cds[1..], raiseOne(cds[0], ts))
}

function raiseOne(cd: Code, ts: seq<Tree>): seq<Tree>
{
  match cd
    case Lf(x) => [Leaf(x)] + ts
    case Nd =>
      if |ts| < 2 then []
      else [Node(ts[1], ts[0])] + ts[2..]
}

// 22:18 videotime
lemma Theorem(t: Tree)
  ensures raise(flatten(t), []) == [t];
{
  match t {
    case Leaf(x) =>
    case Node(t1, t2) =>
      // calc {
      //   raise(flatten(t), []);
      // ==
      //   raise(flatten(Node(t1, t2)), []);
      // == // by def. of flatten
      //   raise(flatten(t1) + flatten(t2) + [Nd], []);
      // ==
      //   // Now we discovered possibility of using `Lemma` and prune this calc, but we can't prune all match because `Lemma` can't be applied to base case
      // ==
      //   [t];
      // }
      calc {
        raise(flatten(t), []);
      == // we need this trivial step, why?
      // Sophia: Perhaps we just need to remind Dafny of that fact
        { assert flatten(t) + [] == flatten(t); }
        raise(flatten(t) + [], []);
      == { Lemma(t, [], []); }
      //raise(cds, [t] + ts)
        raise([], [t] + []);
      ==
        [t];
      }
  }
}

// 26:17, we see this Lemma as helper to prove not properly
// generalized Theorem (too specific to be able to use induction)
// Sophia Drossopoulou says: This lemma shows how the function is working!
// Rustan: This lemma helps me understand a little bit here.
lemma Lemma(t: Tree, cds: seq<Code>, ts: seq<Tree>)
  ensures raise(flatten(t) + cds, ts) == raise(cds, [t] + ts);
{
  match t {
    case Leaf(x) =>
    case Node(t1, t2) =>
      calc {
        raise(flatten(t) + cds, ts);
      == // def. flatten
        raise(flatten(t1) + flatten(t2) + [Nd] + cds, ts);
      == {
        // remind Dafny associativity of concatenation
        assert flatten(t1) + flatten(t2) + [Nd] + cds ==
          flatten(t1) + (flatten(t2) + [Nd] + cds);
      }
        raise(flatten(t1) + (flatten(t2) + [Nd] + cds), ts);
      //raise(flatten(t) + cds                        , ts)
      == // by induction hypothesis
      //raise(cds, [t] + ts)
        raise(flatten(t2) + [Nd] + cds, [t1] + ts);
      ==
        raise(cds, [t] + ts);
      }
  }
}

/*
Power of intelligence is NOT a solve some tediouly defined task.
Power of intelligence is 
* bring to up some useful cloud of defintions (ACTIONS), ways of thinking in particular state.
  Let's rewrite it:
  calc ~ {
    ... cloud of definitions ...
    ... cloud of ACTIONS ...
    ... cloud of possible synonyms, NAMES for that action-state ...
    ... cloud of possible NAMES for that (PAIR of states) ...
    S1: ... cloud of possible NAMES for that (ARROW between states) ...
    calc {
      ... No idea how to produce new right phrases
      We can think about that state as "state with score lower than next"
      i.e. score(S1) < score(S2)
      Let's replace NAMES with SYNONYMS back
    }
    S2: ... cloud of possible SYNONYMS for that (ARROW between states) ...
    Comment: /*
      Now we can do really beautiful (high score) thing:
        we can assume that some move inside some group (in future, i.e. when we learn something new) even
        if we (at the moment when we that move do) don't assume that in the beginning of thinking (construction of that "calcs")

      About score:
        What if `score` is not 1D parameter?
        Can it be vector, 2d, 3d or some general simplex geometric structure like Sierpiński triangle?
https://en.wikipedia.org/wiki/Sierpi%C5%84ski_triangle#/media/File:Sierpinski_Pascal_triangle.svg
    */
  }

  Let's do some experiment with mind:
  * First don't do some "fast" moves, just calm out and try to move
    in state space maximally slow
  * in S2 we can (structures with highest score value) bring up some: {
    1. algebra of SYNONYMS
    2. SYNONYMIC functions (have less score than 1.)
      We can try to replace `functions` with some other words or phrases
      inner calc {
        SYNONYMIC "functions"
        ... (things with bigger score)
      }
      Also we can destroy all stuff to more subblocks like that:
      SYNONYMIC == SYNONYM + (X => like X)
      OR
      SYNONYMIC == SYNONYM + (X => have same properties as X)

      If make equivalence class:
      LIKE = {like, have same properties as} we don't need
      to brute force all cases like:
        branch 1: use "like"
        branch 2: use "have same properties as"

    * 
  }

* ...

Rules to thinking carefully:
* don't do some "fast" moves, just calm out and try to move
    in state space maximally slow
* Think about yourself like about Stockfish: you compute score and
  it's easy to make some `wrong` action that change this score and 
  make this score not useful for you
  + Stockfish increase depth INCREMENTALLY
    i.e.
    it is necessary to keep the depth of diving into the conceptual space constant or monotonically increasing
    i.e. don't 
* don't make sound, just meditate? why?
  Wait for several minutes how your brain construct some 
  "intermediate structures" some that (not all) help you in the future
  to construct new ways of thinking, new (clouds of) synonyms, new solutions


Нужно быть боллее смелым и смотреть на эти вещи как на то , что можно вычислить.
Это не секрет, что нужно добавить в наш лексикон новые понятия, насколько
чтобы мысли которые звучат странно, стали звучать более обоснованно.
Но для этого нужны некоторые `intermediate structures`, они должно роль топлива. А что должно играть роль огня, печки?
Есть зацепка:
  Мы можетм думать не только об измерении score, но и разнице:
    Мы меняем состояние в одном пространстве sp1 и в определенный момент (мы успеем пройти какую-то линию, путь в нём) мы заметим что
  фиксированное (оно не менялось) состояние в пространстве sp2 начало 
  уменьшать свое `score value`
* Дальше можно подумать, что если просто построить функцию `sp1 -> score in sp2`
  но всплывает проблема irreversibility_of_thinkning(IOT):
  мышление не обратимо т.е. мы не можем пройти путь дважды (прямо как в задачках про эйлеровы графы)
  Причины IOT:
    Мы помним "шлейф" фактов и не можем резко менять контекст мышление. резко менять тему, что требует:
  * умение быстро забывать ненужную часть контекста
  * забывать не только сознательно, но и так чтобы информация, относящаяся к старой "позиции" не влияла на оценку новой "позиции"

  Эффекты IOT заставлят нас выбирать путь определенной структуры:
  * запрещает идти поиском в  глубину, так он требует резкой смены контекста
  * но какая стратегия поиска медленно меняет контекст или более общо:
    меняет контекст с постоянной скоростью изменения?

Проблема миража: Очень тяжело думать о задачах так, чтобы они решались правильным путем, {added Phase1}. Когда все части понятийного облаках "преобразуются" равномерно не "отбрасывая вверх" слишком рано неких деталей, конструкций которые могут исказить наш путь, создать мираж, который отведет нас от оазиса в последний момент.
https://travelask.ru/blog/posts/13148-chem-na-samom-dele-yavlyaetsya-mirazh-i-mozhno-li-ego-sfotog

Phrase1:
  нужен предшествующий опыт и постоянная тренировка самостоятельности, когда смотришь как люди что-о решают нужно всегда стараться их обогнать чтобы получить возможность найти новое решение задачи

Пришла странная далекая мысль:
"психодогия личностных отношений иллюзия обмана"

Попробуем её поисследовать ниже:
Чем может помочь изучение иллюзий:
* Если взглянуть с птичего полета:
  calc {
    Анализ иллюзий - это анализ ошибок

    анализ ошибок - это формирование верной стратегии действий

    step A: анализ ошибок - это X

    X = ?

    step B: X это формирование верной стратегии действий

    формирование верной стратегии действий - это {нужен intermediate step in calc ==> created step A}
  }
  

https://vk.com/@kosmoenergetika.moscow-rss-345370676-2096144065
Цитаты:
Психологи очень тяжело отличают иллюзию от нестандартного мышления и неординарного восприятия окружающего мира, слишком многое зависит от характера человека и его отношения к миру. 

Все личностные иллюзии имеют прямое отношение к
* уровню развития личности
* степеням психической защиты человека от стрессовых ситуаций, эгоцентризма, 
* в том числе к иллюзионному восприятию относятся и "розовые" мечты, "витание в облаках", невосприятие чужого и собственного опыта, вплоть до полного его отрицания. 

Множественность восприятия:
Теоретически, сталкиваясь с одинаковым событием внешнего мира, любой человек через конкретные органы получает одинаковую же информацию. Но при этом возникает неограниченное (бесчисленное) множество трактовок этого события. 

... сказать точно, какая именно интерпретация будет правильной и реальной, а какая - личностной иллюзией - просто невозможно (трудно).

Все иллюзии, с точки зрения психологии, относятся к обманам или ошибкам восприятия, возникающим как на основе патологического заболевания, так и на почве ошибочного восприятия у здорового человека.
(Рассморим только `ошибочные восприятия`)

Rephrased:
Иллюзия - ошибка, заблуждение, неадекватное отражение воспринимаемого явления или предмета и его качеств (свойств, весов свойств т.е. какие-то свойства могут быть "скрыты", а другие "привознесены").

Личностная иллюзия - обман личностного влияния на событие, т.е.
  "Как именно я могу повлиять на событие X и могу ли?"

Rephrased:
Люди, обладающие подвижным, ярким и образным воображением (мышлением), более грамотные, начитанные и образованные вызывают в своём воображении иллюзии, которые являются проекциями фантазий на объективную реальность.
(Стоит заметить: создать много процекций - тяжелая работа, и чем больше различных проекций модели создано не только в объективную реальность ну и куда бы то ни было тем лучше для более обстоятельного анализа задачи)


*/