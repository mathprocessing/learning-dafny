include "./lib.dfy"

lemma {:induction false} SizeLemma<T>(t: Tree<T>)
  ensures size(t) == |flatten(t)|
{
  match t {
    case Leaf(x) =>

    case Node(t1, t2) =>
      var Nd': Code<T> := Nd;
      calc {
        size(Node(t1, t2));
      == // def. size
        1 + size(t1) + size(t2);
      == { SizeLemma(t1); SizeLemma(t2); }
        1 + |flatten(t1)| + |flatten(t2)|;
      == // singleton sequence
        |[Nd']| + |flatten(t1)| + |flatten(t2)|;
      ==
        |[Nd] + flatten(t1) + flatten(t2)|;
      == // def. flatten
        |flatten(Node(t1, t2))|;
      }
  }
}