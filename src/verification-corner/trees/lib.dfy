// Video: Writing inductive proofs about programs that operate on trees

// Binary tree that contains a data of type `T` in each node
datatype Tree<T> = Leaf(data: T) | Node(left: Tree<T>, right: Tree<T>)

// `Lf` - something that going from `Leaf` during convertion `Tree` to `Code`
// `Nd` - some indication of internal node
datatype Code<T> = Lf(data: T) | Nd

// Take tree and create sequence of such "codes"
function flatten(t: Tree): seq<Code>
decreases t
{
  match t
  case Leaf(x) => [Lf(x)]
  case Node(t1, t2) => flatten(t1) + flatten(t2) + [Nd]
}

function size(t: Tree): nat
decreases t
{
  match t
  case Leaf(x) => 1
  case Node(t1, t2) => 1 + size(t1) + size(t2)
}

lemma FlattenLength(t: Tree)
ensures |flatten(t)| == size(t)
{
  // Proved automatically by induction on `t`
}