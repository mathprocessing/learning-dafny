// It's possible to import some math functions?
// import Abs from Math;

function Signum(x: int): int
{
  if x > 0 then 1 else
  if x < 0 then -1 else
                0
}

function Abs(x: int): int
{
  if x >= 0 then x else (-x)
}

function Fact(n: nat): nat
{
  if n == 0 then 1 else n * Fact(n-1)
}

lemma ex(x: int)
  ensures x > 5

/* Lemmas about Abs(x):
@ Abs(x) == Sqrt(x) ^ 2
@ Abs(x) == x * Signum(x)

Non-negativity:        Abs(x) >= 0
Positive-definiteness: Abs(x) == 0 ==> x == 0
Multiplicativity:      Abs(x * y) == Abs(x) * Abs(y)
Triangle inequality:   Abs(x + y) <= Abs(x) + Abs(y)

Idempotence:           Abs(Abs(x)) = Abs(x)
Reflection symmetry:   Abs(-x) == Abs(x)

Reverse Triangle inequality: ||x| - |y|| <= |x - y|

@ ||x| - |y|| ==
    if x >= 0 && y >= 0 then |x - y|  && xy >= 0
    if x >= 0 && y < 0  then |x + y|  && xy < 0
    if x < 0 && y >= 0  then |x + y|  && xy < 0
    if x < 0 && y < 0   then |x - y|  && xy >= 0
  i.e.
    x * y >= 0 ==> ||x| - |y|| == |x - y| && 
    x * y < 0  ==> ||x| - |y|| == |x + y|

@ |x - y| + |x + y| == 
    if x - y >= 0 && x + y >= 0 then 2x
    if x - y >= 0 && x + y < 0  then -2y
    if x - y < 0 && x + y > 0   then 2y
    if x - y < 0 && x + y < 0   then -2x
  i.e.
    ||x - y| + |x + y|| == 2x or 2y
  i.e.
    forall x, y: int :: ||x - y| + |x + y|| % 2 == 0





@ x < y ==> |x - y| ==
    if x >= y then {}
    if x <  y then y - x
  i.e.
    x < y ==> |x - y| == y - x



*/

// lemma Example (n: nat, m: nat)
//   requires Abs(n < m) == 0
//   ensures 
// {
  
// }

// lemma L(n: nat)
//   ensures 1 <= Fact(n)
// {

// }