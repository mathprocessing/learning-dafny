datatype Tree<T> = Leaf | Node(left: Tree, data: T, right: Tree)

function Contains<T>(t: Tree<T>, v: T): bool
{
  match t
    case Leaf => false
    case Node(left, x, right) =>
      x == v || Contains(left, v) || Contains(right, v)
}

// array<T> can't be null by definition, if you need `null` instead use array?<T>
method Fill<T>(t: Tree<T>, a: array<T>, start: int) returns (end: int)
  modifies a; // 1. assignment may update an array element not in the enclosing context's modifies clause
  ensures start <= end <= a.Length; // 2. Index out of range
  // requires a.Length <= 1 // Added to force printing of simplest counterexample.
  requires 0 <= start < a.Length // 3. Assertion not hold
  // 4. precondition of recursive called method `requires ...` may not hold
{
  match t {

    case Leaf => {
      end := start;
      assert end <= a.Length; // 3. Assertion not hold
    }

    case Node(left, x, right) => {
      end := Fill(left, a, start);
      if end < a.Length {
        a[end] := x; // 2. Index out of range
        end := Fill(right, a, end + 1); // 4. precondition of recursive called method `requires ...` may not hold
      }
    }

  }
}

// Scheme of writing full and detailed way of solving verification task:
// We add `???` //[because] 1. [Error string] assignment may update an array element not in the enclosing context's modifies clause

/* After 4th step:
4. precondition of recursive called method `requires ...` may not hold
We want to add some
  1) ensures or 2) requires ?

If we add 1) ensures then we can't disable error because ... TODO: make proof of that branch is false or equivalent some other branch

2) changing set of requires clauses in parent method instantly changes spec of rec childs Fill methods.
  But this changing works only for one way (we can think only about childs rec calls) ==> we must add `requires ...`

    case Node(left, x, right) => {
      end := Fill(left, a, start); // Write left tree
      if end < a.Length {
        a[end] := x; // Write in middle single data value
        end := Fill(right, a, end + 1); // Write right tree
      }
    }

*/