// https://github.com/dafny-lang/dafny/blob/01fa5608cf09560d27f7a2a2e1cb9d73921c7fea/Test/triggers/some-terms-do-not-look-like-the-triggers-they-match.dfy

method RewriteMultiSet(s: multiset<int>, t: multiset<int>) {
  // multiset union
  ghost var u0 := forall x | x in s + t :: x < 200;
  ghost var u1 := forall x | x in s || x in t :: x < 200;
  assert u0 == u1 by {
    // here's one way to prove it
    assert forall x :: x in s + t <==> x in s || x in t;
  }

  // multiset intersection
  ghost var i0 := forall x | x in s * t :: x < 200;
  ghost var i1 := forall x | x in s && x in t :: x < 200;
  assert i0 == i1 by {
    // here's a different way to prove this one
    assert i0 == forall x | x in s * t && x in s && x in t :: x < 200;
  }

  // multiset difference
  ghost var d0 := forall x | x in s - t :: x < 200; // fine (multiset difference is not rewritten)
  ghost var d1 := forall x | x in s && s[x] > t[x] :: x < 200;
  assert d0 == d1 by {
    assert forall x :: x in s - t <==> x in s && s[x] > t[x];
  }

  // multiset display
  if * {
    // bad, since these quantifiers have no triggers
    ghost var c0 := forall x | x in multiset{2, 3, 5} :: x < 200; // warning: no trigger
    ghost var c1 := forall x | x in (multiset{2, 3, 5}) :: x < 200; // warning: no trigger
    assert c0 == c1;
  } else {
    // good
    var ms := multiset{2, 3, 5};
    ghost var c0 := forall x | x in ms :: x < 200;
    ghost var c1 := forall x | x in (ms) :: x < 200;
    assert c0 == c1;
  }
}
