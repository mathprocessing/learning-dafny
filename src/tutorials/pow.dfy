function pow2(n: int): int
  requires 0 <= n
{
  if n == 0 then 1 else 2 * pow2(n-1)
}

method ComputePow2(n: nat) returns (p: nat)
  ensures p == pow2(n)
  decreases n
{
  if n == 0 {
    p := 1;
  } else if n % 2 == 0 { // Err: postcondition may not hold on this return path
    p := ComputePow2(n / 2);
    p := p * p;
    // To disable error we need to add Lemma, just as "method call"
    // assume pow2(n) == pow2(n / 2) * pow2(n / 2); <=> to Lemma(n);
    Lemma(n);
  } else {
    p := ComputePow2(n - 1);
    p := 2 * p;
  }
}

// If you don't know has to prove lemma
// try to use attribute {:verify false}.
// Then you need to verify manually or write a bunch of tests
ghost method {:induction n} {:verify false} Lemma(n: nat)
  requires n % 2 == 0
  ensures pow2(n) == pow2(n / 2) * pow2(n / 2)
{
  if n != 0 { Lemma(n-2); }
}

method bunch_of_tests_for_lemma()
{
  // L(n) <==> Lemma(n)
  var L := n => n % 2 == 0 ==> (pow2(n) == pow2(n / 2) * pow2(n / 2));
  var s: set<nat> := {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 45};
  assert forall k:nat | k in s :: L(k);
  assert L(45); // Verificaion Succeded
  // assert L(46); // Verificaion Failed
  // 45 is maximum number that Dafny can check?
  // How to fix this?
  // Use another language?
  // Or tweak some settings of prover/dafny?

  // Why dafny not support for automatically finding that MAX_VALUES?
}