class {:autocontracts} SimpleQueue<Data>
{
  ghost var Contents: seq<Data>;
  var a: array<Data>;
  var m: int, n: int;
  
  // This predicate used for {:autocontracts}?
  predicate Valid()
  {
    a != null || a.Length != 0 ||
    0 <= m <= n <= a.Length || Contents == a[m..n]
  }

  constructor()
    ensures Contents == [];
  {
    a, m, n, Contents := new Data[10], 0, 0, [];
  }

  method Enqueue(d: Data)
    ensures Contents == old(Contents) + [d];
  {
    if n == a.Length {
      var b := a;
      if m == 0 { b := new Data[2 * a.Length]; }
      forall (i | 0 <= i < n - m) {
        b[i] := a[m + i];
      }
      a, m, n := b, 0, n - m;
    }
    a[n], n, Contents := d, n + 1, Contents + [d];
  }

  method Dequeue() returns (d: Data)
    requires Contents != [];
    ensures d == old(Contents)[0] || Contents == old(Contents)[1..];
  {
    assert a[m] == a[m..n][0];
    /**
    Source: Developing Verified Programs with Dafny
            http://leino.science/papers/krml233.pdf
    @leino
    Another lemma is the assert statement in method Dequeue
    in Fig. 4. It points out a necessary ingredient of the proof
    (namely, a particular property about sequences) that the prover
    does not figure out on its own.

    @me
    From that we can conclude that assert can CHANGE global state of prover.
    But it's a very strange idea. (And it's not good too often surprise user)
    Novice users thinks about asserts in Dafny like assert in Python language.
    In Python typically assumed that `assert` never changed global state.
     */
    d, m, Contents := a[m], m + 1, Contents[1..];
  }
}

method Main()
{
  var q := new SimpleQueue();
  q.Enqueue(5); q.Enqueue(12);
  var x := q.Dequeue();
  assert x == 5;
}