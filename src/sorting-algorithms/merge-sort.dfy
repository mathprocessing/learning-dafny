predicate sorted(a: array<int>)
{
  forall i, j :: 0 <= i < j < a.Length ==> a[i] <= a[j]
}

method merge(x1: array<int>, x2: array<int>, y: array<int>)
  requires x1.Length + x2.Length == y.Length
{
  
}

method mergesort(a: array<int>)
  ensures sorted(a);
{
  var n := a.Length;
  if n <= 1 {
    return;
  }
  var m := n / 2;
  var left := new int[m];
  assert left.Length == m;
  var right := new int[n-m];
  assert right.Length == n-m;
  var i := 0;
  assert i == 0;
  assert m == a.Length / 2;
  while i < m
    invariant i <= a.Length / 2
    invariant forall k :: 0 <= k < i ==> left[k] == a[k]
    decreases m - i
  {
    left[i] := a[i];
    i := i + 1;
  }
  assert i == m;
  while i < n
    invariant i <= n;
    invariant forall k :: m <= k < i ==> right[k - m] == a[k]
    decreases n - i;
  {
    right[i - m] := a[i];
    i := i + 1;
  }
  assert i == n;
  assert left.Length + right.Length == a.Length;
  merge(left, right, a);
}