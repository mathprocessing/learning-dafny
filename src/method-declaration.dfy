/**
method {:att1}{:att2} M<T1, T2>(a: A, b: B, c: C)
returns (x: X, y: Y, z: Z)
requires Pre
modifies Frame
ensures Post  
decreases Rank
{
Body
}
*/

/**
If method don't have body
Dafny assumes that the `ensures` clauses are true without proof.
 */

method {:axiom} f<T>(a: T, b: T) returns (c: bool)
  ensures a == b
;

method example<T>(a: T, b: T)
  
{
  var u := f(a, b);
  // TODO:
  // u == ?
}