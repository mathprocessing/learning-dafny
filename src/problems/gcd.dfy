// Used source example: https://github.com/dafny-lang/dafny/blob/master/Test/VerifyThis2015/Problem2.dfy

predicate divisor(d: int, n: int)
  requires d > 0 && n > 0
{
  n % d == 0
}

predicate {:opaque} {:verify false} divisor'(d: int, n: int)
  requires d > 0 && n > 0
  ensures divisor(d, n) <==> divisor'(d, n)
{
  assert divisor(1, 1) <==> divisor'(1, 1);
  assume exists d' :: (d' > 0 && d' == 1);
  exists d' :: d' > 0 && MulTriple(d, d', n)
}

predicate MulTriple(n: int, d: int, a: int)
  requires n > 0 && d > 0
{
  n * d == a
}

lemma {:induction a} OneDividesAnything(a: int)
  requires a > 0
  ensures divisor'(1, a)
{
  // to disable {:opaque} of `divisor'` predicate we need to use `reveal` method
  // i.e. reveal is just for making definition of function public and says: `ignore {:opaque} attribute in that particular case`
  reveal divisor'();  // here, we use the definition of Divides
  assert MulTriple(a, 1, a);
}

predicate prime'(n: nat)
{
  n > 1 && forall d {:matchingloop} {:matchinglooprewrite false} | 1 < d < n :: !divisor(d, n)
}

predicate prime(n: int)
  requires n >= 0
  ensures prime(n) <==> prime'(n)
  // ensures prime(n) ==> forall d | 1 < d < n :: !divisor(d, n)
{
  n >= 2 && forall d | d > 0 :: divisor(d, n) ==> (d == 1 || d == n)
}

predicate coprime(a: int, b: int)
  requires a > 0 && b > 0
{
  forall d | d > 0 :: (divisor(d, a) && divisor(d, b)) ==> d == 1
}

function gcd(a: int, b: int): int
  requires a > 0 && b > 0
  ensures gcd(a, b) > 0
{
  // Just a maximum common divisor
  if a == b then
    a
  else
    if a > b then
      gcd(b, a)
    else
      assert b - a > 0; // from a < b
      gcd(a, b - a) 
}

method test_gcd()
{
  assert gcd(3, 5) == 1;
  assert gcd(2, 4) == gcd(4, 2) == 2;
  assert gcd(6, 10) == 2;
  assert gcd(6, 9) == 3;
  assert exists n | n > 0 :: gcd(1, n) == 1;
  assert !forall n | n > 0 :: gcd(1, n) != 1 by {
    assert gcd(1, 1) == 1;
  }
}

// We don't need induction to prove this lemma
lemma {:induction false} GcdEqual(a: int)
  requires a > 0
  ensures gcd(a, a) == a
{
  // Proved by definition
}

// We don't need induction to prove this lemma
lemma {:induction false} GcdSymmetry(a: int, b: int)
  requires a > 0 && b > 0
  ensures gcd(a, b) == gcd(b, a)
{
  // Proved by definition
}

lemma two_div(m: nat)
  ensures m % 2 == 0 && m >= 2 ==> divisor(2, m)
{}

lemma three_div(m: nat)
  ensures m % 3 == 0 && m >= 3 ==> divisor(3, m)
{}

lemma n_div_inst(n: nat, m: nat)
  requires n >= 1
  ensures m % n == 0 && m >= n ==> divisor(n, m)
{}

lemma n_div(n: nat)
  ensures forall m | 1 <= n <= m :: m % n == 0 ==> divisor(n, m)
{}

method test_prime_2(n: nat)
{
  assert prime'(n) <==> prime(n);
  assert n <= 1 ==> !prime(n);
  assert prime(2);
  assert prime(3);

  // If we prove that `prime(4) == true ==> contradiction` then prover can figure out rest?
  // From `P_decidable: P and !P ==> false`
  // if n % 2 == 0 && n > 0 {
  //   assert divisor(2, n);
  //   // assert prime(n) <==> ;
  // }
  
  // assert () {
  //   assert divisor(2, m);
  // }

  /*
  forall i | 0 <= i < a.Length ensures true; {
    var j := i+1;
    assert j < a.Length ==> a[i] == a[j];
  }
  */
  assert forall n, m:nat {:trigger divisor} :: prime(m) ==> 1 < n < m ==> !divisor(n, m);

  assert prime(4) == false by {
    if prime(4) {
      assert forall n {:trigger divisor(n, 4)} :: 1 < n < 4 ==> !divisor(n, 4);
      assert divisor(2, 4);
      assert false;
    }
  }
  assert prime(5);
  assert !prime(6) by { n_div_inst(2, 6); }
  assert prime(7);
  assert prime(8) == false by { two_div(8); } // assert by is really cool syntax idea to writing proofs that can be complicated

  // `if * {}` syntax makes this two ways intependent from each other

  if * { // first way to prove
    assert prime(9) == false by { 
      assert divisor(3, 9);
    }
  } else { // another way to prove
    assert prime(9) == false by { 
      n_div_inst(3, 9);
    }
  }
}

method {:verify false} test_prime(n: nat)
{
  assert n <= 1 ==> !prime(n);
  assert prime(2);
  assert prime(3);
  assert prime(4) == false; // fails
  assert prime(5);
  assert prime(6) == false; // fails
  assert prime(7);
  assert prime(8) == false; // fails
  assert prime(9) == false; // fails
}

lemma {:induction a} gcd_one_left_inst(a: int)
  requires a > 0
  ensures gcd(1, a) == 1
  decreases a
{
  if gcd(a, 1) == 1 {
    assert gcd(1, a) == 1;
  } else {
    assert gcd(a, 1) > 1;
    assert a - 1 > 0; // i.e. we can pass a-1 as argument to gcd function
    // assert gcd(a, 1) == gcd(a - 2, 1); // Failed
    assert gcd(a, 1) == gcd(a - 1, 1); // Ok
    gcd_one_left_inst(a - 1);
    assert gcd(a - 1, 1) == 1;
  }
}

lemma gcd_one_left()
  ensures forall a | a > 0 :: gcd(1, a) == 1
{
  forall a | a > 0 { gcd_one_left_inst(a); }
}

lemma gcd_range()
  ensures forall a, b :: 1 <= a <= b ==> 1 <= gcd(a, b) <= b

lemma gcd_one_imp_coprime()
  ensures forall a, b | 1 <= a <= b :: gcd(a, b) == 1 ==> coprime(a, b)
{
  forall a, b | 1 <= a <= b ensures gcd(a, b) == 1 ==> coprime(a, b) {
    if (a <= 5 && b <= 5) {
      if a == 4 {
        // GcdSymmetry(4, b);
        // gcd_range();
        // assert 1 <= gcd(4, b) <= 4;
      } else if (a == 5) {
        
      } else {
        assert 1 <= a <= 3;
        assert 1 <= b <= 5;
        gcd_range();
        assert 1 <= gcd(a, b) <= 5;
        if (a == 1) {
          assert gcd(a, b) == 1 by {
            gcd_one_left();
          }
        } else {
          assert 2 <= a <= 3;
          assert 2 <= b <= 5;
        }
      }
    } else {
      assume false;
    }
  }
}

lemma coprime_self_div(a: int)
  requires a > 1
  ensures divisor(a, a) ==> !coprime(a, a)
{}

lemma coprime_self(a: int)
  requires a > 0
  ensures coprime(a, a) <==> a == 1
{
  if a == 1 {} else {
    assert a > 1;
    coprime_self_div(a);
  }
}

lemma gcd_succ_right(n: int)
  requires n > 0
  ensures gcd(n, n+1) == 1

lemma test(a: int, b: int)
  requires a > 0 && b > 0
  requires coprime(a, b)
  ensures divisor(2, a) && divisor(2, b) ==> false;
{

}

function lt_bool(a: bool, b: bool): bool
{
  if a == false then b else false
}

lemma lt_bool_imp()
  ensures forall a:bool, b:bool :: lt_bool(a, b) <==> !(b ==> a)
{

}

lemma divisor_exists(x: int, y: int)
  requires x > 0 && y > 0
  ensures divisor(x, y) ==> exists d :: x * d == y
// ensures exists k | k == 1 :: x * d == y
// MulTriple(n, d, a) <==> n * d == a



lemma divisor_add_right(x: int, y: int)
  requires x > 0 && y > 0
  ensures divisor(x, y) ==> divisor(x, y + x)
{
  // intro H : divisor(x, y)
  if divisor(x, y) {

    // assume !(forall k:nat :: y != k * x ==> k == 0);
    // assert !(forall k:nat :: !(k > 0 ==> y == k * x));
    // assert exists k:nat :: k > 0 ==> y == k * x by {
    //   divisor_exists(x, y);
    // }
  }
  assume false;
}

lemma divisor_add(x: int, y: int, m: int)
  requires x > 0 && y > 0 && m > 0
  ensures divisor(m, x) && divisor(m, y) ==> divisor(m, x + y)
{
  // Check some simple cases (we can skip this proof for first time by checking only simple cases)
  // if x < y && y < 5 {
  //   // OK
  // } else {
  //   assume false;
  // }
  // Let's write precise proof
  if m == 1 {

  } else {
    if x <= y {
      if m >= x {
        assert m > x ==> divisor(m, x) == false;
        if m == x {
          assert divisor(x, y) ==> divisor(x, y + x) by {
            divisor_add_right(x, y);
          }
        }
      } else {
        assert 2 <= m <= x <= y;
        ghost var lhs := divisor(m, x - m) && divisor(m, y) ==> divisor(m, x + y);
        ghost var rhs := divisor(m, x) && divisor(m, y) ==> divisor(m, x + y);
        assume lhs ==> rhs;
        assume false;
      }
    } else {
      divisor_add(y, x, m);
    }
  }
}

// Interesting that proving only left case `divisor(gcd(a, b), a)` is harder
// because we use induction, decreases, ... because (TODO: explain details)
lemma gcd_is_divisor_harder(a: int, b: int)
  requires a > 0 && b > 0
  ensures divisor(gcd(a, b), a)

lemma gcd_is_divisor(a: int, b: int)
  requires a > 0 && b > 0
  ensures divisor(gcd(a, b), a) && divisor(gcd(a, b), b)
  decreases a, b // to check that decreases works use `assume [expr from ensures]`
{
  if a == b {
    assert divisor(gcd(a, a), a) by {
      assert gcd(a, a) == a;
      assert divisor(a, a); // from a % a == 0 by div_self
    }
  } else if a < b {
    // split goal.
    assert divisor(gcd(a, b), a) by {
      gcd_is_divisor(a, b - a);
      assert divisor(gcd(a, b - a), a);
      // assert divisor(gcd(a, b - a), b - a); // I think we don't need this case
      assert divisor(gcd(a, b - a), a) ==> divisor(gcd(a, b), a) by {
        // if and only if + rewrite
        assert divisor(gcd(a, b - a), a) <==> divisor(gcd(a, b), a) by {
          assert gcd(a, b - a) == gcd(a, b);
        }
      }
    }
    assert divisor(gcd(a, b), b) by {
      assert divisor(gcd(a, b - a), b - a) by {
        gcd_is_divisor(a, b - a);
      }
      assert divisor(gcd(a, b - a), a) by {
        gcd_is_divisor(a, b - a);
      }
      divisor_add(a, b - a, gcd(a, b - a));
      // generalize step 1:
      // divisor(m, a) && divisor(m, b - a) ==> divisor(m, b)
      // generalize step 2:
      // divisor(m, x) && divisor(m, y) ==> divisor(m, x + y)
    }
  } else {
    assert b < a;
    assert divisor(gcd(b, a), a) by {
      gcd_is_divisor(b, a);
    }
    assert divisor(gcd(b, a), a) ==> divisor(gcd(a, b), a) by {
      assert gcd(b, a) == gcd(a, b);
    }
  }
}

lemma {:induction m, n} gcd_imp_divs(m: int, n: int)
  requires m > 0 && n > 0
  ensures gcd(m, n) == 2 ==> divisor(2, m) && divisor(2, n);
{
  if m == n {} else
  if m < n {
    gcd_one_left();
    if m == 1 {
      assert gcd(m, n) == 1;
    } else {
      assert m > 1;
      if gcd(m, n) == 2 { // intro H
        assert m < n - 1 by {
          if m == n - 1 {
            assert gcd(n-1, n) == 2;// from introduced hypothesis `gcd(m, n) == 2`
            // Iteresting: just assert expression proves contradiction, but without this assert contradiction not proved
            assert gcd(n-1, n) == 1; // from gcd defintion
          }
        }

        // from gcd(m, n) == 2 ==> exists k :: m == 2*k, n == 2*k
        assume exists k :: m == 2*k;
        assume exists l :: n == 2*l;
        assert divisor(2, m);
        assert divisor(2, n);
      }
    }
  } else {
    gcd_imp_divs(n, m);
  }
}

lemma {:verify false} coprime_imp_gcd_one_inst(a: int, b: int)
  requires 1 <= a <= b
  ensures coprime(a, b) ==> gcd(a, b) == 1
{
  if a == b {
    assert coprime(a, a) ==> a == 1 by {
      if a > 1 {
        // assert coprime(1, 1) == true;
        // assert coprime(a, a) <==> forall d | d > 0 :: divisor(d, a) ==> d == 1;
        // assert forall d | d > 0 :: divisor(d, a) ==> (d == 1 || d == a || 1 < d < a);
        // assert divisor(2, 2); // Last step before idea of `coprime_self_div`
        // assert exists d | d > 1 :: divisor(d, 2);
        // assert !forall d | d > 0 :: divisor(d, 2) ==> d == 1;
        // assert !coprime(2, 2); // Target
        coprime_self(a);
      }
    }
    assert coprime(a, a) ==> gcd(a, a) == 1;
  } else {
    if coprime(a, b) { // analog of intro H : coprime a b in Coq
      if b < 10 {
        // assert gcd(2, 3) == 1;
        // assert gcd(5, 9) == gcd(4, 5) == 1;
        // assert gcd(7, 8) == 1;  // Succeded
        // We need gcd(n-1, n) == 1?
        gcd_succ_right(a);
        assert gcd(4, 6) == 2;
        assert gcd(5, 7) == 1; // Succeded
      } else {
        assert divisor(2, a) && divisor(2, b) ==> gcd(a, b) == 2; // not general fact
        // Stupid thing... precondition is false but i forget to check it...
        assert !(divisor(2, a) && divisor(2, b));
        assert !(divisor(2, a) && divisor(2, b));
        // Let's use lt for booelan to squash two states to one
        // lt a b in booleans equivalent to !(b ==> a)
        if !(divisor(2, b) ==> divisor(2, a)) {
          // In that state user know that assert divisor(2, {a|b}) will checks
          // And possible write assert divisor(2, _); to auto filling hole
          // assert divisor(2, _) && _ == {a, b}; Can be checkable
          assert divisor(2, b);
          assert !divisor(2, a);
          // assert forall m, n | m > 0 && n > 0 :: gcd(m, n) == 2 ==> divisor(2, m) && divisor(2, n); // FAILED
          assert gcd(a, b) != 2;
        } else {
          assume false;
        }
        
        assume false;
      }
    }
  }
}

/**
predicate coprime(a: int, b: int)
  requires a > 0 && b > 0
{
  forall d | d > 0 :: (divisor(d, a) && divisor(d, b)) ==> d == 1
}

function gcd(a: int, b: int): int
  requires a > 0 && b > 0
  ensures gcd(a, b) > 0
  ensures exists k :: a == gcd(a, b) * k
  ensures a % gcd(a, b) == 0
  ensures divisor(gcd(a, b), a)
{
  // Just a maximum common divisor
  if a == b then
    a
  else
    if a > b then
      gcd(b, a)
    else
      assert b - a > 0; // from a < b
      gcd(a, b - a) 
}
 */

method test_coprime()
{
  assert coprime(2, 3) == true;
  assert forall d :: d == 2 ==> (divisor(d, 2) && divisor(d, 4));
  assert coprime(2, 4) == false; // Prover can't figure out without hint assertion
  assert coprime(2, 5) == true;
  assert coprime(2, 6) == false;
  assert coprime(2, 7) == true;
  assert coprime(2, 8) == false;
  assert coprime(3, 1) == true by {
    assert divisor(1, 3);
    assert divisor(3, 3);
    assert divisor(1, 1);
    assert forall d :: d == 3 ==> (divisor(d, 3) && divisor(d, 9));
  }
}

lemma divisor_self()
  ensures forall n | n > 0 :: divisor(n, n);
{
  // Proved by definition
}

// Created just to hide divisor definition, it helps to ensure that some proof
// is correct by hiding part of interface
module Hidden {

lemma divisor_self_instance(n: nat)
  requires n > 0 //         ^^^^^^ variable as argument
  ensures divisor(n, n)

lemma divisor_self()
  ensures forall n | n > 0 :: divisor(n, n)
{ //            ^^^ variable under quaitifier
  // Super cool! It works! We can prove general quatified statements from lemma instance
  // (with variables as arguments like `divisor_self_instance(n: nat)`
  forall n | n > 0 ensures divisor(n, n) { //                 ^^^^^^ variable as argument
    divisor_self_instance(n);
    assert divisor(n, n);
  }
}

predicate divisor(d: int, n: int)
  requires d > 0 && n > 0

}

method test_divisor()
{
  assert !divisor(3, 5);
  assert divisor(1, 6);
  assert divisor(2, 6);
  assert divisor(3, 6);
  assert !divisor(4, 6);
  assert !divisor(5, 6);
  
  ghost var n := 6;
  assert divisor(n, n);

  assert Hidden.divisor(n, n) by {
    Hidden.divisor_self();
    // equivalent to
    // assume forall n | n > 0 :: Hidden.divisor(n, n);
  }
}

method Main()
{
  ghost var m :| m > 0;
  if m == 2 {
    assert m + m == 4;
  } else {
    assert m == 1 || m >= 3;
  }  
}