type Digit = x: int | 0 <= x <= 9

predicate puzzle(x: Digit, y: Digit)
{
  x + y >= 12 && x * y <= 27
}

/**
If you have idea how to elegantly solve some puzzle you can express
this by if/while statements in Dafny.
*/
// method puzzle_proof_1(x: Digit, y: Digit)
// {
//   // Press F7 to show solutions
//   assume puzzle(x, y);
//   if x < y {
//     assert false; // x = 3, y = 9
//   } else {
//     assert false; // x = 9, y = 3
//   }
// }


/**
To verify (or distinguish from other) that some branch of proof is right
we need to block some axioms i.e. use very minimal subset of axioms,
and try to minimize it dynamically through writing proof.

For example:
If we know that y <= x && x <= 9 then
(ONLY) from transitivity we conclude that y <= 9
But if we assume all context(set) of hypotheses we probably have
more  strict constraint on y for example 0 <= y <= 9.

How to verify concrete proof tree in Dafny?
Or create es
*/
predicate P(x: nat, y: nat, a: nat)

// A wrapper for predicate P, holds it definition, we use it because we want to hide definition of P from global scope
predicate P_def(x: nat, y: nat)
{
  forall a : nat :: P(x, y, a) == (x * y <= a * a / 4)
}

// Commutativity of first two arguments in P
lemma {:verify true} P_comm(x: nat, y: nat)
  ensures forall a : nat :: P(x, y, a) == P(y, x, a)
{
  // We assume two cases to helps Rewriter
  assume P_def(x, y);
  assume P_def(y, x);
  // Or we can create general definition using forall:
  // assume forall n, k :: P_def(n, k); // FAILED, TODO: Why this failed?
}

lemma {:verify false} mul_add_prop_helper(x: nat, y: nat)
  // add {:verify false} for disabling verification, it helps to improve speed of veirfication
  // and also you don't need to delete body of function/method to make it axiom
  ensures forall a : nat :: x + y == a ==> P(x, y, a)
  // ensures x == y // Uncomment this to see relationships between lemmas
  decreases x, y
{
  assume P_def(x, y);
  // Without assumption `x < y` (from symmetry x replaced by y and vise versa) Verification Fails
  // INteresting that we can use this assumption implicitly by adding if statement with empty body
  if x < y {
    
  } else {
    if x > y {
      mul_add_prop_helper(y, x); // swap x and y and "move/delegate" this branch to `x < y`
    } else {
      assert x == y;
      assert forall a : nat :: 2 * x == a ==> x * x <= a * a / 4;
      assert x * x <= (2 * x) * (2 * x) / 4;
        assert x * x == (2 * x) * (2 * x) / 4;
      assert x * x <= x * x; // By le_refl: forall x: x <= x;
    }
  }
}

lemma mul_add_prop(x: Digit, y: Digit)
  ensures forall a : nat :: x + y == a ==> P(x, y, a)//x * y <= a * a / 4
  // ensures x == y // Uncomment this to see relationships between lemmas
{
  // Add commutativity lemma
  P_comm(x, y);

  // Bad thing: How to make solver failing when lemma used wrongly?
  // For example: mul_add_prop_helper(x, x) is wrong but Dafny don't helps as with it status (holds/not holds).
  // Maybe still exists a solution like substituting x * y <= a * a / 4 with some general predicate P(x, y, a) or something like this
  // Ok this works, but we also need something like "wrapper" around our resulting lemma in that chain of lemmas (that we try to use),
  // with assumption that `P(x, y, a) == x * y <= a * a / 4`

  // Just `mul_add_prop_helper(x, y);` also works but it's not interesting for learning
  mul_add_prop_helper(y, x);

  // Interesting: Why `mul_add_prop_helper(y, x);` not works?
  /// Answer because commutativity of P(x, y, a) is not obvious...
}

method puzzle_proof_2(x: Digit, y: Digit)
{
  // Press F7 to show solutions
  // Idea of proof: if we have upper bound for x * y then is not bood to have lower bound also.
  // assert x * y <= 27 ==> (x in {1, 3, 9, 27} && y in {1, 3, 9, 27});

  /**
  x + y == 12
  x y    x*y
  0 12   0
  1 11   11
  2 10   12
  3 9    27
  4 8    32
  5 7    35
  6 6    36
  
  x + y == 12 ==> x * y <= 36
  @Generalize
  forall a :: x + y == a ==> x * y <= a * a / 4
   */
  mul_add_prop(x, y);
  assume forall a : nat :: P(x, y, a) <==> x * y <= a * a / 4;
  
  assert forall a :: x + y == a ==> x * y <= a * a / 4;
  // from Digit type constraint:
  assert x + y >= 12 && x <= y ==> x >= 3;

  assume puzzle(x, y);
  assert x >= 3 && y >= 3;
  // if x < y {
  //   assert false; // x = 3, y = 9
  // } else {
  //   assert false; // x = 9, y = 3
  // }
}

predicate F(n: nat)

lemma {:induction m} ind(m : nat)
  ensures F(m)
{
  assume F(0);
  assume forall n:nat {:trigger F} :: F(n) ==> F(n+1);
  // ind(m-1); DON't UNCOMMENT, this launches fast memory consumption by SAT-solver
  // assume forall n:nat {:matchingloop} {:matchinglooprewrite false} :: F(n) ==> F(n+1);
}


method puzzle_while_idea()
{
  
}