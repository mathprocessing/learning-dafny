datatype List<A> = Nil | Cons(head: A, tail: List<A>)
datatype Tree<T> = Empty | Branch(data: T, left: Tree<T>, right: Tree<T>)

const max := (a: nat, b: nat) => if a < b then b else a

// Why it's succseeded?
// predicate is_nil<T>(xs: List<T>)
// {
//   // Empty have type of Tree != List, Danfy must throw error
//   match xs case Empty => true 
// }

function Map<T>(func: T -> T, xs: List<T>): List<T>
{
  match xs
    case Nil => Nil
    case Cons(x, xs') => Cons(func(x), Map(func, xs'))
}

// For more short syntax
function seq_to_list<T>(s: seq<T>): List<T>
{
  if |s| == 0 then
    Nil
  else
    Cons(s[0], seq_to_list(s[1..]))
}

predicate is_nil<T>(xs: List<T>)
{
  match xs case Nil => true case _ => false
}

function length<T>(xs: List<T>): nat
{
  match xs case Nil => 0 case Cons(_, xs') => 1 + length(xs')
}

function append<T>(xs: List<T>, ys: List<T>): List<T>
{
  match (xs, ys)
    case (Nil, _) => ys
    case (Cons(x, xs'), ys) => Cons(x, append(xs', ys))
}

method test_append()
{
  var a := Cons(1, Cons(2, Nil));
  var b := Cons(3, Cons(4, Nil));
  assert append(a, b) == Cons(1, Cons(2, Cons(3, Cons(4, Nil))));
  assert append(a, b) == seq_to_list([1, 2, 3, 4]);
}

method test_length<T>(xs: List<T>)
{
  // assert length(xs) == length(xs.tail) + 1; // Counterexamle: xs = Nil
  assert !is_nil(xs) ==> length(xs) == length(xs.tail) + 1 == 1 + length(xs.tail);
  assert length(xs) == 0 <==> is_nil(xs);

  // var ys: List<T> := Nil;
  // assert is_nil(xs) ==> length(xs) == length(Nil); // Resolver: the type of this expression is underspecified
  // assert is_nil(xs) ==> length(xs) == length(ys); // OK

  // Assumption
  assume length(xs) == 1;
  assert length(xs.tail) == 0;
  // assert exists x: nat :: u == Cons(x, Nil);
}

// Tree methods

predicate is_empty<T>(t: Tree<T>)
{
  match t case Empty => true case _ => false
}

function height<T>(t: Tree<T>): nat
{
  if is_empty(t) then 0 else 1 + max(height(t.left), height(t.right))
}

function traverse<T>(t: Tree<T>): List<T>
{
  match t
    case Empty => Nil
    case _ => append(traverse(t.left), Cons(t.data, traverse(t.right)))
}

