method MakeArray() returns (a: array<int>)
  ensures a.Length > 0
  // To fix error in Main() add this line
  ensures fresh(a) // signals that "array `a` is newly allocated"
{
  return new int[10];
}

method Main()
{
  var a := MakeArray();
  a[0] := 0;
  // error: assignment may update an array element not in the enclosing context's
  // modifies clause
}
