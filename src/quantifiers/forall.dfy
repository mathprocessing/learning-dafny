function sum_range_f_zero(a: nat, b: nat): nat
  requires a <= b
  decreases b - a
{
  if a == b then
    0
    // substitute func(_:nat) == 0
  else
    sum_range_f_zero(a+1, b)
    // substitute func(_:nat) == 0 and use add_left_zero: 0 + n = 0
}

method test_sum_range_f_zero(b: nat)
{
  assert sum_range_f_zero(0, 0) == 0;
  assert sum_range_f_zero(0, 1) == 0;
  assert sum_range_f_zero(0, 2) == 0;
  assert sum_range_f_zero(0, 33) == 0; // OK
  // assert sum_range_f_zero(0, 34) == 0; // FAILED 
  // How to increase 33 to 34?
  // * Change SAT-solver settings
  // * Change dafny setting

  // I think 33 it's really small number to limit reductions?
  // TODO Feature: If that number is reached dafny can print some warning like `lemma can be proved if you increase max_value of NUMBER=33`
  // Solver must magnet without user to prove some set of thorems for infinite sets (using unduction) becouse it more simpler
  // storing in memory `forall n: nat :: P(n)` instead of storing in memory `P(0) && P(1) && P(2) ...`


  // assert sum_range_f_zero(0, b) == 0 ==> sum_range_f_zero(0, b+1) == 0;
  // assert 0 <= b <= 3 ==> sum_range_f_zero(0, b) == 0;
}



// range inclusive `sum_range(f, a, a+2) == f(a) + f(a+1) + f(a+2)`
function sum_range(func: nat -> nat, a: nat, b: nat): nat
  requires a <= b
  decreases b - a
{
  if a == b then
    func(a)
  else
    func(a) + sum_range(func, a+1, b)
}

// lemma {:induction k} sum_range_zero(f: nat -> nat, k: nat)
//   requires forall n: nat :: f(n) == 0
//   ensures sum_range(f, 0, k) == 0
// {

// }

// method test_fg(f: nat -> nat, k :nat)
// {
//   assume forall n: nat :: f(n) == 0;
//   assert f(0) + f(1) + f(2) + f(3) + f(4) + f(5) + f(6) + f(7) == 0;
//   assert sum_range(f, 0, 7) == 0;

//   // Failed to prove
//   // assert sum_range(f, 0, k) == 0;

//   // Base case
//   assert sum_range(f, 0, 0) == 0;
//   // Induction hyp
//   assume forall m: nat {:trigger f} :: f(m) == f(m+1);
//   assert forall n: nat :: sum_range(f, 0, n) == 0 ==> sum_range(f, 0, n+1) == 0;
// }