/**
Can requires helps to add some hyps H1, H2, ... that don't provable by SAT,
and then helps to prove more complicated facts that stays on H1, H2, ...
 */

function repeat(value: nat, size: nat): seq<nat>
{
  if size == 0 then
    []
  else
    [value] + repeat(value, size - 1)
}

predicate increases(s: seq<nat>)
{
  if |s| <= 1 then true
  else
    if s[0] >= s[1] then
      false
    else
      increases(s[1..])
}

method test_inc2(s: seq<nat>)
{
  // assert s[0] >= s[1] || s[1] >= s[2];
  // assert increases([0, 0]);
  // assert !increases([0, 0]);
  // assert increases([0, 0, 0]); // FAILED
}

predicate ind_pr_old(n: nat, P: nat -> bool)
{
  (P(0) && (P(n) ==> P(n + 1))) ==> P(n)
}

predicate ind_pr(P: nat -> bool)
{
  // Without {:trigger P(0)} check_triggering fails, why?
  // Eearlier I'm thinking that :trigger arg just specifies some action, and helps to reduce time of logic resolving
  // But from "check_triggering fails" we can conclude that proposition above is false?
  // forall n: nat {:trigger P(0)} :: P(0) ==> (P(n) ==> P(n + 1)) ==> P(n)

  forall n: nat :: (P(0) && (P(n) ==> P(n + 1))) ==> P(n)
  // List of possible states:
  // {:trigger P(0)} OK
  //     NOTE for future versions of Dafny: this state can be just gussed or resolved backwards from target assertion 
  //         (resolving in arbitrary direction is more general and elegant solution)
  // 
  // {:trigger P(n)}   Verification Failed
  // {:trigger P(n+1)} Verification Failed
  // {:trigger P}      Verification Failed
}

method check_triggering(k: nat, P: nat -> bool)
  requires P(0)
  requires P(k) ==> P(k + 1)
  requires ind_pr(P)
{
  // assert P(0) ==> (P(k) ==> P(k + 1)) ==> P(k);
  /*
  P(0) ==> (P(k) ==> P(k + 1)) ==> P(k)
  let P(k) == true:
    P(0) ==> (true ==> P(k + 1)) ==> true
    P(0) ==> (P(k + 1)) ==> true
    P(0) ==> (P(k + 1) ==> true)
    P(0) ==> true
    true

  let P(k) == false:
    P(0) ==> (false ==> P(k + 1)) ==> false
    P(0) ==> (true ==> false)
    P(0) ==> false
    not P(0)
  */

  // assert P(k);
  // assert P(2);
}

method test_induction(n: nat)
  // Helps to prove increases(repeat(0, n))
  // requires forall m: nat :: increases(repeat(0, m))

  // Let's go some step backwards
  // requires forall P: nat -> bool :: P(0) ==> (P(n) ==> P(n + 1)) ==> P(n)
  requires increases(repeat(0, 0)) ==> (increases(repeat(0, n)) ==> increases(repeat(0, n + 1))) ==> increases(repeat(0, n))
  // We need lambdas https://github.com/dafny-lang/dafny/blob/master/Test/hofs/Lambda.dfy
  // requires ind_pr((m: nat) => increases(repeat(0, m)))
  // requires ind_pr_old(n, (m: nat) => increases(repeat(0, m)))
  // requires ind_pr_old(n, (m: nat) => increases(repeat(0, m)))
{
  // Must fails if in definion of increases changed `>` to `>=`
  assert increases(repeat(0, n));
  // assert false; Fails in two cases

  // Let's prove `forall n :: increases(repeat(0, n))` using induction:
  // 1. Base case
  assert increases(repeat(0, 0));
  // 2. Induction hypothesis
  assert increases(repeat(0, n)) ==> increases(repeat(0, n + 1)); // Failed if in definion of increases changed `>` to `>=`

  // Induction principle: Fails => let's add it to `requires`!
  // assert forall P: nat -> bool :: P(0) ==> (P(n) ==> P(n + 1)) ==> P(n);

  assert increases([42]);
  assert increases([5, 6]);
  assert increases([1,2,3]);
  // assert !increases([1,0]); // Simplest !increases case
  // assert !increases([1,2,3,2]);
  
  // If all elements in s equal to each other then increases(s) == true
  // Simple partial case
  assert increases([0,0,0]);
  // Simple partial case, rewritten
  assert increases(repeat(0, 3));
  // General case
  // assert increases(repeat(0, n));

  assert forall m, k: nat :: m == k ==> repeat(0, m) == repeat(0, k);

} 