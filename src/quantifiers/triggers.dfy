method test_fails(P: nat -> bool, Q: nat -> bool)
  requires forall x {:trigger P(x)} :: P(x) && Q(x)
  ensures Q(0)
{
  // Check fails because :trigger points to P(x) not to Q(x)
}

method test_fails_2(P: nat -> bool, Q: nat -> bool)
  requires forall x {:trigger Q(x)} :: P(x) && Q(x)
  ensures Q(0)
{
  // OK
}

method test_ok(P: nat -> bool, Q: nat -> bool)
  requires forall x {:trigger P(x)} :: P(x) && Q(x)
  ensures Q(0)
{
  assert P(0);
  // OK
}