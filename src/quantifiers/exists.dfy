method Main()
{
  assert exists n: nat :: n == 0; // Rewriter: /!\ No terms found to trigger on. Assertion may not hold.
  // Let's add triggers
  assert exists n: nat {:trigger n} :: n == 0; // Verification Failed (but without red highlighting)
  assert exists n:nat | n < 2 :: n == 0; // Verification Failed
}