/**
Can requires helps to add some hyps H1, H2, ... that don't provable by SAT,
and then helps to prove more complicated facts that stays on H1, H2, ...

Note: Now all three cases of induction principle works fine:
ind_pr, ind_pr_2, ind_pr_3
 */

function repeat(value: nat, size: nat): seq<nat>
{
  if size == 0 then
    []
  else
    [value] + repeat(value, size - 1)
}

predicate increases(s: seq<nat>)
{
  if |s| <= 1 then true
  else
    if s[0] > s[1] then
      false
    else
      increases(s[1..])
}

predicate increases_ge(s: seq<nat>)
{
  if |s| <= 1 then true
  else
    if s[0] >= s[1] then
      false
    else
      increases_ge(s[1..])
}

predicate ind_pr(P: nat -> bool)
{
  // Actions:
  // I tried all possible combinations of placing {:trigger P} near after variable type like that: `forall k:nat {:trigger P} :: ...`
  P(0) && (forall k: nat :: P(k) ==> P(k + 1)) ==> (forall n: nat {:trigger P(0)} :: P(n)) // WORKS, because P(0) is MAGIC THING...
  //  (forall k: nat :: P(0) && P(k) ==> P(k + 1)) ==> (forall n: nat :: P(n))
}

predicate ind_pr_2(n: nat, P: nat -> bool)
{
  P(0) ==> (forall m: nat :: P(m) ==> P(m + 1)) ==> P(n)
}

predicate ind_pr_3(P: nat -> bool)
{
  forall n: nat {:trigger P(0)} :: (P(0) ==> (forall m: nat :: P(m) ==> P(m + 1)) ==> P(n))
}

method try_prove_induction_principle(P: nat -> bool)
{
  // assert P(0) && (forall k: nat :: P(k) ==> P(k + 1)) ==> (forall n: nat :: P(n));
}

method check_triggering(k: nat, P: nat -> bool)
  requires P(0)
  requires forall r: nat :: P(r) ==> P(r + 1)
  requires ind_pr(P)
{
  assert P(k);
  assert P(42);
}

method check_triggering_2(k: nat, P: nat -> bool)
  requires P(0)
  requires forall r: nat :: P(r) ==> P(r + 1)
  requires ind_pr_2(k, P)
  requires ind_pr_2(42, P)
{
  assert P(k);
  assert P(42); // If line `requires ind_pr_2(42, P)` is commented out ==> Fails, because we explicity provide an argument `k`
  // But we can fix it by adding `requires ind_pr_2(42, P)`, you can uncomment that line to see result
}

method check_triggering_3(k: nat, P: nat -> bool)
  requires P(0)
  requires forall r: nat :: P(r) ==> P(r + 1)
  requires ind_pr_3(P)
{
  assert P(k);
  assert P(42);
}

// TODO: statuses of APPLY INDUCTION must be right
method test_induction_gr_equal(n: nat)
  // requires ind_pr((m: nat) => increases(repeat(0, m))) // OK
  // requires ind_pr_2(n, (m: nat) => increases(repeat(0, m))) // OK
  requires ind_pr_3((m: nat) => increases(repeat(0, m))) // OK
{
  // TODO: statuses of APPLY INDUCTION must be right

  // if operation is `>=`

  assert !increases_ge([0, 0, 0]); // not increases_ge
  // 1. Base case
  assert increases_ge(repeat(0, 0)); // OK
  // 2. Induction hypothesis
  // assert increases_ge(repeat(0, n)) ==> increases_ge(repeat(0, n + 1)); // Fails

  // APPLY INDUCTION MUST HAVE STATUS: FAIL
  // assert increases_ge(repeat(0, n)); // Fails
}

method test_induction_gr(n: nat, P: nat -> bool)
  // requires P(0) && (forall k: nat :: P(k) ==> P(k + 1)) ==> (forall n: nat :: P(n)) // FAILS
  // requires increases(repeat(0, 0)) ==> (forall m: nat :: increases(repeat(0, m)) ==> increases(repeat(0, m + 1))) ==> increases(repeat(0, n)) // OK
  // requires ind_pr((m: nat) => increases(repeat(0, m))) // OK
  // requires ind_pr_2(n, (m: nat) => increases(repeat(0, m))) // OK
  requires ind_pr_3((m: nat) => increases(repeat(0, m))) // WORKS  FINE!!! We must use trigger: P(0), but why not just P? or P(n)?
{
  // if operation is `>`

  assert increases([0, 0, 0]); // OK
  // 1. Base case
  assert increases(repeat(0, 0)); // OK
  // 2. Induction hypothesis
  assert forall m: nat :: increases(repeat(0, m)) ==> increases(repeat(0, m + 1)); // OK

  // APPLY INDUCTION MUST HAVE STATUS: OK
  assert increases(repeat(0, n)); // OK
}