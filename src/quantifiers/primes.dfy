type Digit = x: int | 0 <= x <= 9
type Flag = x: int | 0 <= x <= 1

predicate P(n: int)
predicate Q(n: int)

function prime(x: int): bool
  requires x >= 0
  ensures x >= 0

function flip(x: Flag): Flag
  ensures flip(x) != x

/** Properties
Transitivity: d(a, b) ==> d(b, c) ==> d(a, c)
@ d(a, b) && d(b, a) ==> |a| == |b|

@ 
*/
predicate divisor(m: nat, x: nat)
{
  1 <= m <= x && x % m == 0
}


method Main(x: Digit, y: Digit)
{
  assert flip(0) == 1;
  assert flip(1) == 0;
  // var s: seq<nat> := seq<nat>(i => i);

  if * { // What `*` means?
    assert 5 == 5;
  } else {

  }
  // var dset: seq<nat> := set(divisor(x, 2));
  assert !divisor(0, 2);
  // assert forall y :: y in s ==> y < 2;
}

method forall_exists()
{
  assert (!forall m :: P(m)) <==> exists m :: !P(m);
  assert (!forall m :: P(m)) <==> exists m :: (P(m) ==> false);
  assert (forall m :: !P(m)) <==> (!exists m :: P(m));
}

/**
coprime x y =
  !exists m :: 1 < m <= min(x, y) ==> (x % m == 0 && y % m == 0)

  // D(x) is a set of divisors, i.e. m such that divisor(m, x)

  forall m: m in D(x) || m in D(y) ==> m == 1

  gcd(x, y) == 1

  D(x) & D(y) == {1}
    where `&` is a set intersection

prime x <==> D(x) == {1, x}

Lemma: prime x || prime y ==> x != y ==> coprime x y
 */