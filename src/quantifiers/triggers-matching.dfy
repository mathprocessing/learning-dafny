predicate R(i: int)

// lemma loopForall()
// {
//   assume R(0);
//   assume forall i: nat :: i >= 0 ==> R(i) ==> R(i+1);
//   assert R(100); // FAILS
// }

lemma loopForall_2()
{
  assume R(0);
  // ... {:matchinglooprewrite true} ... FAILS
  assume forall i: nat {:matchingloop} {:matchinglooprewrite false} :: i >= 0 ==> R(i) ==> R(i+1);
  assert R(100); // for all values <= 100: OK, but for 101 FAILS
  assert R(101); // OK only if previous assert uncommented
  assert R(200);
  assert R(300);
  assert R(400);
  assert R(500);
  assert R(600);
  assert R(700);
  assert R(800);
  // ... I think we can do it infinitely. But really?
  // In this example dafny works strange, not obvious for novice but fine
}

lemma testExists_1()
{
  assume R(0);
  assume !R(1);
  assume R(2);
  assume !R(3);
  assume R(4);
  assume !R(5);
  assume !R(6); // Comment this and see that task becomes unsolvable, because `exists i :: R(i) == R(i+1)` no more contant `true` (i.e. it becomes undetermined true || false)
  assert exists i :: R(i) == R(i+1); // In this example dafny works fine
}

lemma testExists_assume()
{
  assume exists i :: R(i) && R(i+1) && !R(i+2);
  // i=0: 110**... j=1: 1(10)**...
  // i=1: *110*... j=2: *1(10)*...
  // i=2: **110... j=3: **1(10)...
  // ...
  // i=+inf: *****... j=i+1
  assert exists j :: R(j) && !R(j+1);
  // 
}

// This maybe hard task to prover
lemma testExists_assume_harder()
{
  // Without {:matchingloop} attribute Succeeds but probably take more step iterations
  // ... {:matchinglooprewrite true} ... Falls into an infinite loop
  assume exists i {:matchingloop} {:matchinglooprewrite false} :: R(i) && R(i+1) && (forall k | k > i+1 :: !R(k));
  // i=0: 11000... j=1: 1(10)00...
  // i=1: *1100... j=2: *1(10)0...
  // i=2: **110... j=3: **1(10)...
  // ...
  // i=+inf: *****... j=i+1
  assert exists j :: R(j) && !R(j+1);
}

/* 
If we want to automate changing/mutating attribute values we need something like A* heuristic for proof goal
to make predictions about branches/processes and it distance to goal.
Process 1 [have Dist1]: {:matchinglooprewrite false} . . . . . Dist1 < Dist2 "Decide to gain more computation power to Process 1"
Process 2 [have Dist2]: {:matchinglooprewrite true}  . . . . . .
                                                   ^^^^^^^^^^^^
                                                   Just time/iteration steps
Dist1: Heuristic distance from Process1.state to goal.state
Dist2: Heuristic distance from Process2.state to goal.state
*/

lemma testExists_assume_harder_2()
{
  // Without {:matchingloop} attribute Succeeds but probably take more step iterations
  // ... {:matchinglooprewrite true} ... Falls into an infinite loop
  assume exists i {:matchingloop} {:matchinglooprewrite false} | i >= 0 :: R(i) && R(i+1) && (forall k  | k < i+1 :: !R(k));
  // i=0: 11***... j=1: 1(10)**...
  // i=1: 011**... j=2: 01(10)*...
  // i=2: 0011*... j=3: 001(10)...
  // ...
  // i=+inf: *****... j=i+1
  assert exists j | j >= 0 :: R(j) && !R(j+1);
}

predicate P(n: int)
{
  n == 0
}

lemma exists_nat_numbers(n: int)
{
  // assert exists n :: n == 0; // Fails
  // Try to add some assumption
  assert P(0);
  assert exists n :: P(n);
  // i.e. We need a wrapper around `n == 0` and give hint to solver that `P(0)` is true
}