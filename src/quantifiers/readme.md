# Exists
In dafny exists have very strange behaviour:
see `exists.dfy` in this directory.

Even simple things not automated:

```dafny
assert exists n: nat :: n < 3;
// Statement above not proves automatically, this is very painful.
```