include "./expressions.dfy"

function method applyAndSelf(e: bex): Option<bex>
{
  match e
    case And(l, r) => if eq(l, r) 
                        then Some(l)
                        else None
    case _ => None // None means rule not applied
}