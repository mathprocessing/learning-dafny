include "./expressions.dfy"
include "./erules.dfy"

lemma testEval()
{
  var e := And(Const(true), Const(false));
  assert eval(e) == false;
  var a, b: bool;
  assert eval(And(Const(a), Const(b))) <==> a && b;
  // Test some simplification rule:
  var beforeSimplification := And(Const(a), Const(false));
  var afterSimplification := Const(false);
  assert sameEval(beforeSimplification, afterSimplification);
}
lemma testRules()
{
  if case true =>
  {
    var e := And(Const(false), Const(false));
    assert applyAndSelf(e).value == Const(false);
  } case true =>
  {
    var u;
    var e := And(u, u);
    assert applyAndSelf(e).value == u;
  } case true => 
  {
    var e := And(Const(false), Const(true));
    assert applyAndSelf(e).None?;
  }
}

lemma testNumberOfNodes()
{
  var e := And(Const(false), Const(false));
  assert e.nodes() == 3;
  var b;
  e := Const(b);
  assert e.nodes() == 1;
}
