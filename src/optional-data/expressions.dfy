include "./types.dfy"

datatype Expr<T> = Id(left: Expr) // Added for fun! :), because we can prove/write something easier/compactly if we use `Id`
              | Not(left: Expr)
              | Const(value: T)
              | Or(left: Expr, right: Expr)
              | And(left: Expr, right: Expr)
              | Xor(left: Expr, right: Expr)
              | Imp(left: Expr, right: Expr)
{
  // number of nodes
  function method nodes(): nat
  {
    match this
      case Id(e) => 1 + e.nodes()
      case Not(e) => 1 + e.nodes()
      case Const(val) => 1
      case _ => 1 + this.left.nodes() + this.right.nodes()
  }

  function method nargs(): nat
  {
    if this.Id? || this.Not? || this.Const?
    then 1
    else 2
  }

  lemma {:induction false} NodesGeOne()
  ensures this.nodes() >= 1
  {}

  lemma {:induction false} NodesOneImpConst()
  requires this.nodes() == 1
  ensures this.Const?
  {}

  lemma {:induction this} NodesTwoImpIdOrNotConst()
  requires this.nodes() == 2
  ensures this.Id? || this.Not?
  ensures this.left.Const?
  {
    var b := this.left.value;
    var solution1 := Id(Const(b));
    var solution2 := Not(Const(b));
    assert this == solution1 || this == solution2;
    // WISH: just write `assert this in {Id(Const(b)), Not(Const(b))}` and everything works for arbitrary number of elements in set.
  }

  lemma {:induction false} NargsSuccEqNodesImp()
  requires 1 + this.nargs() == this.nodes()
  ensures 2 <= this.nodes() <= 3
  {
    if case this.nargs() == 1 => {
      assert this.nodes() == 2;
      assert this.Id? || this.Not? by { NodesTwoImpIdOrNotConst(); }
    } case this.nargs() == 2 => {
      assert this.nodes() == 3;
    }
  }

  lemma {:induction this} NodesGeNargs()
  // WISH: some general mechanism (manually we can use exists see: "string_repr.dfy") to check that `A >= B + n` can be improved to `A >= B + n + 1`
  ensures this.nodes() >= this.nargs()
  {
    if this.nargs() == 2 {} else {
      // This if statement answers on question: "Why next expression is not lemma: `this.nodes() >= 1 + this.nargs()`"
      if this.nodes() == 1 {
        assert this.Const?; // this assert needs `{:induction this}`
        assert this.nodes() == this.nargs();
        // WISH: write somehow this in Dafny:
        // META_ASSERT !EXPREQ(this.nodes() == this.nargs(), this.nodes() == 1 + this.nargs());
      }
    }
  }
}

// Boolean expression
type bex = Expr<bool>

function method eval(e1: bex): bool {
  match e1
    case Id(e) => eval(e)
    case Not(e) => !eval(e)
    case Or(a, b) => eval(a) || eval(b)
    case And(a, b) => eval(a) && eval(b)
    case Xor(a, b) => !(eval(a) <==> eval(b))
    case Imp(a, b) => eval(a) ==> eval(b)
    case Const(val) => val
}

// Equivalnce of evaluation results
predicate method sameEval(e1: bex, e2: bex) {
  eval(e1) <==> eval(e2)
}

// Equivalence of terms (Exact equality of AST's)
predicate method eq(e1: bex, e2: bex) {
  e1 == e2
}

lemma test()
{

}