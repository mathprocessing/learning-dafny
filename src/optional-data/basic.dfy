include "./types.dfy"

method {:verify false} test()
{
  var r: Stream<int>;
  var s: Stream<int> := More(1, More(42, r));
  assert s.rest.elem == 42;
  assert s.rest.rest == r;
}

// lemma loopImpliesLoopTwice(s: Stream) returns (res: int)
// // requires s.rest == s
// // ensures s.rest.rest == s
// // requires res == 1
// {
//   var i := 0;
//   var c := s;
//   while c.rest == c {
//     i := i + 1;
//     c := c.rest;
//   }
//   res := i;
// }

type onat = Option<nat>

function add(a: onat, b: onat): onat
{
  if a.None? || b.None? then None
  else Some(a.value + b.value)
}

predicate formula(P: bool, Q: bool, R: bool): (res: bool)
{
  if P then true
  else if Q then false
  else R
}

lemma FormulaSearch(P: bool, Q: bool, R: bool, res: bool)
requires formula(P, Q, R) <==> res
{
  calc <==> {
    if P then true else if Q then false else R;
    !P ==> if Q then false else R;
    !P ==> ((Q ==> false) && (!Q ==> R));
    !P ==> (!Q && (!Q ==> R));
    !P ==> (!Q && R); // Simpliest formula that I found
    (!P ==> !Q) && (!P ==> R);
    (Q ==> P) && (!R ==> P); // Can we simplify this TO `_ ==> _ ==> _` or TO `(_ ==> _) ==> _`
    // `no formula exists for R ==> P ==> Q` by bruteforce
    // Goal: _ ==> _ ==> _  Fill holes and possible to add brackets `()`
    // Automate proof of `Expression steps` using Dafny library "expressions.dfy"
  }
}

predicate subset(a: onat, b: onat) {
  if b.None? then true
  else if a.None? then false
  else a.value == b.value
}

// subset(a, b) <==> (a != b ==> _)
// How to automate generation of this Hole? Or if it impossible we can show proof of that (or `smooth proof` that explains why this improssible).
// lemma subsetDef(a: onat, b: onat)
// ensures subset(a, b) <==> (a != b ==> )
// {}

method sub(c: onat, a: onat) returns (b: onat)
// ensures c == add(a, b) // we can write method using equality, instead we use `subset`
ensures subset(c, add(a, b))
{
  if c.None? || a.None? {
    b := None;
    return;
  }
  assert c != None && a != None;
  if c.value >= a.value {
    b := Some(c.value - a.value);
  } else {
    b := None;
  }
}

lemma testSub()
{

}