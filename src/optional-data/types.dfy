datatype List<T> = Nil | Cons(head: T, tail: List)
codatatype Stream<T> = More(elem: T, rest: Stream)
datatype Option<T> = None | Some(value: T)