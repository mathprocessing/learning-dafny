datatype Event = Empty | Message (s: string)

method Main(e: Event)
{
  match e 
    case Empty => { assume false; }
    case _ => { }
  /**
    All match statement equivalent to
    `assume e != Empty;`
  */
  assert e != Empty;
}