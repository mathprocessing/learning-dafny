// const a: int[]; is wrong syntax
// const a: array<int> := new int[3]; also wrong
// TODO: Can we define array as a const?
//       Maybe not because it uses heap allocation?

method array_example()
{
  var arr := new int[3](i => i + 1);
  // see https://stackoverflow.com/questions/50421068/dafny-assigning-sequence-slice-to-array
  // and introduction from this
  // https://dafny-lang.github.io/dafny/OnlineTutorial/guide.html
  assert forall i: nat :: i < arr.Length ==> arr[i] == i + 1;
}

/** 10.5.1 Sequences and array, page 78/242
Sequences and arrays are indexable and have a length.
So the idiom to iterate over the contents is well-known.
*/
method ArraySum(a: array<int>)
{
  var i := 0;
  var sum := 0;
  while i < a.Length
    invariant i <= a.Length
    decreases a.Length - i
  {
    sum := sum + a[i];
    i := i + 1;
  }
}

method SequenceSum(a: seq<int>)
{
  var i: nat := 0;
  var sum: int := 0;
  while i < |a|
    invariant i <= |a|
    decreases |a| - i
  {
    sum := sum + a[i];
    // TODO: assert sum == 
    i := i + 1;
  }
}

/**
Sets

union:        +
difference:   -
intersection: *
disjointness: !!

subset: <, <=
superset: >, >=

cardinality: |s|
membership: e in s, e !in s = !(e in s)

Multisets

multiplicity of `e` in `s`: s[e]
change of multiplicity:     s[e := n]

Sequences
seq(5, i => i*i) equivalent to [0, 1, 4, 9, 16]
|s|: nat          sequence length
s[i]: T           sequence selection
s[i := e]: seq<T> sequence update
e in s            membership
s[lo..hi]         subsequence
s[lo..], s[..hi]  drop, take
s[slices]: seq<seq<T>> slice
multiset(s)       sequence convertion to a `multiset<T>`
 */
method sequence_example()
{
  // var s: set<int> := {};
  var t: seq<seq<real>> := [3.14, 2.7, 1.41, 1985.44, 100.0, 37.2][1:0:3];
  assert |t| == 3;
  assert t[0] == [3.14];
  assert t[1] == [];
  assert t[2] == [2.7, 1.41, 1985.44];
  var u: seq<seq<bool>> := [true, false, false, true][1:1:];
  // type infer feature: seq<Joker()> or seq<_> can be helpful
  assert |u| == 3;
  assert u[0] + u[1] == [true, false];
  // assert u[2] == [false, true] == seq(u, i => i*i);
}

method example_of_user_helping()
{
  var n: nat;
  assert n >= 0;
  var i, start: int;
  var a, b: nat;
  i := start;
  if (i > a && i < b) {
    i := i + 1;
    assert a < b - 1;
  } else {
    assert i < b ==> i <= a;
    assert i > a ==> i >= b;
    if (i > a) {
      assert i >= b;
      i := i - 1;
    }
  }
  assert (start == i) || (start == i-1) || (start == i+1);
  /**
  Example of helping #2:
  e1 in E
  e2 in E
  ...
  e1 = (start == i) || (start == i-1) || ...;
  e2 = (start == i) || (start == i-1) || (start == i);
  e3 = (start == i) || (start == i-1) || (start == i) || (start == i+1);
  ...
  S = set() ==> S0 != S1 != S2 != ...
  general = (start == i+S0) || (start == i+S1) || ... || (start == i+Sn)

  User defined:
  1. possible actions: [
    apply("=="),
    apply("+"),
    apply("||"),
    place(i),
    place(start),
  ]

  2. filter for states: 
    lambda state: state in E // expr must be in E
  */
  assert (start == i-1) ==> a < b - 1;

  
  /**
  Example helping to user:
  assert (start == i[operation][int]) ==> a < b - 1;
  resolves to
  assert (start == i-$H) ==> a < b - 1;
  where $H <= 1
  and if we select strongest clause
  from $H == 1, $H == 2, ...
   we endup with:
  assert (start == i-1) ==> a < b - 1;

  What if siblings of start_Expr is undetermined?
  How we calculate siblings(start_Expr)?
  * Define general function siblings(e: expr)?

  User может перепутать две переменные местами в надежде получить верное
  утверждение, как ему правильно подсказать?
  1. намутировать несколько копий Expr чтобы показать верные варианты
  (бесконечный список выражений) отсортированный по возрастанию расстояния от start_Expr
  среди этих копий будет находится верное выражение
  2. Стоит заметить что часто пользователь нуждается в нескольких ограничениях
  на объект: выражение не только должно быть верным, но и соответствовать по смыслу
  варианты смысла:
  * выражение есть часть шага некоторого доказательства
  * выражение демонстрирует идею как начать доказательство, рассуждение
  * выражение является "более сильным" чем его "сверстники" сходного типа
    Пример: `i > a` "более сильно" чем `i > a - 1`,
    так из одного "сильного" следует всё множество "слабых братьев" выражения
   */
}