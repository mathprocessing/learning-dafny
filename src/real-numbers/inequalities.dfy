/*
1. x > 0 -> x > sin(x)

2. x > 1 -> x^2 > sin(x)

3. sin(x) + cos(x) > 3/2

*/

const pi: real

lemma PiValue()
ensures 3.14 < pi < 3.15

function sin(x: real): real
function cos(x: real): real

lemma sinBound(x: real)
ensures -1.0 <= sin(x) <= 1.0

lemma cosBound(x: real)
ensures -1.0 <= cos(x) <= 1.0

lemma sinHordeBound(x: real, dx: real)
ensures -dx <= sin(x + dx) - sin(x) <= dx

lemma cosHordeBound(x: real, dx: real)
ensures -dx <= cos(x + dx) - cos(x) <= dx

lemma sinZero()
ensures sin(0.0) == 0.0

lemma cosZero()
ensures cos(0.0) == 1.0

lemma sinToCos(x: real)
ensures cos(x) == sin(pi / 4.0 - x)

lemma Pif(x: real)
ensures sin(x) * sin(x) + cos(x) * cos(x) == 1.0

function D(x: real): real
{
  cos(x) - 1.0
}

lemma first(x: real)
requires x > 0.0
ensures x > sin(x)
{
  if x > 1.0 {
    sinBound(x);
  } else {
    // Can we prove without using derivative?
    // (sin(x) - x)' = cos(x) - 1 <= 0
    assert cos(x) - 1.0 <= 0.0 by {
      cosBound(x);
    }
    assume D(x) <= 0.0 ==> sin(x) - x <= 0.0;
    // assert -dx <= sin(x + dx) - sin(x) <= dx;
    assert sin(x) - x <= 0.0;
    assert x >= sin(x);
    if x == 0.0 {
      assert x == sin(x) by { sinZero(); }
    } else {
      // sin(0 + dx) - sin(0) in dx * [cos(0) - dx, cos(1)]
      // sin(0 + dx) - sin(0) in dx * [1 - dx, 1]
      // assume dx = 0.1
      // sin(0 + dx) - sin(0) in 0.1 * [0.9, 1]
      // sin(0.1) in [0.09, 0.1]
    }
    assume false;
  }
}

lemma {:induction false} third(x: real)
ensures sin(x) + cos(x) < 1.5
{
  // sin(x)^2 + 2*cos(x)*sin(x) + cos(x)^2 < 2.25
  // 1 + 2*cos(x)*sin(x) < 2.25  by Pif()
  // 2*cos(x)*sin(x) < 1.25
  // sin(2x) < 1.25
  // ==>
  // (sin(x) + cos(x))^2 < 1.5^2
  // |sin(x) + cos(x)| < 1.5
}

method test()
{
  assert 1.0 + 1.0 == 2.0;
  assert 0.5 - 0.5 == 0.0;
  assert -0.1 <= cos(0.1) - cos(0.0) <= 0.1 by { cosHordeBound(0.0, 0.1); }
  assert cos(0.0) == 1.0 by { cosZero(); }
  assert 0.9 <= cos(0.1) <= 1.0 by { cosBound(0.1); }
}
