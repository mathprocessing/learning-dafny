predicate divisor(d: int, n: int)
  requires d > 0 && n > 0

lemma divisor_self_instance(n: nat)
  requires n > 0 //         ^^^^^^ variable as argument
  ensures divisor(n, n)

lemma divisor_self()
  ensures forall n | n > 0 :: divisor(n, n)
{ //            ^^^ variable under quaitifier
  // Super cool! It works! We can prove general quatified statements from lemma instance
  // (with variables as arguments like `divisor_self_instance(n: nat)`
  forall n | n > 0 ensures divisor(n, n) { //                 ^^^^^^ variable as argument
    divisor_self_instance(n);
    assert divisor(n, n);
  }
}

// Demo of "proof branching"
method test_divisor()
{
  if * {
    assert divisor(6, 6) by {
      divisor_self();
      // equivalent to
      // assume forall n | n > 0 :: divisor(n, n);
    }
  } else {
    assert divisor(6, 6) by {
      // divisor_self();
      // equivalent to
      assume forall n | n > 0 :: divisor(n, n);
    }
  }
}