/**
If you see message: "assert might not hold".
You can fix this by:
1. Adding some requires. (If too much: Check that `assume false` holds, then delete some requires)
2. Deleting some ensures. (If too much: Your method X does not have sufficient specification, you can see it in other methods which uses method X)
*/

method FailingPostCondition_step_1(b: bool) returns (i: int)
  ensures 2 <= i
{
  var j := if !b then 3 else 1;
  if b {
    i := j;
    assert 2 <= i;// This assertion might not hold
    return;
  }
  i := 2;
}

/**
From DafnyRef.pdf May 11 2022, page 255
To debug why this assert might not hold we need to `move this assert up`,
which is similar to computing the weakest precondition.

For example, if we have `x := Y; assert F;` and the `assert F;` might not hold,
the weakest precondition for it hold before `x := Y;` can be written as the
assertion `assert F[x := Y];`, where we replace every occurence of `x` in `F` into `Y`.

Let's do it in our example:
*/

method FailingPostCondition_step_2(b: bool) returns (i: int)
  ensures 2 <= i
{
  var j := if !b then 3 else 1;
  if b {
    assert 2 <= j;// This assertion might not gold
    i := j;
    assert 2 <= i; // This holds! Because it assumes that all previous asserts (add lemmas) is TRUE.
    return;
  }
  i := 2;
}

method FailingPostCondition_step_3(b: bool) returns (i: int)
  ensures 2 <= i
{
  var j := if !b then 3 else 1;
  // Add weakest percondition
  assert b ==> 2 <= j; // This assertion might not gold
  if b {
    assert 2 <= j;
    i := j;
    assert 2 <= i;
    return;
  }
  i := 2;
}

method FailingPostCondition_step_4(b: bool) returns (i: int)
  ensures 2 <= i
{
  assert b ==> 2 <= (if !b then 3 else 1); // This assertion might not gold
  var j := if !b then 3 else 1;
  assert b ==> 2 <= j;
  if b {
    assert 2 <= j;
    i := j;
    assert 2 <= i;
    return;
  }
  i := 2;
}


/* At this point, this is pure logic. We can simplify the assumption:
b ==> 2 <= (if !b then 3 else 1)
!b || (if !b then 2 <= 3 else 2 <= 1)
!b || (if !b then true else false)
!b || !b;
!b;

Now we can understand what went wrong:
  When `b` is true, all of these formulas above are false, this is why
                    the Dafny verifier was not able to prove them.
*/




/**
Computing the weakest precondition is a - ....
 */