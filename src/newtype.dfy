newtype N = x: int | x <= 1
const value: N

function f(): N
{
  var result :| result <= 1;
  result
}

function inc(x: N): N
{
  if x < 1 then x + 1 else x
}

newtype int8 = x: int | -128 <= x < 128
const c: int8

method example()
{
  // assert -128 <= c < 128; // error: may violate type constraint
  // right bound (128) don't have on type int8
  // we need to explicitly convert `c` to type int firstly
  assert -128 <= c as int < 128;
  // or rewrite condition using (<=) operation:
  assert -128 <= c <= 127;
}