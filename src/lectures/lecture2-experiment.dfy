// Binary search: Let;s  write and verify it.

// Pre/post conitions

/**
Idea of binary search:
1. Given sorted array A.
   
  sorted = forall element in array :: element <= element.next()

2. Given some key input_key

3. Task: Find key k == input_key in array A.
  Key can be found by strategy `divide and conquier`
  * If key not in right half of array -> adjust search parameters (update search state)
  * If key not in left  half of array -> adjust search parameters (update search state)

4. Let's find some invariant for search
  hyp 1: left <= key <= right (INVARIANT)

  Begin of search: input_key in interval [0..L-1]
  End of search: input_key in {key} (some 1-element set)



 */

// Return type of key index can be Option<int>
method BinarySearch(a: array<int>, k: int) returns (pos: int, found: bool)
  ensures 0 <= pos <= a.Length
{
  // while(!key_found)
  // {
  //   var middle := 
  //   if (k < ) {

  //   }
  // }
}

method BinarySearchRec(a: array<int>, left: nat, right: nat, k: int) returns (pos: nat, found: bool)
  requires left <= right
  requires left <= a.Length
  requires right <= a.Length
  ensures left <= pos <= right
  decreases right - left
{
  if left >= right {
    pos := left;
    found := false;
  } else {
    var middle: nat := (left + right) / 2;
    if k < a[middle]
    {
      // if array is sorted: pos < middle
      pos, found := BinarySearchRec(a, left, middle, k);
    } else if k == a[middle] {
      pos := middle;
      found := true;
    } else {
      assert k > a[middle];
      // if array is sorted: pos >= middle
      pos, found := BinarySearchRec(a, middle + 1, right, k);
    }
  }
}

method linearSearch(a: array<int>, k: int) returns (pos: nat, found: bool)
  requires 0 <= a.Length <= 10
  ensures pos <= a.Length
  ensures found && 0 <= pos < a.Length ==> a[pos] == k
  ensures found == false ==> pos == 0
{
  var i: nat := 0;
  while i < a.Length
    invariant i <= a.Length
  {
    var value := a[i];
    if value == k {
      return i, true;
    }
    i := i + 1;
  }
  return 0, false;
}

method test_BinarySearchRec()
{
  var arr := new int[3](i => 0);
  // assert BinarySearchRec(arr, 0, 3, 1) == (1, true);
  var pos, found := linearSearch(arr, 0);
  assert pos == 0;
}