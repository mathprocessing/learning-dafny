/**
Dutch Flag is sorting algorithm

All elements in the array have only 3 colors (Red, Blue, White)
 Red     White   Unknown      Blue
0------r-------w----------b----------a.Length

Goal is to shrink Unkown region to nothing (minimize b - w).

*/
datatype Color = Red | White | Blue
// Reflexive comparison (i.e. holds this lemma le_refl: r <= r)
predicate Below(c: Color, d: Color) // <=> function Below(c: Color, d: Color): bool
{
  c == Red || c == d || d == Blue
}

method DutchFlag(a: array<Color>)
  // We allowed to modify elements of array and
  // outside this method all other methods know that elements of the array maybe modified
  modifies a
  ensures forall i, j :: 0 <= i < j < a.Length ==> Below(a[i], a[j])
  // Set of input elements equal to set of output element (or array equals up to permutation)
  // Convert element of array to sequence: `a[0..a.Length]`: seq<Color>
  // Multiset of elements in a po-state should equal to multiset of elements in a pre-state
  // po-state = after method execution (modifying array)
  // pre-state = before method execution (modifying array)
  ensures multiset(a[..]) == multiset(old(a[..]))
{
  var r, w, b := 0, 0, a.Length;
  while w < b
    invariant 0 <= r <= w <= b <= a.Length
    invariant forall i :: 0 <= i < r ==> a[i] == Red
    invariant forall i :: r <= i < w ==> a[i] == White
    invariant forall i :: b <= i < a.Length ==> a[i] == Blue
    invariant multiset(a[..]) == multiset(old(a[..]))
  {
    match a[w]
    case Red =>
      a[r], a[w] := a[w], a[r];
      r, w := r + 1, w + 1;
    case White =>
      w := w + 1;
    case Blue =>
      // a[b-1] := a[w];
      a[b-1], a[w] := a[w], a[b-1];
      b := b - 1;
  }
}

/** Exercise for lecture 3:
Make simpler sorting algorithm for example for booleans.
Booleans is a just 2 values (true, false) and you can sort it in a similar way.

More harder:
Use 4 colors.
 */