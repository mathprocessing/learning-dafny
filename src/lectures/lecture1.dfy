// Forall syntax
// assert forall a, b, c : nat :: a * (b + c) / 6 - a * b / 6 == a * c / 6;

// Loop invariants

/**
  Methods have assignments, loops, side effects in the heap but function
  is always terminates and never change state of the program.
*/
function Fib(n: nat): nat
{
  // body is an expression
  if n < 2 then n else Fib(n-2) + Fib(n-1)
}

// You can think about method as more fast implementation of Fib function
method ComputeFib(n: nat) returns (x: nat)
  // Post/Pre condition never be executed after compilation
  // You can freely use computationally slow specs
  ensures x == Fib(n) 
{
  // loop can have ANY number of execution sequences (iterations)
  // 0 <= iters <= max_iters
  // What is n? We would like to iterate that loop n times.
  // Base_hyp: 
  // invarint holds on FIRST iteration of the loop
  // Induction_hyp:
  // if inv holds on nth iteration ==> inv holds for (n+1)th iteration

  // Note: invariant holds just before first loop condition `i < n` and
  // just after loop end

  // 1. LET x == Fib(i) (because our loop must do something with Fib?)
  // 2. To make Base_hyp true we need to define x := 0
  x := 0;
  // 3. From y == Fib(i+1) and i == 0 (loop start) we have y == Fib(1)
  var y := 1; // Fib(1) == 1
  var i := 0;
  while i < n
    invariant 0 <= i <= n
    invariant x == Fib(i) && y == Fib(i+1)
    decreases n - i
  {
    // 4. Fib(i+1) == Fib(i) + Fib(i-1)
    // new_y == new_x + Fib(i-1)
    // new_y == new_x + old_x (or old_old_y)
    // new_y == old_y + old_x
    // new_x == old_y
    var old_y := y;
    y := y + x;
    // x == Fib(i) and Fib(i) == Fib(i-1) + Fib(i-2) == old_x + old_old_x
    // old_x + old_old_x is more complicated than old_y ==> we use old_y
    x := old_y;
    i := i + 1;
  }
  /**
  invariant x == Fib(i) && y == Fib(...
  At this point we have two choices:
  1. y = Fib(i-1), if i == 0 then we call Fib(-1) but -1 is not a natural number
    Next action:
    We try to make invariant more complicated
    i > 0 ==> y == Fib(i-1)
  2. y = Fib(i+1)
  */

  /* Цикл видит мир (внешнее состояние машины) со своей системы отсчета
  В каждой итерации может возникать своё ограничение на состояние машины
  R1, R2, R3, ...
  Если рассмотреть только те ограничения, которые выполняются независимо
  от номера итерации, то они суть инварианты.

  inv1 && inv2 && ... == Union of all iteration states
  inv1 && inv2 && ... == Solution[iter==0] || Solution[iter==1] || ...

  Перечение инвариантов цикла есть объединение состояний машины по всем итерациям

  Если вдруг инваринтов не достаточно для написаиня кода в цикле:
  Из рассуждения такого рода следует что например
  для одноого подмножества итераций цикла может быть одна группа инваритов, а
  для другого подмножества другая.
  Set A: iters % 2 == 0 ==> invA1 && invA2 && ... && invCommon1 && ...
  Set B: iters % 2 == 1 ==> invB1 && invB2 && ... && invCommon1 && ...
  invCommon1 присутствует как в первой так и во второй группе.

  Но на практике это эквивалентно одной группе так как можно просто дабавить
  ко всем инвариантам Precondition i % 2 == {0,1}
  где i переменная - счетчик итераций, её нужно будет создать перед циклом.

  */
  
}

function Sum(n: nat): nat
  decreases n
{
  if n == 0 then 0 else Sum(n-1) + n
}

function Sum_short(n: nat): nat {
  // Uncomment returned statement to check that this it is wrong because of n = 2
  // is counterexample: 2 * ((2 + 1) / 2) = 2 * (3 / 2) = 2 * 1 = 2
  // but Sum(2) = Sum(1) + 2 = Sum(0) + 1 + 2 = 0 + 1 + 2 = 3, 3 != 2

  // n * ((n + 1) / 2)
  n * (n + 1) / 2
}

lemma {:induction n} lemmaSum(n: nat)
  ensures Sum(n) == Sum_short(n)
{
  
}

// function SumQubic_mock(n: nat): nat
// {
//   // var k :| k >= 0; Bad idea becouse k interpreted as a constant
//   // k
// }

function SumQubic(n: nat): nat
{
  if n == 0 then 0 else SumQubic(n - 1) + Sum(n)
}

method check_SubQubic(n: nat)
{
  assert SumQubic(n+1) == SumQubic(n) + Sum(n+1);
  assert SumQubic(0) == 0;
  assert SumQubic(1) == SumQubic(0) + Sum(1);
}


function SumQubic_short(n: nat): nat
{
  n * (n + 1) * (n + 2) / 6
}

function Qdiff(n: nat): nat
{
  // SumQubic_short(n+1) - SumQubic_short(n)
  (6 * SumQubic_short(n+1) - 6 * SumQubic_short(n)) / 6
}



method testEqualitiesFromCalc(n : nat, a: nat, b: nat, c: nat)
  requires b <= 5
{

  // assert a * (b + c) / 6 - a * b / 6 == a * c / 6;

  // Counterexample a == 1, c == 1, b == 5  to check this create `requires b <= 0` and increase by 1 to some number
  // assert 1 * (b + 1) / 6 - 1 * b / 6 == 1 * 1 / 6;
  // assert (b + 1) / 6 - b / 6 == 1 / 6;
  assert 5 / 6 == 0;
  assert (5 + 1) / 6 == 1;
  // assert (5 + 1) / 6 - 5 / 6 == 5 / 6;
  // assert 1 == 0;


  // assert (n + 1) * (n + 2) * (n + 3) / 6 - n * (n + 1) * (n + 2) / 6 == (n + 1) * (n + 2) * (n + 3) / 6 - (n + 1) * (n + 2) * n / 6;
  //

  // a * (b + c) - a * b == a * c
  // let a = (n + 1) * (n + 2)
  // let b = n
  // let c = 3
  // (n + 1) * (n + 2) * (n + 3) - (n + 1) * (n + 2) * n == (n + 1) * (n + 2) * 3
  assert (n + 1) * (n + 2) * (n + 3) - (n + 1) * (n + 2) * n == (n + 1) * (n + 2) * 3;
  // assert (n + 1) * (n + 2) * (n + 3) / 6 - (n + 1) * (n + 2) * n / 6 == (n + 1) * (n + 2) * 3 / 6; // Cat't verify
  // assert (n + 1) * (n + 2) * 3 / 6 == (n + 1) * (n + 2) / 2; // OK
}

lemma QdiffEqSum(n: nat)
  // ensures Qdiff(n) == Sum_short(n+1)
  // ensures SumQubic_short(n+1) == Sum_short(n+1) + SumQubic_short(n)
  requires n < 100
{
  assert (n * (n+1)) % 2 == 0;
  // calc == {
  //   ((n + 1) / 3) * ((n + 2) / 6);
  //   ((n + 1) * (n + 2)) / 18;
  // }
  // calc == {
  //   Qdiff(n);
  //   SumQubic_short(n+1) - SumQubic_short(n);
  //   (n + 1) * (n + 2) * (n + 3) / 6 - n * (n + 1) * (n + 2) / 6;
  //   (n + 1) * (n + 2) * (n + 3) / 6 - (n + 1) * (n + 2) * n / 6;
  //   (n + 1) * (n + 2) * 3 / 6;
  //   (n + 1) * (n + 2) / 2;
  //   Sum_short(n+1);
  // }
}

method check_SumQubic(n: nat)
{
  
  assert SumQubic_short(0) == 0; // diff = 1 = 1
  assert SumQubic_short(1) == 1; // diff = 3 = 1 + 2
  assert SumQubic_short(2) == 4; // diff = 6 = 1 + 2 + 3
  assert SumQubic_short(3) == 10;


  assert [Sum(1), Sum(2), Sum(3), Sum(4), Sum(5)] == [1, 3, 6, 10, 15];
  assert Qdiff(0) == 1;
  assert Qdiff(1) == 3;
  assert Qdiff(2) == 6;
  // assert Qdiff(3) == 10; // Verification failed by timeout 15 sec (wtf?)
  // Let check number of steps that needs for calculation
  // 0. Qdiff(3) == 10
  // 1. (6 * SumQubic_short(4) - 6 * SumQubic_short(3)) / 6
  // 2. (6 * SumQubic_short(4) - 6 * SumQubic_short(3)) / 6
  // 3. (6 * 4 * (4 + 1) * (4 + 2) / 6 - 6 * 3 * (3 + 1) * (3 + 2) / 6) / 6
  //    (4 * 5 * 6 - 3 * 4 * 5) / 6 = (4 * 5 * (6 - 3)) / 6 = 10

  // assert [Qdiff(0), Qdiff(1), Qdiff(2), Qdiff(3), Qdiff(4)] == [1, 3, 6, 10, 15]; Can't verify if `Qdiff(n) = (6 * SumQubic_short(n+1) - 6 * SumQubic_short(n)) / 6`

  // assert Qdiff(0) == Sum_short(1);
  // assert Qdiff(1) == Sum_short(2);
  // assert Qdiff(10) == Sum_short(11);
  // assert Qdiff(20) == Sum_short(21);
  // Very long verifing, z3 need help
  // assert Qdiff(n) == Sum_short(n+1);
}

// method ComputeSum(n: nat) returns (result: nat)
//   ensures result == Sum(n)
// {
//   var i := 0;
//   result := 0;
//   while (i < n)
//   {
//     result := result + i;
//     i := i + 1;
//   }
// }