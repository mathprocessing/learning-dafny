// Press right button mouse menu to see this keys:
// F7 - Show counterexample
// F8 - Hide counterexample

// forall i, j :: 0 <= i ==> 0 < j < a.Length ==> i <= j ==> a[i] <= a[j]
method BinarySearch(a: array<int>, key: int) returns (r: int)
  // requires a.Length < 2 // need this for finding simplest counterexample (Press F7)
  // requires a.Length > 0 ==> key == a[0] + 4

  // sorted predicate
  requires forall i, j :: 0 <= i < j < a.Length ==> a[i] <= a[j]

  ensures 0 <= r ==> r < a.Length && a[r] == key
  ensures r < 0 ==> forall i :: 0 <= i < a.Length ==> a[i] != key
{
  var lo, hi := 0, a.Length;
  while lo < hi
    invariant 0 <= lo <= hi <= a.Length
    // Left "not here" zone
    invariant forall i :: 0 <= i < lo ==> a[i] != key
    // Right "not here" zone
    invariant forall i :: hi <= i < a.Length ==> a[i] != key
    decreases hi - lo
  {
    var mid := (lo + hi) / 2;
    if key < a[mid] {
      hi := mid;
    } else if (key > a[mid]) {
      // lo := mid; // fails to prove that loop ends (by decreases clause)
      // Counterexample: lo = 0, hi = 1, mid = 0, a.Length = 1, key > a[0]
      lo := mid + 1; // OK
    } else {
      r := mid;
      return;
      // return mid; // also fine in dafny
    }
  }
  // in our spec: ensures r < 0 ==> ... means we must return a negative value
  return -1;
}

/** Exercise for lecture 2:
Modify the binary search so that it would always return some indication between
0 and length of the array inclusive.
Where you would have inserted a key, if you want to insert it into the array.
In other words, you would like to return a value `r` such that `0 <= r <= a.Length` and
such that all values to the left of `r` are strictly smaller then a `key` (forall i :: i < r ==> a[i] < key)
and from all values to the right of `r` or either key are larger. (forall i :: i > r ==> a[i] > key)
*/