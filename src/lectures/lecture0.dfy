method Triple_(x: int) returns (r: int)
  ensures r == 3*x
{
  if (x == 0) {
    r := 0;
  } else {
    r := x * 3;
    assert r != 0;
  }
}

method Triple(x: int) returns (r: int)
  ensures r >= 3*x
{
  if x >= 0 {
    var y := Double(x);
    // dafny reason about Double from specification even if method `Double`
    // is not implemented but `ensures r == 2*x` exists.
    r := x + y;
  } else {
    var y := Double(-x);
    // r := x - y; wrong
    // assert r <= 3*x; // not equivalent to `ensures r >= 3*x`
    r := x + y;
  }
}

method Double(x: int) returns (r: int)
  requires 0 <= x // Pre condition (assumes that is always true for input args)
  ensures r >= 2*x // Post condition (after executing body)
{
  r := x + x;
}

method SumMax(x: int, y : int) returns (s: int, m: int)
  ensures s == x + y
  ensures x <= m && y <= m
  ensures m == x || m == y
{
  s := x + y;
  if x < y {
    m := y;
  } else {
    m := x;
  }
}

method SumMaxBackwards(s: int, m : int) returns (x: int, y: int)
  // Interesting: Spec for inv(SumMax) stays the same!
  ensures s == x + y
  ensures x <= m && y <= m
  ensures m == x || m == y
  // except this:
  requires s <= 2 * m
  ensures x <= y
{
  /**
  x == s - y
  x == 0 ==> s == y
    if y >= 0
      m == y && s == m
    else
      m == x && s < 0

    if m > 0
      m != x, m == y, s == m
    else
      ...

  if s == 10, m == 7
    (x, y) can be
      (<0, >10): m == y
        (0, 10): m == 10
         (1, 9): m == 9
         (2, 8): m == 8
         (3, 7): m == 7 solution
         (4, 6): m == 6
         (5, 5): m == 5
         (6, 4): m == 6
         (7, 3): m == 7 solution
         (8, 2): m == 8
         (9, 1): m == 9
        (10, 0): m == 10
      (>10, <0): m == x

      but (3, 7) and (7, 3) can glued to (3, 7) with assumption that x <= y
      => that means we can add `ensures x <= y`

  from x <= y we conclude that m == y, now `y` is knows but what about `x`?
  s == x + y 
  x == s - y

  m == x || m == y
  Cases (m == x || m == y) {
    m == x:
      x <= m && y <= m simps to
      [y <= m, s == m + y]
    m == y:
      x <= m && y <= m simps to
      [x <= m, s == x + m]
  }
  lemma: s <= 2 * m  => we can add this to Pre condition (requires)
  */
  
  y := m;
  x := s - m;
}