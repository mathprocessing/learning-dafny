// DafnyRef.pdf June 21, 2022: Page 181/300

lemma docalc(x : int, y: int)
ensures (x + y) * (x + y) == x * x + 2 * x * y + y * y
{
  calc {
  (x + y) * (x + y);
  ==
  // distributive law: (a + b) * c == a * c + b * c
  x * (x + y) + y * (x + y);
  ==
  // distributive law: a * (b + c) == a * b + a * c
  x * x + x * y + y * x + y * y;
  ==
  calc {
  y * x;
  ==
  x * y;
  }
  x * x + x * y + x * y + y * y;
  ==
  calc {
  x * y + x * y;
  ==
  // a = 1 * a
  1 * x * y + 1 * x * y;
  ==
  // Distributive law
  (1 + 1) * x * y;
  ==
  2 * x * y;
  }
  x * x + 2 * x * y + y * y;
  }
}