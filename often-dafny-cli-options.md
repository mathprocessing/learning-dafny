  # Verification options
```
  /dafnyVerify:<n>
      0 - stop after typechecking
      1 - continue on to translation, verification, and compilation
  /verifyAllModules
      Verify modules that come from an include directive
  /noCheating:<n>
      0 (default) - allow assume statements and free invariants
      1 - treat all assumptions as asserts, and drop free.
  /induction:<n>
      0 - never do induction, not even when attributes request it
      1 - only apply induction when attributes request it
      2 - apply induction as requested (by attributes) and also
          for heuristically chosen quantifiers
      3 - apply induction as requested, and for
          heuristically chosen quantifiers and lemmas
      4 (default) - apply induction as requested, and for lemmas
  /inductionHeuristic:<n>


  /autoTriggers:<n>
      0 - Do not generate {:trigger} annotations for user-level quantifiers.
      1 (default) - Add a {:trigger} to each user-level quantifier. Existing
                    annotations are preserved.
```

# z3 options
```
/noNLarith    Reduce Z3's knowledge of non-linear arithmetic (*,/,%).

/extractCounterexample
    If verification fails, report a detailed counterexample for the first
    failing assertion. Requires specifying the /mv option as well as
    /proverOpt:O:model_compress=false and /proverOpt:O:model.completion=true.

  /printModel:<n>
                0 (default) - do not print Z3's error model
                1 - print Z3's error model
  /printModelToFile:<file>
                print model to <file> instead of console
  /mv:<file>    Specify file to save the model with captured states
                (see documentation for :captureState attribute)
```

## Prover options
```
  /proverHelp   Print prover-specific options supported by /proverOpt.

  /errorLimit:<num>
                Limit the number of errors produced for each procedure
                (default is 5, some provers may support only 1).
                Set num to 0 to find as many assertion failures as possible.
  /timeLimit:<num>
                Limit the number of seconds spent trying to verify
                each procedure
  /rlimit:<num>
                Limit the Z3 resource spent trying to verify each procedure
  /errorTrace:<n>
                0 - no Trace labels in the error output,
                1 (default) - include useful Trace labels in error output,
                2 - include all Trace labels in the error output
```

# Compilation options
```
  /compile:<n>  0 - do not compile Dafny program
      1 (default) - upon successful verification of the Dafny
          program, compile it to the designated target language
          (/noVerify automatically counts as failed verification)
      2 - always attempt to compile Dafny program to the target
          language, regardless of verification outcome
      3 - if there is a Main method and there are no verification
          errors and /noVerify is not used, compiles program in
          memory (i.e., does not write an output file) and runs it
      4 - like (3), but attempts to compile and run regardless of
          verification outcome

  /compileTarget:<lang>
      cs (default) - Compilation to .NET via C#
      go - Compilation to Go
      js - Compilation to JavaScript
      java - Compilation to Java
      py - Compilation to Python
      cpp - Compilation to C++

      Note that the C++ backend has various limitations (see Docs/Compilation/Cpp.md).
      This includes lack of support for BigIntegers (aka int), most higher order
      functions, and advanced features like traits or co-inductive types.

  /compileVerbose:<n>
      0 - don't print status of compilation to the console
      1 (default) - print information such as files being written by
          the compiler to the console

  /out:<file>
      filename and location for the generated target language files
```

# Boogie options
```
  Multiple .bpl files supplied on the command line are concatenated into one
  Boogie program.

  /lib           : Include library definitions
  /proc:<p>      : Only check procedures matched by pattern <p>. This option
                   may be specified multiple times to match multiple patterns.
                   The pattern <p> matches the whole procedure name and may
                   contain * wildcards which match any character zero or more
                   times.
  /noProc:<p>    : Do not check procedures matched by pattern <p>. Exclusions
                   with /noProc are applied after inclusions with /proc.
  /noResolve     : parse only
  /noTypecheck   : parse and resolve only

  /print:<file>  : print Boogie program after parsing it
                   (use - as <file> to print to console)
  /pretty:<n>
                0 - print each Boogie statement on one line (faster).
                1 (default) - pretty-print with some line breaks.
```