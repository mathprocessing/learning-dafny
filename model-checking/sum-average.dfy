opaque function sum(n: nat): nat
{
  if n == 0 then 0 else n + sum(n - 1)
}

opaque function {:verify false} averageSum(n: int): nat
requires n > 0
{
  sum(n) / n
}

/*
Assume we know sum(n) / n for odd n.
How to find sum(n+1) / (n+1)?

sum(n+1) = n+1 + sum(n)

av(n+1) = (n+1 + sum(n)) / n+1 = 1 + sum(n) / (n+1) = 1 + k/n - 1 = k/n = av(n)   WTF??? Where difference between odd and even n?

k / (n+1) = k / n - k / (n * (n+1)) = k/n - 1

k / n - k / (n + 1) = (k * (n + 1) - k * n) / n (n + 1) = k / (n * (n+1))

sum(n) / n = sum(n) / (n+1) + 1


*/

method {:verify false} testsum()
{
  reveal sum(), averageSum();
  assert sum(3) == 6;
  assert averageSum(1) == 1;
  assert averageSum(3) == 2;
}

lemma ModMul(a: nat, b: nat)
requires b > 0
ensures (a * b) % b == 0
decreases a
{
  if a == 0 { assert 0 % b == 0; }
  else {
    calc == {
      (a * b) % b;
      (a * b) % b - 0;
      (a * b) % b - b % b;
      { 
        assert a * b >= b;
        assert a * b - b >= 0;
        assume false;
      }
      (a * b - b) % b;
      ((a - 1) * b) % b;
    }
    ModMul(a - 1, b);
  }
}

lemma SumN(x: nat)
// ensures (x * (x + 1)) % 2 == 0
ensures sum(x) == x * (x + 1) / 2

lemma {:verify true} AverageSumLinear(n: int)
requires n > 0
ensures averageSum(2*n-1) == n
{
  if n == 1 {
    assert averageSum(1) == 1 by { reveal sum(), averageSum(); }
  } else {
    assert averageSum(2*n-1) == sum(2*n-1) / (2*n-1) by { reveal sum(), averageSum(); }
    assert sum(2*n-1) / (2*n-1) == n by {
      assert sum(2*n-1) == (2*n-1) * (2*n) / 2 by { SumN(2*n-1); }
      calc == {
        sum(2*n-1);
        (2*n-1) * (2*n) / 2;
        n * (2*n - 1);
      }
      assert n >= 2;
      var k := (2*n - 1);
      assert k >= 3;
      calc == {
        (n * k) / k;
        {
          assert (n * k) % k == 0 by { ModMul(n, k); }
        }
        n;
      }
      assert sum(2*n-1) / (2*n - 1) == n;
    }

    // AverageSumLinear(n - 1);
    // assert averageSum(2*n-3) == n - 1;
    // assert sum(2*n-3) / (2*n - 3) == n - 1;
    // assert sum(2*n-3) == 2*n - 3 + sum(2*n-4);

    
  }
}

