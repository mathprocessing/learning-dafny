method test2(p: bool, q: bool, r: bool)
{
  if (p <==> (p || q)) && ((p || q) <==> (p || q || r)) {
    assert r ==> p;
    assert q ==> p;
  }
  if (p || q) <==> (p || q || r) {
    assert (r && !q) ==> p;
    assert r ==> (p || q);
    // assert (!r && !q) ==> p; don't cheks?
    if (r == false) && (q == false) {
      assert p;
    }
  }
}