function lmax(xs: seq<nat>): nat
requires |xs| >= 1
decreases xs
{
  if |xs| == 1 then xs[0] else (
    var m := lmax(xs[1..]);
    if xs[0] >= m then xs[0] else m
  )
}

predicate isBounded(f: seq<nat>)
{
  forall i | 0 <= i < |f| :: f[i] < |f|
}

type func = f: seq<nat> | isBounded(f) witness [0]

lemma FuncBounded(f: seq<nat>)
requires isBounded(f)
ensures forall j | 0 <= j < |f| :: f[j] < |f|

function comp(f: seq<nat>, g: seq<nat>): seq<nat>
requires |f| == |g|
requires isBounded(g)
{
  seq(|f|, i requires 0 <= i < |f| => f[g[i]])
}

// f(f(x))
function sq(f: seq<nat>): seq<nat>
requires isBounded(f)
{
  comp(f, f)
}

lemma Th1(f: func, i: int)
requires 0 <= i < |f|
ensures var s := sq(f); s[i] != i ==> f[i] != i
{}

method test(f: func, s: func)
requires |f| >= 2 && f[0] <= 1 && f[1] <= 1
requires s == sq(f)
requires forall i, j | 0 <= i < j < |f| :: f[i] != f[j]
ensures |s| >= 2
{
  assert (f[..2] == [0, 1]) || (f[..2] == [1, 0]);
  assert s[..2] == [0, 1];
  
}

method test2(f: func, s: func)
requires |f| >= 2 && f[0] <= 2 && f[1] <= 2
requires s == sq(f)
requires forall i, j | 0 <= i < j < |f| :: f[i] != f[j]
ensures |s| >= 2
{
  assert (f[..2] == [0, 1]) || (f[..2] == [1, 0]);
  assert s[..2] == [0, 1];
  
}