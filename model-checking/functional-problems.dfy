function {:verify false} lmax(xs: seq<nat>): nat
requires |xs| >= 1
decreases xs
{
  if |xs| == 1 then xs[0] else (
    var m := lmax(xs[1..]);
    if xs[0] >= m then xs[0] else m
  )
}

predicate isBounded(f: seq<nat>)
{
  (|f| >= 1 && lmax(f) < |f|) || |f| == 0
}

type func = f: seq<nat> | isBounded(f) witness [0]

lemma test()
{
  if exists k :: k > 0 {
    var k :| k > 0;
    assert k == k;
  }
}

lemma AnyLeMax'(f: seq<nat>)
requires isBounded(f)
ensures forall j | 0 <= j < |f| :: f[j] <= lmax(f)
{
  if exists j | 0 <= j < |f| :: f[j] > lmax(f) {
    var j :| 0 <= j < |f| && f[j] > lmax(f);
    assert |f| > 0 by { assert f[j] > f[0]; }
    
    assume false;
  }
}


lemma AnyLeMax(f: seq<nat>, j: int)
requires 0 <= j < |f|
requires isBounded(f)
ensures f[j] <= lmax(f)
// {
//   assert |f| > 0;
//   if |f| == 1 || j == 0 {
//     assert j == 0;
//     assert f[j] <= lmax(f);
//   } else {
//     var f': seq<nat> := f[1..];
//     assert |f'| == |f| - 1;
//     assert 0 <= j - 1 < |f'|;
//     assert f'[j - 1] <= lmax(f') by {
//       assert lmax(f') <= |f'|;
//       assert lmax(f') < |f'| by {
//         if lmax(f') == |f'| {

//           assume false;
//         }
//       }
//       AnyLeMax(f', j - 1);
//     }
//   }
//   // if j == 0 {
//   //   assert f[0] <= lmax(f);
//   // } else {
//   //   assert f[j - 1] <= lmax(f) by { AnyLeMax(f, j - 1); }

//   //   assume false;
//   // }
// }

lemma {:verify false} FuncBounded(f: seq<nat>, l: nat)
requires |f| == l
requires isBounded(f)
ensures forall j | 0 <= j < l :: f[j] < l
{
  forall j | 0 <= j < l ensures f[j] < l {
    calc { f[j]; <= { AnyLeMax(f, j); } lmax(f); < l; }
  }
}

function {:verify false} comp(f: seq<nat>, g: seq<nat>): seq<nat>
requires |f| == |g|
requires isBounded(g)
{
  var l := |f|;
  FuncBounded(g, l);
  seq(l, i requires 0 <= i < l => f[g[i]]) // assume 0 <= i < l && 0 <= g[i] < l; 
}