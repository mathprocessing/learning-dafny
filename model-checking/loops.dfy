function max(x: int, y: int): int {
  if x >= y then x else y
}

method ComputeMax(x: nat) returns (i: int)
ensures i == max(x, 3)
{
  i := x;
  while i < 3
  invariant x <= 3 ==> x <= i <= 3
  invariant x > 3 ==> i == x
  // x to i = i to 3 i.e. path C = path A + path B
  // invariant x >= 3 ==> 3 <= x <= i
  {
    i := i + 1;
  }
  // assert i != x ==> i == 3;
  return i;
}

lemma ModP(a: nat, b: nat, c: int)
requires c > 0
ensures (a + b) % c == a % c + b % c
{
  if b == 0 {
    assert 0 % c == 0;
  } else {
    assert (a + b - 1) % c == a % c + (b - 1) % c by {ModP(a, b - 1, c); }
    assert (a + b) % c - 1 % c == a % c + b % c - 1 % c by {
      // Todo
      assume (a + b - 1) % c == (a + b) % c - 1 % c;
      assume (b - 1) % c == b % c - 1 % c;
    }
    assert (a + b) % c == a % c + b % c;
  }
}

lemma ModAdd(u: nat, a: int)
requires a > 0
ensures (u + a) % a == u % a
{
  ModP(u, a, a);
  assert u % a + a % a == (u + a) % a;
  assert u % a == (u + a) % a by { assert a % a == 0; }
}

lemma Div1(a: nat, b: int)
requires b > 0
requires a < b
ensures a / b == 0
{}

method Mod2(x: nat, a: nat) returns (r: nat)
requires a > 0
ensures r == x % a
{
  var y := x;
  while y >= a
  invariant y % a == x % a
  invariant y >= 0
  {
    // forall u { ModAdd(u, a); }
    ModAdd(y - a, a);
    y := y - a;
  }
  // assert y == x % a; // to prove this add invariant y >= 0 or explicitly add type to y variable declaration
  return y;
}