include "events-defs.dfy"

// Theorem
lemma SomeSendHappensBeforeRec(rec: ev)
requires rec.eventType == Receive
ensures exists s: ev :: s.t <= rec.t
{
  AxiomReceiveHaveSend(rec);
  var s': ev :| s'.id == rec.id && s'.eventType == Send; // without inv2 we can't establish existence
  assert s'.eventType == Send by {
    AxiomUnique(rec);
    if case s'.eventType == Receive => {
      assert s' == rec;
    } case s'.eventType == Send => {}
  }
  assert s'.t <= rec.t by { AxiomSendBeforeReceive(s', rec); }
}

lemma haveDifferentTimestamps(e1: ev, e2: ev)
requires e1.t < e2.t
requires e1.id == e2.id
ensures e1.eventType == Send
ensures e2.eventType == Receive
{
  // assert e1 != e2;
  assert (e1.eventType == Receive && e2.eventType == Send) || (e1.eventType == Send && e2.eventType == Receive) by {
    AxiomUnique(e1); // assert false;
    // WISH: If dafny sees user input assert "something that not false" then dafny must conclude:
    // "User assumes that at this state we can't prove false" => do some suggestions, corrections, print set of actions

    // Note: If we can't guarantee that from some lemma we can't prove false then we can HIDE it
  }
  var rec: ev, send: ev;
  if e1.eventType == Receive {
    rec, send := e1, e2;
  } else {
    rec, send := e2, e1;
  }
  assert rec.eventType == Receive;
  assert send.eventType == Send;
  assert send.t <= rec.t by {
    AxiomSendBeforeReceive(send, rec);
  }
}

lemma NotExistsOtherSend(send: ev, rec: ev)
requires rec.eventType == Receive
requires send.eventType == Send
requires rec.id == send.id
ensures !exists e: ev | e != send && e != rec :: e.id == send.id
{
  AxiomUniquePair(send, rec);
}

predicate sorted(xs: seq<int>)
decreases xs
{
  (|xs| <= 1) || (|xs| >= 2 && xs[0] <= xs[1] && sorted(xs[1..]))
}

lemma test()
{
  var p1: mpair := (Event(Send, 0, 99), Event(Receive, 0, 100));
  assert !exists e | e != p1.0 && e != p1.1 :: e.id == getId(p1) by {
    AxiomUniquePair(p1.0, p1.1);
  }
}

lemma AllPairsAreValid(e1: ev, e2: ev)
requires e1 != e2
requires e1.id == e2.id
requires e1.t <= e2.t
ensures e1.t < e2.t ==> validPair((e1, e2))
ensures e1.t == e2.t ==> (validPair((e1, e2)) || validPair((e2, e1)))
{
  if e1.t == e2.t {
    assert e1.eventType != e2.eventType;
  } else {
    assert e1.eventType == Send by {
      if e1.eventType == Receive {
        assert e2.eventType == Send by { AxiomUnique(e1); }
        AxiomSendBeforeReceive(e2, e1);
      }
    }
    assert e2.eventType == Receive by { AxiomUnique(e1); }
  }
}

