// d : M -> M -> R
function d<T>(x: T, y: T): (r: real)

// Metric space us an ordered pair (M, d) where M us a set and
// d is a `metric` on M, i.e. a function d: M x M -> R
// satisfying the following axioms for all points x, y, z in M:

// 1. The distance from a point to itself is zero
lemma ZeroDiagonal<T>(x: T)
ensures d(x, x) == 0.0

// 2. The distance between two distinct points is always
// positive
lemma Positivity<T>(x: T, y: T)
requires x != y
ensures d(x, y) > 0.0

lemma WeakPositivity<T>(x: T, y: T)
requires x != y
ensures d(x, y) != 0.0
// ensures d(x, y) > 0.0
// can be wekened to d(x, y) != 0.0

// 3. The distance from x to y is always the same as the distance
// from y to x
lemma Symmetry<T>(x: T, y: T)
ensures d(x, y) == d(y, x)

// 4. You can arrive at z from x by taking a detour through y, but
// this will not make your journet any faster than the shortest path
lemma TriangleInequality<T>(x: T, y: T, z: T)
ensures d(x, z) <= d(x, y) + d(y, z)


// Theorem d >= 0
lemma NonNegative<T>(x: T, y: T)
ensures d(x, y) >= 0.0
{
  if x == y {
    ZeroDiagonal(x);
  } else {
    Positivity(x, y);
  }
}

// Proof of theorem d >= 0 without second axiom
lemma NonNegative'<T>(x: T, y: T)
ensures d(x, y) >= 0.0
{
  assert d(x, x) <= d(x, y) + d(y, x) by { TriangleInequality(x, y, x); }
  assert d(x, x) == 0.0 by { ZeroDiagonal(x); }
  assert d(x, y) == d(y, x) by { Symmetry(x, y); }
  assert 0.0 <= 2.0 * d(x, y);
}

// "the distance between two points is zero iff they are equal"
lemma FirstSecondCombined<T>(x: T, y: T)
ensures x == y <==> d(x, y) == 0.0
{
  if x == y {
    ZeroDiagonal(x);
  } else {
    WeakPositivity(x, y);
  }
}

type vec = (real, real)
function norm(x: vec): real
{
  x.0 * x.0 + x.1 * x.1
}

function add(x: vec, y: vec): vec
{
  (x.0 + y.0, x.1 + y.1)
}

function abs(x: real): real
{
  if x >= 0.0 then x else -x
}

lemma Quadratic(x: real, a: real)
requires x * x == a * a
ensures x == a || x == -a
{}

// how to make it opaque?
const {:opaque} origin: vec := (0.0, 0.0);
// function {:opaque} origin(): vec { (0.0, 0.0) }

lemma {:axiom} Translation(x: vec, y: vec, a: vec)
ensures d(x, y) == d(add(x, a), add(y, a))


lemma {:axiom} NormDef(x: vec)
ensures d(x, origin) * d(x, origin) == norm(x)

method test(v1: vec, v2: vec, x0: real, y0: real)
requires v1 == (0.0, y0)
requires v2 == (x0, y0)
ensures d(v1, v2) == abs(x0)
{
  assert d(origin, origin) == 0.0 by {
    ZeroDiagonal(origin);
  }
  assert add((x0, 0.0), (0.0, y0)) == v2;
  ghost var dv :| add(dv, v1) == v2;
  assert dv == (x0, 0.0);
  assert d(v1, v2) == abs(x0) by {
    assert d(dv, origin) * d(dv, origin) == norm(dv) by { NormDef(dv); }
    assert norm(dv) == x0 * x0;
    calc == {
      d(v1, v2);
      {
        assert add(origin, v1) == v1 by { reveal origin; }
        assert add(dv, v1) == v2;
        Symmetry(v1, v2);
      }
      d(add(dv, v1), add(origin, v1));
      {
        Translation(dv, origin, v1);
      }
      d(dv, origin);
      {
        NonNegative(dv, origin);
        Quadratic(d(dv, origin), x0);
      }
      abs(x0);
    }
  }
}