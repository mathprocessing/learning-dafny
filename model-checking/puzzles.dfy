include "./pow.dfy"

lemma {:verify false} Together(a: real, b: real, c: real)
requires c != 0.0
ensures a/c + b/c == (a+b)/c
{}

lemma L2(n': real, n: nat)
requires n >= 3
requires (n as real) == n'
ensures pow((n' + 1.0)/n', n-1) * (n' + 1.0)/n' > pow(n'/ (n'-1.0), n-1)
{
  assert n' >= 3.0;
  if n == 3 {
    assert pow((n' + 1.0)/n', n-1) * (n' + 1.0)/n' > pow(n'/ (n'-1.0), n-1) by {
      assert pow(4.0/3.0, 2) * 4.0/3.0 > pow(3.0/2.0, 2);
      assert pow((n' + 1.0)/n', 2) * (n' + 1.0)/n' > pow(n'/ (n'-1.0), 2);
    }
  }
  assume false;
  // 4/3 < 3/2
  // (4/3) ^ (n-1) * 4/3 > (3/2) ^ (n-1)
}

// ((n+1)/n)^n > 2
lemma {:verify false} {:induction false} L1(n: int)
requires n >= 2
ensures pow(1.0 + (1.0/(n as real)), n) > 2.0
decreases n
{
  if n == 2 {
    assert pow(1.5, 2) > 2.0 by { assert pow(1.5, 2) == 1.5 * 1.5; }
  } else {
    assert pow(1.0 + (1.0/((n-1) as real)), n-1) > 2.0 by { L1(n-1); }
    var n' := n as real;
    assert pow(1.0 + (1.0/((n-1) as real)), n-1) == pow(n'/ (n'-1.0), n-1) by {
      calc == {
        pow(1.0 + (1.0/((n-1) as real)), n-1);
        {
          assert 1.0 == ((n-1) as real)/((n-1) as real);
        }
        pow(((n-1) as real)/((n-1) as real) + (1.0/((n-1) as real)), n-1);
        {
          assert ((n-1) as real) + 1.0 == n';
          assert (((n-1) as real)/((n-1) as real) + (1.0/((n-1) as real)) == (((n-1) as real) + 1.0) / ((n-1) as real));
        }
        pow(n'/((n-1) as real), n-1);
        {
          assert ((n-1) as real) == n' - 1.0;
        }
        pow(n'/ (n'-1.0), n-1);
      }
    }
    assert pow(1.0 + 1.0/n', n) > pow(n'/ (n'-1.0), n-1) by {
      assert pow(1.0 + 1.0/n', n) == pow((n' + 1.0)/n', n-1) * (n' + 1.0)/n' by {
        calc == {
          pow(1.0 + 1.0/n', n);
          {
            assert 1.0 == n'/n';
            assert n'/n' + 1.0/n' == (n'+1.0)/n' by { Together(n', 1.0, n'); }
          }
          pow((n' + 1.0)/n', n);
          {
            ghost var k' := (n' + 1.0)/n';
            PowDef(k', n);
          }
          pow((n' + 1.0)/n', n-1) * (n' + 1.0)/n';
        }
      }

      assert pow((n' + 1.0)/n', n-1) * (n' + 1.0)/n' > pow(n'/ (n'-1.0), n-1) by {
        L2(n', n);
      }

      // assert pow((n' + 1.0)/n', n-1) * (n' + 1.0)/n' > pow(n'/ (n'-1.0), n-1);
      // assert pow((n' + 1.0)/n', n-1) > pow(n'/ (n'-1.0), n-1);
      // assert (n' + 1.0)/n' > n'/ (n'-1.0);
      // assert (n' + 1.0)/n' - n'/ (n'-1.0) > 0.0;
      // assert (n' + 1.0)*(n' - 1.0)/((n'-1.0) * n') - n'*n'/ ((n'-1.0) * n') > 0.0;
      // assert ((n' + 1.0)*(n' - 1.0) - n'*n')/((n'-1.0) * n') > 0.0;
      // assert (n'*n' - 1.0)/((n'-1.0) * n') > 0.0;
      // assert (n' + 1.0)/n' > 0.0;
      // calc == {
      //   (n' + 1.0)/n';
      //   (- 1.0)/((n'-1.0) * n');
      //   {
      //     assert n' >= 2.0;
      //     assert n'-1.0 != 0.0;
      //     assert n' != 0.0;
      //     assert (n'-1.0) * n' != 0.0;
      //     assert  ((n' + 1.0)*(n' - 1.0) - n'*n')
      //   }
      //   ((n' + 1.0)*(n' - 1.0) - n'*n')/((n'-1.0) * n');
      // }
    }
  }
}

// function floor(x: real): int
// requires x >= 0.0
// {
//   if 0.0 <= x < 1.0 then 0 else 1 + floor(x - 1.0)
// }

// lemma floorEqFloor(x: real)
// requires x >= 0.0
// ensures x.Floor == floor(x)
// {}


// method test(x: real)
// {
//   // assert pow(x, 0) == 1.0;
//   // assert pow(x, 1) == x;
//   // assert pow(x, 2) == x * x;
//   // assert pow(x, 3) == x * x * x;
//   assert 5.3.Floor == 5;
// }