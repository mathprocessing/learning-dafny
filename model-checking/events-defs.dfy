type timestamp = int
type id = int
// t1: timestamp, t2: timestamp
datatype etype = Send | Receive
datatype ev = Event(eventType: etype, id: int, t: timestamp)

// type mumu = x: int | x > 8 witness 9
// type sortedseq = x: seq<int> | sorted(x) witness [0, 1]

predicate validPair(p: (ev, ev))
{
  var (e1, e2) := p;
  e1.eventType == Send && e2.eventType == Receive && e1.t <= e2.t && e1.id == e2.id
}

type mpair = x: (ev, ev) | validPair(x) witness (Event(Send, 0, 0), Event(Receive, 0, 0))

function getId(p: mpair): int
ensures p.0.id == p.1.id
{
  p.0.id
}

lemma AxiomSendBeforeReceive(send: ev, rec: ev)
requires send.eventType == Send
requires rec.eventType == Receive
requires send.id == rec.id
ensures send.t <= rec.t

lemma AxiomUniquePair(send: ev, rec: ev)
requires send.eventType == Send
requires rec.eventType == Receive
requires send.id == rec.id
ensures !exists e: ev | e != send && e != rec :: e.id == send.id

lemma AxiomUnique(e: ev)
ensures !exists e': ev | e' != e :: e'.eventType == e.eventType && e.id == e'.id

// Can we prove this from upper axioms?
lemma AxiomReceiveHaveSend(rec: ev)
requires rec.eventType == Receive
ensures exists s: ev | s.eventType == Send :: s.id == rec.id
//{
  // If we instead of axioms we define state machine with two processes
  // and for each step of whole system we have one from some set of actions for each process then
  // we can prove all this axioms

  // But for current set of axioms we can ask:
  // This set of axioms is enough (equivalent) to state-machine defintion?
//}

// This theorem can't be proved because receive happens after send and may not exists at some time slice t
// But if we have some history or logs without errors and with time bounds for sending messages...
// number of events must be even

// lemma TheoremSendHaveReceive(send: ev)
// requires send.eventType == Send
// ensures exists r: ev | r.eventType == Receive :: send.id == r.id
// {
//   if !exists r: ev | r.eventType == Receive :: send.id == r.id {
//     var r: ev :| r != send && r.id == send.id;
//     assert r.eventType == Receive by { AxiomUnique(r); }
//   }
// }