function f(x: int): int

lemma Prop1(x: int)
ensures f(f(x)) == -x

lemma Bounds()
ensures -4 <= f(0) <= 4
ensures -4 <= f(1) <= 4
ensures -4 <= f(2) <= 4
ensures -8 <= f(3) <= 8
ensures -8 <= f(-3) <= 8
ensures -8 <= f(4) <= 8

lemma fAuto()
ensures f(f(-7)) == 7
ensures f(f(-6)) == 6
ensures f(f(-5)) == 5
ensures f(f(-4)) == 4
ensures f(f(-3)) == 3
ensures f(f(-2)) == 2
ensures f(f(-1)) == 1
ensures f(f(0)) == 0
ensures f(f(1)) == -1
ensures f(f(2)) == -2
ensures f(f(3)) == -3
ensures f(f(4)) == -4
ensures f(f(5)) == -5
ensures f(f(6)) == -6
ensures f(f(7)) == -7
{
  forall i | -7 <= i <= 7 ensures f(f(i)) == -i {
    Prop1(i);
  }
}
