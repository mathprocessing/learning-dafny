include "./defs.dfy"

lemma fZero()
ensures f(0) == 0
{
  fAuto();
  Bounds(); // если это можно доказать для конечного множества, то скорее всего удасться и для бесконечного
}

lemma fOne()
ensures f(1) != 1
ensures f(1) != 0
ensures f(1) != -1
ensures f(-1) != 1
ensures f(-1) != 0
ensures f(-1) != -1
{
  fAuto();
}

// избавление от ошибок в коде счерез минимизацию длины тела леммы может помочь упорядичить процесс дебаггинга
lemma fOneEqualTwo()
ensures f(1) == 2 ==> f(2) == -1
ensures f(1) == -2 ==> f(2) == 1
ensures f(-1) == -2 ==> f(2) == -1
ensures f(-1) == 2 ==> f(2) == 1
{
  fZero();
  fAuto();
}

// lemma Test()
// // requires forall x: int :: f(f(x)) == -x
// requires f(1) == -2

// ensures f(1) != 1
// ensures f(1) != 0
// ensures f(1) != -1

// // -----
// ensures f(0) == 0
// ensures f(-1) == -2
// // ensures f(-2) == 1
// // ensures f(1) == 2
// // ensures f(2) == -1
// ensures false
// {
//   fAuto();
//   Bounds();
//   // assert 57 * 17 == 969;
// }


// lemma Test()
// // requires forall x: int :: f(f(x)) == -x

// requires f(1) == 2

// // -----
// ensures f(0) == 0
// ensures f(-1) == -2
// ensures f(-2) == 1
// ensures f(1) == 2
// ensures f(2) == -1
// ensures false
// {
//   fProps();
//   Prop2();
//   // assert 57 * 17 == 969;
// }