// Model Checking https://www.youtube.com/watch?v=GIrOek9sGyQ&list=PLK50zIm6tHRiKFJvKu1a7q_z2tcXnBUHp&index=8

datatype Location = l1 | l2

function next(x: Location): Location
{
  match x
    case l1 => l2
    case l2 => l1
}

lemma Double(l: Location)
ensures next(next(l)) == l
{}


function iterate(x: Location, i: nat): Location
{
  if i == 0 then x else next(iterate(x, i - 1))
}

method test()
{
  assert iterate(l1, 0) == l1;
  assert iterate(l2, 1) == l1;
  assert iterate(l1, 1) == l2;
  assert iterate(l2, 0) == l2;
}

lemma {:induction false} A(x: Location, i: nat)
requires i > 0
ensures iterate(x, i) == l2 ==> iterate(x, i - 1) == l1
{}



lemma {:induction false} B(x: Location, i: nat)
ensures iterate(x, i) == iterate(x, i + 2)
{}

lemma {:induction false} C(x: Location, i: nat, n: nat)
ensures iterate(x, i) == iterate(x, i + 2 * n)
decreases n
{
  if n <= 1 {
    assert iterate(x, i) == iterate(x, i + 2);
  } else {
    C(x, i, n-1);
  }
}

method test2()
{
  forall x: Location, n: nat ensures iterate(x, 0) == iterate(x, 2*n) {
    C(x, 0, n);
  }
  // assume forall x: Location, n: nat :: iterate(x, 0) == iterate(x, 2*n);
  assert iterate(l1, 0) == iterate(l1, 2000) by {
    assert 2000 == 2 * 1000;
  }
}