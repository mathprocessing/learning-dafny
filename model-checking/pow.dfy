function pow(x: real, n: nat): (res: real)
ensures x == 1.0 ==> res == 1.0
decreases n
{
  if n == 0 then 1.0 else (x * pow(x, n - 1))
}

lemma PowDef(x: real, n: int)
requires n > 0
ensures pow(x, n) == x * pow(x, n - 1)
{
  assert n != 0;
  assert n - 1 >= 0;
}