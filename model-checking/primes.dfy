opaque function even(n: nat): (res: bool)
{
  if n == 0 then true else !even(n-1)
}

lemma {:induction false} ModTwo(n: nat)
requires even(n)
ensures n % 2 == 0
decreases n
{
  if n <= 1 {
    assert 0 % 2 == 0;
    assert !even(1) by { reveal even(); }
  } else {
    calc == {
      even(n); { reveal even(); }
      !even(n-1); { reveal even(); }
      even(n-2);
    }
    ModTwo(n - 2);
    assert (n - 2) % 2 == 0;
  }
}

opaque function prime(n: nat): (res: bool)
ensures n < 2 ==> !res
ensures n == 2 ==> res
ensures n > 2 ==> even(n) ==> !res
{
  assert even(n) ==> n % 2 == 0 by {
    reveal even();
    if even(n) { ModTwo(n); }
  }
  n >= 2 && !exists k | 1 < k < n :: n % k == 0
}

lemma divides(k: int, n: int)
requires n >= 2
requires 1 < k < n
requires n % k == 0
ensures !prime(n)
{
  reveal prime();
}

// check(1, 2) ==> prime(2)
// check(2, 3) ==> prime(3)
// check(2, 5) && check(3, 5) && check(4, 5) ==> prime(5)

// goal: prime(5)
// subgoals: check(2, 5) && check(3, 5) && check(4, 5)

opaque predicate check(k: int, n: int)
requires n >= 2
requires k > 0
{
  n % k != 0
}

// method evalCheck(k: nat, n: nat)
// requires n >= 2
// requires 1 < k < n
// {

// }

// lemma Check(n: nat)
// requires forall k | 1 < k < n :: check(k, n)
// ensures prime(n)

lemma Check(k: int, n: int)
requires n >= 2
requires 1 < k < n
requires prime(k)
ensures n % k != 0 ==> check(k, n)
ensures forall i | n/3 < i < n && 2*i != n :: check(i, n)
// ensures (forall k | 1 < k < n :: check(k, n)) ==> prime(n)
ensures (forall k | 1 < k < n && prime(k) :: check(k, n)) ==> prime(n)

lemma CheckAfterHalf(k: int, n: int)
requires n >= 2
requires n/3 < k < n
requires 2*k != n
ensures check(k, n)
{
  reveal check();
}
/*
(17 ^ 2) % 10 = (7 ^ 2) % 10 = 9
(17 ^ 2) / 10 % 10 = 
= ((10a) ^ 2 + 7 ^ 2) / 10 % 10
= (a ^ 2 * 100 + 7 ^ 2 + 2*7a) / 10 % 10
= (2*7a + 7 ^ 2) / 10 % 10
= 7 * (2*a + 7) / 10 % 10 = 3

17 * 17 = 100 + 140 + 49 = 289
17 * 17 = 100 + 7 * (2 * 10  + 7)
 = (4 * 5  + 5 + 2)
 = 7 * (5 * 5 + 2)
 = 7 * 25 + 7 * 2
 = *7* + 1*

8* = 4 + 4* = 1(4)
*/

lemma TwoIsPrime()
ensures prime(2)
{}

lemma ThreeIsPrime()
ensures prime(3)
{
  // assert check(2, 3);
  Check(2, 3);
}

lemma MulModDistr(a: nat, b: nat, c: int)
requires c > 0
ensures (a * b) % c == (a % c) * (b % c)


lemma MulMod(i: nat, j: nat)
requires i != 0
ensures (i * j) % i == 0
{
  MulModDistr(i, j, i);
}

lemma NotPrime(i: int, j: int)
requires i >= 2
requires j >= 2
ensures var u := i * j; !prime(u)
{
  var u := i * j;
  assert exists k | 1 < k < u :: u % k == 0 by {
    MulMod(i, j);
  }
  reveal prime();
}

predicate odd(n: nat) { !even(n) }

method test()
{
  // reveal prime();
  assert !prime(0);
  assert !prime(1);
  assert prime(2);
  assert prime(3) by { Check(2, 3); }
  assert !prime(4) by { reveal even(); }
  assert prime(5) by { Check(2, 5); }
  assert !prime(6) by { /* divides(3, 6);*/ NotPrime(2, 3); }
  assert prime(7) by {
    assert 7/3 == 2;
    // need to check primes in 2..2
    Check(2, 7);
  }
 
  assert prime(17) by {
    // How to restrict sequence below to be optimal?
    // i.e. if we move odd(17) by { reveal even(); } below 17/2
    // or something like that dafny don't check
    // We need abstraction like `Sequence.add("CHECK odd(17)")`?
    assert odd(17) by { reveal even(); }
    assert 17 > 2;
    assert 17/3 == 5;
    // need to check ptimes in 2..5
    // Subgoals: find 17/3
    // check([2, 3, 5, < 17/3]) and prove that 17 is odd
    // 2. Prove that sequence below more optimal then upper:
    // Subgoals: [prove that 17 is odd, find 17/3, check([2, 3, 5, < 17/3])]
    Check(2, 17);
    Check(3, 17);
    Check(5, 17);
  }
  assert !prime(8) by { reveal even(); }
  // Idea: 
  // for some number n we can generate subgoal steps and prove
  // that executing this subgoal steps are enough to achieve
  // main proof goal
}