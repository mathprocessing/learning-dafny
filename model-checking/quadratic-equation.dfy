function max(x: real, y: real): real
{
  if x >= y then x else y
}

function min(x: real, y: real): real
{
  if x <= y then x else y
}

lemma TwoSquares(x: real, a: real)
requires x * x == a * a
ensures x == a || x == -a
{}

lemma VietaSimple(x0: real, x1: real, x: real)
ensures (x - x0) * (x - x1) == x * x - (x0 + x1) * x + x0 * x1
{}

lemma Factor2(x0: real, x1: real, x: real)
requires (x - x0) * (x - x1) == 0.0
ensures x == x0 || x == x1
{}

lemma MulIneq(a: real, b: real)
requires a * b >= 0.0
ensures (a >= 0.0 && b >= 0.0) || (a <= 0.0 && b <= 0.0)
{}

lemma FactorIneqPos(x0: real, x1: real, x: real)
requires (x - x0) * (x - x1) >= 0.0
// ensures x >= max(x0, x1) || x <= min(x0, x1)
ensures !(x0 < x < x1)
{
  MulIneq(x - x0, x - x1);
}

lemma FactorIneqNeg(x0: real, x1: real, x: real)
requires (x - x0) * (x - x1) <= 0.0
requires x0 <= x1
ensures x0 <= x <= x1
{
  assert x0 <= x <= x1 || x1 <= x <= x0 by { MulIneq(x0 - x, x - x1); }
}

lemma Example(x: real)
requires x * x - 3.0 * x + 2.0 == 0.0
ensures x == 1.0 || x == 2.0
{}

lemma Example2(x: real, y: real)
requires x * x == x * y
ensures x == 0.0 || x == y
{
  Factor2(0.0, y, x);
  // forall x0, x1 | (x - x0) * (x - x1) == 0.0 ensures x == x0 || x == x1 {
  //   if ((x - x0) * (x - x1) == 0.0) {
  //     Factor2(x0, x1, x);
  //   }
  // } // too slow for prove without supplying arguments: (0.0, y, x)
}

lemma Par(x: real, a: real)
requires (x - 1.0) * (x - a) <= 0.0 ==> x > a
ensures x >= 1.0 && x <= a ==> x > a
// ensures !(x >= 1.0 && x <= a)
ensures x < 1.0 || x > a
// Wolfram
ensures a <= 1.0 ==> x != a
ensures a > 1.0 ==> !(1.0 <= x <= a)
ensures x != a
ensures x < a ==> x < 1.0
ensures x >= 1.0 ==> x >= a
ensures 1.0 <= x <= a ==> (a <= 1.0 && x == a)
ensures !(1.0 <= x <= a)
{
  if (x - 1.0) * (x - a) <= 0.0 {
    assert x > a;
    MulIneq(1.0 - x, x - a);
    if 1.0 <= a {
      // FactorIneqNeg(1.0, a, x);
      assert 1.0 <= x <= a;
      assert false;
    } else {
      assert 1.0 > a;
      // FactorIneqNeg(a, 1.0, x);
      assert a < x <= 1.0;
    }
  } else {
    assert !(1.0 <= x <= a);
  }
  assert !(1.0 <= x && x <= a) || (a < x && a < 1.0);
  assert x < 1.0 || a < x || (a < x && a < 1.0);
  assert x < 1.0 || a < x;
}

lemma ParSimpl(x: real, a: real)
ensures ((x - 1.0) * (x - a) <= 0.0 ==> x > a)
  <==> ((x < 1.0 && x < a) || x > a)
{
  assert ((x - 1.0) * (x - a) <= 0.0 ==> x > a)
  ==> ((x < 1.0 && x < a) || x > a) by {
    if (x - 1.0) * (x - a) <= 0.0 {
      MulIneq(1.0 - x, x - a);
      assert (x > a) ==> ((x < 1.0 && x < a) || x > a);
    } else {
      assert ((x < 1.0 && x < a) || x > a) by {
        if 1.0 <= x <= a {// || x == a {
          assert false;
        }
      }
    }
  }

  assert ((x - 1.0) * (x - a) <= 0.0 ==> x > a)
  <== ((x < 1.0 && x < a) || x > a) by {
    if x == a {}
    if x < a && x < 1.0 {
      assert x - 1.0 < 0.0;
      assert x - a < 0.0;
      assert (x - 1.0) * (x - a) > 0.0 by {
        MulIneq(x - 1.0, x - a);
        assert (x - 1.0) * (x - a) >= 0.0;
        assert (x - 1.0) * (x - a) != 0.0 by {
          if (x - 1.0) * (x - a) == 0.0 {
            Factor2(1.0, a, x);
            assert x == 1.0 || x == a;
          }
        }
      }
      assert !((x - 1.0) * (x - a) <= 0.0);
    }
  }

}