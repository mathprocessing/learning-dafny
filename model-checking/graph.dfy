// type Graph = x: nat, y: nat | 

datatype graph = Graph(rel: (nat, nat) -> bool)

predicate isLoopless(g: graph, v: nat)
{
  !g.rel(v, v)
}

function SingleEdge(v1: nat, v2: nat): graph
{
  Graph((i, j) => i == v1 && j == v2)
}

function EmptyGraph(): graph
{
  Graph((_, _) => false)
}

function FullGraph(): graph
{
  Graph((_, _) => true)
}

datatype myset<!T> = MySet(has: T -> bool) {

}

function EmptySet<T>(): myset<T>
{
  // s.mem == (i: T) => false
  MySet((i: T) => false)
}

method test(g: graph)
{
  if g == FullGraph() {
    assert forall a, b :: g.rel(a, b);
  }
}

function union(a: myset<nat>, b: myset<nat>): myset<nat>
{
  MySet((i : nat) => (a.has(i) || b.has(i)))
}

lemma SetEq(A: myset<nat>, B: myset<nat>)
requires forall i :: A.has(i) <==> B.has(i)
ensures A == B

lemma UnionSelf(s: myset<nat>)
ensures union(s, s) == s
{
  SetEq(union(s, s), s);
}

method testset(A: myset<nat>, B: myset<nat>)
requires A == EmptySet()
ensures union(A, B) == B
ensures union(B, B) == B
ensures union(B, A) == B
ensures union(A, A) == A
{
  // We don't need to specify arguments
  forall a: myset<nat>, b: myset<nat> | (forall i :: a.has(i) <==> b.has(i)) ensures a == b {
    SetEq(a, b);
  }
}

datatype notint<!T> = Some(x: T)

lemma Lem1<T>(n1: notint<T>, n2: notint<T>)
ensures (n1.x == n2.x) <==> (n1 == n2)
{}