function method f(x: int): int

const c := f(0)

lemma E(a: int, b: int)
ensures f(2 * a) + 2 * f(b) == f(f(a + b))

lemma Step1(x: int)
ensures f(2 * x) == 2 * f(x) - c
{
  E(0, x);
  E(x, 0);
  // assert f(0) + 2 * f(x) == f(f(x)) by { E(0, x); }
  // assert f(2*x) + 2 * f(0) == f(f(x)) by { E(x, 0); }
  // assert f(2*x) == 2 * f(x) - f(0);
}

lemma Simple()
ensures f(-4) == 2 * f(-2) - c
ensures f(-2) == 2 * f(-1) - c
  ensures f(-4) == 4 * f(-1) - 3 * c
ensures f(2) == 2 * f(1) - c
ensures f(4) == 2 * f(2) - c
{
  Step1(-2);
  Step1(-1);
  Step1(1);
  Step1(2);
}

lemma CaseZero(c: int)
requires c == 0

lemma SymmE(a: int, b: int)
ensures 2 * (f(a) + f(b)) == f(f(a + b)) + c
{
  E(a, b);
  Step1(a);
}

lemma Diag(a: int)
ensures f(f(2 * a)) == 4 * f(a) - c
{
  // E(a, a); Step1(a);
  SymmE(a, a);
}

lemma AboutC()
ensures f(c) == 3 * c // by Diag(0)
ensures f(f(2 * c)) == 11 * c
ensures f(2 * c) == 2 * f(c) - c // by Step1(c)
ensures f(2 * f(c) - c) == 11 * c
{
  assert f(c) == 3 * c by { Diag(0); }
  if c != 0 {
    assert exists k :: f(k) == 3 * k; // interesting property for general function like f : Z -> Z
  }
  Diag(c);
  assert f(2 * c) == 2 * f(c) - c by { Step1(c); }
}

// Second derivative is zero
lemma Linearity(x: int)
ensures f(-x) - 2 * f(0) + f(x) == 0
{
  SymmE(x, -x);
  AboutC();
}

lemma Linearity2(x: int)
ensures f(x+1) - f(x) == f(-x) - f(-x-1)
{
  Linearity(x);
  Linearity(x+1);
}

lemma test(a: int)
{
  
  // if c == 1 {
  //   assert f(1) == 3 by { AboutC(); }
  //   assert f(2) == 5 by { AboutC(); } // f(2 * c) == 2 * f(c) - c ==> f(2) == 5
  //   assert f(5) == 11 by { AboutC(); } // f(2 * f(c) - c) == 11 * c ==>    f(5) == 11 * c
  //   // 2 * (f(a) + f(b)) == f(f(a + b)) + 1;
    
  //   SymmE(a, -a);
  //   assert 2 * (f(a) + f(-a)) == f(f(0)) + 1;
  //   assert f(a) + f(-a) == 2; // Linearity
  // }

  assert f(-c) == -c by {
    Linearity(c);
    AboutC();
  }
  assert f(c) + 3 * f(-c) == 0 by { AboutC(); }
  assert 2 * (f(f(c)) + f(3 * f(-c))) == f(f(0)) + c by { SymmE(f(c), 3 * f(-c)); }
  assert f(3 * c) + f(3 * -c) == 2 * c;
  assert f(c)     + f(-c)     == 2 * c;

}

lemma BackToEquation()
{
  E(1, -1);
  assert f(2) + 2 * f(-1) == 3 * c by { AboutC(); }
  E(-1, 1);
  assert f(-2) + 2 * f(1) == 3 * c by { AboutC(); }

  assert f(2) + 2 * f(-1) == f(-2) + 2 * f(1); // E1
  Linearity2(1);
  // assert f(2) - f(1) == f(-1) - f(-2); // E2
  assert f(2) - f(-1) ==  -f(-2) + f(1); // rearrange E2
  assert 2 * f(2) + f(-1) == 3 * f(1); // add with E1
  assert f(-1) - 3 * f(1) + 2 * f(2) == 0; // to cononical form

  // Linearity2(-2);
  // assert -f(2) + f(-1) == f(-2) - f(1); // same as raaranged E2. TODO prove this and ensures correctness in Dafny using DAG proof notation
  // Something like: Call of Linearity2(-2) <==> Call of Linearity2(1)

  // Linearity multiplies: We found link between Interval with length 1 and Interval with length 2
  assert f(1) - f(-1) == 2 * (f(2) - f(1));
  assert f(1) + f(-1) == 2 * f(0);
  // + TODO: How to check this? That we use + of expressions?
  assert f(1) == (f(0) + f(2)) / 2; // f(Middle point) = average of f(left) and f(right) neighbors
  // -
  assert f(-1) == f(0) - f(2) + f(1);
    

  assert f(0) + f(2) == 2 * f(1); // moved version of
  assert f(-1) + f(1) == 2 * f(0);
  // Can we prove: f(x-1) + f(x+1) == 2 * f(x)?
  // assert f(-1) == 2 * f(0) - f(1); // No new info, it's ~ to Linearity(1)
}

lemma LinearityFull(x: int)
ensures f(x-1) + f(x+1) == 2 * f(x)

// Step1 : f(2 * x) == 2 * f(x) - c
lemma DoubleF(x: int)
ensures f(f(x)) == 2 * f(x) + c
{
  assert f(f(x)) == f(2 * x) + 2 * c by { E(x, 0); }
  Step1(x);
}

lemma SumF(x: int, y: int)
ensures f(x + y) == f(x) + f(y) - c
{
  // assert 2 * (f(x) + f(y)) - c == f(f(x + y)) by { SymmE(x, y); }
  // assert f(x) + f(y) == f(x + y) + c by { DoubleF(x + y); }
  // <==>
  // E(x, y); Step1(x);
  // E(x + y, 0); Step1(x + y); // DoubleF(x + y);
  // <==>
  E(x, y);
  E(0, x); E(x, 0);
  E(x + y, 0); E(0, x + y);
}

lemma ConstantDerivative(x: int)
ensures f(x + 1) - f(x) == f(1) - f(0)
{ 
  SumF(x, 1); // i.e. we can prove this using only four tokens [0, 1, x, x + 1] and substitute them to `E(a, b)`
}


// {
//   AboutC();
//   Linearity(1);
//   Linearity(-1);
//   Linearity(2);
//   Linearity(-2);
//   E(1, -1);
//   E(-1, 1);
//   E(-2, 2);
//   E(2, -2);
//   E(-x, x);
//   Linearity(c);
//   assert f(-c) == -c;
//   Linearity(x);
//   Linearity(-x);
//   SymmE(x, -x);
//   SymmE(1, -1);
//   SymmE(-1, 1);
//   SymmE(2, -1);
//   SymmE(-2, 1);
//   // forall a: int, b: int { SymmE(a, b); }
//   // forall a: int, b: int { E(a, b); }
//   // forall a: int { Linearity2(a); }
  
//   assert f(0) + f(2) == 2 * f(1);
//   assert f(1) + f(3) == 2 * f(2) by {
//     // f(x) + f(-x) + f(x + 2) + f(-(x+2)) = 
//     if f(1) + f(3) != 2 * f(2) {
//       assert f(1) == 2 * c - f(-1);
//       Linearity(-3);
//       Linearity(2);
//       assert 2 * c - f(-1) + 2 * c - f(-3) != 2 * (2 * c - f(-2));
//       assert 4 * c - (f(-1) + f(-3)) != 4 * c - 2 * f(-2);
//       assert f(-1) + f(-3) != 2 * f(-2); // let's add
//       assert f(-1) + f(1) + f(-3) + f(3) != 2 * (f(-2) + f(2));
//     }
//   }
// }
