"""
>>> a = Agent(100, 200, 0.5, 1, 10); a
p: (100, 200), v: (0.5, 1), r: 10

>>> a.update(); a
p: (100.5, 201), v: (0.5, 1), r: 10

Let's collide with zero velocity
>>> a = Agent(100, 100, 0, 0, 100)

a_box in 0..200
>>> a.get_collide_box()
Box(0, 0, 200, 200)

>>> b = Agent(250, 100, 0, 0, 50)
>>> assert not a.can_collide(b)
>>> b = Agent(249, 100, 0, 0, 50)
>>> assert a.can_collide(b)

>>> b = Agent(250, 100, 0, 0, 80)

We assume that density = 1/pi
mass = density * pi * r ^ 2 = 1/pi * pi * r ^ 2 = r ^ 2

>>> a.mass
10000

Rule 1: Mass and momentum can't change during collide
>>> a.mass + b.mass
16400

>>> addv(a.momentum, b.momentum)
(0, 0)

>>> a
p: (100, 100), v: (0, 0), r: 100

>>> b
p: (250, 100), v: (0, 0), r: 80

>>> a.collide(b)

After collide step radius of b must be equal to `dist - a.r`
>>> dist = abs(b.x - a.x); dist
150

>>> dist - a.r, b.r
(50, 50)

>>> a.mass + b.mass
16400

>>> addv(a.momentum, b.momentum)
(0, 0)






"""

def addv(p, q):
  """ Add two vectors represented as tuples """
  px, py = p; qx, qy = q
  return px + qx, py + qy

class Box:
  def __init__(self, x, y, w, h):
    self.x, self.y, self.w, self.h = x, y, w, h

  def __repr__(self):
    return f'Box({self.x}, {self.y}, {self.w}, {self.h})'

class Agent:
  def __init__(self, x, y, vx, vy, r):
    self.x, self.y, self.vx, self.vy, self.r = x, y, vx, vy, r

  def __repr__(self):
    return f'p: ({self.x}, {self.y}), v: ({self.vx}, {self.vy}), r: {self.r}'

  def update(self):
    self.x += self.vx
    self.y += self.vy
    # collide must be after update?

  @property
  def mass(self):
    return self.r ** 2

  @property
  def momentum(self):
    return self.mass * self.vx, self.mass * self.vy

  def get_collide_box(self):
    d = 2*self.r
    return Box(self.x - self.r, self.y - self.r, d, d)

  def get_sq_distance(self, agent):
    dx, dy = self.x - agent.x, self.y - agent.y
    return dx*dx + dy*dy

  def can_collide(self, agent) -> bool:
    dist_sq = self.get_sq_distance(agent)
    return dist_sq < (self.r + agent.r) ** 2

  def collide(self, agent):
    if not self.can_collide(agent):
      return # no collide

    if self.r < agent.r:
      max_agent = agent
      min_agent = self
    else:
      max_agent = self
      min_agent = agent
    
    all_momentum = max_agent.momentum + min_agent.momentum

    # before collide

    # min_agent.r + max_agent.r > dist

    # after

    # min_agent'.r + max_agent'.r = dist
    # also 
    #   max_agent'.r > max_agent.r and min_agent'.r < min_agent.r

    




    # max_agent

if __name__ == '__main__':
  import doctest
  doctest.testmod(optionflags=doctest.FAIL_FAST)
