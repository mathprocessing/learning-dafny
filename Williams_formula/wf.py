from math import cos, pi, factorial

def integers(n):
  """
  >>> list(integers(0))
  []

  >>> list(integers(1))
  [0]

  >>> list(integers(3))
  [0, 1, -1, 2, -2]

  """
  i = 0
  while i < n:
    if i == 0:
      yield 0
    else:
      yield i
      yield -i
    i += 1

def check(h, N = 2):
  info = None
  for i in integers(N + 1):
    if not h(i):
      info = i
      assert info is None, f'counterexample: {info}'


def check2(h, N = 2):
  info = None
  for i in integers(N + 1):
    for j in integers(N + 1):
      if not bool(h(i, j)):
        info = i, j
        assert False, f'counterexample: {info}'


def eq(a, b):
  return bool(a) == bool(b)

def eqmod2(a, b):
  return a % 2 == b % 2

def eqmod3(a, b):
  return a % 3 == b % 3

def imp(a, b):
  return b if a else True

def min_example():
  """
  # >>> def check2(h): assert all(h(a, b) for a in r for b in r)
  >>> check(lambda x: x <= x * x)
  
  >>> check2(lambda a, b: -min(-a, -b) == max(a, b), 10)

  >>> check(lambda b: (max(1, b) == b) == (b >= 1), 10)

  max(a, b) == b <==> if a <= b then b else a

  (if a <= b then b else a) == b ==> b >= a

  (if a <= b then b else a) == b <== b > a

  (if a <= b then b else a) == b ==> b > a

  c.e. a = b
  (if a <= a then a else a) == a ==> a > a
  (if a <= a then a else a) == a ==> False

  (if True then a else a) != a
  a != a

  >>> check2(lambda a, b: imp(a ** 2 + b ** 2 == a + b, abs(a + b) == a + b), 10)

  % 2
  0^2 = 0, 1^2 = 1
  x^2 = x

  % 3
  0 -> 0, 1 -> 1, 2 -> 1

  x^2 <==> x > 0

  > check(lambda x: (x * x) % 3 == x if x % 3 <= 1 else (x - 1) % 3, 5)
  
  >>> check(lambda x: (x * x) % 2 == x % 2, 5)

      mod 3 arithmetic rules:

      x ** 3 == x
      >>> check(lambda x: eqmod3(x * x * x, x), 3)

      3 * x == 0
      >>> check(lambda x: eqmod3(3*x, 0), 3)
      
      2 * x == -x
      >>> check(lambda x: eqmod3(2*x, -x), 3)

      x ** 2 == ?

      >>> [int(0.67 + int((i - 0.4) * 2.78)) % 3 for i in range(3)]
      [0, 1, 1]

      >>> [(i * i) % 3 for i in range(3)]
      [0, 1, 1]

      using random mutation and manual search
      >>> check(lambda x: eqmod3(x*x, int(0.01 + int((x % 3 - 0.5) * 3))), 10)

      >>> check2(lambda x, y: eqmod3(x + 2*y, -2*x - y), 3)

  >>> check2(lambda a, b: eq((a ** 2 + b ** 2) % 2 == (a + b) % 2, True), 2)

    >>> check2(lambda a, b: eq((a ** 2 + b ** 2) % 3 == (a + b) % 3, (a * (a - 1) + b * (b - 1)) % 3 == 0), 3)

  >>> check2(lambda a, b: a ** 2 + b ** 2 >= a + b, 10)

  >> check2(lambda a, b: max(a, a + b) == a + b, 10)

  """

def f(j):
  """
  >>> [(j, f(j)) for j in range(1, 10)]
  [(1, 2.0), (2, 1.0), (3, 1.0), (4, 1.75), (5, 5.0), (6, 20.166666666666668), (7, 103.0), (8, 630.125), (9, 4480.111111111111)]
  """
  assert j > 0
  n = factorial(j - 1) + 1
  # assert n % j == 0
  return n / j


def cospi(x):
  """
  >>> [(i, cos(i * pi)) for i in range(6)]
  [(0, 1.0), (1, -1.0), (2, 1.0), (3, -1.0), (4, 1.0), (5, -1.0)]

  >>> [(i, cospi(i)) for i in range(6)]
  [(0, 1.0), (1, -1.0), (2, 1.0), (3, -1.0), (4, 1.0), (5, -1.0)]
  """
  return cos(pi * x)


def sum2(i):
  """
  >>> [(k, sum2(k)) for k in range(10)]
  [(0, 0), (1, 1), (2, 2), (3, 3), (4, 3), (5, 4), (6, 4), (7, 5), (8, 5), (9, 5)]

  [(0, 0), (1, 1.0), (2, 2.0), (3, 3.0), (4, 3.5), (5, 4.5), (6, 5.25), (7, 6.25), (8, 7.103553390593385), (9, 7.986575612152976)]
  """
  return sum(int(cospi(f(j)) ** 2) for j in range(1, i+1))



def prime(n):
  """
  An Exact Formula for the Primes: Willans' Formula
  https://www.youtube.com/watch?v=j5s0h42GfvM

  Original paper:
    On formulae for the nth prime number by Christopher Paul Willans (1942-1971) 
    The Univerity, Birmingham

  Wilson's theorem.
  (j - 1)! + 1 is divisible by j precisely when j is prime or 1. 

  1. Modify prime detector to output 0 or 1. (Using int(cos(pi * _) ^ 2))
  2. Use A_n(a) = int( (n/1+a) ** (1/n) ) for n >= 1 and a >= 0
     because it has properties
     A_n(a) = 1 for a < n; A_n(a) = 0 for a >= n.


58


Ответить

@AndreasHontzia says to @rosiefay7283
23.08.23 - 10 месяцев назад
Evaluating the formula is way slower, than running the efficient algorithm.
But from the formula, you could derive some attributes or invariants, 
that could help to get new insights. I am still looking for an example to demonstrate this.

„branchless programming“?

@xxSwAmPxx
8 месяцев назад
We can use a better upper-bound. Namely: prime(n) < n*(log(n) + log(log(n))) for all n >= 6.

  >>> n = 1
  >>> terms = 2 ** n; terms
  2

  >>> sum2(1), sum2(2)
  (1, 2)

  >>> 1 + int( (n / sum2(1)) ** (1/n) + (n / sum2(2)) ** (1/n) )
  2

  >>> [(k, prime(k)) for k in range(1, 6 + 1)]
  [(1, 2), (2, 3), (3, 5), (4, 7), (5, 11), (6, 13)]

  """
  return 1 + sum(int((n / sum2(i)) ** (1/n)) for i in range(1, 1 + 2 ** n))


if __name__ == '__main__':
  import doctest
  doctest.testmod()