dafny tricky syntax

# imports
https://github.com/dafny-lang/dafny/blob/master/Test/examples/induction-principle-code/AST.dfy
```
include "Utils.dfy"

module AST {
  import opened Utils
  ...
  // `MaxF` is a function that already defined in "Utils.dfy"
  MaxF(var f := (e: Expr_Raw) requires e in avals => e.Depth(); f, avals, 0)
}
```

# CLI OPTIONS

/autoTriggers:0
/optimizeResoluton:0
/induction:0,1,4
------------+----+++

forall k: ORDINAL { OnlyOddNegs_Correct'(k, x); }

assert expr by {
  assume expr;
}

newtype Six = x | 6 <= x witness 6

# LetExpr
Syntax:
* `var name : type := expr [with type = type];`
* `var name :| bool_expr;`

```dafny
assert 6 ==
 var f := this.v + x; // 4
 var g :| old(f + this.w) == g; // 6
g;

ensures (r + var t := r; t*2) == 3*r;

forall i | 0 <= i < a.Length ensures true; {
  var j := i+1;
  assert j < a.Length ==> a[i] == a[j];
}

assert forall i :: 0 <= i < a.Length ==> var j := i+1; j < a.Length ==> a[i] == a[j];
```


# Induction
```dafny
method Theorem0(n: int)
  requires 1 <= n;
  ensures 1 <= Fib(n);
{
  if (n < 3) {

  } else {
    Theorem0(n-2);
    Theorem0(n-1);
  }
}
```

# Recursive match
```dafny
var y := match anotherList case Nil => (match anotherList case Nil => 5) case Cons(h, t) => h;
```

# Axiom
```dafny
function TBitvector(int) : Ty uses {
  axiom (forall w: int :: { TBitvector(w) } Inv0_TBitvector(TBitvector(w)) == w);
}
```

# Forall

## forall in methods
```dafny
forall i | 0 <= i < a.Length && i % 2 == 0 {
  a[i] := a[(i + 1) % a.Length] + 3;
}
```

## forall in type definition
```dafny
type NoWitness_EffectlessArrow<!A(!new), B> = f: A ~> B // error: cannot find witness
| forall a :: f.reads(a) == {}
```

## forall attributes
Attributes:
* `:nowarn` - disable warning messages
* `:autotriggers false` - may be equivalent to setting option `/autoTriggers:0`
* `{:trigger t1} {:trigger t2} ...`
  Sequence of triggers (really tricky thing)

```dafny
assert forall n {:nowarn} :: n >= 0 || n < 0;

assert forall n {:autotriggers false} :: n >= 0 || n < 0;

lemma forall_1(i: int) requires forall j {:split false} {:matchinglooprewrite false} :: j >= 0 ==> (P(j) && (Q(j) ==> P(j+1)))

method Theorem0(tree: Tree<T>)
  ensures 1 <= LeafCount(tree)
{
  assert forall t: Tree<T> {:induction} :: 1 <= LeafCount(t);
}
```

## forall new syntax resolution
```dafny
var _ := forall x <- 5 :: x < 10;
var _ := forall x <- [5], y <- 5 :: x < 10;

forall k <- {-3, 4} {
  requires something;
  arr[i,j]:=k;
}
```

## github issue74
```dafny
function{:opaque} f(x:int):int { x }

lemma L()
ensures forall x:int :: f(x) == x
{ 
forall x:int
 ensures f(x) == x {
  reveal f();
 }
 assert forall x:int :: f(x) == x;
}
```

# Witness
```dafny
ghost witness
  var ww := assert 2 < 23 by {
    return; // error: return not allowed in assert-by
  } 2;
ww + 7
```

# non-sequentialized forall
```dafny
// non-sequentialized forall statement
forall a: CellA <- s {
  a.data := c.data + a.data - 2;
}
```

# if exists
1.
```dafny
// `if exists` in method body
if exists x | x > 0 {
  // We can see `x` in current scope
  assert x == x;
}

// `if exists` as functional expression
TODO
```

2.
https://github.com/dafny-lang/dafny/blob/master/Test/VerifyThis2015/Problem2.dfy
```dafny
if exists k :: DividesBoth(k, a, b) && k > d {
  var k :| DividesBoth(k, a, b) && k > d;
  d := k;
  assert Divides(d, a);
  DividesUpperBound(d, a);
  assert d <= a;
  DividesUpperBound(d, b);
  assert d <= b;
} else {
  return;
}
```

# if var?
see https://github.com/dafny-lang/dafny/blob/master/Test/examples/induction-principle.md
```
  if res1.Success? { // Not even in the interpreter (subsumed by the assignment with `:-`)!
    var (_, ctx1) := res1.value; // Duplicates the interpreter
    InterpExpr_Pure(e2, locals, ctx1); // *Recursive call*
  }
  else {} // Trivial case
```

# for statement
https://github.com/dafny-lang/dafny/blob/master/Test/examples/parser_combinators.dfy
```
method Main(args: seq<string>) {
    if |args| <= 1 {
      return;
    }
    for i := 1 to |args| {
      var input := args[i];
      print "\"", input, "\"", ": ";
      match Engine(input).Parentheses(0) {
        case Success(_, n) => print n, " nested parentheses";
        case Failure() => print "no valid parse";
      }
      print "\n";
    }
  }
}
```

# 2d matrix default type
https://github.com/dafny-lang/dafny/blob/master/Test/dafny4/Bug145.dfy
```dafny
ghost function existential(mat: array2<bool>): bool
    reads mat
{
    exists i, j :: 0 <= i < mat.Length0 && 0 <= j < mat.Length1 && mat[i,j]
}
```

# if { case => (case =>) } short syntax
https://github.com/dafny-lang/dafny/blob/master/Test/VerifyThis2015/Problem2.dfy
```dafny
if {
      case pc0 == 0             => a0, pc0 := a, 1;
      case pc0 == 1             => b0, pc0 := b, 2;
      case pc0 == 2 && a0 > b0  => GcdDecrease(a, b);
                                   a, pc0 := a0 - b0, 0;
      case pc0 == 2 && a0 < b0  => pc0 := 0;
      case pc0 == 2 && a0 == b0 => pc0 := 3;
      case pc1 == 0             => b1, pc1 := b, 1;
      case pc1 == 1             => a1, pc1 := a, 2;
      case pc1 == 2 && b1 > a1  => Symmetry(a, b); GcdDecrease(b, a); Symmetry(b - a, a);
                                   b, pc1 := b1 - a1, 0;
      case pc1 == 2 && b1 < a1  => pc1 := 0;
      case pc1 == 2 && b1 == a1 => pc1 := 3;
    }
```

# `:|` symbol
https://github.com/dafny-lang/dafny/blob/master/Test/VerifyThis2015/Problem2.dfy
```
d :| DividesBoth(d, a, b);
```